/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.0 Professional
Automatic Program Generator
� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 10/18/2017
Author  : NeVaDa
Company : 
Comments: 


Chip type               : ATmega16
Program type            : Application
AVR Core Clock frequency: 8.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 256
*****************************************************/
#include "main.h"
// Declare your global variables here


float Temp;
unsigned char *T=0;
char lcd_buff[16];

char led_stt=0;


void main(void)
{
// Declare your local variables here

// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTA=0x00;
DDRA=0xFF;

// Port B initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTB=0x00;
DDRB=0x00;

// Port C initialization
// Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=In Func1=In Func0=In 
// State7=0 State6=0 State5=0 State4=0 State3=0 State2=P State1=P State0=T 
PORTC=0x06;
DDRC=0xF8;

// Port D initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTD=0x00;
DDRD=0x00;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 125.000 kHz
// Mode: Normal top=0xFFFF
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: On
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x03;
TCNT1H=0x6D;
TCNT1L=0x84;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer2 Stopped
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
MCUCR=0x00;
MCUCSR=0x00;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x04;

// USART initialization
// USART disabled
UCSRB=0x00;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

// ADC initialization
// ADC disabled
ADCSRA=0x00;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;


// 1 Wire Bus initialization
// 1 Wire Data port: PORTC
// 1 Wire Data bit: 0
// Note: 1 Wire port settings must be specified in the
// Project|Configure|C Compiler|Libraries|1 Wire IDE menu.
w1_init();



// Alphanumeric LCD initialization
// Connections specified in the
// Project|Configure|C Compiler|Libraries|Alphanumeric LCD menu:
// RS - PORTA Bit 0
// RD - PORTA Bit 1
// EN - PORTA Bit 2
// D4 - PORTA Bit 4
// D5 - PORTA Bit 5
// D6 - PORTA Bit 6
// D7 - PORTA Bit 7
// Characters/line: 16
lcd_init(16);

ds18b20_init(T,0,0,DS18B20_12BIT_RES);  // Khoi tao DS18b20, do phan giai 12bit
LED_STT=1;
SPEAK=0;
lcd_gotoxy(0,0);
lcd_puts("Fire Warning");
delay_ms(2000);
lcd_clear();

// Global enable interrupts
#asm("sei");
while (1)
      {
      // Place your code here
        Temp=ds18b20_temperature(T);  // Gan gia tri nhiet do do duoc vao bien Temp 
        sprintf(lcd_buff,"Temp=%.1f",Temp);
        lcd_gotoxy(0,0);  
        lcd_puts(lcd_buff);
        delay_ms(100);     
        //Xoa dong 2 cua LCD16x2
        sprintf(lcd_buff,"               ");
        lcd_gotoxy(0,1);                 
        lcd_puts(lcd_buff); 
        led_stt=0;
        //Khi co tin hieu bao ro ri gas
        while(GAS_IN==0)
        {                      
            led_stt=1;
            sprintf(lcd_buff,"Ro ri khi Gas  ");
            lcd_gotoxy(0,1);  
            lcd_puts(lcd_buff); 
        }
   
        //Khi co tin hieu bao anh sang mo do khoi toa ra va nhiet do trong phong cao
        while((CS_IN==0) && (Temp>40))       
        {
           led_stt = 1;
           sprintf(lcd_buff,"Canh bao chay  ");
           lcd_gotoxy(0,1);  
           lcd_puts(lcd_buff); 
           Temp=ds18b20_temperature(T);
        }
        
        
        //Khi nhiet do cao bat thuong
        while(Temp>40)       
        {
           led_stt = 1;
           sprintf(lcd_buff,"Canh bao nhiet ");
           lcd_gotoxy(0,1);  
           lcd_puts(lcd_buff); 
           Temp=ds18b20_temperature(T);
        }
        
      }
}




// Timer1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
    // Place your code here 
    TCNT1H=0x6D;
    TCNT1L=0x84; 
    if(led_stt==1)
    {
        LED_STT=~LED_STT;
        SPEAK=~SPEAK;
    } 
    else
    {
        LED_STT=1;
        SPEAK=0;
    }    
}