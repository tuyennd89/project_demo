#ifndef __MAIN_H
#define __MAIN_H


#include <mega16.h>
#include <delay.h>

// 1 Wire Bus interface functions
#include <1wire.h>

// DS1820 Temperature Sensor functions
#include <ds18b20.h>

// Alphanumeric LCD Module functions
#include <alcd.h>
#include <stdio.h>



#define LED_STT     PORTC.4
#define SPEAK       PORTC.3

#define CS_IN       PINC.1
#define GAS_IN      PINC.2







#endif /*__MAIN_H*/