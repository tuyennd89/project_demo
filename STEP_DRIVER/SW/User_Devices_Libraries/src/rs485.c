#include "rs485.h"
#include "main.h"

void RS485_USART4_PutChar(unsigned char C)
{
	RS485_EN = 1;
	USART4_PutChar(C);
	delay_ms(10);
	RS485_EN = 0;
}




void RS485_USART4_PutString(unsigned char *S)
{
	RS485_EN = 1;
	USART4_PutString(S);
	USART4_PutChar('\n');
	delay_ms(10);
	RS485_EN = 0;
}

