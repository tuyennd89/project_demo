#include "sim908.h"

char call_buffer[20];
extern char lcd_buff[16];
extern char USART1_Buffer[BUFFE_SIZE];
char SIM9XX_Error_Buff[20]={'\0'};
extern uint8_t Receiver_Ok;
uint8_t	New_SMS;
uint8_t	New_Call;
uint8_t Off_Call;

uint8_t Send_Fail = 0;
//Use for Start_screen_update function
extern uint8_t SIM_Initialize_Bar_Status;


//Use count num reset
volatile uint8_t SIM_Num_Reset=0;



void Clear_Buffer(void)
{
	uint16_t i;
	uint16_t len=strlen(USART1_Buffer);
	for(i=0;i<len;i++)
		USART1_Buffer[i] = '\0';
}
	
	
void Sim908_SendAT_Cmd(char *AT_Cmd)
{
	USART_PutString(USART1,(unsigned char*)AT_Cmd);
	USART_PutChar(USART1,0x0d);					//CR
	USART_PutChar(USART1,0x0A);					//LF
}

uint8_t Sim908_Echo_Off(void)
{
	uint8_t temp;
	uint16_t TimeOut=5000;
	Clear_Buffer();
	Receiver_Ok=0;
	Sim908_SendAT_Cmd("ATE0");
	while(!Receiver_Ok && TimeOut>0)
	{
		TimeOut--;
		delay_ms(1);
	}
	Receiver_Ok=0;
	temp=strlen(USART1_Buffer);
	if(	(USART1_Buffer[temp-2]=='O') &&
		(USART1_Buffer[temp-1]=='K')
		)
	{
		Clear_Buffer();
		return 1;
	}		
	else
		return 0;
}

uint8_t Sim908_SendAT_Cmd_Ok(char *AT_Cmd)
{
	uint16_t TimeOut=2000;
	Clear_Buffer();
	Receiver_Ok=0;
	Sim908_SendAT_Cmd(AT_Cmd);
	while(!Receiver_Ok && TimeOut>0)
	{
		TimeOut--;
		delay_ms(1);
	}
	Receiver_Ok=0;
#ifdef DEBUG_ENABLE
    Debug_Display(USART1_Buffer);
#endif
	if( (strcmp(USART1_Buffer,"OK")==0) || (strcmp(USART1_Buffer,"SHUT OK")==0))
	{
		Clear_Buffer();
		return 1;
	}
	else
		return 0;
}

//Init for GSM
uint8_t SIM90X_GSM_Init(void)
{
	uint8_t Val_Return = 1;
	
	if(Sim908_Echo_Off()==0)
	{
		Val_Return = 0;
	}

	if(Sim908_SendAT_Cmd_Ok("AT")==0)
	{
		Val_Return = 0;
	}
	
	if(Sim908_SendAT_Cmd_Ok("AT+CMGF=1")==0)
	{
		Val_Return = 0;
	}
	
	//Disable clip 
	if(Sim908_SendAT_Cmd_Ok("AT+CLIP=0")==0)
	{
		Val_Return = 0;
	}
	
	return Val_Return;
}


uint8_t SIM90X_GPRS_Init(void)
{
	uint8_t Val_Return = 1;
	
	if(Sim908_SendAT_Cmd_Ok("AT+CIPSHUT")==0)
	{
		Val_Return = 0;
	}
	
	if(Sim908_SendAT_Cmd_Ok("AT+CIPMUX=0")==0)
	{
		Val_Return = 0;
	}
	
	if(Sim908_SendAT_Cmd_Ok("AT+CGATT=1")==0)
	{
		Val_Return = 0;
	}
	
	if(Sim908_SendAT_Cmd_Ok("AT+CSTT=\"WWW\",\"\",\"\"")==0)
	{
		Val_Return = 0;
	}
	
	if(Sim908_SendAT_Cmd_Ok("AT+CIICR")==0)
	{
		Val_Return = 0;
	}
	
	return Val_Return;
}


uint8_t Sim908_Init(void)
{
	uint8_t count_gsm_init=0;
	uint8_t count_gprs_init=0;
	
	//Wait for sim power on
	delay_ms(20000);
START_INIT:	
	
	
  if(SIM90X_GSM_Init()==0)
	{
		#ifdef DEBUG_ENABLE
			Debug_Display("Init again because gsm fail");
		#endif
		count_gsm_init++;
		if(count_gsm_init>5)
			return 1;
		SIM_Reset();
		goto START_INIT;
	}
	
	if(SIM90X_GPRS_Init()==0)
	{
		#ifdef DEBUG_ENABLE
			Debug_Display("Init again because GPRS fail");
		#endif
		count_gprs_init++;
		if(count_gprs_init>3)
			return 2;
		SIM_Reset();
		goto START_INIT;
	}
	
	return 0;
}

void Processor_Buffer(void)
{
	uint8_t i,j;
	if(memcmp(USART1_Buffer,"+CMTI:",6)==0)
	{
		New_SMS=1;
		Receiver_Ok=0;
	}
	if(memcmp(USART1_Buffer,"+CLIP:",6)==0)
	{
		j=0;
		for(i=8;i<19;i++)
			{
				if((USART1_Buffer[i]>='0') && (USART1_Buffer[i]<='9'))
					call_buffer[j++]=USART1_Buffer[i];
			}
		New_Call=1;
		Off_Call=0;
		Receiver_Ok=0;
	}
	
	if(memcmp(USART1_Buffer,"NO CARRIER",10)==0)
	{
		Off_Call=1;
		New_Call=0;
		Receiver_Ok=0;
	}
}

int FindChr_In_Str(char *Str,char chr,uint8_t Locate)
{
	int len;
	int i;
	int	j=0;
	len=strlen(Str);
	for(i=0;i<len;i++)
	{
		if(Str[i]==chr)
		{
			j++;
			if(j==Locate)
				return i;
		}
	}
	return 0;
}

void Read_SMS(char *phone_num, char *text)
{
	int _3th_coma;
	int _4th_coma;
	int _8th_coma;
	int i,n,m;
	int j=0;
	if(New_SMS==1)
	{
		Clear_Buffer();
		Sim908_SendAT_Cmd("AT+CMGR=1");
		while(Receiver_Ok==0);
		_3th_coma=FindChr_In_Str(USART1_Buffer,'\"',3);
		_4th_coma=FindChr_In_Str(USART1_Buffer,'\"',4);
		_8th_coma=FindChr_In_Str(USART1_Buffer,'\"',8);
		j=0;
		n=_3th_coma+1;
		m=_4th_coma;
		for(i=n;i<m;i++)
			phone_num[j++]=USART1_Buffer[i];
		j=0;
		n=_8th_coma+1;
		m=strlen(USART1_Buffer);
		for(i=n;i<m;i++)
			text[j++]=USART1_Buffer[i];
		New_SMS=0;
		Receiver_Ok=0;
		Clear_Buffer();
		Sim908_SendAT_Cmd_Ok("AT+CMGD=1");
	}
}

void Check_Call(void)
{
	if(New_Call)
	{
		//LCD_GotoXY(0,0);
		//LCD_PutStr(call_buffer);
		//LCD_GotoXY(0,1);
		//LCD_PutStr("Calling         ");
	}
	
	if(Off_Call)
	{
		//LCD_GotoXY(0,1);
		//LCD_PutStr("Da Dap May      ");
	}
}


uint8_t Connect_Server(char *IP_Addr, char *PORT)
{
	char cmd[100];
	//Hien thi thong tin Debug
	Debug_Display("Connect to server...");
	sprintf(cmd,"AT+CIPSTART=\"TCP\",\"%s\",\"%s\"",IP_Addr,PORT);
	if(Sim908_SendAT_Cmd_Ok("AT+CIPSHUT")==0)
		return 0;
	if(Sim908_SendAT_Cmd_Ok(cmd)==0)
		return 0;
	delay_ms(100);
	while(Receiver_Ok==0);
	Receiver_Ok=0;
	//Hien thi thong tin Debug
	Debug_Display(USART1_Buffer);
	if( strcmp(USART1_Buffer,"CONNECT OK")==0)
	{
		Clear_Buffer();
		return 1;
	}
	else
	{
		Clear_Buffer();
		return 0;
	}
}

uint8_t Sim908_GPRS_SendData(char *Data)
{
	uint16_t Time_Out=5000;
	
	Receiver_Ok=0;
	Clear_Buffer();
	Sim908_SendAT_Cmd("AT+CIPSEND");
	delay_ms(100);
	while((Receiver_Ok==0) && (Time_Out>0))
	{
		Time_Out--;
		delay_ms(1);
	}
	
	if(Receiver_Ok==0)
	{
		Send_Fail=1;
	  Debug_Display(USART1_Buffer);
		return 0;
	}
	else
	{
		Receiver_Ok=0;
	  Clear_Buffer();
		Time_Out=5000;
	  USART_PutString(USART1,(unsigned char*)Data);
	  USART_PutChar(USART1,0x1A);    
		while((Receiver_Ok==0) && (Time_Out>0))
		{
			Time_Out--;
			delay_ms(1);
		}
		if(Receiver_Ok==0)
		{
			Send_Fail=1;
			Debug_Display("Disconnetc. Cannot Send Data");
			return 0;
		}
		else
		{
			Receiver_Ok=0;
			//Hien thi thong tin Debug
			Debug_Display(USART1_Buffer);
			if( strcmp(USART1_Buffer,"SEND OK")==0)
			{
				Clear_Buffer();
				return 1;
			}
			else
			{
				Clear_Buffer();
				return 0;
			}
		}
	}
}

uint8_t Sim90X_GPRS_SendHTTP_Requets(char *Request)
{
	uint16_t Time_Out=5000;
	
	Clear_Buffer();
	Sim908_SendAT_Cmd("AT+CIPSEND");
	delay_ms(100);
	while((Receiver_Ok==0) && (Time_Out>0))
	{
		Time_Out--;
		delay_ms(1);
	}
	if(Receiver_Ok==0)
	{
		Send_Fail=1;
	  Debug_Display("Disconnetc. Cannot Send Data");
		return 0;
	}
	else
	{
		Receiver_Ok=0;
	  Clear_Buffer();
		Time_Out=5000;
		USART_PutChar(USART1,0x0D);
	  USART_PutString(USART1,(unsigned char*)Request);
		USART_PutChar(USART1,0x0A);
		USART_PutChar(USART1,0x0D);
		USART_PutChar(USART1,0x0A);
	  USART_PutChar(USART1,0x1A);    
		while((Receiver_Ok==0) && (Time_Out>0))
		{
			Time_Out--;
			delay_ms(1);
		}
		if(Receiver_Ok==0)
		{
			Send_Fail=1;
			Debug_Display("Disconnetc. Cannot Send Data");
			return 0;
		}
		else
		{
			Receiver_Ok=0;
			//Hien thi thong tin Debug
			Debug_Display(USART1_Buffer);
			if( strcmp(USART1_Buffer,"SEND OK")==0)
			{
				Clear_Buffer();
				return 1;
			}
			else
			{
				Clear_Buffer();
				return 0;
			}
		}
	}
}

void Sim908_GPRS_DataProcess(void)
{
	if(Receiver_Ok)
	{
		Receiver_Ok=0;
		Clear_Buffer();
	}
}


uint8_t SIM9XX_GetIMSI(char *IMSI)
{
	unsigned char len;
	
	//Hien thi thong tin Debug
	Debug_Display("GET IMSI");
	
	Clear_Buffer();
	Sim908_SendAT_Cmd(GET_IMSI);
	while(Receiver_Ok==0);
	Receiver_Ok=0;
	
	len=strlen(USART1_Buffer);
	USART1_Buffer[len-2]='\0';
	strcpy(IMSI,USART1_Buffer);
	//Hien thi thong tin Debug
	Debug_Display(USART1_Buffer);
	//xoa Buffer nhan
	Clear_Buffer();

	return 0;	
}


void SIM_Reset(void)
{
	SIM_PWR_CTRL = 1;
	delay_ms(2000);
	SIM_PWR_CTRL = 0;
}


void SIM_Power_On(void)
{
    SIM_PWR_CTRL = SIM_POWER_ON;
    delay_ms(30000);
}


void SIM_Power_Off(void)
{
    SIM_PWR_CTRL = SIM_POWER_OFF;
    delay_ms(3000);
}



uint8_t Start_Sync_Network_Time(void)
{
	Sim908_SendAT_Cmd_Ok("AT+SAPBR=3,1,\"Contype\",\"GPRS\"");
	Sim908_SendAT_Cmd_Ok("AT+SAPBR=3,1,\"APN\",\"CMNET\"");
	Sim908_SendAT_Cmd_Ok("AT+SAPBR=1,1");
	Sim908_SendAT_Cmd_Ok("AT+CNTPCID=1");
	Sim908_SendAT_Cmd_Ok("AT+CNTP=\"202.120.2.101\",28");
	
	Sim908_SendAT_Cmd("AT+CNTP");
	delay_ms(5000);
	if(Receiver_Ok)
	{
		#ifdef DEBUG_ENABLE
			Debug_Display(USART1_Buffer);
		#endif
		Receiver_Ok=0;
		Clear_Buffer();
	}
	return 0;
}


void SIM90X_GetTime(uint8_t *hour, uint8_t *minute, uint8_t *second, uint8_t *date, uint8_t *month, uint16_t *year)
{
	uint16_t TimeOut=2000;
	char *start;
	char _hour[3]={0},_minute[3]={0},_second[3]={0},_date[3]={0},_month[3]={0},_year[3]={0};
	
	Clear_Buffer();
	Receiver_Ok=0;
	Sim908_SendAT_Cmd("AT+CCLK?");
	while(!Receiver_Ok && TimeOut>0)
	{
		TimeOut--;
		delay_ms(1);
	}
	if(Receiver_Ok)
	{
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(USART1_Buffer);
		#endif
		start=strchr(USART1_Buffer,'"');
		start++;																						//Year
		strncpy(_year,start,2);
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(_year);
		#endif
		*year = 2000 + (_year[0]-48)*10 + (_year[1]-48);
		
		start+=3;																					//month
		strncpy(_month,start,2);
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(_month);
		#endif
		*month = (_month[0]-48)*10 + (_month[1]-48);
		
		start+=3;																					//Date
		strncpy(_date,start,2);
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(_date);
		#endif
		*date = (_date[0]-48)*10 + (_date[1]-48);
		
		start+=3;																					//Hour
		strncpy(_hour,start,2);
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(_hour);
		#endif
		*hour = (_hour[0]-48)*10 + (_hour[1]-48);
		
		start+=3;																					//Minute
		strncpy(_minute,start,2);
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(_minute);
		#endif
		*minute = (_minute[0]-48)*10 + (_minute[1]-48);
		
		start+=3;																					//Second
		strncpy(_second,start,2);
		#ifdef DEBUG_GET_TIME_ENABLE
			Debug_Display(_second);
		#endif
		*second = (_second[0]-48)*10 + (_second[1]-48);
		
		
		Receiver_Ok=0;
		Clear_Buffer();
	}
		Receiver_Ok=0;
		Clear_Buffer();
}


//Ham nay viet ngay 12/11/2017
//Giai doan hoan thien tinh nang gui tien cho mach dieu khien may nuoc nong nang luong mat troi
uint8_t count_isr_usart_idle=0;

void Sim90X_CheckMoney(char *info,char *respond)
{
		uint16_t TimeOut=10000;
		char *start;
		char *end;
		uint16_t length=0,i=0;
		char cmd[]={'\0'};
		
		sprintf(cmd,"AT+CUSD=1,\"%s\"",info);
		Clear_Buffer();
		count_isr_usart_idle=0;
		Sim908_SendAT_Cmd(cmd);
		while(count_isr_usart_idle<2 && TimeOut>0)
		{
			TimeOut--;
			delay_ms(1);
		}
		if(count_isr_usart_idle>=2)
		{

			if(strncmp(USART1_Buffer,"+CUSD",5)==0)
			{
				start=strchr(USART1_Buffer,0x22);  //'"'
				end = strrchr(USART1_Buffer,0x22);  //'"'
				start++;
				length=end - start;
				for(i=0;i<length;i++)
				{
					respond[i] = start[i];
				}
				respond[length]='\0';
			}
			count_isr_usart_idle=0;
			Clear_Buffer();
		}
}







