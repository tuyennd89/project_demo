#include "__74hc595.h"

extern uint8_t Relay_Status[5];
extern uint32_t U32_Relay_Status;

void __74HC595_Pin_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(__74HC595_RCC_Periph,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin =__74HC595_CLK_Pin | __74HC595_LATCH_Pin | __74HC595_DATA_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(__74HC595_PORT,&GPIO_InitStructure);
}


void __74HC595_Shift_Byte(uint8_t data)
{
	uint8_t i;
	uint8_t temp = data;
	
	for(i=0;i<8;i++)
	{
		if((temp&0x01)==0x01)
			__74HC595_DATA = 1;
		else
			__74HC595_DATA = 0;
		__74HC595_CLK = 0;
		__74HC595_CLK = 1;
		temp >>= 1;
	}
}


void __74HC595_Latch(void)
{
	__74HC595_LATCH = 0;
	__74HC595_LATCH = 1;
}


void __74HC595_OutWord(uint32_t Word)
{
	uint32_t temp = Word;
	
	__74HC595_Shift_Byte((temp>>24)&0xff);
	__74HC595_Shift_Byte((temp>>16)&0xff);
	__74HC595_Shift_Byte((temp>>8)&0xff);
	__74HC595_Shift_Byte(temp&0xff);
	
	__74HC595_Latch();
}

void ON_RELAY(uint32_t Relay_Num)
{
	uint32_t temp = Relay_Num;
	
	U32_Relay_Status |= temp;
	
	__74HC595_OutWord(U32_Relay_Status);
	//Relay_Status_Save(U32_Relay_Status);
}



void OFF_RELAY(uint32_t Relay_Num)
{
	uint32_t temp = (~Relay_Num);
	
	U32_Relay_Status &= temp;
	
	__74HC595_OutWord(U32_Relay_Status);
	//Relay_Status_Save(U32_Relay_Status);
}


void Relay_Status_Save(uint32_t Relay_Stt)
{
	uint32_t temp_relay = Relay_Stt;

	Relay_Status[3] = (temp_relay>>24)&0xFF;
	Relay_Status[2] = (temp_relay>>16)&0xFF;
	Relay_Status[1] = (temp_relay>>8)&0xFF;
	Relay_Status[0] = (temp_relay)&0xFF;
	
	//EEpromEmulator_WriteBytes(RELAY_STATUS_LOCATION,(uint8_t*)Relay_Status,RELAY_STATUS_LEN);
}





