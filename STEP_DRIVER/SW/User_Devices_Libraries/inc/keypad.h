#ifndef __KEYPAD_H
#define __KEYPAD_H

#include "stm32f10x.h"
#include "def_stm32f10x.h"

#define KEYPAD_ROW1	PINB_0
#define KEYPAD_ROW2	PINB_1
#define KEYPAD_ROW3	PINB_2
#define KEYPAD_ROW4	PINB_3

#define KEYPAD_COL1	PORTB_4
#define KEYPAD_COL2	PORTB_5
#define KEYPAD_COL3	PORTB_6
#define KEYPAD_COL4	PORTB_7

#define GPIO_KEYPAD	GPIOB
#define PIN_COL_KEYPAD		(GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7)
#define PIN_ROW_KEYPAD		(GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3)

void KeyPad_Pin_Config(void);
uint8_t KEY_IS_PUSH(void);
void KEYPAD_CheckCol(uint8_t i);
uint8_t KEYPAD_GetKey(void);



#endif

