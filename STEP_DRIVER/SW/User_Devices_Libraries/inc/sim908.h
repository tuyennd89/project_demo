#ifndef __SIM908_H
#define __SIM908_H

#include "stm32f10x.h"
#include "def_stm32f10x.h"
#include "string.h"
#include "stdio.h"
//#include "lcd16x2.h"
#include "my_uart.h"
#include "delay.h"
#include <ctype.h>
#include "glcdlib.h"
#include "debug.h"

#define SIM_MOBIPHONE


#define BUFFE_SIZE            512


//Pin of USART TX
#define USART_TX                        PORTA_9


#define GET_IMSI    "AT+CIMI"

#define SIM_PWR_CTRL    PORTB_5

#define SIM_POWER_OFF   1
#define SIM_POWER_ON    0



//Define AT+CNTP parameter response
#define CNTP_OK                         1           //network time synchronization is successfull
#define CNTP_NETWORK_ERR                61          //Network error
#define CNTP_DNS_ERR                    62          //DNS resolution error
#define CNTP_CONNECT_ERR                63          //Connection error
#define CNTP_RESPONSE_ERR               64          //Service response error
#define CNTP_RESPONSE_TIMEOUT_ERR       65          //Service response timeout





void Clear_Buffer(void);
void Sim908_SendAT_Cmd(char *AT_Cmd);
uint8_t Sim908_Echo_Off(void);
uint8_t Sim908_SendAT_Cmd_Ok(char *AT_Cmd);
uint8_t Sim908_Init(void);
void Processor_Buffer(void);
void Read_SMS(char *phone_num, char *text);
int FindChr_In_Str(char *Str,char chr,uint8_t Locate);
void Process_SMS(void);
void Check_Call(void);
uint8_t Connect_Server(char *IP_Addr, char *PORT);
uint8_t Sim908_GPRS_SendData(char *Data);

void Sim908_GPRS_DataProcess(void);
uint8_t Sim90X_GPRS_SendHTTP_Requets(char *Request);


uint8_t SIM9XX_GetIMSI(char *IMSI);

void SIM_Reset(void);
void SIM_Power_On(void);
void SIM_Power_Off(void);


uint8_t SIM90X_GSM_Init(void);
uint8_t SIM90X_GPRS_Init(void);

uint8_t Start_Sync_Network_Time(void);
void SIM90X_GetTime(uint8_t *hour, uint8_t *minute, uint8_t *second, uint8_t *date, uint8_t *month, uint16_t *year);

//Ham nay viet ngay 12/11/2017
//Giai doan hoan thien tinh nang gui tien cho mach dieu khien may nuoc nong nang luong mat troi
void Sim90X_CheckMoney(char *info,char *respond);


#endif

