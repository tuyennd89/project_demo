#include "keypad4x4.h"

unsigned char key_value[4][4]={'D','C','B','A',
                               '#','9','6','3',
                               '0','8','5','2',
                               '*','7','4','1'
                               };


void Keypad_Pin_Configuration(void)
{
#ifdef USE_STM8S
    /*Set output for column*/
    GPIO_Init(KEYPAD_PORT, (GPIO_Pin_TypeDef)(KEYPAD_COL1_PIN | KEYPAD_COL2_PIN | KEYPAD_COL3_PIN | KEYPAD_COL4_PIN), GPIO_MODE_OUT_PP_LOW_FAST);
    /*Set input for row*/
    GPIO_Init(KEYPAD_PORT, (GPIO_Pin_TypeDef)(KEYPAD_ROW1_PIN | KEYPAD_ROW2_PIN | KEYPAD_ROW3_PIN | KEYPAD_ROW4_PIN), GPIO_MODE_IN_PU_NO_IT);
#endif
#ifdef USE_STM32
     /*Set output for column*/
    Pin_Configuration(KEYPAD_PORT,(KEYPAD_COL1_PIN | KEYPAD_COL2_PIN | KEYPAD_COL3_PIN | KEYPAD_COL4_PIN),GPIO_Speed_2MHz,GPIO_Mode_Out_PP);
    /*Set input for row*/
    Pin_Configuration(KEYPAD_PORT,(KEYPAD_ROW1_PIN | KEYPAD_ROW2_PIN | KEYPAD_ROW3_PIN | KEYPAD_ROW4_PIN),GPIO_Speed_2MHz,GPIO_Mode_IPU);
#endif  /*USE_STM32*/
    KEYPAD_PORT_OUT=0xFF;
}


void Keypad_Select_Col(unsigned char col)
{
    unsigned char temp=0xFF;
    
    temp&=(~(1<<col));
    KEYPAD_PORT_OUT=temp;
}


unsigned char Keypad_GetValue(void)
{
    unsigned char Key_Return=0;
    unsigned char col,row;
    unsigned char temp=0;
    
    for(col=0;col<4;col++)
    {
        Keypad_Select_Col(col);
        for(row=0;row<4;row++)
        {
            temp=(~((KEYPAD_PORT_IN&0xF0)>>4));
            temp&=0x0F;
            if(temp==(1<<row))
            {
                Key_Return=key_value[col][row];
                while((KEYPAD_PORT_IN&0xF0)<0xF0);
            }
        }
    }
    return Key_Return;
}

