#ifndef __KEYPAD_H
#define __KEYPAD_H

#include "main.h"

#define USE_STM32

#ifdef USE_STM8S
    /*Column*/
    #define             KEYPAD_COL1_PIN             GPIO_PIN_0
    #define             KEYPAD_COL2_PIN             GPIO_PIN_1
    #define             KEYPAD_COL3_PIN             GPIO_PIN_2
    #define             KEYPAD_COL4_PIN             GPIO_PIN_3
    /*Row*/
    #define             KEYPAD_ROW1_PIN             GPIO_PIN_4
    #define             KEYPAD_ROW2_PIN             GPIO_PIN_5
    #define             KEYPAD_ROW3_PIN             GPIO_PIN_6
    #define             KEYPAD_ROW4_PIN             GPIO_PIN_7

    #define             KEYPAD_PORT                 GPIOB
    #define             KEYPAD_PORT_OUT             PORTB
    #define             KEYPAD_PORT_IN              PINB
#endif  /*USE_STM8S*/

#ifdef USE_STM32
 /*Column*/
    #define             KEYPAD_COL1_PIN             GPIO_Pin_0
    #define             KEYPAD_COL2_PIN             GPIO_Pin_1
    #define             KEYPAD_COL3_PIN             GPIO_Pin_2
    #define             KEYPAD_COL4_PIN             GPIO_Pin_3
    /*Row*/
    #define             KEYPAD_ROW1_PIN             GPIO_Pin_4
    #define             KEYPAD_ROW2_PIN             GPIO_Pin_5
    #define             KEYPAD_ROW3_PIN             GPIO_Pin_6
    #define             KEYPAD_ROW4_PIN             GPIO_Pin_7

    #define             KEYPAD_PORT                 GPIOA
    #define             KEYPAD_PORT_OUT             PORTA
    #define             KEYPAD_PORT_IN              PINA

#endif  /*USE_STM32*/


void Keypad_Pin_Configuration(void);
void Keypad_Select_Col(unsigned char col);
unsigned char Keypad_GetValue(void);















#endif /*__KEYPAD_H*/



