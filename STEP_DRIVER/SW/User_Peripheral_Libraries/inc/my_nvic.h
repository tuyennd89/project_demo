#ifndef __MY_NVIC_H
#define __MY_NVIC_H

#include "stm32f10x.h"

void NVIC_Configuration(void);

#endif
