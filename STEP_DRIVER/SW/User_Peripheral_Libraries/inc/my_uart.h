#ifndef __MY_UART_H
#define __MY_UART_H

#include "stm32f10x.h"
#include "stdio.h"
#include "string.h"


#define USART1_RxPin	GPIO_Pin_10
#define USART1_TxPin	GPIO_Pin_9
#define USART1_GPIO		GPIOA

#define USART3_RxPin	GPIO_Pin_11
#define USART3_TxPin	GPIO_Pin_10
#define USART3_GPIO		GPIOB

#define USART4_RxPin	GPIO_Pin_11
#define USART4_TxPin	GPIO_Pin_10
#define USART4_GPIO		GPIOC



void USART1_PinConfigure(void);
void USART1_Init(uint32_t BaudRate, FunctionalState NewState);
void USART1_PutChar(unsigned char C);
void USART1_PutString(unsigned char *S);

void USART1_PutNumber(uint32_t num);

void USART3_Init(uint32_t BaudRate, FunctionalState NewState);
void USART3_PinConfigure(void);
void USART3_PutChar(unsigned char C);
void USART3_PutString(unsigned char *S);

void UART4_PinConfigure(void);
void UART4_Init(uint32_t BaudRate, FunctionalState NewState);
void USART4_PutChar(unsigned char C);
void USART4_PutString(unsigned char *S);

void USART_PutChar(USART_TypeDef* USARTx, unsigned char C);
void USART_PutString(USART_TypeDef* USARTx, unsigned char *S);

#endif

