#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f10x.h"
#include <stdio.h>
#include <math.h>
#include "my_uart.h"
#include "glcd_gphone_spi.h"
#include "keypad4x4.h"
#include "delay.h"
#include "def_stm32f10x.h"
#include "Step_Driver.h"

#define ENABLE_DRIVER				1
#define DISABLE_DRIVER			0

#define RELAY_ON						0
#define RELAY_OFF						1

#define SPEED_MAX       1
#define SPEE_MIN        100

#define RELAY1_PIN					GPIO_Pin_9
#define RELAY1_PORT					GPIOB
#define RELAY1							PORTB_9

#define RELAY2_PIN					GPIO_Pin_8
#define RELAY2_PORT					GPIOB
#define RELAY2							PORTB_8

#define ENABLE_PIN					GPIO_Pin_10
#define ENABLE_PORT					GPIOB
#define EN_MOTOR						PORTB_10

#define DIR_PIN							GPIO_Pin_1
#define DIR_PORT						GPIOB
#define DIR									PORTB_1

#define STEP_PIN						GPIO_Pin_0
#define STEP_PORT						GPIOB
#define STEP								PORTB_0

#define INPUT_PIN						GPIO_Pin_6
#define INPUT_PORT					GPIOB
#define INPUT								PINB_6



void Timer_Configuration(void);
void NVIC_Configuration(void);

void Pin_Configuration(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIOSpeed_TypeDef GPIOSpeed, GPIOMode_TypeDef GPIOMode);

















#endif /*__MAIN_H*/
