#ifndef __GLCD_GPHONE_SPI_H
#define __GLCD_GPHONE_SPI_H
#include "main.h"



#define             AO_PIN          GPIO_Pin_14
#define             SDA_PIN         GPIO_Pin_15
#define             SCK_PIN         GPIO_Pin_13
#define             CS_PIN          GPIO_Pin_12

#define            GLCD_PORT        GPIOB

#define             AO              PORTB_14
#define             SDA             PORTB_15
#define             SCK             PORTB_13
#define             CS              PORTB_12


void lcd_pin_config(void);
void lcd_init(void);
void lcd_puts(char *s);
void lcd_putchar(char dat);
void lcd_clear(void);
void lcd_gotoxy(uint8_t x, uint8_t y);
void lcd_write(uint8_t cd, uint8_t byte);

#endif  /*__GLCD_GPHONE_SPI_H*/

