#include "Step_Driver.h"

ModeTypeDef                     SysMode = Normal;
Motor_StatusTypeDef             Step_Motor_Status;

uint32_t clk_count;
float speed;
float actual_round;
float setting_round;
uint32_t step_setting;
uint32_t so_nguyen,so_thuc;

char lcd_buff[20];
char pause=0;
char p_thuc=0;
char count_so_thuc=0;
char dir=0;

extern uint8_t Time_Clk;

void Normal_Mode(void)
{
	uint8_t key;
	
	key=Keypad_GetValue();
  
    if(key=='A')
    {
        SysMode=Setting;
        count_so_thuc=0;
        p_thuc=0;
        lcd_clear();
        delay_ms(100);
    }
  
    /*Khoi dong dong co*/
    if((key=='B') || (INPUT==0))
    {
        if(Step_Motor_Status==NOT_RUN)
        {
            if(pause==0)
            {
                Step_Motor_Status=RUN;
                EN_MOTOR=ENABLE_DRIVER;
                clk_count=0;
                TIM_Cmd(TIM4, ENABLE);
            }else
            if(pause==1)
            {
                Step_Motor_Status=RUN;
                EN_MOTOR=ENABLE_DRIVER;
                pause=0;
                TIM_Cmd(TIM4, ENABLE);
            }
            RELAY1 = RELAY_OFF;
            RELAY2 = RELAY_OFF;
        }
    }
  
    /*Stop dong co*/
    if(key=='C')
    {
        if(Step_Motor_Status==RUN)
        {
             pause=1;
             Step_Motor_Status=NOT_RUN;
             EN_MOTOR=DISABLE_DRIVER;
             TIM_Cmd(TIM4, DISABLE);
        }
    }
  
    /*Reset gia tri cac bien dem so vong*/
    if((key=='D') && (Step_Motor_Status==NOT_RUN))
    {
        setting_round=0;
    }  
    Main_Menu(SysMode);
}






void Setting_Mode(void)
{
	uint8_t key;
	
    RELAY1 = RELAY_OFF;
	RELAY2 = RELAY_OFF;
	key=Keypad_GetValue();
    
    /*Chuyen sang default mode*/
    if(key=='A')
    {
        step_setting=setting_round*400;
        SysMode=Normal;
        lcd_clear();
        so_nguyen=0;
        so_thuc=0;
        delay_ms(100);
    }
    
    /*reset gia tri bien cai dat so vong quay*/
    if(key=='C')
    {
        step_setting=0;
        setting_round=0;
        p_thuc=0;
        so_nguyen=0;
        so_thuc=0;
        count_so_thuc=0;
    }
    
    /*Tinh toan so vong quay duoc nhap vao tu ban phim*/
    if((key>='0') && (key<='9'))
    {
        if(p_thuc==0)
        {
            so_nguyen*=10;
            so_nguyen+=(key-48);
        } else
        if((p_thuc==1) && (count_so_thuc<3) )
        {
            so_thuc*=10;
            so_thuc+=(key-48);
            count_so_thuc++;
        }
        //setting_round*=10;
        //setting_round+=(key-48);
        setting_round = (float)so_nguyen + (float)so_thuc/pow(10,count_so_thuc);
    }
    
    /*Speed up*/
    if(key=='B')
    {
        Time_Clk--;
        if(Time_Clk<=SPEED_MAX)
            Time_Clk=SPEED_MAX;
        TIM_SetAutoreload(TIM4,Time_Clk);
        speed=(float)1000/(Time_Clk*2*200);
    }
    
     /*Speed down*/
    if(key=='D')
    {
        Time_Clk++;
        if(Time_Clk>=SPEE_MIN)
            Time_Clk=SPEE_MIN;
        TIM_SetAutoreload(TIM4,Time_Clk);
        speed=(float)1000/(Time_Clk*2*200);
    }
    
    if(key=='*')
    {
        if(p_thuc==0)
        {
            count_so_thuc=0;
            p_thuc=1;
        }
    }
    
    if(key=='#')
    {
        DIR = ~DIR;
        dir++;
        if(dir>1)
            dir=0;
    }
    Main_Menu(Setting);
}




void Main_Menu(ModeTypeDef mode_menu)
{
    float actual_round = (float)clk_count/400;
   
    
    if(mode_menu==Normal)
    {
        lcd_gotoxy(3,0);
        lcd_puts("LINHKIENMCU.VN");
        
        sprintf(lcd_buff,"Round Run: %.3f ",actual_round);
        lcd_gotoxy(0,1);
        lcd_puts(lcd_buff);
        
        sprintf(lcd_buff,"Round Setting: %.3f ",setting_round);
        lcd_gotoxy(0,2);
        lcd_puts(lcd_buff); 
        lcd_gotoxy(0,6);
        lcd_puts("A:Setting.    B:Start");
        lcd_gotoxy(0,7);
        lcd_puts("C:Stop.       D:Reset.");
        
    }else
    if(mode_menu==Setting)
    {
        lcd_gotoxy(3,0);
        lcd_puts("LINHKIENMCU.VN");
        
        sprintf(lcd_buff,"Round Setting: %.3f ",setting_round);
        lcd_gotoxy(0,2);
        lcd_puts(lcd_buff);
        
        lcd_gotoxy(0,3);
        if(dir)
        {
            lcd_puts("#:DIR: ->");
        }
        else
        {
            lcd_puts("#:DIR: <-");
        }
        
        sprintf(lcd_buff,"SPEED: %.3f   RPS ",speed);
        lcd_gotoxy(0,4);
        lcd_puts(lcd_buff);
        
        lcd_gotoxy(0,5);
        lcd_puts("D:Speed Down.");
        lcd_gotoxy(0,6);
        lcd_puts("B:Speed Up.");
        
        lcd_gotoxy(0,7);
        lcd_puts("C:Clear.       A:Ok.");
    }
}



/**
  * @brief  This function handles TIM4 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM4_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
	{
		STEP=~STEP;
        clk_count++;
        if(clk_count>=step_setting)
        {
            EN_MOTOR=ENABLE_DRIVER;
            TIM_Cmd(TIM4, DISABLE);
            Step_Motor_Status=NOT_RUN;
            RELAY1 = RELAY_ON;
            RELAY2 = RELAY_ON;
        }
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update); 
	}
}








