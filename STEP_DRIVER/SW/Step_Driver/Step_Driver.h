#ifndef __STEP_DRIVER_H
#define __STEP_DRIVER_H

#include "main.h"

typedef enum
{
	Normal = 0,
	Setting
}
ModeTypeDef;

typedef enum
{
  NOT_RUN=0,
  RUN
}
Motor_StatusTypeDef;

void Normal_Mode(void);
void Setting_Mode(void);
void Main_Menu(ModeTypeDef mode_menu);
















#endif /*__STEP_DRIVER_H*/
