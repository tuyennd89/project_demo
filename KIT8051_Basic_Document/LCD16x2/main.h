/**
  ******************************************************************************
  * Ten Tep      :        main.h
  * Tac Gia      :        Nguyen Quy Nhat
  * Cong Ty      :        MinhHaGroup
  * Website      :        BanLinhKien.Vn
  * Phien Ban    :        V1.0.0
  * Ngay         :        31-08-2012
  * Tom Tat      :        Khai bao cac thu vien.
  *                       Cau hinh mot so chuc nang cho trinh bien dich.
  *                       Cau hinh mot so chuc nang cho CHIP.
  *                       Dinh nghia I/O.
  *                         
  ******************************************************************************
  */
#ifndef __MAIN_H
#define __MAIN_H

/*	Kieu So Nguyen Co Dau	*/
typedef   signed          char int8_t;
typedef   signed 	        int int16_t;
typedef   signed long     int int32_t;

/*	Kieu So Nguyen Khong Dau */
typedef   unsigned         char uint8_t;
typedef   unsigned 	       int  uint16_t;
typedef   unsigned long    int  uint32_t;
/*	Kieu So Thuc */
typedef   float            float32_t;

/*********** HW ************/
#include "AT89X52.h"
#include "stdio.h"
#include "lcd16x2/lcd_16x2.h"
#include "delay/delay.h"

/*********** P0 ************/

#define LCD_D4				P2_4
#define LCD_D5				P2_5
#define LCD_D6				P2_6
#define LCD_D7				P2_7

/*********** P1 ************/


/*********** P2 ************/
#define LCD_RS			  P3_7
#define LCD_RW				P3_6
#define LCD_EN				P3_5

/*********** P3 ************/


#endif
/******************************KET THUC FILE******************************
______________________________NGUYEN QUY NHAT______________________________*/