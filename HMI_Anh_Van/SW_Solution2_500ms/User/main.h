#ifndef __MAIN_H
#define __MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f10x.h"
#include "def_stm32f10x.h"
#include "my_uart.h"
#include "my_nvic.h"
#include "modbusSlave.h"
#include "var.h"
#include "modbus1.h"

//#define TEST_MSG

typedef enum
{
    false=0,
    true
}
boolean;



#define BUFFER_SIZE    50

#define DEBUG

#define MODBUS1_TX                            PORTA_11


#define LED_STT                               PORTB_12


#define LED_1                                 PORTB_1  
#define LED_2                                 PORTB_0
#define LED_3                                 PORTA_7
#define LED_4                                 PORTA_6
#define LED_5                                 PORTA_5
#define LED_6                                 PORTA_4
#define LED_7                                 PORTA_3
#define LED_8                                 PORTA_2
#define LED_9                                 PORTA_1
#define LED_10                                PORTC_15
#define LED_11                                PORTC_14
#define LED_12                                PORTC_13
#define LED_13                                PORTB_9
#define LED_14                                PORTB_8
#define LED_15                                PORTB_7
#define LED_16                                PORTB_6
#define LED_17                                PORTB_5
#define LED_18                                PORTB_4
#define LED_19                                PORTB_3
#define LED_20                                PORTA_15





#define BUTTON_NUM                            20
#define LED_NUM                               20


void delay_ms(uint32_t nms);
void Timer_Configuration(void);
void RS485_Puts(char *s);
void RS485_PutChar(char c);
void RS485_PutNum(uint8_t num);

void Debug_PutNum(char *s, uint32_t num);

void RS485_4_Puts(char *s);
void RS485_4_PutChar(char c);


void AssignedAddrForHmiLed(void);
void AssignedAddrForHmiButton(void);
void led_blink(uint8_t i);
void Write_Led(uint8_t i,uint8_t value);















#endif /*__MAIN_H*/
