/**
  ******************************************************************************
  * @file    GPIO/IOToggle/main.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "def_stm32f10x.h"

/** @addtogroup STM32F10x_StdPeriph_Examples
  * @{
  */

/** @addtogroup GPIO_IOToggle
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
GPIO_InitTypeDef GPIO_InitStructure;
volatile uint32_t time_count=0;

modbusDigReg_TypeDef led_hmi[LED_NUM];
modbusAnaReg_TypeDef button_hmi[BUTTON_NUM];

modbusAnaReg_TypeDef x;

boolean led_stt[20]={false};
uint8_t led_blink_count[20]={0};
uint16_t led_time_count[20]={0};


char buff[20];



/* Private function prototypes -----------------------------------------------*/



/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
    uint8_t i=0;
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */     
    SystemInit();
       
    /* GPIOB Periph clock enable */
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);		
		GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    /* Configure PD0 and PD2 in output pushpull mode */
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_12);
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_11 | GPIO_Pin_15);
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    Timer_Configuration();
    NVIC_Configuration();
    USART3_Init(9600,DISABLE);
    
    modbusDevice_setId(1);
    Modbus1_Init(USART1,57600);
    
   // AssignedAddrForHmiLed();
    AssignedAddrForHmiButton();
    
    for(i=0;i<20;i++)
    {
        led_hmi[i].value = 0;
    }
		LED_STT = 1;
		
    LED_1=LED_2=LED_3=LED_4=LED_5=LED_6=LED_7=LED_8=LED_9=LED_10=0;
    LED_11=LED_12=LED_13=LED_14=LED_15=LED_16=LED_17=LED_18=LED_19=LED_20=0;
  /* To achieve GPIO toggling maximum frequency, the following  sequence is mandatory. 
     You can monitor PD0 or PD2 on the scope to measure the output signal. 
     If you need to fine tune this frequency, you can add more GPIO set/reset 
     cycles to minimize more the infinite loop timing.
     This code needs to be compiled with high speed optimization option.  */
    //RS485_4_Puts("Project HMI WEINTEK\n");
    USART3_PutString("Project HMI WEINTEK\n");
    while (1)
    {
        
        Modbus1_Run();
				for(i=0;i<20;i++)
				{
					if(button_hmi[i].value == 255)
					{
						led_stt[i] = true;
						button_hmi[i].value = 0;
					}
					if(led_stt[i] == true)
					{
						led_blink(i);
					}
				}
				
    }
}


void Timer_Configuration(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
   
    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Prescaler = ((SystemCoreClock)/1000000)-1;                          //Ftimer=1Mhz
    TIM_TimeBaseStructure.TIM_Period = 1150 - 1;                              //1ms
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
    TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);
    TIM_Cmd(TIM4, ENABLE);
    
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);   
}

void TIM4_IRQHandler(void)
{
    uint8_t j;
    if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
    {
        time_count++;
        if(time_count>100)
        {
            time_count=0;
            //ttt++;
            LED_STT = ~LED_STT;
            //led_hmi[1].value = ~led_hmi[1].value;
        }
        for(j=0;j<20;j++)
        {
            if(led_stt[j] == true)
            {
                led_time_count[j]++;
            }
        }
        TIM_ClearITPendingBit(TIM4, TIM_IT_Update); 
    }
}


void delay_ms(uint32_t nms)
{
    uint32_t i,j;
    for(i=0;i<nms;i++)
    {
        for(j=0;j<7200;j++);
    }
}


void AssignedAddrForHmiLed(void)
{
    uint8_t i=0;
    
    for(i=0;i<LED_NUM;i++)
    {
        modbusRegBank_assigned_DigRegAddr(i+1,&led_hmi[i]);
    }
}

void AssignedAddrForHmiButton(void)
{
    uint8_t i=0;
    
    for(i=0;i<BUTTON_NUM;i++)
    {
        modbusRegBank_assigned_AnaRegAddr(i+40001,&button_hmi[i]);
    }
}



void led_blink(uint8_t i)
{
		if(led_time_count[i]<50)
		{
			if(i==9)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
			} else
			if(i==12)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
				Write_Led(i+3,1);
			} else
			if(i==16)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
			} else
			if(i==19)
			{
				Write_Led(i,1);
				Write_Led(3,1);
			}
			else
			{
				Write_Led(i,1);
			}
		} else
		if((led_time_count[i]>=50) && (led_time_count[i]<100))
		{
			if(i==9)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
			} else
			if(i==12)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
				Write_Led(i+3,0);
			} else
			if(i==16)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
			} else
			if(i==19)
			{
				Write_Led(i,0);
				Write_Led(3,0);
			}
			else
			{
				Write_Led(i,0);
			}
		} else
		if((led_time_count[i]>=100) && (led_time_count[i]<150))
		{
			if(i==9)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
			} else
			if(i==12)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
				Write_Led(i+3,1);
			} else
			if(i==16)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
			} else
			if(i==19)
			{
				Write_Led(i,1);
				Write_Led(3,1);
			}
			else
			{
				Write_Led(i,1);
			}
		} else
		if((led_time_count[i]>=150) && (led_time_count[i]<200))
		{
			if(i==9)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
			} else
			if(i==12)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
				Write_Led(i+3,0);
			} else
			if(i==16)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
			} else
			if(i==19)
			{
				Write_Led(i,0);
				Write_Led(3,0);
			}
			else
			{
				Write_Led(i,0);
			}
		} else
		if((led_time_count[i]>=200) && (led_time_count[i]<500))
		{
			if(i==9)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
			} else
			if(i==12)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
				Write_Led(i+3,1);
			} else
			if(i==16)
			{
				Write_Led(i,1);
				Write_Led(i+1,1);
				Write_Led(i+2,1);
			} else
			if(i==19)
			{
				Write_Led(i,1);
				Write_Led(3,1);
			}
			else
			{
				Write_Led(i,1);
			}
		} else
		if(led_time_count[i]>=500)
		{
			if(i==9)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
			} else
			if(i==12)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
				Write_Led(i+3,0);
			} else
			if(i==16)
			{
				Write_Led(i,0);
				Write_Led(i+1,0);
				Write_Led(i+2,0);
			} else
			if(i==19)
			{
				Write_Led(i,0);
				Write_Led(3,0);
			}
			else
			{
				Write_Led(i,0);
			}
			led_stt[i] = false;         //satus off
			led_time_count[i] = 0;
		}
}

void Debug_PutNum(char *s, uint32_t num)
{
    char buff[10]={0};
    
    USART3_PutString(s);
    sprintf(buff,"%u\n",num);
    USART3_PutString(buff);
}

void Write_Led(uint8_t i,uint8_t value)
{
	switch(i)
  {
      case 0:
          LED_1 = value;
          break;
      case 1:
          LED_2 = value;
          break;
      case 2:
          LED_3 = value;
          break;
      case 3:
          LED_4 = value;
          break; 
      case 4:
          LED_5 = value;
          break;
      case 5:
          LED_6 = value;
          break;
      case 6:
          LED_7 = value;
          break;
      case 7:
          LED_8 = value;
          break;
      case 8:
          LED_9 = value;
          break;
      case 9:
          LED_10 = value;
          break;
      case 10:
          LED_11 = value;
          break;
      case 11:
          LED_12 = value;
          break;
      case 12:
          LED_13 = value;
          break;
      case 13:
          LED_14 = value;
          break; 
      case 14:
          LED_15 = value;
          break;
      case 15:
          LED_16 = value;
          break;
      case 16:
          LED_17 = value;
          break;
      case 17:
          LED_18 = value;
          break;
      case 18:
          LED_19 = value;
          break;
      case 19:
          LED_20 = value;
          break;
		}
	
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
