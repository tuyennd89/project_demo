#include <stdint.h>
#include "main.h"

#ifndef _MODBUSDEVICE
#define _MODBUSDEVICE

#include "modbusRegBank.h"
#include "modbus.h"


void modbusDevice(void);
void modbusDevice_setId(unsigned char id);
unsigned char modbusDevice_getId(void);


#endif  /*_MODBUSDEVICE*/
