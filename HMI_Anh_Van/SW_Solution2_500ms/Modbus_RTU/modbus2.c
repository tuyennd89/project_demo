#include "modbusSlave.h"
#include "modbus2.h"
#include "modbusDevice.h"


unsigned char *_msg2,_len2;

unsigned int _baud2, _crc2, _frameDelay2;
uint16_t *_data2;


unsigned char Modbus2_RX_Buff[BUFFER_SIZE]={'\0'};
unsigned char Modbus2_TX_Buff[50]={'\0'};
uint8_t Modbus2_Index=0;
boolean Modbus2_Received;

/*
Set the Serial Baud rate.
Reconfigure the UART for 8 data bits, no parity, and 1 stop bit.
and flush the serial port.
*/
void Modbus2_Init(USART_TypeDef* USARTx, uint32_t  BaudRate)
{
    USART2_Init(BaudRate,ENABLE);
    modbusRegBank();
    MODBUS2_TX = 0;
}


/*
  Checks the UART for query data
*/
void Modbus2_CheckSerial(void)
{
    if(Modbus2_Received == true)
    {
        Modbus2_Received=false;
        _len2=Modbus2_Index;
        Modbus2_Index=0;
    }
}

/*
Copies the contents of the UART to a buffer
*/
void Modbus2_SerialRx(void)
{
    unsigned char i;

    //allocate memory for the incoming query message
    _msg2 = (unsigned char*) malloc(_len2);
        //copy the query byte for byte to the new buffer
    for (i=0 ; i < _len2 ; i++)
        _msg2[i] = Modbus2_RX_Buff[i];
}



void Modbus2_Run(void)
{

//  unsigned char deviceId;
    unsigned char funcType;
    uint16_t field1;
    uint16_t field2;
    uint16_t field3;
    
    uint8_t j=0;
    //int i;
    
    //initialize mesasge length
    _len2 = 0;

    //check for data in the recieve buffer
    Modbus2_CheckSerial();

    //if there is nothing in the recieve buffer, bail.
    if(_len2 == 0)
    {
        return;
    }
    else
    {
        //RS485_PutNum(_len2);
    }

    //retrieve the query message from the serial uart
    Modbus2_SerialRx();
    
    //if the message id is not 255, and
    // and device id does not match bail
    if( (_msg2[0] != 0xFF) && 
        (_msg2[0] != modbusDevice_getId()) )
    {
        return;
    }
    //calculate the checksum of the query message minus the checksum it came with.
    _crc2 = Modbus_CalcCrc(_msg2,_len2);

    //if the checksum does not match, ignore the message
    if ( _crc2 != ((_msg2[_len2 - 2] << 8) + _msg2[_len2 - 1]))
        return;
    //copy the function type from the incoming query
    funcType = _msg2[1];

    //copy field 1 from the incoming query
    field1    = (_msg2[2] << 8) | _msg2[3];

    //copy field 2 from the incoming query
    field2  = (_msg2[4] << 8) | _msg2[5];
    
  _data2 = (uint16_t*)malloc(field2);
    
  for(j=0;j<field2;j++)
  {
      _data2[j]=(_msg2[(j*2)+7] << 8) | _msg2[(j*2)+8];
  }
    
    //Data in case WRITE_MULTIPLE_REGISTERS
    field3  = (_msg2[7] << 8) | _msg2[8];
    
    //free the allocated memory for the query message
    free(_msg2);
    //reset the message length;
    _len2 = 0;

    //generate query response based on function type
    switch(funcType)
    {
    case READ_DI:
        _len2 = Modbus_GetDigitalStatus(funcType, field1, field2,Modbus2_TX_Buff);
        //RS485_Puts("READ_DI:\n");
        break;
    case READ_DO:
        _len2 = Modbus_GetDigitalStatus(funcType, field1, field2,Modbus2_TX_Buff);
        //RS485_Puts("READ_AO:\n");
        break;
    case READ_AI:
        _len2 = Modbus_GetAnalogStatus(funcType, field1, field2,Modbus2_TX_Buff);
        //RS485_Puts("READ_AI:\n");
        break;
    case READ_AO:
        _len2 = Modbus_GetAnalogStatus(funcType, field1, field2,Modbus2_TX_Buff);
        break;
    case WRITE_DO:
        _len2 = Modbus_SetStatus(funcType, field1, field2,Modbus2_TX_Buff);
        break;
    case WRITE_AO:
        _len2 = Modbus_SetStatus(funcType, field1, field2,Modbus2_TX_Buff);
        break;
    case WRITE_MULTIPLE_COILS:
        _len2 = Modbus_SetStatus(funcType, field1, field2,Modbus2_TX_Buff);
        break;
    case WRITE_MULTIPLE_REGISTERS:
        _len2 = Modbus_SetNStatus(funcType, field1, _data2,field2,Modbus2_TX_Buff);
        //Debug_Display("WRITE_MULTIPLE_REGISTERS\n");
        break;
    default:
        //return;
        break;
    }
    free(_data2);

    //if a reply was generated
    if(_len2)
    {
        int i;
        MODBUS2_TX = 1;
        //send the reply to the serial UART
        //Senguino doesn't support a bulk serial write command....
        for(i = 0 ; i < _len2 ; i++)
            USART2_PutChar(Modbus2_TX_Buff[i]);
        //free the allocated memory for the reply message
        delay_ms(10);
        Clear_Modbus2_RX_Buff();
        Clear_Modbus2_TX_Buff();
        //reset the message length
        _len2 = 0;
        Modbus2_Index=0;
        MODBUS2_TX = 0;
    }
}






void USART2_IRQHandler(void)
{
    if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
    {
        Modbus2_RX_Buff[Modbus2_Index++]=USART_ReceiveData(USART2);
    }
    if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
    {
        USART2->SR;
        USART2->DR;
        Modbus2_Received=true;
    }
}
        

void Clear_Modbus2_RX_Buff(void)
{
    uint16_t i=0;
      for(i=0;i<BUFFER_SIZE;i++)
         Modbus2_RX_Buff[i]='\0';
}    

void Clear_Modbus2_TX_Buff(void)
{
    uint16_t i=0;
      for(i=0;i<50;i++)
         Modbus2_TX_Buff[i]='\0';
}


