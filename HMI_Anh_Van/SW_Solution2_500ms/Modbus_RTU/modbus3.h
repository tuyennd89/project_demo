#include <stdint.h>
#include "main.h"

#ifndef MODBUSSLAVE3
#define MODBUSSLAVE3

#include "modbus.h"
#include "modbusDevice.h"


#define bitSet(x,n)                x|=(1<<n)
#define bitClear(x,n)              x&=(~(1<<n))

       
void Modbus3_Init(USART_TypeDef* USARTx, uint32_t  BaudRate);  
uint32_t Modbus3_GetBaudrate(void);

void Modbus3_CheckSerial(void);   
void Modbus3_SerialRx(void);
void Modbus3_Run(void);
void Clear_Modbus3_RX_Buff(void);
void Clear_Modbus3_TX_Buff(void);


#endif /*MODBUSSLAVE1*/
