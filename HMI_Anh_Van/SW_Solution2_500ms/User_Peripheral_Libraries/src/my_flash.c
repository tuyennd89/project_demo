#include "my_flash.h"
#include "string.h"

volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;
uint32_t Address=0x00;

void Write_Flash(uint32_t Start_Addr, char *DATA)
{
	uint8_t n=0;
	/* Unlock the Flash Bank1 Program Erase controller */
  FLASH_UnlockBank1();
	/* Clear All pending flags */
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
	do
		FLASHStatus = FLASH_ErasePage(FLASH_START_ADDRESS);
	while(FLASHStatus!=FLASH_COMPLETE);
	Address=FLASH_START_ADDRESS;
  while((Address < FLASH_END_ADDRESS) && (FLASHStatus == FLASH_COMPLETE)&&n<strlen(DATA))
  {
    FLASHStatus = FLASH_ProgramWord(Address, DATA[n++]);
    Address = Address + 4;
  }
}

uint32_t Read_Flash(uint32_t Addr)
{
	uint32_t temp;
	temp = (*(__IO uint32_t*) Addr);
	return temp;
}
