#include "my_exti.h"

uint8_t exti_flag = 0;
uint8_t exti_flag1 = 0;

void My_Exti_Config(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	
	
	/* Enable GPIOA clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  
  /* Configure PA.00 pin as input floating */
  //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);


  /* Connect EXTI0 Line to PA.00 pin */
  //GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource11 | GPIO_PinSource12 | GPIO_PinSource14 | GPIO_PinSource15);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource12 | GPIO_PinSource15);

  /* Configure EXTI0 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line12 | EXTI_Line15;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
}


/**
  * @brief  This function handles External lines 15 to 10 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI15_10_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line12) != RESET)
   {
		Debug_Display("Interrupn Pin 12");
		exti_flag = 1;
    EXTI_ClearITPendingBit(EXTI_Line12);
   }
	
	if(EXTI_GetITStatus(EXTI_Line15) != RESET)
   {
		Debug_Display("Interrupn Pin 15");
		exti_flag1 = 1;
    EXTI_ClearITPendingBit(EXTI_Line15);
   }
}






