
#include <stdint.h>
#include "main.h"

#ifndef _MODBUSREGBANK
#define _MODBUSREGBANK

#include <modbus.h>
//#include <Wprogram.h>


typedef struct modbusDigReg
{
	unsigned int address;
	unsigned char value;

	struct modbusDigReg *next;
}modbusDigReg_TypeDef;

typedef struct modbusAnaReg
{
	unsigned int address;
	unsigned int value;

	struct modbusAnaReg *next;
}modbusAnaReg_TypeDef;



void modbusRegBank(void);
void modbusRegBank_add(unsigned int Addr);
unsigned int modbusRegBank_get(unsigned int Addr);
void modbusRegBank_set(unsigned int Addr,unsigned int Data);
void * modbusRegBank_search(unsigned int Addr);
void modbusRegBank_assigned_AnaRegAddr(uint16_t Addr, modbusAnaReg_TypeDef *Val);
void modbusRegBank_assigned_DigRegAddr(uint16_t Addr, modbusDigReg_TypeDef *Val);

#endif /*_MODBUSREGBANK*/
