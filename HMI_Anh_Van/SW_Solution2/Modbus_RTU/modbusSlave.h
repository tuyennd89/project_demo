#include <stdint.h>
#include "main.h"

#ifndef MODBUSSLAVE
#define MODBUSSLAVE

#include "modbus.h"
#include "modbusDevice.h"


#define bitSet(x,n)				x|=(1<<n)
#define bitClear(x,n)			x&=(~(1<<n))

                
                
void Modbus_Init(USART_TypeDef* USARTx, uint32_t  BaudRate, FunctionalState NewState);  
uint32_t Modbus_GetBaudrate(void);
uint16_t Modbus_CalcCrc(unsigned char *msg_recv, uint8_t length);
void Modbus_CheckSerial(void);   
void Modbus_SerialRx(void);
uint8_t Modbus_GetDigitalStatus(unsigned char funcType, uint16_t startreg, uint16_t numregs,unsigned char *tx_buff);          
uint8_t Modbus_GetAnalogStatus(unsigned char funcType, uint16_t startreg, uint16_t numregs,unsigned char *tx_buff);
uint8_t Modbus_SetStatus(unsigned char funcType, uint16_t reg, uint16_t val,unsigned char *tx_buff);
uint8_t Modbus_SetNStatus(unsigned char funcType, uint16_t reg, uint16_t *val, uint16_t num_register,unsigned char *tx_buff);
void Modbus_Run(void);
void Clear_Modbus_RX_Buff(void);




#endif
