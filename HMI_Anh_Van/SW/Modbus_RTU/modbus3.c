#include "modbusSlave.h"
#include "modbus3.h"
#include "modbusDevice.h"


unsigned char *_msg3,_len3;

unsigned int _baud3, _crc3, _frameDelay3;
uint16_t *_data3;


unsigned char Modbus3_RX_Buff[BUFFER_SIZE]={'\0'};
unsigned char Modbus3_TX_Buff[50]={'\0'};
uint8_t Modbus3_Index=0;
boolean Modbus3_Received;

/*
Set the Serial Baud rate.
Reconfigure the UART for 8 data bits, no parity, and 1 stop bit.
and flush the serial port.
*/
void Modbus3_Init(USART_TypeDef* USARTx, uint32_t  BaudRate)
{
    USART3_Init(BaudRate,ENABLE);
    modbusRegBank();
    MODBUS3_TX = 0;
}


/*
  Checks the UART for query data
*/
void Modbus3_CheckSerial(void)
{
    if(Modbus3_Received == true)
    {
        Modbus3_Received=false;
        _len3=Modbus3_Index;
        Modbus3_Index=0;
    }
}

/*
Copies the contents of the UART to a buffer
*/
void Modbus3_SerialRx(void)
{
    unsigned char i;

    //allocate memory for the incoming query message
    _msg3 = (unsigned char*) malloc(_len3);
        //copy the query byte for byte to the new buffer
        for (i=0 ; i < _len3 ; i++)
            _msg3[i] = Modbus3_RX_Buff[i];
}



void Modbus3_Run(void)
{

//    unsigned char deviceId;
    unsigned char funcType;
    uint16_t field1;
    uint16_t field2;
    uint16_t field3;
    
    uint8_t j=0;
    //int i;
    
    //initialize mesasge length
    _len3 = 0;

    //check for data in the recieve buffer
    Modbus3_CheckSerial();

    //if there is nothing in the recieve buffer, bail.
    if(_len3 == 0)
    {
        return;
    }
    else
    {
        //RS485_PutNum(_len3);
    }

    //retrieve the query message from the serial uart
    Modbus3_SerialRx();
    
    //if the message id is not 255, and
    // and device id does not match bail
    if( (_msg3[0] != 0xFF) && 
        (_msg3[0] != modbusDevice_getId()) )
    {
        return;
    }
    //calculate the checksum of the query message minus the checksum it came with.
    _crc3 = Modbus_CalcCrc(_msg3,_len3);

    //if the checksum does not match, ignore the message
    if ( _crc3 != ((_msg3[_len3 - 2] << 8) + _msg3[_len3 - 1]))
        return;
    
    //copy the function type from the incoming query
    funcType = _msg3[1];

    //copy field 1 from the incoming query
    field1    = (_msg3[2] << 8) | _msg3[3];

    //copy field 2 from the incoming query
    field2  = (_msg3[4] << 8) | _msg3[5];
    
  _data3 = (uint16_t*)malloc(field2);
    
  for(j=0;j<field2;j++)
  {
      _data3[j]=(_msg3[(j*2)+7] << 8) | _msg3[(j*2)+8];
  }
    
    //Data in case WRITE_MULTIPLE_REGISTERS
    field3  = (_msg3[7] << 8) | _msg3[8];
    
    //free the allocated memory for the query message
    free(_msg3);
    //reset the message length;
    _len3= 0;

    //generate query response based on function type
    switch(funcType)
    {
    case READ_DI:
        _len3 = Modbus_GetDigitalStatus(funcType, field1, field2,Modbus3_TX_Buff);
        //RS485_Puts("READ_DI:\n");
        break;
    case READ_DO:
        _len3 = Modbus_GetDigitalStatus(funcType, field1, field2,Modbus3_TX_Buff);
        //RS485_Puts("READ_AO:\n");
        break;
    case READ_AI:
        _len3 = Modbus_GetAnalogStatus(funcType, field1, field2,Modbus3_TX_Buff);
        //RS485_Puts("READ_AI:\n");
        break;
    case READ_AO:
        _len3 = Modbus_GetAnalogStatus(funcType, field1, field2,Modbus3_TX_Buff);
        break;
    case WRITE_DO:
        _len3 = Modbus_SetStatus(funcType, field1, field2,Modbus3_TX_Buff);
        break;
    case WRITE_AO:
        _len3 = Modbus_SetStatus(funcType, field1, field2,Modbus3_TX_Buff);
        break;
    case WRITE_MULTIPLE_COILS:
        _len3 = Modbus_SetStatus(funcType, field1, field2,Modbus3_TX_Buff);
        break;
    case WRITE_MULTIPLE_REGISTERS:
        _len3 = Modbus_SetNStatus(funcType, field1, _data3,field2,Modbus3_TX_Buff);
        //Debug_Display("WRITE_MULTIPLE_REGISTERS\n");
        break;
    default:
        //return;
        break;
    }
    free(_data3);

    //if a reply was generated
    if(_len3)
    {
        int i;
        MODBUS3_TX = 1;
        //send the reply to the serial UART
        //Senguino doesn't support a bulk serial write command....
        for(i = 0 ; i < _len3 ; i++)
            USART3_PutChar(Modbus3_TX_Buff[i]);
        //free the allocated memory for the reply message
        delay_ms(10);
        Clear_Modbus3_RX_Buff();
        Clear_Modbus3_TX_Buff();
        //reset the message length
        _len3 = 0;
        Modbus3_Index=0;
        MODBUS3_TX = 0;
    }
}






void USART3_IRQHandler(void)
{
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
    {
        Modbus3_RX_Buff[Modbus3_Index++]=USART_ReceiveData(USART3);
    }
    if(USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)
    {
        USART3->SR;
        USART3->DR;
        Modbus3_Received=true;
    }
}
        

void Clear_Modbus3_RX_Buff(void)
{
    uint16_t i=0;
      for(i=0;i<BUFFER_SIZE;i++)
         Modbus3_RX_Buff[i]='\0';
}    

void Clear_Modbus3_TX_Buff(void)
{
    uint16_t i=0;
      for(i=0;i<50;i++)
         Modbus3_TX_Buff[i]='\0';
}


