#include <stdint.h>
#include "main.h"

#ifndef MODBUSSLAVE2
#define MODBUSSLAVE2

#include "modbus.h"
#include "modbusDevice.h"


#define bitSet(x,n)                x|=(1<<n)
#define bitClear(x,n)              x&=(~(1<<n))

       
void Modbus2_Init(USART_TypeDef* USARTx, uint32_t  BaudRate);  
uint32_t Modbus2_GetBaudrate(void);

void Modbus2_CheckSerial(void);   
void Modbus2_SerialRx(void);
void Modbus2_Run(void);
void Clear_Modbus2_RX_Buff(void);
void Clear_Modbus2_TX_Buff(void);


#endif /*MODBUSSLAVE1*/
