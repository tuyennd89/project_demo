#include "modbusRegBank.h"
#include <stdlib.h>


modbusDigReg_TypeDef    *_digRegs,
                        *_lastDigReg;
                            
modbusAnaReg_TypeDef    *_anaRegs,
                        *_lastAnaReg;

                
void modbusRegBank(void)
{
    _digRegs        = 0;
    _lastDigReg     = 0;
    _anaRegs        = 0;
    _lastAnaReg     = 0;
}

void modbusRegBank_assigned_DigRegAddr(uint16_t Addr, modbusDigReg_TypeDef *Val)
{
    Val->address = Addr;
	Val->value = 0;
    Val->next  = 0;
		if(_digRegs == 0)
    {
				_digRegs = Val;
        _lastDigReg = _digRegs;
    }
    else
    {
        _lastDigReg->next = Val;
        _lastDigReg = Val;
    }
}
             
void modbusRegBank_assigned_AnaRegAddr(uint16_t Addr, modbusAnaReg_TypeDef *Val)
{
		Val->address = Addr;
		Val->value = 0;
    Val->next  = 0;
		if(_anaRegs == 0)
    {
				_anaRegs = Val;
        _lastAnaReg = _anaRegs;
    }
    else
    {
        _lastAnaReg->next = Val;
        _lastAnaReg = Val;
    }
}

void modbusRegBank_add(unsigned int Addr)
{
    if(Addr<20000)
    {
        modbusDigReg_TypeDef *temp;

        temp = (modbusDigReg_TypeDef *) malloc(sizeof(modbusDigReg_TypeDef));
        temp->address = Addr;
        temp->value        = 0;
        temp->next        = 0;

        if(_digRegs == 0)
        {
            _digRegs = temp;
            _lastDigReg = _digRegs;
        }
        else
        {
            //Assign the last register's next pointer to temp;
            _lastDigReg->next = temp;
            //then make temp the last register in the list.
            _lastDigReg = temp;
        }
    }    
    else
    {
        modbusAnaReg_TypeDef *temp;

        temp = (modbusAnaReg_TypeDef *) malloc(sizeof(modbusAnaReg_TypeDef));
        temp->address = Addr;
        temp->value = 0;
        temp->next = 0;

        if(_anaRegs == 0)
        {
            _anaRegs = temp;
            _lastAnaReg = _anaRegs;
        }
        else
        {
            _lastAnaReg->next = temp;
            _lastAnaReg = temp;
        }
    }
}

unsigned int modbusRegBank_get(unsigned int Addr)
{
    if(Addr < 20000)
    {
        modbusDigReg_TypeDef * regPtr;
        regPtr = (modbusDigReg_TypeDef *) modbusRegBank_search(Addr);
        if(regPtr)
            return(regPtr->value);
        else
            return(0);    
    }
    else
    {
        modbusAnaReg_TypeDef * regPtr;
        regPtr = (modbusAnaReg_TypeDef *) modbusRegBank_search(Addr);
        if(regPtr)
            return(regPtr->value);
        else
            return(0);    
    }
}

void modbusRegBank_set(unsigned int Addr,unsigned int Data)
{
    //for digital data
    if(Addr < 20000)
    {
        modbusDigReg_TypeDef * regPtr;
        //search for the register address
        regPtr = (modbusDigReg_TypeDef *) modbusRegBank_search(Addr);
        //if a pointer was returned the set the register value to true if value is non zero
        if(regPtr)
				{
            if(Data)
						{
							regPtr->value = 0xFF;
            }
            else
						{
							regPtr->value = 0x00;
            }
				}						
    }
    else
    {
        modbusAnaReg_TypeDef * regPtr;
        //search for the register address
        regPtr = (modbusAnaReg_TypeDef *) modbusRegBank_search(Addr);
        //if found then assign the register value to the new value.
        if(regPtr)
            regPtr->value = Data;
    }
}

void * modbusRegBank_search(unsigned int Addr)
{
    //if the requested address is 0-19999 
    //use a digital register pointer assigned to the first digital register
    //else use a analog register pointer assigned the first analog register

    if(Addr < 20000)
    {
        modbusDigReg_TypeDef *regPtr = _digRegs;

        //if there is no register configured, bail
        if(regPtr == 0)
            return(0);

        //scan through the linked list until the end of the list or the register is found.
        //return the pointer.
        do
        {
            if(regPtr->address == Addr)
                return(regPtr);
            regPtr = regPtr->next;
        }
        while(regPtr);
    }
    else
    {
        modbusAnaReg_TypeDef *regPtr = _anaRegs;

        //if there is no register configured, bail
        if(regPtr == 0)
            return(0);

        //scan through the linked list until the end of the list or the register is found.
        //return the pointer.
        do
        {
            if(regPtr->address == Addr)
                return(regPtr);
            regPtr = regPtr->next;
        }
        while(regPtr);
    }
    return(0);
}


