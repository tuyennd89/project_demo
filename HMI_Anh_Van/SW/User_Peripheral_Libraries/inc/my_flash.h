#ifndef __MY_FLASH_H
#define __MY_FLASH_H

#include "stm32f10x.h"

#define FLASH_START_ADDRESS  ((uint32_t)0x801F800)
#define FLASH_END_ADDRESS  ((uint32_t)0x801FC00)

#define Addr1	((uint32_t)0x801F800)
#define Addr2	((uint32_t)0x801F900)

void Write_Flash(uint32_t Start_Addr, char *DATA);
uint32_t Read_Flash(uint32_t Addr);

#endif

