#include "my_uart.h"

/***********************************USART1********************************************/
void USART1_PinConfigure(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = USART1_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART1_GPIO, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = USART1_TxPin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(USART1_GPIO, &GPIO_InitStructure);
}

void USART1_Init(uint32_t BaudRate, FunctionalState NewState)
{
    USART_InitTypeDef    USART_InitStructure;
    
    /*Pin USART1 Configure*/
    USART1_PinConfigure();
    /*Enable clock usart1*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_AFIO,ENABLE);
    
    USART_InitStructure.USART_BaudRate=BaudRate;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    
    USART_Init(USART1,&USART_InitStructure);
    
    if(NewState==ENABLE)
    {
        USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
        USART_ITConfig(USART1,USART_IT_IDLE,ENABLE);
    }
    
    USART_Cmd(USART1,ENABLE);
}

void USART1_PutChar(unsigned char C)
{
    USART_PutChar(USART1,C);
}


void USART1_PutString(unsigned char *S)
{
    USART_PutString(USART1,S);
}


void USART1_PutNumber(uint32_t num)
{
	unsigned char UBuffer[10];
	sprintf((char*)UBuffer,"%u\n",(uint32_t)num);
	USART1_PutString(UBuffer);
}
/***********************************END USART1********************************************/


/***********************************USART2********************************************/
void USART2_PinConfigure(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = USART2_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART2_GPIO, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = USART2_TxPin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(USART2_GPIO, &GPIO_InitStructure);
}

void USART2_Init(uint32_t BaudRate, FunctionalState NewState)
{
    USART_InitTypeDef    USART_InitStructure;
    
    /*Pin USART1 Configure*/
    USART2_PinConfigure();
    /*Enable clock usart1*/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
    
    USART_InitStructure.USART_BaudRate=BaudRate;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    
    USART_Init(USART2,&USART_InitStructure);
    
    if(NewState==ENABLE)
    {
        USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
        USART_ITConfig(USART2,USART_IT_IDLE,ENABLE);
    }
    
    USART_Cmd(USART2,ENABLE);
}

void USART2_PutChar(unsigned char C)
{
    USART_PutChar(USART2,C);
}


void USART2_PutString(unsigned char *S)
{
    USART_PutString(USART2,S);
}


void USART2_PutNumber(uint32_t num)
{
	unsigned char UBuffer[10];
	sprintf((char*)UBuffer,"%u\n",(uint32_t)num);
	USART2_PutString(UBuffer);
}
/***********************************END USART2********************************************/

/***********************************USART3********************************************/
void USART3_PinConfigure(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = USART3_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART3_GPIO, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = USART3_TxPin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(USART3_GPIO, &GPIO_InitStructure);
}

void USART3_Init(uint32_t BaudRate, FunctionalState NewState)
{
    USART_InitTypeDef    USART_InitStructure;
    
    /*Pin USART3 Configure*/
    USART3_PinConfigure();
    /*Enable clock usart3*/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
    
    USART_InitStructure.USART_BaudRate=BaudRate;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    
    USART_Init(USART3,&USART_InitStructure);
    
    if(NewState==ENABLE)
    {
        USART_ITConfig(USART3,USART_IT_RXNE,ENABLE);
        USART_ITConfig(USART3,USART_IT_IDLE,ENABLE);
    }
   
    USART_Cmd(USART3,ENABLE);
}

void USART3_PutChar(unsigned char C)
{
    USART_PutChar(USART3,C);
}


void USART3_PutString(unsigned char *S)
{
    USART_PutString(USART3,S);
}
/***********************************END USART3********************************************/


/***********************************USART4********************************************/
void UART4_PinConfigure(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = USART4_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART4_GPIO, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = USART4_TxPin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(USART4_GPIO, &GPIO_InitStructure);
}

void UART4_Init(uint32_t BaudRate, FunctionalState NewState)
{
    USART_InitTypeDef    USART_InitStructure;
    
    /*Pin USART4 Configure*/
    UART4_PinConfigure();
    /*Enable clock usart4*/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4,ENABLE);
    
    USART_InitStructure.USART_BaudRate=BaudRate;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    
    USART_Init(UART4,&USART_InitStructure);
    
    if(NewState==ENABLE)
    {
        USART_ITConfig(UART4,USART_IT_RXNE,ENABLE);
        USART_ITConfig(UART4,USART_IT_IDLE,ENABLE);
    }
    
    USART_Cmd(UART4,ENABLE);
}

void USART4_PutChar(unsigned char C)
{
    USART_PutChar(UART4,C);
}


void USART4_PutString(unsigned char *S)
{
    USART_PutString(UART4,S);
}

/***********************************END USART4********************************************/

/***********************************USART5********************************************/
void UART5_PinConfigure(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = USART5_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART5_RX_GPIO, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = USART5_TxPin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(USART5_TX_GPIO, &GPIO_InitStructure);
}

void UART5_Init(uint32_t BaudRate, FunctionalState NewState)
{
    USART_InitTypeDef    USART_InitStructure;
    
    /*Pin USART4 Configure*/
    UART5_PinConfigure();
    /*Enable clock usart4*/
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5,ENABLE);
    
    USART_InitStructure.USART_BaudRate=BaudRate;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    
    USART_Init(UART5,&USART_InitStructure);
    
    if(NewState==ENABLE)
    {
        USART_ITConfig(UART5,USART_IT_RXNE,ENABLE);
        USART_ITConfig(UART5,USART_IT_IDLE,ENABLE);
    }
    
    USART_Cmd(UART5,ENABLE);
}

void USART5_PutChar(unsigned char C)
{
    USART_PutChar(UART5,C);
}


void USART5_PutString(unsigned char *S)
{
    USART_PutString(UART5,S);
}

/***********************************END USART5********************************************/


void USART_PutChar(USART_TypeDef* USARTx, unsigned char C)
{
		while (!(USARTx->SR & 0x40))
		{;}
    USARTx->DR=C;
    //while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
}

void USART_PutString(USART_TypeDef* USARTx, unsigned char *S)
{
    uint16_t i;
    uint16_t len = strlen((char*)S);
    for(i=0;i<len;i++)
        {
            USART_PutChar(USARTx,S[i]);
        }
}


