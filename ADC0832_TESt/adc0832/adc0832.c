#include "adc0832.h"
void ADC_Delay(unsigned char i)
{
	while(i--);
}

void ADC_Clk()
{
		ADCLK=1;
    ADC_Delay(ADC_TIME);
    ADCLK=0;
		ADC_Delay(ADC_TIME);
}
void ADC0832_Start()
{
	  ADDI=1;
    ADC_Delay(ADC_TIME);
    ADCS=0;
    ADC_Delay(ADC_TIME);
    ADC_Clk();
}
void ADC0832_Channel(unsigned char channel)
{
	channel+=2;
    ADDI=channel&0x1;
    ADC_Clk();
    ADCLK=1;
    ADDI=(channel>>1)&0x1;
    ADC_Delay(ADC_TIME);
	
    ADCLK=0;
    ADDI=1;
	ADC_Delay(ADC_TIME);
}
unsigned int ADC0832_Read(unsigned char channel)    
{
    unsigned int Data=0;
	unsigned char i,temp;
	ADC0832_Start();
	ADC0832_Channel(channel);
	//Lay byte cao
    for(i=0;i<8;i++)
    {
    temp=ADDO;
		Data|=(temp<<(7-i));
    ADC_Clk();
    }
	Data<<=8;
	//Lay byte thap
    for(i=0;i<8;i++)
    {
    temp=ADDO;
		Data|=(temp<<i);
    ADC_Clk();
    }
    ADCS=1;
    ADCLK=0;
    ADDO=1;
    return(Data); 
}
