/**
  ******************************************************************************
  * Ten Tep		:		  adc0832.h
  * Tac Gia		:	  	Nguyen Quy Nhat
  * Cong Ty		:			MinhHaGroup
  *	Website 	:			BanLinhKien.Vn
  * Phien Ban	:			V1.0.0
  * Ngay			:    	31-07-2012
  * Tom Tat   :     Khai bao cac ham dieu khien ADC0832
  *           
  *
  ******************************************************************************
  * Chu Y		:				Can dinh nghia cac chan 
	*									ADCLK,ADDI,ADDO,ADCS vao ham main.h
  *							 
  ******************************************************************************
  */
#ifndef __ADC_0832_H
#define __ADC_0832_H
#include "main.h"
#define ADC_TIME	2
void ADC_Delay(unsigned char i);
void ADC_Clk_Start();
void ADC_Clk();
void ADC0832_Start();
void ADC0832_Channel(unsigned char chanel);
unsigned int ADC0832_Read(unsigned char channel) ;   
#endif
