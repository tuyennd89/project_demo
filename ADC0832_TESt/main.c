#include "main.h"
#include "var.h" 
unsigned int Adc_Data;
int count=0;

#define DIV     50
/*********************************EXTERN VARIABLE***********************************/ 
int main(void)
{
/***********************************BIEN CUC BO*************************************/
  
	char ubuff[16];
	
	UART_Init();
	ADC0832_Read(0);
	TMOD&=0xF0;
	TMOD|=0x01;				//Timer 0, 16 bit
	TH0=0x3C;
	TL0=0xB0;
	TF0=0;
	TR0=1;
	ET0=1;          /* 1=Enable Timer 0 interrupt */           
	EA=1;
	
  while(1)
  {
			sprintf(ubuff,"adc=%d\n",Adc_Data);
			UART_Puts(ubuff);
			left_to_right();
			blink_led(3);
			sang_dan_left_to_right();
			blink_led(3);
  }
	return 1;
}


void blink_led(int num)
{
	int i=0;
	for(i=0;i<num;i++)
	{
		P2=0xff;
		P0=0xff;
		delay_ms(Adc_Data/DIV);
		P2=0x00;
		P0=0x00;
		delay_ms(Adc_Data/DIV);
	}
}

void left_to_right(void)
{
	int i=0;
	char x=0x01;
	for(i=0;i<8;i++)
	{
		P0=x;
		delay_ms(Adc_Data/DIV);
		x<<=1;
	}
	
	P0=0x00;
	P2=0x80;
	delay_ms(Adc_Data/DIV);
	P2=0x40;
	delay_ms(Adc_Data/DIV);
	P2=0x00;
}

void sang_dan_left_to_right(void)
{
	int i=0;
	char x=0x01;
	P0=0x00;
	P2=0x00;
	for(i=0;i<8;i++)
	{
		P0=x;
		delay_ms(Adc_Data/DIV);
		x<<=1;
		x|=0x01;
	}
	P2=0x80;
	delay_ms(Adc_Data/DIV);
	P2=0xC0;
	delay_ms(Adc_Data/DIV);
}
	

void ISR_TIMER0(void) interrupt 1
{
	count++;
	if(count>1)
	{
		count=0;
		Adc_Data=ADC0832_Read(0);
	}
}

