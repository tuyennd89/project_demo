/**
  ******************************************************************************
  * Ten Tep		:		  lcd_16x2.c
  * Tac Gia		:	  	Nguyen Quy Nhat
  * Cong Ty		:			MinhHaGroup
  *	Website 	:			BanLinhKien.Vn
  * Phien Ban	:			V1.0.0
  * Ngay			:    	31-07-2012
  * Tom Tat   :     Dinh nghia cac ham dieu khien LCD 16x2.
  *           
  *
  ******************************************************************************
  * Chu Y		:				Can dinh nghia cac chan LCD_RS,LCD_RW,LCD_EN,
	*									LCD_D4,LCD_D5,LCD_D6,LCD_D7 vao ham main.h
  *							 
  ******************************************************************************
  */
#include "lcd_16x2.h"
//Tao Xung
 void LCD_Enable(void)
{
 LCD_EN =1;
 delay_us(3);
 LCD_EN=0;
 delay_us(50); 
}
//Ham Gui 4 Bit Du Lieu Ra LCD
 void LCD_Send4Bit( unsigned char Data )
{
 LCD_D4= Data & 0x01;
 LCD_D5= (Data>>1)&1;
 LCD_D6= (Data>>2)&1;
 LCD_D7= (Data>>3)&1;
}
// Ham Gui 1 Lenh Cho LCD
 void LCD_SendCommand (unsigned char command )
{
LCD_Send4Bit  ( command >>4 );/* Gui 4 bit cao */
LCD_Enable () ;
LCD_Send4Bit  ( command  );		/* Gui 4 bit thap*/
LCD_Enable () ;
}
// Ham Khoi Tao LCD
 void LCD_Init ( void )
{
LCD_Send4Bit(0x00);
delay_ms(20);
LCD_RS=0;
LCD_RW=0;
LCD_Send4Bit(0x03);
LCD_Enable();
delay_ms(5);
LCD_Enable();
delay_us(100);
LCD_Enable();
LCD_Send4Bit(0x02);
LCD_Enable();
LCD_SendCommand( 0x28 );      // giao thuc 4 bit, hien thi 2 hang, ki tu 5x8
LCD_SendCommand( 0x0c); 	// cho phep hien thi man hinh
LCD_SendCommand( 0x06 );      // tang ID, khong dich khung hinh
LCD_SendCommand( Clear_LCD ); // xoa toan bo khung hinh
}
void LCD_Gotoxy(unsigned char x, unsigned char y)
{
  unsigned char address;
  if(!y)
  address = (Line_1+x);
  else
  address = (Line_2+x);
  delay_us(1000);
  LCD_SendCommand(address);
  delay_us(50);
}
// Ham Xoa Man Hinh LCD
void LCD_Clear()
{
  LCD_SendCommand( Clear_LCD );  
  delay_us(10);
}
// Ham Gui 1 Ki Tu Len LCD
 void LCD_PutChar ( unsigned char Data )
{
 LCD_RS=1;
 LCD_SendCommand( Data );
 LCD_RS=0 ;
}
void LCD_Puts (char *s)
{
   while (*s)
   {
      LCD_PutChar(*s);
      s++;
   }
}





