/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "stm32f0xx_hal.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "enc28j60.h"
#include "ethernet.h"
#include "ip.h"
#include "arp.h"
#include "icmp.h"
#include "tcp.h"
#include "http.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define ENC28J60_RST_Pin GPIO_PIN_13
#define ENC28J60_RST_GPIO_Port GPIOC
#define ENC28J60_CS_Pin GPIO_PIN_4
#define ENC28J60_CS_GPIO_Port GPIOA
#define LED_Pin GPIO_PIN_14
#define LED_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define ENC28J60_SCK_Pin GPIO_PIN_5
#define ENC28J60_SCK_GPIO_Port GPIOA

#define ENC28J60_MISO_Pin GPIO_PIN_6
#define ENC28J60_MISO_GPIO_Port GPIOA

#define ENC28J60_MOSI_Pin GPIO_PIN_7
#define ENC28J60_MOSI_GPIO_Port GPIOA

#define ENC28J60_SCK_H() ENC28J60_SCK_GPIO_Port->BSRR=(uint32_t)ENC28J60_SCK_Pin
#define ENC28J60_SCK_L() ENC28J60_SCK_GPIO_Port->BRR=(uint32_t)ENC28J60_SCK_Pin

#define ENC28J60_CS_H() ENC28J60_CS_GPIO_Port->BSRR=(uint32_t)ENC28J60_CS_Pin
#define ENC28J60_CS_L() ENC28J60_CS_GPIO_Port->BRR=(uint32_t)ENC28J60_CS_Pin

#define ENC28J60_MOSI_H() ENC28J60_MOSI_GPIO_Port->BSRR=(uint32_t)ENC28J60_MOSI_Pin
#define ENC28J60_MOSI_L() ENC28J60_MOSI_GPIO_Port->BRR=(uint32_t)ENC28J60_MOSI_Pin

#define ENC28J60_RST_H() ENC28J60_RST_GPIO_Port->BSRR=(uint32_t)ENC28J60_RST_Pin
#define ENC28J60_RST_L() ENC28J60_RST_GPIO_Port->BRR=(uint32_t)ENC28J60_RST_Pin

#define LED_H()	LED_GPIO_Port->BSRR=(uint32_t)LED_Pin
#define LED_L() LED_GPIO_Port->BRR=(uint32_t)LED_Pin



#define ETHERNET_VIEW
#define ARP_VIEW
#define IP_VIEW
#define TCP_VIEW
#define HTTP_VIEW




#define SPI_HW_USED
#define SPI_TIMEOUT				1000
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
