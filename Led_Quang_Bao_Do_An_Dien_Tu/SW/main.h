#ifndef __MAIN_H
#define	__MAIN_H
/************************INCLUDE FILE********************************/
#include "AT89X52.h"
/*++++++++++++STANDAR C++++++++++++++*/
#include "stdio.h"
#include "string.h"
#include <intrins.h>
/*+++++++++++++USER+++++++++++++++++*/
#include "delay/delay.h"
#include "adc0832/adc0832.h"
#include "uart/uart.h"
/**************************DEFINE********************************/ 
//+++++++++++++++++++++++++DEFINE PORT0+++++++++++++++++++++++++++
//+++++++++++++++++++++++++DEFINE PORT1+++++++++++++++++++++++++++
#define ADCLK P1_0
#define ADCS	P1_3
#define ADDI	P1_2
#define ADDO	P1_2

void blink_led(int num);
void left_to_right(void);
void sang_dan_left_to_right(void);

#endif
