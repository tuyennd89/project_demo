/**
  ******************************************************************************
  * Ten Tep		:		  lcd_16x2.h
  * Tac Gia		:	  	Nguyen Quy Nhat
  * Cong Ty		:			MinhHaGroup
  *	Website 	:			BanLinhKien.Vn
  * Phien Ban	:			V1.0.0
  * Ngay			:    	31-07-2012
  * Tom Tat   :     Khai bao cac ham dieu khien LCD 16x2.
  *									Dinh nghia mot so hang su dung cho LCD.
  *           
  *
  ******************************************************************************
  * Chu Y		:				Can dinh nghia cac chan LCD_RS,LCD_RW,LCD_EN,
	*									LCD_D4,LCD_D5,LCD_D6,LCD_D7 vao ham main.h
  *							 
  ******************************************************************************
  */
#ifndef __LCD_16X2_H
#define __LCD_16X2_H
#include "main.h"
// Dinh Nghia Cac Hang So
#define Line_1 0x80
#define Line_2 0xC0
#define Clear_LCD 0x01
 void LCD_Enable(void);	//Tao xung Enable LCD
 void LCD_Send4Bit(unsigned char  Data);//Ham Gui 4 Bit Du Lieu Ra LCD
 void LCD_SendCommand (unsigned char command );// Ham Gui 1 Lenh Cho LCD
 void LCD_Init ( void );// Ham Khoi Tao LCD
 void LCD_Gotoxy(unsigned char x, unsigned char y);// Ham Thiet Lap Vi Tri Con Tro
 void LCD_Clear();// Ham Xoa Man Hinh LCD
 void LCD_PutChar ( unsigned char c );// Ham Gui 1 Ki Tu Len LCD
 void LCD_Puts (unsigned char *s);// Ham Gui 1 Chuoi Ki Tu Len LCD
#endif




