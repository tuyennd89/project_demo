/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.0 Professional
Automatic Program Generator
� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 11/13/2017
Author  : NeVaDa
Company : 
Comments: 


Chip type               : ATmega16
Program type            : Application
AVR Core Clock frequency: 8.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 256
*****************************************************/

#include "main.h"

unsigned char index=0,count=0;
unsigned int Display_Value=0;
unsigned int adc_value=0;



unsigned int pulse=0,speed=0,speed_t=0;

unsigned char ma_led_7[10]={5,221,70,84,156,52,36,93,4,20};

#define ADC_VREF_TYPE 0x40

// Read the AD conversion result
unsigned int read_adc(unsigned char adc_input)
{
ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
// Delay needed for the stabilization of the ADC input voltage
delay_us(10);
// Start the AD conversion
ADCSRA|=0x40;
// Wait for the AD conversion to complete
while ((ADCSRA & 0x10)==0);
ADCSRA|=0x10;
return ADCW;
}

// Declare your global variables here

void main(void)
{
// Declare your local variables here

// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTA=0x00;
DDRA=0x00;

// Port B initialization
// Func7=In Func6=In Func5=In Func4=In Func3=Out Func2=Out Func1=Out Func0=Out 
// State7=T State6=T State5=T State4=T State3=0 State2=0 State1=0 State0=0 
PORTB=0x00;
DDRB=0x0F;

// Port C initialization
// Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=Out Func1=Out Func0=Out 
// State7=1 State6=1 State5=1 State4=1 State3=1 State2=1 State1=1 State0=1 
PORTC=0xFF;
DDRC=0xFF;

// Port D initialization
// Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=0 State6=0 State5=0 State4=T State3=T State2=T State1=P State0=P 
PORTD=0x03;
DDRD=0xE0;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: 7.813 kHz
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 8000.000 kHz
// Mode: Fast PWM top=ICR1
// OC1A output: Non-Inv.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x82;
TCCR1B=0x19;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: 7.813 kHz
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;                                
TCCR2=0x07;
TCNT2=0xD8;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: On
// INT0 Mode: Falling Edge
// INT1: Off
// INT2: Off
GICR|=0x40;
MCUCR=0x02;
MCUCSR=0x00;
GIFR=0x40;



// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x41;

// USART initialization
// USART disabled
UCSRB=0x00;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

// ADC initialization
// ADC Clock frequency: 1000.000 kHz
// ADC Voltage Reference: AVCC pin
// ADC Auto Trigger Source: ADC Stopped
ADMUX=ADC_VREF_TYPE & 0xff;
ADCSRA=0x83;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;

// Global enable interrupts
#asm("sei")

ICR1=4000;

EN_MOTOR=0;
while (1)
      {
      // Place your code here
        adc_value = read_adc(7); 
        if(adc_value<200)
          adc_value=200;
        if(adc_value>900)
          adc_value=900;
        OCR1A=adc_value*4; 
        if(count>100)
        {            
            Display_Value=speed_t/100;    
            speed_t=0;
            count=0;
        }
        
        if(BT1==0)
        {
            delay_ms(10);
            if(BT1==0)
            {    
                OCR1A=0;        
                EN_MOTOR = ~EN_MOTOR;
                delay_ms(1000);
                while(BT1==0);    
            }
        }
        
        if(BT2==0)
        {
            delay_ms(10);
            if(BT2==0)
            {    
                EN_MOTOR=0;
                delay_ms(1000);
                DIR = ~DIR;  
                EN_MOTOR=1;
                while(BT2==0);    
            }
        }       
      }
}




// Timer2 overflow interrupt service routine
interrupt [TIM2_OVF] void timer2_ovf_isr(void)
{
// Place your code here
    #asm("cli")
    TCCR2=0x00;             //Stop timer 2
    TCNT2=220;  
    speed=pulse;
    speed=(speed*217)/334; 
    speed=(speed*60)/41;
    speed_t+=speed;
    pulse=0;
    //Turn off all led 7
    LED1=LED2=LED3=LED4=0;
    index++;   
    count++;
    if(index==1)
    {
        LED_DATA =  ma_led_7[Display_Value/1000];     
        LED1=1;     
    }
    if(index==2)
    {
        LED_DATA =  ma_led_7[(Display_Value%1000)/100];
        LED2=1;     
    } 
    if(index==3)
    {
        LED_DATA =  ma_led_7[(Display_Value%100)/10];
        LED3=1;     
    }   
    if(index==4)
    {
        LED_DATA =  ma_led_7[(Display_Value%100)%10];
        LED4=1; 
        index=0;    
    }
    TCCR2=0x07;            //Re-start timer 2 
    #asm("sei")
}



// External Interrupt 0 service routine
interrupt [EXT_INT0] void ext_int0_isr(void)
{
// Place your code here
    #asm("cli")
    pulse++;  
    #asm("sei")
}