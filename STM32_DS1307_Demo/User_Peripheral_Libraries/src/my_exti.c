#include "my_exti.h"

uint8_t exti_flag = 0;

extern uint8_t id_main_menu;
extern uint8_t id_menu;
extern uint8_t id_setting;
extern uint8_t enable_setting_threshold;

extern uint8_t Temp_Threshold;
extern uint8_t Pressure_Threshold;

//Mang chua password nguoi dung nhap vao
extern char user_password[6];
extern uint8_t password_index;
extern uint8_t password_stt;

void My_Exti_Config(void)
{
    EXTI_InitTypeDef   EXTI_InitStructure;
   
    /* Connect EXTI11, EXTI12, EXTI15  Line to PA.00 pin */
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource11 | GPIO_PinSource12 | GPIO_PinSource15);

    /* Configure EXTI11, EXTI12, EXTI15 line */
    EXTI_InitStructure.EXTI_Line = EXTI_Line2 | EXTI_Line3 | EXTI_Line11 | EXTI_Line12 | EXTI_Line15;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource3);
    
    EXTI_InitStructure.EXTI_Line = EXTI_Line3;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOD,GPIO_PinSource2);
    
    EXTI_InitStructure.EXTI_Line = EXTI_Line2;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}


/**
  * @brief  This function handles External lines 15 to 10 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI15_10_IRQHandler(void)
{
		/*Nut Auto --> Exit*/
    if(EXTI_GetITStatus(EXTI_Line11) != RESET)
    {
        //Debug_Display("Interrupn Pin 11");
				exti_flag = 1;
        id_menu=0;
        id_setting = 0;
        enable_setting_threshold = 0;
        password_stt=0;    
				password_index=0;
        EXTI_ClearITPendingBit(EXTI_Line11);
    }
    
		/*Nut Menu  -->Scroll*/ 
    if(EXTI_GetITStatus(EXTI_Line12) != RESET)
    {
        //Debug_Display("Interrupn Pin 12");
				exti_flag = 1;
        if(id_menu==0)
        {
            id_main_menu++;
            if(id_main_menu>3)
            {
                id_main_menu=0;
            }   
        }
        
				/*id_menu==1 hoac id_menu==4, la 2 menu co protect boi password*/	
        if(id_menu==1)
        {
            id_setting++;
            if(id_setting>1)
            {
                id_setting=0;
            }
        }
        EXTI_ClearITPendingBit(EXTI_Line12);
    }
    
		/*Nut LEFT -->Down*/
    if(EXTI_GetITStatus(EXTI_Line15) != RESET)
    {
        //exti_flag = 1;
				/*Truong hop cai dat threshold: id_menu=1*/
				if(id_menu==1)
				{
					if(password_stt==0)
					{
						user_password[password_index]--;
						if(user_password[password_index]<'0')
							user_password[password_index]='9';
					}
					else
					{
						if(enable_setting_threshold==1)
						{
							if(id_setting==0)
							{
                Temp_Threshold--;
                if(Temp_Threshold<1)
                    Temp_Threshold=100;
							}
            
							if(id_setting==1)
							{
                Pressure_Threshold--;
                if(Pressure_Threshold<1)
                    Pressure_Threshold=10;
							}
						}
					}
				}
				
				/*Truong hop Reset ID: id_menu=4*/
				if(id_menu==4)
				{
					if(password_stt==0)
					{
						user_password[password_index]--;
						if(user_password[password_index]<'0')
							user_password[password_index]='9';
					}
					else
					{
						/*do not things*/
					}
				}
        EXTI_ClearITPendingBit(EXTI_Line15);
    }
}



/**
  * @brief  This function handles External lines 2 interrupt request.
  * @param  None
  * @retval None
  */
/*Nut RIGHT -->Up*/
void EXTI2_IRQHandler(void)
{
    if(EXTI_GetITStatus(EXTI_Line2) != RESET)
    {
        //Debug_Display("Interrupn Pin 2");
				/*Truong hop cai dat threshold: id_menu=1*/
				if(id_menu==1)
				{
					if(password_stt==0)
					{
						user_password[password_index]++;
						if(user_password[password_index]>'9')
							user_password[password_index]='0';
					}
					else
					{
						if(enable_setting_threshold==1)
						{
							if(id_setting==0)
							{
                Temp_Threshold++;
                if(Temp_Threshold>100)
                    Temp_Threshold=1;
							}
            
							if(id_setting==1)
							{
                Pressure_Threshold++;
                if(Pressure_Threshold>10)
                    Pressure_Threshold=1;
							}
						}
					}
				}
				
				/*Truong hop Reset ID: id_menu=4*/
				if(id_menu==4)
				{
					if(password_stt==0)
					{
						user_password[password_index]++;
						if(user_password[password_index]>'9')
							user_password[password_index]='0';
					}
					else
					{
						/*do not things*/
					}
				}
        EXTI_ClearITPendingBit(EXTI_Line2);
    }
}


/**
  * @brief  This function handles External lines 3 interrupt request.
  * @param  None
  * @retval None
  */
/*Nut Power -->Enter*/
void EXTI3_IRQHandler(void)
{
    if(EXTI_GetITStatus(EXTI_Line3) != RESET)
    {
				exti_flag = 1;
        if(id_menu==1)
        {
						if(password_stt==0)
						{
							password_index++;
							if(password_index>5)
								password_index=0;
						}
						else
						{
							enable_setting_threshold ++;
							if(enable_setting_threshold>1)
							{
									enable_setting_threshold=0;
							}
						}
        }
				
				/*Truong hop Reset ID: id_menu=4*/
				if(id_menu==4)
				{
					if(password_stt==0)
						{
							password_index++;
							if(password_index>6)
								password_index=0;
						}
				}
        
        if(id_menu==0)
        {
            id_menu = id_main_menu+1;
        }
				
        EXTI_ClearITPendingBit(EXTI_Line3);
    }
}
