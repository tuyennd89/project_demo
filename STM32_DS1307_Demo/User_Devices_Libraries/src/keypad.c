#include "keypad.h"
#include "delay.h"

static uint8_t KEY_4X4[4][4]={
															'1','2','3','A',
															'4','5','6','B',
															'7','8','9','C',
															'F','E','0','D'
															};

void KeyPad_Pin_Config(void)
{
	GPIO_InitTypeDef	GPIO_Init_Structure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); 
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	//Config Colum Pin Is Output
	GPIO_Init_Structure.GPIO_Pin=PIN_COL_KEYPAD;
	GPIO_Init_Structure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init_Structure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init(GPIO_KEYPAD,&GPIO_Init_Structure);
	
	//Config Row Pin Is Input
	GPIO_Init_Structure.GPIO_Pin=PIN_ROW_KEYPAD;
	GPIO_Init_Structure.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init(GPIO_KEYPAD,&GPIO_Init_Structure);
}

uint8_t KEY_IS_PUSH(void)
{
	if((KEYPAD_ROW1==0)||(KEYPAD_ROW2==0)||(KEYPAD_ROW3==0)||(KEYPAD_ROW4==0))
		return 1;
	else
		return 0;
}
void KEYPAD_CheckCol(uint8_t i)
{
	KEYPAD_COL1=1;KEYPAD_COL2=1;KEYPAD_COL3=1;KEYPAD_COL4=1;
	if(i==0)
		KEYPAD_COL1=0;
	else if(i==1)
		KEYPAD_COL2=0;
	else if(i==2)
		KEYPAD_COL3=0;
	else if(i==3)
		KEYPAD_COL4=0;
}
uint8_t KEYPAD_GetKey(void)
{
	uint8_t i;
	KEYPAD_COL1=0;KEYPAD_COL2=0;KEYPAD_COL3=0;KEYPAD_COL4=0;
	if(KEY_IS_PUSH())
	{
		delay_ms(10);
		if(KEY_IS_PUSH())
		{
			for(i=0;i<4;i++)
			{
				KEYPAD_CheckCol(i);
				if(!KEYPAD_ROW1)	return KEY_4X4[3][i];
				if(!KEYPAD_ROW2)	return KEY_4X4[2][i];
				if(!KEYPAD_ROW3)	return KEY_4X4[1][i];
				if(!KEYPAD_ROW4)	return KEY_4X4[0][i];
			}
		}
	}
	return 0;
}
