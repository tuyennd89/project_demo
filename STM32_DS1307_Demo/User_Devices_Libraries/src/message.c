#include "message.h"
#include "string.h"
#include "stdio.h"
#include "EEpromEmulator.h"

#define ID_TEST "000010000100000000000100000000000001000000010000000000000001"
#define MS_TEST    "00100101011101010000101000000001001000110100010101100111100010010001000100010001000100000000000000000000"

extern char IMSI_BIN[64];
extern char ID_MODULE[65];

extern uint8_t Receiver_Ok;
extern char USART1_Buffer[BUFFE_SIZE];
extern uint8_t ID_Status;

//Cac bien duoc su dung trong hoat dong module
extern uint8_t sys_mode;

extern uint8_t mode_status;
extern uint8_t para_status;
extern uint8_t tc_status;

//Cac bien su dung cho ouput mode
//Mode bom doi luu
extern Mode_Pump_TypeDef                                                             Mode_Convection_Pump;
//Mode bom cap nuoc lanh
extern Mode_Pump_TypeDef                                                             Mode_Cold_Water_Supply_Pump;
//Mode bom hoi duong ong
extern Mode_Pump_TypeDef                                                             Mode_Return_Pump;
//Mode bom tang ap
extern Mode_Pump_TypeDef                                                             Mode_Incresed_pressure_Pump;
//Mode bom nhiet bon gia nhiet
extern Mode_Pump_TypeDef                                                             Mode_Heat_Pump;
//Mode dien tro nhiet
extern Mode_Heater_Resistor_TypeDef                                                  Mode_Heater_Resistor;
//Mode van dien tu 3 nga
extern Mode_Valve_TypeDef                                                            Mode_Three_Way_Valve;
//mode van dien tu 1 chieu
extern Mode_Valve_TypeDef                                                            Mode_Blakflow_Valve;


//Cac bien cho Parametter config
//Nguong dieu khien bom doi luu
extern Para_Convection_Pump_TypeDef                                                 Para_Convection_Pump;
//Nguong dieu khien bom cap nuoc lanh
extern Para_Cold_Water_Supply_Pump_TypeDef                                          Para_Cold_Water_Supply_Pump;
//Nguong dieu khien bom hoi duong ong
extern Para_Return_Pump_TypeDef                                                     Para_Return_Pump;
//Nguong dieu khien bom tang ap
extern Para_Incresed_pressure_Pump_TypeDef                                          Para_Incresed_pressure_Pump;
//Nguong dieu khien bom bon gia nhiet
extern Para_Heat_Pump_TypeDef                                                       Para_Heat_Pump;
//Nguong dieu khien dien tro nhiet
extern Para_Heater_Resistor_TypeDef 																								Para_Heater_Resistor;
//Nguong dieu khien van dien tu 3 nga
extern Para_Three_Way_Valve_TypeDef                                                  Para_Three_Way_Valve;
//Nguong dieu khien van 1 chieu
extern Para_Blakflow_Valve_TypeDef                                                   Para_Blakflow_Valve;

//Bien dung cho Timer Counter config
extern Timer_Counter_TypeDef                                                         Timer_Counter_Config;


extern uint8_t Relay_Status[5];
extern uint32_t U32_Relay_Status;

//Vong lap mai mai khi co loi voi ban tin
void Message_Halt(void)
{
    while(1)
    {
        ;
    }
}

//chuyen doi so IMSI tu he thap phan sang he BCD
void Convert_IMSI_2bin(char *imsi_bin, char * imsi_dec)
{
    unsigned char i, len;
    
    //Hien thi thong tin Debug
    Debug_Display("Chuyen Doi Ma IMSI sang he nhi phan");
    len = strlen(imsi_dec);
    for(i=0;i<len;i++)
    {
        switch(imsi_dec[i])
        {
            case '0':
                strcat(imsi_bin,NUM_0);
                break;
            case '1':
                strcat(imsi_bin,NUM_1);
                break;
            case '2':
                strcat(imsi_bin,NUM_2);
                break;
            case '3':
                strcat(imsi_bin,NUM_3);
                break;
            case '4':
                strcat(imsi_bin,NUM_4);
                break;
            case '5':
                strcat(imsi_bin,NUM_5);
                break;
            case '6':
                strcat(imsi_bin,NUM_6);
                break;
            case '7':
                strcat(imsi_bin,NUM_7);
                break;
            case '8':
                strcat(imsi_bin,NUM_8);
                break;
            case '9':
                strcat(imsi_bin,NUM_9);
                break;
        }
    }
    //Hien thi thong tin Debug
    Debug_Display(imsi_bin);
}



//Ham gui yeu cau cap ID
uint8_t send_Request_ID(void)
{
    char Buffer_TX[255] = {'\0'};
    uint8_t Val_Return;

    //Hien thi thong tin Debug
    Debug_Display("Request ID...");
    
    strcat(Buffer_TX,NEW_MD_NOTIFY);
    strcat(Buffer_TX,IE_IMSI);
    //strcat(Buffer_TX,IMSI_BIN1);
    strcat(Buffer_TX,IMSI_BIN);
    strcat(Buffer_TX,RESERVED_IMSI);
    
    Val_Return = Sim908_GPRS_SendData(Buffer_TX);
    Val_Return = Watting_Confirm_Message(20000);
    
    return Val_Return;
}

//Kiem tra ban tin gui xuong
uint8_t Message_Valid(void)
{
    uint8_t value_return=0;
    if(strlen(USART1_Buffer)>24)
    {
        //Hien thi thong tin Debug
        Debug_Display("Co du lieu gui den:");
        Debug_Display(USART1_Buffer);
        Debug_Display("So ky tu nhan duoc:");
        Debug_Display_num(strlen(USART1_Buffer));    
        Receiver_Ok=0;
        if(StrCmpNByte(USART1_Buffer,DL_SYS_MODE_CONF,0)==1)
            value_return = SysModeCfg;
        if(StrCmpNByte(USART1_Buffer,DL_OUT_MODE_CONF,0)==1)
            value_return = OutModeCfg;
        if(StrCmpNByte(USART1_Buffer,DL_PARAM_CONF,0)==1)
            value_return = ParamCfg;
        if(StrCmpNByte(USART1_Buffer,DL_TC_CONF,0)==1)
            value_return = TC_Cfg;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_SYS_MODE,0)==1)
            value_return = CheckSysMode;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_OUT_MODE,0)==1)
            value_return = CheckOutMode;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_PARAM,0)==1)
            value_return = CheckParam;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_TC,0)==1)
            value_return = CheckTC;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_SYS_STATUS,0)==1)
            value_return = CheckSysStatus;
        if(StrCmpNByte(USART1_Buffer,DL_ALARM_CONFIRM,0)==1)
            value_return = Al_Confirm;
        if(StrCmpNByte(USART1_Buffer,DL_CANCEL_ALARM_CONFIRM,0)==1)
            value_return = Al_Cancel_Confirm;
        if(StrCmpNByte(USART1_Buffer,DL_ID_ASSIGN,0)==1)
            value_return = ID_Assign;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_MODULE_ID,0)==1)
            value_return = Check_ID;
        if(StrCmpNByte(USART1_Buffer,DL_CHECK_ACCOUNT,0)==1)
            value_return = Check_Money;
        if(StrCmpNByte(USART1_Buffer,DL_RECHARGE_ACCOUNT,0)==1)
            value_return = Pay_Card;
        if(StrCmpNByte(USART1_Buffer,DL_PASS_RESET,0)==1)
            value_return = Reset_Pass;
        if(StrCmpNByte(USART1_Buffer,DL_NEW_MD_CONFIRM,0)==1)
            value_return = New_Module;
        if(StrCmpNByte(USART1_Buffer,DL_EMER_STOP_CONFIRM,0)==1)
            value_return = Stop_Confirm;
        if(StrCmpNByte(USART1_Buffer,DL_CANCEL_EMER_STOP_CONFIRM,0)==1)
            value_return = Cancel_Stop_Confirm;
        if(StrCmpNByte(USART1_Buffer,DL_REQ_EMER_STOP,0)==1)
            value_return = Req_Stop;
        if(StrCmpNByte(USART1_Buffer,DL_CANCEL_REQ_STOP_CONFIRM,0)==1)
            value_return = Cancel_Req_Stop;
        //Clear_Buffer();
    }
    return value_return;
}


//Kiem tra thanh phan IE dau tien
//Tra ve 0: Khong co ban tin gui xuong
//Tra ve 1: Ban tin gui xuong co thanh phan IE dau tien dung
uint8_t FirstIE_Valid(uint8_t msg_name)
{
    uint8_t val_return = 0;
    uint8_t IE_Name_Status=0;
    uint8_t IE_Content_Status=0;
    uint8_t IE_Reserved_Status=0;

    if((msg_name == ID_Assign) || (msg_name == New_Module))
    {
        IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_IMSI,8);
        IE_Content_Status = StrCmpNByte(USART1_Buffer,IMSI_BIN,24);
        IE_Reserved_Status = StrCmpNByte(USART1_Buffer,RESERVED_IMSI,84);
        if((IE_Name_Status == 1) && (IE_Content_Status == 1) && (IE_Reserved_Status == 1))
            val_return = 1;
    }
    else
    {
        IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_ID,8);
        IE_Content_Status = StrCmpNByte(USART1_Buffer,ID_MODULE,24);
        if((IE_Name_Status == 1) && (IE_Content_Status == 1))
            val_return = 1;
    }
    return val_return;
}
//Kiem tra noi dung thanh phan IE thu 2
uint8_t SecondIE_Valid(uint8_t msg_name)
{
    uint8_t Val_Return = 0;
    uint8_t IE_Name_Status=0;
    
    //Ban tin ID Assign
    if(msg_name == ID_Assign)
    {
       //Kiem tra thanh phan ten IE co phai la IE_ID hay ko?
       IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_ID,104);
       if(IE_Name_Status == 1)
       {
          Val_Return = 1;
       }
    }
        
    //Neu la ban tin cau hinh module
    if(msg_name == SysModeCfg)
    {
       //Kiem tra thanh phan ten IE co phai la IE_SYSMODE hay ko?
       IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_SYS_MODE,88);
       //neu dung, tra ve gia tri 1
       if(IE_Name_Status == 1)
       {
          Val_Return = 1;
       }
    }
        
    //Neu la ban tin cau hinh output mode
    if(msg_name == OutModeCfg)
    {
       //Kiem tra thanh phan ten IE co phai la IE_OUTMODE hay ko?
       IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_OUT_MODE,88);
       //neu dung, tra ve gia tri 1
       if(IE_Name_Status == 1)
       {
          Val_Return = 1;
       }
    }
        
    //Neu la ban tin cau hinh parametter
    if(msg_name == ParamCfg)
    {
       //Kiem tra thanh phan ten IE co phai la cua IE_PARAM hay ko?
       IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_PARAM,88);
       //neu dung, tra ve gia tri 1
       if(IE_Name_Status == 1)
       {
          Val_Return = 1;
       }
    }
        
    //Neu la ban tin cau hinh Time/Counter
    if(msg_name == TC_Cfg)
    {
       //Kiem tra thanh phan ten IE co phai la cua IE_TC hay ko?
       IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_TC,88);
       //neu dung, tra ve gia tri 1
       if(IE_Name_Status == 1)
       {
          Val_Return = 1;
       }
    }
        
    //Neu la ban tin ALARM ACKNOWLEDGE (ban tin xac nhan da nhan duoc ban tin canh bao tu Module)
    if(msg_name == Al_Confirm)
    {
       //Kiem tra thanh phan ten IE co phai la cua IE_ALARM hay ko?
       IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_ALARM,88);
       //neu dung, tra ve gia tri 1
       if(IE_Name_Status == 1)
       {
          Val_Return = 1;
       }
    }
        
    //Neu la ban tin ALARM CLEARANCE ACKNOWLEDGE(ban tin xac nhan da nhan duoc ban tin huy canh bao tu Module)
    if(msg_name == Al_Cancel_Confirm)
    {
        //Kiem tra thanh phan ten IE co phai la cua IE_ALARM hay ko?
        IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_ALARM,88);
        //neu dung, tra ve gia tri 1
        if(IE_Name_Status == 1)
        {
            Val_Return = 1;
        }
    }
                
    //Neu la ban tin check money
    if(msg_name == Check_Money)
    {
        //Kiem tra thanh phan ten IE co phai la cua IE_CHECK_MONEY hay ko?
        IE_Name_Status = StrCmpNByte(USART1_Buffer,IE_CHECK_MONEY,88);
        //neu dung, tra ve gia tri 1
        if(IE_Name_Status == 1)
        {
            Val_Return = 1;
        }
    }
        
    return Val_Return;
}


void MSG_process(void)
{
    uint8_t msg_name;
    uint8_t first_ie_stt=0;
    uint8_t second_ie_stt=0;
    uint8_t i=0,len=0;
    char *start;
    char *end;
    char buffer_cmd[20]={0};
    //mang de lay cac gia tri truyen tu server xuong de xu ly
    char buffer_str[9]={'\0'};

    //Kiem tra xem co nhan duoc ban tin hay ko
    if(Receiver_Ok)
    {
        msg_name = Message_Valid();
        if(msg_name)
        {
            first_ie_stt = FirstIE_Valid(msg_name);
            second_ie_stt = SecondIE_Valid(msg_name);
            //Nhung ban tin chi co 1 IE
            if((first_ie_stt == 1) && (second_ie_stt == 0))
            {
                Send_MSG_Report(msg_name);
            }
            //kiem tra xem msg co 2 thanh phan ie ko
            if((first_ie_stt == 1) && (second_ie_stt == 1))
            {
                //Neu la ban tin cau hinh module
                if(msg_name == ID_Assign)
                {
                    //Neu module chua duoc set ID
                    if(ID_Status != ID_READY)
                    {
                        strcpynbyte(ID_MODULE,USART1_Buffer,120,strlen(USART1_Buffer));
                        Debug_Display("ID MODULE duoc set LA");
                        Debug_Display(ID_MODULE);
                        //EEpromEmulator_WriteBytes(ID_LOCATION,(uint8_t*)ID_MODULE,ID_LEN);
                        EEP_24CXX_WriteS(ID_LOCATION,(uint8_t*)ID_MODULE);
                        ID_Status = ID_READY;
                        //EEpromEmulator_WriteBytes(ID_STATUS_LOCATION,&ID_Status,1);
                        EEP_24CXX_Write(ID_STATUS_LOCATION,ID_Status);
                        //Gui ban tin confirm
                        Send_Message_Confirm(ID_Assign);
                    }
                    else
                    {
                        Debug_Display("ID MODULE co san la");
                        Debug_Display(ID_MODULE);
                    } //Neu module chua duoc set ID
                } //Neu la ban tin cau hinh module
                
                //Neu la ban tin cau hinh module
                if(msg_name == SysModeCfg)
                {
                    strcpynbyte(buffer_str,USART1_Buffer,104,strlen(USART1_Buffer));
                    Debug_Display("SYSMODE duoc set");
                    Debug_Display(buffer_str);
                    sys_mode = ConvertStr2Num(buffer_str);
                    Debug_Display_num(strlen(buffer_str));
                    Debug_Display_num(sys_mode);
                    //EEpromEmulator_WriteBytes(SYS_MODE_LOCATION,&sys_mode,SYS_MODE_LEN);                //Write to Epprom
                    EEP_24CXX_Write(SYS_MODE_LOCATION,sys_mode);
                    Send_Message_Confirm(SysModeCfg);
                }
                
                //Neu la ban tin cau hinh OUTPUT_MODE
                if(msg_name == OutModeCfg)
                {
                    Debug_Display("BOM DOI LUU OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,104,112);
                    Mode_Convection_Pump.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE BOM DOI LUU");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Convection_Pump.mode);
                    //EEpromEmulator_WriteBytes(Mode_Convection_Pump_Location,&Mode_Convection_Pump.mode,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Convection_Pump_Location,Mode_Convection_Pump.mode);
                    Debug_Display_Mode(Mode_Convection_Pump.mode);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,112,120);
                    Mode_Convection_Pump.time = ConvertStr2Num(buffer_str);
                    Debug_Display("TIME BOM DOI LUU");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Convection_Pump.time);
                    //EEpromEmulator_WriteBytes(Mode_Convection_Pump_Location+1,&Mode_Convection_Pump.time,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Convection_Pump_Location+1,Mode_Convection_Pump.time);
                    
                    Debug_Display("BOM CAP NUOC LANH OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,128,136);
                    Mode_Cold_Water_Supply_Pump.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE BOM CAP NUOC LANH");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Cold_Water_Supply_Pump.mode);
                    //EEpromEmulator_WriteBytes(Mode_Cold_Water_Supply_Pump_Location,&Mode_Cold_Water_Supply_Pump.mode,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Cold_Water_Supply_Pump_Location,Mode_Cold_Water_Supply_Pump.mode);
                    Debug_Display_Mode(Mode_Cold_Water_Supply_Pump.mode);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,136,144);
                    Mode_Cold_Water_Supply_Pump.time = ConvertStr2Num(buffer_str);
                    Debug_Display("TIME BOM CAP NUOC LANH");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Cold_Water_Supply_Pump.time);
                    //EEpromEmulator_WriteBytes(Mode_Cold_Water_Supply_Pump_Location+1,&Mode_Cold_Water_Supply_Pump.time,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Cold_Water_Supply_Pump_Location+1,Mode_Cold_Water_Supply_Pump.time);
                    
                    
                    Debug_Display("BOM HOI DUONG ONG OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,152,160);  
                    Mode_Return_Pump.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE BOM HOI DUONG ONG");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Return_Pump.mode);
                    //EEpromEmulator_WriteBytes(Mode_Return_Pump_Location,&Mode_Return_Pump.mode,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Return_Pump_Location,Mode_Return_Pump.mode);
                    Debug_Display_Mode(Mode_Return_Pump.mode);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,160,168);
                    Mode_Return_Pump.time = ConvertStr2Num(buffer_str);
                    Debug_Display("TIME BOM HOI DUONG ONG");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Return_Pump.time);
                    //EEpromEmulator_WriteBytes(Mode_Return_Pump_Location+1,&Mode_Return_Pump.time,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Return_Pump_Location+1,Mode_Return_Pump.time);

                    Debug_Display("BOM TANG AP OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,176,184);
                    Mode_Incresed_pressure_Pump.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE BOM TANG AP");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Incresed_pressure_Pump.mode);
                    //EEpromEmulator_WriteBytes(Mode_Incresed_pressure_Pump_Location,&Mode_Incresed_pressure_Pump.mode,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Incresed_pressure_Pump_Location,Mode_Incresed_pressure_Pump.mode);
                    Debug_Display_Mode(Mode_Incresed_pressure_Pump.mode);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,184,192);
                    Mode_Incresed_pressure_Pump.time = ConvertStr2Num(buffer_str);
                    Debug_Display("TIME BOM TANG AP");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Incresed_pressure_Pump.time);
                    //EEpromEmulator_WriteBytes(Mode_Incresed_pressure_Pump_Location+1,&Mode_Incresed_pressure_Pump.time,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Incresed_pressure_Pump_Location+1,Mode_Incresed_pressure_Pump.time);
                    
                    Debug_Display("BOM NHIET BON GIA NHIET OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,200,208);
                    Mode_Heat_Pump.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE BOM NHIET BON GIA NHIET");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Heat_Pump.mode);
                    Debug_Display_Mode(Mode_Heat_Pump.mode);
                    
                    Debug_Display("DIEN TRO NHIET OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,216,224);
                    Mode_Heater_Resistor.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE DIEN TRO NHIET");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Heater_Resistor.mode);
                    //EEpromEmulator_WriteBytes(Mode_Heater_Resistor_Location,&Mode_Heater_Resistor.mode,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Heater_Resistor_Location,Mode_Heater_Resistor.mode);
                    Debug_Display_Mode(Mode_Heater_Resistor.mode);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,224,232);
                    Mode_Heater_Resistor.delta_t2 = ConvertStr2Num(buffer_str);
                    Debug_Display("NGUONG NHIET DO DELTA T2");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Heater_Resistor.delta_t2);
                    //EEpromEmulator_WriteBytes(Mode_Heater_Resistor_Location+1,&Mode_Heater_Resistor.delta_t2,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Heater_Resistor_Location+1,Mode_Heater_Resistor.delta_t2);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,232,240);
                    Mode_Heater_Resistor.delta_time_r2 = ConvertStr2Num(buffer_str);
                    Debug_Display("NGUONG THOI GIAN KICH HOAT R2");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Heater_Resistor.delta_time_r2);
                    //EEpromEmulator_WriteBytes(Mode_Heater_Resistor_Location+2,&Mode_Heater_Resistor.delta_time_r2,1);                //Write to Epprom
                    EEP_24CXX_Write(Mode_Heater_Resistor_Location+2,Mode_Heater_Resistor.delta_time_r2);
                    
                    /*
                    strcpynbyte(buffer_str,USART1_Buffer,240,248);
                    Mode_Heater_Resistor.reserved = ConvertStr2Num(buffer_str);
                    Debug_Display("Byte du phong");
                  Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Heater_Resistor.reserved);
                    EEpromEmulator_WriteBytes(Mode_Heater_Resistor_Location+3,&Mode_Heater_Resistor.reserved,1);                //Write to Epprom
                    */
                    
                    Debug_Display("VAN DIEN TU 3 NGA OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,248,256);
                    Mode_Three_Way_Valve.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE VAN DIEN TU 3 NGA");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Three_Way_Valve.mode);
                    Debug_Display_Mode(Mode_Three_Way_Valve.mode);
                    
                    Debug_Display("VAN DIEN TU 1 CHIEU OUTPUT MODE");
                    strcpynbyte(buffer_str,USART1_Buffer,264,272);
                    Mode_Blakflow_Valve.mode = ConvertStr2Num(buffer_str);
                    Debug_Display("MODE VAN DIEN TU 1 CHIEU");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Mode_Blakflow_Valve.mode);
                    Debug_Display_Mode(Mode_Blakflow_Valve.mode);
                    
                    mode_status=STATUS_VALID;
                    EEP_24CXX_Write(MODE_STATUS_LOCATION,mode_status);
                    
                    //EEpromEmulator_WriteBytes(SYS_MODE_LOCATION,&sys_mode,SYS_MODE_LEN);
                    Send_Message_Confirm(OutModeCfg);
                }
                //Ban tin Parametter config
                if(msg_name == ParamCfg)
                {
                    Debug_Display("Nguong dieu khien Bom doi luu");
                    strcpynbyte(buffer_str,USART1_Buffer,104,112);
                    Para_Convection_Pump = ConvertStr2Num(buffer_str);
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Convection_Pump);
                    //EEpromEmulator_WriteBytes(Para_Convection_Pump_Location,&Para_Convection_Pump,Para_Convection_Pump_Len);                //Write to Epprom
                    EEP_24CXX_Write(Para_Convection_Pump_Location,Para_Convection_Pump);
                    
                    Debug_Display("Nguong dieu khien Bom cap nuoc lanh");
                    strcpynbyte(buffer_str,USART1_Buffer,112,120);
                    Para_Cold_Water_Supply_Pump.M1 = ConvertStr2Num(buffer_str);
                    Debug_Display("Muc nuoc M1");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Cold_Water_Supply_Pump.M1);
                    //EEpromEmulator_WriteBytes(Para_Cold_Water_Supply_Pump_Location,&Para_Cold_Water_Supply_Pump.M1,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Cold_Water_Supply_Pump_Location,Para_Cold_Water_Supply_Pump.M1);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,120,128);
                    Para_Cold_Water_Supply_Pump.M2 = ConvertStr2Num(buffer_str);
                    Debug_Display("Muc nuoc M2");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Cold_Water_Supply_Pump.M2);
                    //EEpromEmulator_WriteBytes(Para_Cold_Water_Supply_Pump_Location+1,&Para_Cold_Water_Supply_Pump.M2,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Cold_Water_Supply_Pump_Location+1,Para_Cold_Water_Supply_Pump.M2);
                    
                    Debug_Display("Nguong dieu khien Bom hoi duong ong");
                    strcpynbyte(buffer_str,USART1_Buffer,128,136);
                    Para_Return_Pump.Time_T1_Hour = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T1 hh");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Return_Pump.Time_T1_Hour);
                    //EEpromEmulator_WriteBytes(Para_Return_Pump_Loaction,&Para_Return_Pump.Time_T1_Hour,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Return_Pump_Loaction,Para_Return_Pump.Time_T1_Hour);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,136,144);
                    Para_Return_Pump.Time_T1_Min = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T1 mm");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Return_Pump.Time_T1_Min);
                    //EEpromEmulator_WriteBytes(Para_Return_Pump_Loaction+1,&Para_Return_Pump.Time_T1_Min,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Return_Pump_Loaction+1,Para_Return_Pump.Time_T1_Min);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,144,152);
                    Para_Return_Pump.Time_T2_Hour = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T2 hh");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Return_Pump.Time_T2_Hour);
                    //EEpromEmulator_WriteBytes(Para_Return_Pump_Loaction+2,&Para_Return_Pump.Time_T2_Hour,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Return_Pump_Loaction+2,Para_Return_Pump.Time_T2_Hour);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,152,160);
                    Para_Return_Pump.Time_T2_Min = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T2 mm");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Return_Pump.Time_T2_Min);
                    //EEpromEmulator_WriteBytes(Para_Return_Pump_Loaction+3,&Para_Return_Pump.Time_T2_Min,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Return_Pump_Loaction+3,Para_Return_Pump.Time_T2_Min);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,160,168);
                    Para_Return_Pump.Delta_Temp = ConvertStr2Num(buffer_str);
                    Debug_Display("Nguong nhiet do Delata T");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Return_Pump.Delta_Temp);
                    //EEpromEmulator_WriteBytes(Para_Return_Pump_Loaction+4,&Para_Return_Pump.Delta_Temp,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Return_Pump_Loaction+4,Para_Return_Pump.Delta_Temp);
                    
                    Debug_Display("Nguong dieu khien Bom Tang ap");
                    strcpynbyte(buffer_str,USART1_Buffer,168,176);
                    Para_Incresed_pressure_Pump = ConvertStr2Num(buffer_str);
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Incresed_pressure_Pump);
                    //EEpromEmulator_WriteBytes(Para_Incresed_pressure_Pump_Location,&Para_Incresed_pressure_Pump,Para_Incresed_pressure_Pump_Len);                //Write to Epprom
                    EEP_24CXX_Write(Para_Incresed_pressure_Pump_Location,Para_Incresed_pressure_Pump);

                    Debug_Display("Nguong dieu khien Bom Bon gia nhiet");
                    strcpynbyte(buffer_str,USART1_Buffer,176,184);
                    Para_Heat_Pump = ConvertStr2Num(buffer_str);
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Heat_Pump);
                    //EEpromEmulator_WriteBytes(Para_Heat_Pump_Location,&Para_Heat_Pump,Para_Heat_Pump_Len);                //Write to Epprom
                    EEP_24CXX_Write(Para_Heat_Pump_Location,Para_Heat_Pump);
                    
                    Debug_Display("Nguong dieu khien Dien tro nhiet");
                    strcpynbyte(buffer_str,USART1_Buffer,184,192);
                    Para_Heater_Resistor.Delta_Temp = ConvertStr2Num(buffer_str);
                    Debug_Display("Nguong thoi gian dieu khien");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Heater_Resistor.Delta_Temp);
                    //EEpromEmulator_WriteBytes(Para_Heater_Resistor_Location,&Para_Heater_Resistor.Delta_Time,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Heater_Resistor_Location,Para_Heater_Resistor.Delta_Temp);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,192,200);
                    Para_Heater_Resistor.Delta_Time = ConvertStr2Num(buffer_str);
                    Debug_Display("Nguong nhiet do dieu khien");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Heater_Resistor.Delta_Time);
                    //EEpromEmulator_WriteBytes(Para_Heater_Resistor_Location+1,&Para_Heater_Resistor.Delta_Temp,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Heater_Resistor_Location+1,Para_Heater_Resistor.Delta_Time);
                    
                    Debug_Display("Nguong dieu khien Van dien tu 3 nga");
                    strcpynbyte(buffer_str,USART1_Buffer,200,208);
                    Para_Three_Way_Valve.Time_T1_Hour = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T1 hh");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Three_Way_Valve.Time_T1_Hour);
                    //EEpromEmulator_WriteBytes(Para_Three_Way_Valve_Location,&Para_Three_Way_Valve.Time_T1_Hour,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Three_Way_Valve_Location,Para_Three_Way_Valve.Time_T1_Hour);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,208,216);
                    Para_Three_Way_Valve.Time_T1_Min = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T1 mm");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Three_Way_Valve.Time_T1_Min);
                    //EEpromEmulator_WriteBytes(Para_Three_Way_Valve_Location+1,&Para_Three_Way_Valve.Time_T1_Min,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Three_Way_Valve_Location+1,Para_Three_Way_Valve.Time_T1_Min);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,216,224);
                    Para_Three_Way_Valve.Time_T2_Hour = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T2 hh");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Three_Way_Valve.Time_T2_Hour);
                    //EEpromEmulator_WriteBytes(Para_Three_Way_Valve_Location+2,&Para_Three_Way_Valve.Time_T2_Hour,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Three_Way_Valve_Location+2,Para_Three_Way_Valve.Time_T2_Hour);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,224,232);
                    Para_Three_Way_Valve.Time_T2_Min = ConvertStr2Num(buffer_str);
                    Debug_Display("Dau khung thoi gian T1 mm");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Three_Way_Valve.Time_T2_Min);
                    //EEpromEmulator_WriteBytes(Para_Three_Way_Valve_Location+3,&Para_Three_Way_Valve.Time_T2_Min,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Three_Way_Valve_Location+3,Para_Three_Way_Valve.Time_T2_Min);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,232,240);
                    Para_Three_Way_Valve.Delta_Temp = ConvertStr2Num(buffer_str);
                    Debug_Display("Nguong nhiet do delta T");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Three_Way_Valve.Delta_Temp);
                    //EEpromEmulator_WriteBytes(Para_Three_Way_Valve_Location+4,&Para_Three_Way_Valve.Delta_Temp,1);                //Write to Epprom
                    EEP_24CXX_Write(Para_Three_Way_Valve_Location+4,Para_Three_Way_Valve.Delta_Temp);
                    
                    Debug_Display("Nguong dieu khien Van dien tu 1 chieu");
                    strcpynbyte(buffer_str,USART1_Buffer,240,248);
                    Para_Blakflow_Valve= ConvertStr2Num(buffer_str);
                    Debug_Display("Nguong nhiet do T");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Para_Blakflow_Valve);
                    //EEpromEmulator_WriteBytes(Para_Blakflow_Valve_Location,&Para_Blakflow_Valve,Para_Blakflow_Valve_Len);                //Write to Epprom
                    EEP_24CXX_Write(Para_Blakflow_Valve_Location,Para_Blakflow_Valve);
                    //Gui ban tin Confirm
                    para_status=STATUS_VALID;
                    EEP_24CXX_Write(PARA_STATUS_LOCATION,para_status);
                    Send_Message_Confirm(ParamCfg);
                }
                //Ban tin Timer Counter config
                if(msg_name == TC_Cfg)
                {
                    Debug_Display("Timer Counter Config");
                    strcpynbyte(buffer_str,USART1_Buffer,104,112);
                    Timer_Counter_Config.Confirm_Time = ConvertStr2Num(buffer_str);
                    Debug_Display("Confirm Timer");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Timer_Counter_Config.Confirm_Time);
                    //EEpromEmulator_WriteBytes(Timer_Counter_Config_Location,&Timer_Counter_Config.Confirm_Time,1);                //Write to Epprom
                    EEP_24CXX_Write(Timer_Counter_Config_Location,Timer_Counter_Config.Confirm_Time);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,112,120);
                    Timer_Counter_Config.Resend_Time = ConvertStr2Num(buffer_str);
                    Debug_Display("Resend Timer");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Timer_Counter_Config.Resend_Time);
                    //EEpromEmulator_WriteBytes(Timer_Counter_Config_Location+1,&Timer_Counter_Config.Resend_Time,1);                //Write to Epprom
                    EEP_24CXX_Write(Timer_Counter_Config_Location+1,Timer_Counter_Config.Confirm_Time);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,120,128);
                    Timer_Counter_Config.Report_Time = ConvertStr2Num(buffer_str);
                    Debug_Display("Report Timer");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Timer_Counter_Config.Report_Time);
                    //EEpromEmulator_WriteBytes(Timer_Counter_Config_Location+2,&Timer_Counter_Config.Report_Time,1);                //Write to Epprom
                    EEP_24CXX_Write(Timer_Counter_Config_Location+2,Timer_Counter_Config.Report_Time);
                    
                    strcpynbyte(buffer_str,USART1_Buffer,128,136);
                    Timer_Counter_Config.MSG_Counter_Limit = ConvertStr2Num(buffer_str);
                    Debug_Display("Counter");
                    Debug_Display(buffer_str);
                    Debug_Display_num(Timer_Counter_Config.MSG_Counter_Limit);
                    //EEpromEmulator_WriteBytes(Timer_Counter_Config_Location+3,&Timer_Counter_Config.MSG_Counter_Limit,1);                //Write to Epprom
                    EEP_24CXX_Write(Timer_Counter_Config_Location+3,Timer_Counter_Config.MSG_Counter_Limit);
                    
                    //Gui ban tin Confirm
                    tc_status=STATUS_VALID;
                    EEP_24CXX_Write(TC_STATUS_LOCATION,tc_status);
                    Send_Message_Confirm(TC_Cfg);
                }
                                
                if(msg_name == Check_Money)
                {
                    start=strchr(USART1_Buffer,'&');
                    end=strrchr(USART1_Buffer,'%');
                    start+=2;
                    end--;
                    len=end-start;
                    for(i=0;i<len;i++)
                    {
                        buffer_cmd[i]=start[i];
                    }
                    buffer_cmd[len]=0;
                    //Gui report len server
                    Account_Report(buffer_cmd);
                }
            }  //kiem tra xem msg co 2 thanh phan ie ko
        }
        Clear_Buffer();
    } //Kiem tra xem co nhan duoc ban tin hay ko
}




void Read_Para_Config(void)
{

    Debug_Display("Read Parametter Config");

    Para_Convection_Pump = EEP_24CXX_Read(Para_Convection_Pump_Location);
    Debug_Display("Nguong dieu khien bom doi luu luu trong chip");
    Debug_Display_num(Para_Convection_Pump);
    

    Para_Cold_Water_Supply_Pump.M1 = EEP_24CXX_Read(Para_Cold_Water_Supply_Pump_Location);
    Debug_Display("Nguong dieu khien bom cap nuoc lanh");
    Debug_Display("Muc nuoc M1 luu trong chip");
    Debug_Display_num(Para_Cold_Water_Supply_Pump.M1);
    

    Para_Cold_Water_Supply_Pump.M2 = EEP_24CXX_Read(Para_Cold_Water_Supply_Pump_Location+1);
    Debug_Display("Muc nuoc M2 luu trong chip");
    Debug_Display_num(Para_Cold_Water_Supply_Pump.M2);
    

    Para_Return_Pump.Time_T1_Hour = EEP_24CXX_Read(Para_Return_Pump_Loaction);
    Debug_Display("Nguong dieu khien bom hoi duong ong");
    Debug_Display("Dau khung thoi gian T1 hh luu trong chip");
    Debug_Display_num(Para_Return_Pump.Time_T1_Hour);
    
 
    Para_Return_Pump.Time_T1_Min = EEP_24CXX_Read(Para_Return_Pump_Loaction+1);
    Debug_Display("Dau khung thoi gian T1 mm luu trong chip");
    Debug_Display_num(Para_Return_Pump.Time_T1_Min);
    

    Para_Return_Pump.Time_T2_Hour = EEP_24CXX_Read(Para_Return_Pump_Loaction+2);
    Debug_Display("Dau khung thoi gian T2 hh luu trong chip");
    Debug_Display_num(Para_Return_Pump.Time_T2_Hour);
    

    Para_Return_Pump.Time_T2_Min = EEP_24CXX_Read(Para_Return_Pump_Loaction+3);
    Debug_Display("Dau khung thoi gian T2 mm luu trong chip");
    Debug_Display_num(Para_Return_Pump.Time_T2_Min);
    

    Para_Return_Pump.Delta_Temp = EEP_24CXX_Read(Para_Return_Pump_Loaction+4);
    Debug_Display("Nguong nhiet do delta T luu trong chip");
    Debug_Display_num(Para_Return_Pump.Delta_Temp);
    

    Para_Incresed_pressure_Pump = EEP_24CXX_Read(Para_Incresed_pressure_Pump_Location);
    Debug_Display("Nguong Dieu khien bom tang ap luu trong chip");
    Debug_Display_num(Para_Incresed_pressure_Pump);

    Para_Heat_Pump = EEP_24CXX_Read(Para_Heat_Pump_Location);
    Debug_Display("Nguong Dieu khien Bon gia nhiet luu trong chip");
    Debug_Display_num(Para_Heat_Pump);
    
 
    Para_Heater_Resistor.Delta_Temp = EEP_24CXX_Read(Para_Heater_Resistor_Location);
    Debug_Display("Nguong Dieu khien Dien tro nhiet");
    Debug_Display("Nguong thoi gian delta T luu trong chip");
    Debug_Display_num(Para_Heater_Resistor.Delta_Temp);
    
  
    Para_Heater_Resistor.Delta_Time = EEP_24CXX_Read(Para_Heater_Resistor_Location+1);
    Debug_Display("Nguong thoi gian delta T luu trong chip");
    Debug_Display_num(Para_Heater_Resistor.Delta_Time);

    Para_Three_Way_Valve.Time_T1_Hour = EEP_24CXX_Read(Para_Three_Way_Valve_Location);
    Debug_Display("Nguong dieu khien van dien tu 3 nga");
    Debug_Display("Dau khung thoi gian T1 hh luu trong chip");
    Debug_Display_num(Para_Three_Way_Valve.Time_T1_Hour);
    
    Para_Three_Way_Valve.Time_T1_Min = EEP_24CXX_Read(Para_Three_Way_Valve_Location+1);
    Debug_Display("Dau khung thoi gian T1 mm luu trong chip");
    Debug_Display_num(Para_Three_Way_Valve.Time_T1_Min);
    

    Para_Three_Way_Valve.Time_T2_Hour = EEP_24CXX_Read(Para_Three_Way_Valve_Location+2);
    Debug_Display("Dau khung thoi gian T2 hh luu trong chip");
    Debug_Display_num(Para_Three_Way_Valve.Time_T2_Hour);
    
    Para_Three_Way_Valve.Time_T2_Min = EEP_24CXX_Read(Para_Three_Way_Valve_Location+3);
    Debug_Display("Dau khung thoi gian T2 mm luu trong chip");
    Debug_Display_num(Para_Three_Way_Valve.Time_T2_Min);
    

    Para_Three_Way_Valve.Delta_Temp = EEP_24CXX_Read(Para_Three_Way_Valve_Location+4);
    Debug_Display("Nguong nhiet do delta T luu trong chip");
    Debug_Display_num(Para_Three_Way_Valve.Delta_Temp);
    

    Para_Blakflow_Valve = EEP_24CXX_Read(Para_Blakflow_Valve_Location);
    Debug_Display("Nguong Dieu khien van 1 chieu cho bom tang ap");
    Debug_Display_num(Para_Blakflow_Valve);
    
    

    Debug_Display("Read Output Mode Config");
    Mode_Convection_Pump.mode = EEP_24CXX_Read(Mode_Convection_Pump_Location);
    Debug_Display("Mode Bom doi luu luu trong chip");
    Debug_Display_num(Mode_Convection_Pump.mode);
    Debug_Display_Mode(Mode_Convection_Pump.mode);
    
    Mode_Convection_Pump.time = EEP_24CXX_Read(Mode_Convection_Pump_Location+1);
    Debug_Display("Time Bom doi luu luu trong chip");
    Debug_Display_num(Mode_Convection_Pump.time);
    
    Mode_Cold_Water_Supply_Pump.mode = EEP_24CXX_Read(Mode_Cold_Water_Supply_Pump_Location);
    Debug_Display("Mode Bom cap muoc lanh luu trong chip");
    Debug_Display_num(Mode_Cold_Water_Supply_Pump.mode);
    Debug_Display_Mode(Mode_Cold_Water_Supply_Pump.mode);
    
    Mode_Cold_Water_Supply_Pump.time = EEP_24CXX_Read(Mode_Cold_Water_Supply_Pump_Location+1);
    Debug_Display("Time Bom cap muoc lanh luu trong chip");
    Debug_Display_num(Mode_Cold_Water_Supply_Pump.time);
    
    Mode_Return_Pump.mode = EEP_24CXX_Read(Mode_Return_Pump_Location);
    Debug_Display("Mode Bom hoi duong ong luu trong chip");
    Debug_Display_num(Mode_Return_Pump.mode);
    Debug_Display_Mode(Mode_Return_Pump.mode);
    
    Mode_Return_Pump.time = EEP_24CXX_Read(Mode_Return_Pump_Location+1);
    Debug_Display("Time Bom hoi duong ong luu trong chip");
    Debug_Display_num(Mode_Return_Pump.time);
    
    Mode_Incresed_pressure_Pump.mode = EEP_24CXX_Read(Mode_Incresed_pressure_Pump_Location);
    Debug_Display("Mode Bom tang ap luu trong chip");
    Debug_Display_num(Mode_Incresed_pressure_Pump.mode);
    Debug_Display_Mode(Mode_Incresed_pressure_Pump.mode);
    
    Mode_Incresed_pressure_Pump.time = EEP_24CXX_Read(Mode_Incresed_pressure_Pump_Location+1);
    Debug_Display("Time Bom tang ap luu trong chip");
    Debug_Display_num(Mode_Incresed_pressure_Pump.time);
    
    Mode_Heater_Resistor.mode = EEP_24CXX_Read(Mode_Heater_Resistor_Location);
    Debug_Display("Mode Dien tro nhiet luu trong chip");
    Debug_Display_num(Mode_Heater_Resistor.mode);
    Debug_Display_Mode(Mode_Heater_Resistor.mode);
    
    Mode_Heater_Resistor.delta_t2 = EEP_24CXX_Read(Mode_Heater_Resistor_Location+1);
    Debug_Display("Nguong nhiet do delta t2 luu trong chip");
    Debug_Display_num(Mode_Heater_Resistor.delta_t2);
    
    Mode_Heater_Resistor.delta_time_r2 = EEP_24CXX_Read(Mode_Heater_Resistor_Location+2);
    Debug_Display("Nguong nhiet do delta t2 luu trong chip");
    Debug_Display_num(Mode_Heater_Resistor.delta_time_r2);
    
    
    Timer_Counter_Config.Confirm_Time = EEP_24CXX_Read(Timer_Counter_Config_Location);
    Debug_Display("Read timer counter config ");
    Debug_Display("Confirm Timer luu trong chip ");
    Debug_Display_num(Timer_Counter_Config.Confirm_Time);
    
    Timer_Counter_Config.Resend_Time = EEP_24CXX_Read(Timer_Counter_Config_Location+1);
    Debug_Display("Resend Timer luu trong chip ");
    Debug_Display_num(Timer_Counter_Config.Resend_Time);
    
    Timer_Counter_Config.Report_Time = EEP_24CXX_Read(Timer_Counter_Config_Location+2);
    Debug_Display("Report Timer luu trong chip ");
    Debug_Display_num(Timer_Counter_Config.Report_Time);
    
    Timer_Counter_Config.MSG_Counter_Limit = EEP_24CXX_Read(Timer_Counter_Config_Location+3);
    Debug_Display("Counter luu trong chip ");
    Debug_Display_num(Timer_Counter_Config.MSG_Counter_Limit);
    
    Debug_Display("Read ID Status");
    ID_Status = EEP_24CXX_Read(ID_STATUS_LOCATION);
    Debug_Display_num(ID_Status);
    
    Debug_Display("Read ID Module");
    if(ID_Status==ID_READY)
    EEPROM_24CXX_ReadS(ID_LOCATION,64,(uint8_t*)ID_MODULE);
    Debug_Display("ID module luu trong chip");
    Debug_Display(ID_MODULE);
    
    sys_mode = EEP_24CXX_Read(SYS_MODE_LOCATION);
    Debug_Display("SYS_MODE HIEN TAI");
    Debug_Display_num(sys_mode);
    
    mode_status = EEP_24CXX_Read(MODE_STATUS_LOCATION);
    para_status = EEP_24CXX_Read(PARA_STATUS_LOCATION);
    tc_status = EEP_24CXX_Read(TC_STATUS_LOCATION);
}


//Report gia tri sensor
/*
uint8_t Sensor_Report(uint8_t mode)
{
    char Buffer_TX[255]={'\0'};
    char buff1[9],buff2[9],buff3[9],buff4[9],buff5[9],buff6[9],buff7[9],buff8[9],buff9[9],buff10[9],buff11[9];
    
    strcat(Buffer_TX,SENSOR_REPORT);
    strcat(Buffer_TX,IE_ID);
    strcat(Buffer_TX,RESERVED_ID);
    strcat(Buffer_TX,ID_TEST);
    strcat(Buffer_TX,IE_SENSOR_VALUE);
    if(mode==32)
    {
        convertbin2str(t_dt,buff1);
        convertbin2str(t_bsl,buff2);
        convertbin2str(l_w_bsl,buff3);
        convertbin2str(t_bgn,buff4);
        convertbin2str(p_bgn,buff5);
        convertbin2str(light_dt,buff6);
        convertbin2str(t_top_bsl,buff7);
        convertbin2str(cb_tran,buff8);
        convertbin2str(p_d_ong,buff9);
        convertbin2str(t_d_ong_1,buff10);
        convertbin2str(t_d_ong_2,buff11);
        
        strcat(Buffer_TX,buff1);
        strcat(Buffer_TX,buff2);
        strcat(Buffer_TX,buff3);
        strcat(Buffer_TX,buff4);
        strcat(Buffer_TX,buff5);
        strcat(Buffer_TX,buff6);
        strcat(Buffer_TX,buff7);
        strcat(Buffer_TX,buff8);
        strcat(Buffer_TX,buff9);
        strcat(Buffer_TX,buff10);
        strcat(Buffer_TX,buff11);
    }
    strcat(Buffer_TX,RESERVED_SENSOR);
#ifdef USART3_DEBUG
        USART_PutString(USART3,Buffer_TX);
      USART_PutChar(USART3,'\n');
#else
#endif
    return 1;
}e
*/

