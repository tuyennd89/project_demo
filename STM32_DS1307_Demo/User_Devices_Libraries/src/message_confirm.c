#include "message.h"

extern uint8_t Receiver_Ok;
extern char USART1_Buffer[BUFFE_SIZE];
extern char IMSI_BIN[64];
extern char ID_MODULE[65];

//Kiem tra ban tin confirm tu server gui ve
//Tra ve 0 neu khong nhan duoc ban tin confirm
//Tra ve 1 neu nhan duoc ban tin confirm
uint8_t Message_Confirm_Valid(void)
{
    uint8_t Val_Return = 0;
    uint8_t msg_name;
     
    msg_name = Message_Valid();
    if(msg_name)
    {
        if(msg_name == New_Module)
        {
            if(FirstIE_Valid(msg_name)==1)
                Val_Return = 1;
        }
				
				if(msg_name == Al_Confirm)
        {
            if(FirstIE_Valid(msg_name)==1)
                Val_Return = 1;
        }
    }
    return Val_Return;
}

//Cho nhan ban tin confirm sau khi gui ban tin di
uint8_t Watting_Confirm_Message(uint16_t time_out)
{
    uint8_t val_return = 0;
    
    //Hien thi thong tin Debug
	  Debug_Display("Waiting Confirm Message");
    
    val_return = Message_Confirm_Valid();
    while((val_return == 0) && (time_out > 0))
    {
        val_return = Message_Confirm_Valid();
        delay_ms(1);
        time_out --;
    }
    if(val_return == 0)
    {
        Debug_Display("Khong nhan doc ban tin confirm, Dung hoat dong");
        //Message_Halt();
    }
    if(val_return == 1)
    {
        Debug_Display("Da nhan duoc ban tin confirm tu server");
    }
		return val_return;
}

//Gui ban tin confirm da nhan duoc MSG tu server
uint8_t Send_Message_Confirm(uint8_t msg_name)
{
    char Buffer_TX[512] = {'\0'}; 
    uint8_t Val_Return = 0;
    
    //Hien thi thong tin Debug
	Debug_Display("Send Confirm MSG");
    
    //SYSTEM MODE CONFIG COMPLETE
    if(msg_name == SysModeCfg)
    {
        strcat(Buffer_TX,SYS_MODE_CONF_COMPLETE);
    }
    //OUTPUT MODE CONFIG COMPLETE
    if(msg_name == OutModeCfg)
    {
        strcat(Buffer_TX,OUT_MODE_CONF_COMPLETE);
    }
    //PARAMETER CONFIG COMPLETE
    if(msg_name == ParamCfg)
    {
        strcat(Buffer_TX,PARAMETER_CONF_COMPLETE);
    }
    //TIMER/COUNTER CONFIG COMPLETE
    if(msg_name == TC_Cfg)
    {
        strcat(Buffer_TX,TC_CONF_COMPLETE);
    }
    //ID ASSIGNMENT COMPLETE
    if(msg_name == ID_Assign)
    {
      strcat(Buffer_TX,ID_ASSIGN_COMPLETE);
			strcat(Buffer_TX,IE_IMSI);
			strcat(Buffer_TX,IMSI_BIN);
			strcat(Buffer_TX,RESERVED_IMSI);
    }
     //RECHARGE ACCOUNT COMPLETE
    if(msg_name == Reset_Pass)
    {
        strcat(Buffer_TX,RECHARGE_ACC_COMPLETE);
    }
    //PASS RESET COMPLETE
    if(msg_name == Reset_Pass)
    {
        strcat(Buffer_TX,SYS_MODE_CONF_COMPLETE);
    }
    
    //Add IE name of ID to buffer
    strcat(Buffer_TX,IE_ID);
    //Add ID  to buffer
    strcat(Buffer_TX,ID_MODULE);
    //Send Buffer_TX
    Val_Return = Sim908_GPRS_SendData(Buffer_TX);
    
    return Val_Return;
}


//Kiem tra xem co ban tin confirm cho ban tin canh bao da gui di hay ko
uint8_t Alarm_Confirm_Message_Valid(uint8_t Alarm)
{
	uint8_t Val_Return = 0;
	uint8_t msg_name;
	uint8_t first_ie_stt=0;
	uint8_t second_ie_stt=0;
	//mang de lay cac gia tri truyen tu server xuong de xu ly
	char buffer_str[9]={'\0'};
	uint8_t Alarm_Recv;

	if(Receiver_Ok)
	{
		msg_name = Message_Valid();
		if(msg_name)
		{
			first_ie_stt = FirstIE_Valid(msg_name);
			second_ie_stt = SecondIE_Valid(msg_name);
			if((first_ie_stt == 1) && (second_ie_stt == 1))
			{
				if(msg_name == Al_Confirm)
				{
					strcpynbyte(buffer_str,USART1_Buffer,104,strlen(USART1_Buffer));
					Alarm_Recv = ConvertStr2Num(buffer_str);
					if(Alarm_Recv == Alarm)
						Val_Return = 1;
					else
						Val_Return = 0;
				}
				
				if(msg_name == Al_Cancel_Confirm)
				{
					strcpynbyte(buffer_str,USART1_Buffer,104,strlen(USART1_Buffer));
					Alarm_Recv = ConvertStr2Num(buffer_str);
					if(Alarm_Recv == Alarm)
						Val_Return = 1;
					else
						Val_Return = 0;
				}
			} /*(first_ie_stt == 1) && (second_ie_stt == 1)*/
		}/*msg_name is valid*/
	}
	
	return Val_Return;
}

//Cho nhan ban tin confirm da nhan duoc ban tin canh bao sau khi gui ban tin canh bao di
uint8_t Watting_Alarm_Confirm_Message(uint16_t time_out, uint8_t Alarm)
{
	uint8_t val_return = 0;
	uint8_t Temp_Alarm = Alarm;
	uint16_t Wait_Time = time_out;
	
	//Hien thi thong tin Debug
	Debug_Display("Waiting Alarm Confirm Message");
  
  val_return = Alarm_Confirm_Message_Valid(Temp_Alarm);
  while((val_return == 0) && (Wait_Time > 0))
  {
      val_return = Alarm_Confirm_Message_Valid(Temp_Alarm);
      delay_ms(1);
      Wait_Time --;
  }
  if(val_return == 0)
  {
      Debug_Display("Khong nhan doc ban tin confirm, Dung hoat dong");
      //Message_Halt();
  }
  if(val_return == 1)
  {
      Debug_Display("Da nhan duoc ban tin confirm tu server");
  }
	return val_return;
}


