#include "glcdlib.h"

unsigned char font[]= {
         0,   0,   0,   0,   0,   0,   0,       //' ' 32
         0,   6,  95,  95,   6,   0,   0,       //'!'
         0,   7,   7,   0,   7,   7,   0,       //'"'
         20, 127, 127,  20, 127, 127,  20,      //'#'
         36,  46, 107, 107,  58,  18,   0,      //'$'
         70, 102,  48,  24,  12, 102,  98,      //'%'
         48, 122,  79,  93,  55, 122,  72,      //'&'
         4,   7,   3,   0,   0,   0,   0,       //'''
         0,  28,  62,  99,  65,   0,   0,       //'('
         0,  65,  99,  62,  28,   0,   0,       //')'
         8,  42,  62,  28,  28,  62,  42,       //'*'
         8,   8,  62,  62,   8,   8,   0,       //'+'
         0, 128, 224,  96,   0,   0,   0,       //','
         8,   8,   8,   8,   8,   8,   0,       //'-'
         0,   0,  96,  96,   0,   0,   0,       //'.'
         96,  48,  24,  12,   6,   3,   1,      //'/'
         62, 127, 113,  89,  77, 127,  62,      //'0'
         64,  66, 127, 127,  64,  64,   0,      //'1'
         98, 115,  89,  73, 111, 102,   0,      //'2'
         34,  99,  73,  73, 127,  54,   0,      //'3'
         24,  28,  22,  83, 127, 127,  80,      //'4'
         39, 103,  69,  69, 125,  57,   0,      //'5'
         60, 126,  75,  73, 121,  48,   0,      //'6'
         3,   3, 113, 121,  15,   7,   0,       //'7'
         54, 127,  73,  73, 127,  54,   0,      //'8'
         6,  79,  73, 105,  63,  30,   0,       //'9'
         0,   0, 102, 102,   0,   0,   0,       //':'
         0, 128, 230, 102,   0,   0,   0,       //';'
         8,  28,  54,  99,  65,   0,   0,       //'<'
         36,  36,  36,  36,  36,  36,   0,      //'='
         0,  65,  99,  54,  28,   8,   0,       //'>'
         2,   3,  81,  89,  15,   6,   0,       //'?'
         62, 127,  65,  93,  93,  31,  30,      //'@'
         124, 126,  19,  19, 126, 124,   0,     //'A'
         65, 127, 127,  73,  73, 127,  54,      //'B'
         28,  62,  99,  65,  65,  99,  34,      //'C'
         65, 127, 127,  65,  99,  62,  28,      //'D'
         65, 127, 127,  73,  93,  65,  99,      //'E'
         65, 127, 127,  73,  29,   1,   3,      //'F'
         28,  62,  99,  65,  81, 115, 114,      //'G'
         127, 127,   8,   8, 127, 127,   0,     //'H'
         0,  65, 127, 127,  65,   0,   0,       //'I'
         48, 112,  64,  65, 127,  63,   1,      //'J'
         65, 127, 127,   8,  28, 119,  99,      //'K'
         65, 127, 127,  65,  64,  96, 112,      //'L'
         127, 127,  14,  28,  14, 127, 127,     //'M'
         127, 127,   6,  12,  24, 127, 127,     //'N'
         28,  62,  99,  65,  99,  62,  28,      //'O'
         65, 127, 127,  73,   9,  15,   6,      //'P'
         30,  63,  33, 113, 127,  94,   0,      //'Q'
         65, 127, 127,   9,  25, 127, 102,      //'R'
         38, 111,  77,  89, 115,  50,   0,      //'S'
         3,  65, 127, 127,  65,   3,   0,       //'T'
         127, 127,  64,  64, 127, 127,   0,     //'U'
         31,  63,  96,  96,  63,  31,   0,      //'V'
         127, 127,  48,  24,  48, 127, 127,     //'W'
         67, 103,  60,  24,  60, 103,  67,      //'X'
         7,  79, 120, 120,  79,   7,   0,       //'Y'
	     71,  99, 113,  89,  77, 103, 115,      //'Z'
	     0, 127, 127,  65,  65,   0,   0,       //'['
	     1,   3,   6,  12,  24,  48,  96,       //'\'
	     0,  65,  65, 127, 127,   0,   0,       //']'
	     8,  12,   6,   3,   6,  12,   8,       //'^'
	     128, 128, 128, 128, 128, 128, 128,     //'_'
	     0,   0,   3,   7,   4,   0,   0,       //'`'
	     32, 116,  84,  84,  60, 120,  64,      //'a'
	     65, 127,  63,  72,  72, 120,  48,      //'b'
         56, 124,  68,  68, 108,  40,   0,      //'c'
	     48, 120,  72,  73,  63, 127,  64,      //'d'
	     56, 124,  84,  84,  92,  24,   0,      //'e'
	     72, 126, 127,  73,   3,   2,   0,      //'f'
	     56, 188, 164, 164, 252, 120,   0,      //'g'
	     65, 127, 127,   8,   4, 124, 120,      //'h'
	     0,  68, 125, 125,  64,   0,   0,       //'i'
	     96, 224, 128, 128, 253, 125,   0,      //'j'
	     65, 127, 127,  16,  56, 108,  68,      //'k'
	     0,  65, 127, 127,  64,   0,   0,       //'l'
	     120, 124,  28,  56,  28, 124, 120,     //'m'
	     124, 124,   4,   4, 124, 120,   0,     //'n'
	     56, 124,  68,  68, 124,  56,   0,      //'o'
	     0, 252, 252, 164,  36,  60,  24,       //'p'
	     24,  60,  36, 164, 248, 252, 132,      //'q'
	     68, 124, 120,  76,   4,  28,  24,      //'r'
	     72,  92,  84,  84, 116,  36,   0,      //'s'
	     0,   4,  62, 127,  68,  36,   0,       //'t'
	     60, 124,  64,  64,  60, 124,  64,      //'u'
	     28,  60,  96,  96,  60,  28,   0,      //'v'
	     60, 124, 112,  56, 112, 124,  60,      //'w'
	     68, 108,  56,  16,  56, 108,  68,      //'x'
	     60, 188, 160, 160, 252, 124,   0,      //'y'
	     76, 100, 116,  92,  76, 100,   0,      //'z'
	     8,   8,  62, 119,  65,  65,   0,       //'{'
	     0,   0,   0, 119, 119,   0,   0,       //'|'
	     65,  65, 119,  62,   8,   8,   0,      //'}'
	     2,   3,   1,   3,   2,   3,   1,       //'~'
	     255, 129,  129,  129,  129, 129, 255,  //''
	     14, 159, 145, 177, 251,  74,   0       //'?
};
unsigned char font5x7[] = {
	0x00, 0x00, 0x00, 0x00, 0x00,// (space)[32]
	0x00, 0x00, 0x5F, 0x00, 0x00,// !      [33]
	0x00, 0x07, 0x00, 0x07, 0x00,// "      [34]
	0x14, 0x7F, 0x14, 0x7F, 0x14,// #      [35]
	0x24, 0x2A, 0x7F, 0x2A, 0x12,// $      [36]
	0x23, 0x13, 0x08, 0x64, 0x62,// %      [37]
	0x36, 0x49, 0x55, 0x22, 0x50,// &      [38]
	0x00, 0x05, 0x03, 0x00, 0x00,// '      [39]
	0x00, 0x1C, 0x22, 0x41, 0x00,// (      [40]
	0x00, 0x41, 0x22, 0x1C, 0x00,// )      [41]
	0x08, 0x2A, 0x1C, 0x2A, 0x08,// *      [42]
	0x08, 0x08, 0x3E, 0x08, 0x08,// +      [43]
	0x00, 0x50, 0x30, 0x00, 0x00,// ,      [44]
	0x08, 0x08, 0x08, 0x08, 0x08,// -      [45]
	0x00, 0x60, 0x60, 0x00, 0x00,// .      [46]
	0x20, 0x10, 0x08, 0x04, 0x02,// /      [47]
	0x3E, 0x51, 0x49, 0x45, 0x3E,// 0      [48]
	0x00, 0x42, 0x7F, 0x40, 0x00,// 1      [49]
	0x42, 0x61, 0x51, 0x49, 0x46,// 2      [50]
	0x21, 0x41, 0x45, 0x4B, 0x31,// 3      [51]
	0x18, 0x14, 0x12, 0x7F, 0x10,// 4      [52]
	0x27, 0x45, 0x45, 0x45, 0x39,// 5      [53]
	0x3C, 0x4A, 0x49, 0x49, 0x30,// 6      [54]
	0x01, 0x71, 0x09, 0x05, 0x03,// 7      [55]
	0x36, 0x49, 0x49, 0x49, 0x36,// 8      [56]
	0x06, 0x49, 0x49, 0x29, 0x1E,// 9      [57]
	0x00, 0x36, 0x36, 0x00, 0x00,// :      [58]
	0x00, 0x56, 0x36, 0x00, 0x00,// ;      [59]
	0x00, 0x08, 0x14, 0x22, 0x41,// <      [60]
	0x14, 0x14, 0x14, 0x14, 0x14,// =      [61]
	0x41, 0x22, 0x14, 0x08, 0x00,// >      [62]
	0x02, 0x01, 0x51, 0x09, 0x06,// ?      [63]
	0x32, 0x49, 0x79, 0x41, 0x3E,// @      [64]
	0x7E, 0x11, 0x11, 0x11, 0x7E,// A      [65]
	0x7F, 0x49, 0x49, 0x49, 0x36,// B      [66]
	0x3E, 0x41, 0x41, 0x41, 0x22,// C      [67]
	0x7F, 0x41, 0x41, 0x22, 0x1C,// D      [68]
	0x7F, 0x49, 0x49, 0x49, 0x41,// E      [69]
	0x7F, 0x09, 0x09, 0x01, 0x01,// F      [70]
	0x3E, 0x41, 0x41, 0x51, 0x32,// G      [71]
	0x7F, 0x08, 0x08, 0x08, 0x7F,// H      [72]
	0x00, 0x41, 0x7F, 0x41, 0x00,// I      [73]
	0x20, 0x40, 0x41, 0x3F, 0x01,// J      [74]
	0x7F, 0x08, 0x14, 0x22, 0x41,// K      [75]
	0x7F, 0x40, 0x40, 0x40, 0x40,// L      [76]
	0x7F, 0x02, 0x04, 0x02, 0x7F,// M      [77]
	0x7F, 0x04, 0x08, 0x10, 0x7F,// N      [78]
	0x3E, 0x41, 0x41, 0x41, 0x3E,// O      [79]
	0x7F, 0x09, 0x09, 0x09, 0x06,// P      [80]
	0x3E, 0x41, 0x51, 0x21, 0x5E,// Q      [81]
	0x7F, 0x09, 0x19, 0x29, 0x46,// R      [82]
	0x46, 0x49, 0x49, 0x49, 0x31,// S      [83]
	0x01, 0x01, 0x7F, 0x01, 0x01,// T      [84]
	0x3F, 0x40, 0x40, 0x40, 0x3F,// U      [85]
	0x1F, 0x20, 0x40, 0x20, 0x1F,// V      [86]
	0x7F, 0x20, 0x18, 0x20, 0x7F,// W      [87]
	0x63, 0x14, 0x08, 0x14, 0x63,// X      [88]
	0x03, 0x04, 0x78, 0x04, 0x03,// Y      [89]
	0x61, 0x51, 0x49, 0x45, 0x43,// Z      [90]
	0x00, 0x00, 0x7F, 0x41, 0x41,// [      [91]
	0x02, 0x04, 0x08, 0x10, 0x20,// "\"    [92]
	0x41, 0x41, 0x7F, 0x00, 0x00,// ]      [93]
	0x04, 0x02, 0x01, 0x02, 0x04,// ^      [94]
	0x40, 0x40, 0x40, 0x40, 0x40,// _      [95]
	0x00, 0x01, 0x02, 0x04, 0x00,// `      [96]
	0x20, 0x54, 0x54, 0x54, 0x78,// a      [97]
	0x7F, 0x48, 0x44, 0x44, 0x38,// b      [98]
	0x38, 0x44, 0x44, 0x44, 0x20,// c      [99]
	0x38, 0x44, 0x44, 0x48, 0x7F,// d      [100]
	0x38, 0x54, 0x54, 0x54, 0x18,// e      [101]
	0x08, 0x7E, 0x09, 0x01, 0x02,// f      [102]
	0x08, 0x14, 0x54, 0x54, 0x3C,// g      [103]
	0x7F, 0x08, 0x04, 0x04, 0x78,// h      [104]
	0x00, 0x44, 0x7D, 0x40, 0x00,// i      [105]
	0x20, 0x40, 0x44, 0x3D, 0x00,// j      [106]
	0x00, 0x7F, 0x10, 0x28, 0x44,// k      [107]
	0x00, 0x41, 0x7F, 0x40, 0x00,// l      [108]
	0x7C, 0x04, 0x18, 0x04, 0x78,// m      [109]
	0x7C, 0x08, 0x04, 0x04, 0x78,// n      [110]
	0x38, 0x44, 0x44, 0x44, 0x38,// o      [111]
	0x7C, 0x14, 0x14, 0x14, 0x08,// p      [112]
	0x08, 0x14, 0x14, 0x18, 0x7C,// q      [113]
	0x7C, 0x08, 0x04, 0x04, 0x08,// r      [114]
	0x48, 0x54, 0x54, 0x54, 0x20,// s      [115]
	0x04, 0x3F, 0x44, 0x40, 0x20,// t      [116]
	0x3C, 0x40, 0x40, 0x20, 0x7C,// u      [117]
	0x1C, 0x20, 0x40, 0x20, 0x1C,// v      [118]
	0x3C, 0x40, 0x30, 0x40, 0x3C,// w      [119]
	0x44, 0x28, 0x10, 0x28, 0x44,// x      [120]
	0x0C, 0x50, 0x50, 0x50, 0x3C,// y      [121]
	0x44, 0x64, 0x54, 0x4C, 0x44,// z      [122]
	0x00, 0x08, 0x36, 0x41, 0x00,// {      [123]
	0x00, 0x00, 0x7F, 0x00, 0x00,// |      [124]
	0x00, 0x41, 0x36, 0x08, 0x00,// }      [125]
	0x08, 0x08, 0x2A, 0x1C, 0x08,// ->     [126]
	0x08, 0x1C, 0x2A, 0x08, 0x08,// <-     [127]
    0x00, 0x64, 0x92, 0x92, 0xF4,// a^     [128]
    0x3C, 0x7E, 0xFF, 0xFF, 0xFF,// (|     [129]
    0xFF, 0xFF, 0xFF, 0x7E, 0x3C,//  |)     [130]
    0x00, 0x07, 0x05, 0x07, 0x00,//  do     [131]
};
unsigned char font3x5[]={
        0x00, 0x00, 0x00,                        // Code for char  
        0x00, 0x17, 0x00,                        // Code for char !
        0x03, 0x00, 0x03,                        // Code for char "
        0x1F, 0x0A, 0x1F,                        // Code for char #
        0x14, 0x1F, 0x0A,                        // Code for char $
        0x09, 0x04, 0x12,                        // Code for char %
        0x1A, 0x15, 0x1E,                        // Code for char &
        0x03, 0x01, 0x00,                        // Code for char '
        0x0E, 0x11, 0x00,                        // Code for char (
        0x11, 0x0E, 0x00,                        // Code for char )
        0x15, 0x0E, 0x15,                        // Code for char *
        0x04, 0x0E, 0x04,                        // Code for char +
        0x10, 0x10, 0x00,                        // Code for char ,
        0x04, 0x04, 0x04,                        // Code for char -
        0x00, 0x10, 0x00,                        // Code for char .
        0x18, 0x04, 0x03,                        // Code for char /
        0x0E, 0x11, 0x0E,                        // Code for char 0
        0x12, 0x1F, 0x10,                        // Code for char 1
        0x19, 0x15, 0x12,                        // Code for char 2
        0x11, 0x15, 0x0A,                        // Code for char 3
        0x0C, 0x0A, 0x1F,                        // Code for char 4
        0x17, 0x15, 0x1D,                        // Code for char 5
        0x1F, 0x15, 0x1D,                        // Code for char 6
        0x01, 0x1D, 0x07,                        // Code for char 7
        0x0A, 0x15, 0x0A,                        // Code for char 8
        0x17, 0x15, 0x1F,                        // Code for char 9
        0x00, 0x14, 0x00,                        // Code for char :
        0x10, 0x14, 0x00,                        // Code for char ;
        0x04, 0x0A, 0x11,                        // Code for char <
        0x14, 0x14, 0x14,                        // Code for char =
        0x11, 0x0A, 0x04,                        // Code for char >
        0x01, 0x15, 0x02,                        // Code for char ?
        0x1F, 0x11, 0x17,                        // Code for char @
        0x1E, 0x05, 0x1E,                        // Code for char A
        0x1F, 0x15, 0x0A,                        // Code for char B
        0x0E, 0x11, 0x11,                        // Code for char C
        0x1F, 0x11, 0x0E,                        // Code for char D
        0x1F, 0x15, 0x11,                        // Code for char E
        0x1F, 0x05, 0x01,                        // Code for char F
        0x0E, 0x11, 0x1D,                        // Code for char G
        0x1F, 0x04, 0x1F,                        // Code for char H
        0x11, 0x1F, 0x11,                        // Code for char I
        0x08, 0x10, 0x0F,                        // Code for char J
        0x1F, 0x04, 0x1B,                        // Code for char K
        0x1F, 0x10, 0x10,                        // Code for char L
        0x1F, 0x06, 0x1F,                        // Code for char M
        0x1F, 0x02, 0x1F,                        // Code for char N
        0x0E, 0x11, 0x0E,                        // Code for char O
        0x1F, 0x05, 0x02,                        // Code for char P
        0x0E, 0x19, 0x1E,                        // Code for char Q
        0x1F, 0x05, 0x1A,                        // Code for char R
        0x16, 0x15, 0x0D,                        // Code for char S
        0x01, 0x1F, 0x01,                        // Code for char T
        0x1F, 0x10, 0x1F,                        // Code for char U
        0x0F, 0x10, 0x0F,                        // Code for char V
        0x1F, 0x0C, 0x1F,                        // Code for char W
        0x1B, 0x04, 0x1B,                        // Code for char X
        0x03, 0x1C, 0x03,                        // Code for char Y
        0x19, 0x15, 0x13,                        // Code for char Z
        0x1F, 0x11, 0x00,                        // Code for char [
        0x03, 0x04, 0x18,                        // Code for char BackSlash
        0x11, 0x1F, 0x00,                        // Code for char ]
        0x02, 0x01, 0x02,                        // Code for char ^
        0x00, 0x00, 0x00,                        // Code for char _
        0x01, 0x03, 0x00,                        // Code for char `
        0x18, 0x14, 0x1C,                        // Code for char a
        0x1F, 0x14, 0x08,                        // Code for char b
        0x08, 0x14, 0x14,                        // Code for char c
        0x08, 0x14, 0x1F,                        // Code for char d
        0x0E, 0x15, 0x12,                        // Code for char e
        0x04, 0x1E, 0x05,                        // Code for char f
        0x0C, 0x04, 0x1C,                        // Code for char g
        0x1F, 0x04, 0x18,                        // Code for char h
        0x00, 0x1D, 0x00,                        // Code for char i
        0x00, 0x1D, 0x00,                        // Code for char j
        0x1F, 0x08, 0x14,                        // Code for char k
        0x00, 0x1F, 0x00,                        // Code for char l
        0x1C, 0x0C, 0x1C,                        // Code for char m
        0x1C, 0x04, 0x18,                        // Code for char n
        0x08, 0x14, 0x08,                        // Code for char o
        0x1C, 0x14, 0x08,                        // Code for char p
        0x08, 0x14, 0x1C,                        // Code for char q
        0x1C, 0x04, 0x00,                        // Code for char r
        0x10, 0x1C, 0x04,                        // Code for char s
        0x04, 0x1E, 0x14,                        // Code for char t
        0x1C, 0x10, 0x1C,                        // Code for char u
        0x0C, 0x10, 0x0C,                        // Code for char v
        0x1C, 0x18, 0x1C,                        // Code for char w
        0x14, 0x08, 0x14,                        // Code for char x
        0x0C, 0x10, 0x0C,                        // Code for char y
        0x04, 0x1C, 0x10,                        // Code for char z
        0x04, 0x1F, 0x11,                        // Code for char {
        0x00, 0x1F, 0x00,                        // Code for char |
        0x11, 0x1F, 0x04,                        // Code for char }
        0x01, 0x02, 0x01,                        // Code for char ~
        0x0C, 0x0A, 0x0C,                        // Code for char 
};


void glcd_delay(void)
{
}              

void delay_en(uint32_t time)
{
	uint32_t index;
	for(index=(72*time);index!=0;index--)
		{
		}
}


void mcu_set_pin_out(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void mcu_set_pin_in(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  /* GPIOD Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
  
  /* Configure PD0 and PD2 in output pushpull mode */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
}


/*------------------------ AVR => GLCD    -----------------------------------*/
void glcd_out_set(void)
{
  mcu_set_pin_out();
  LCD_EN = 0;  
}  

/*-----------------------  GLCD => AVR ---------------------------------------*/
void glcd_in_set(void)
{
  mcu_set_pin_in();
  LCD_EN = 0;  
    
}

/*----------------------- chon chip ks0108 trai hoac phai --------------------*/
void glcd_set_side( unsigned char side)
{
    glcd_out_set();
    if(side==1){               // chip phai
        LCD_CS1 = 0;//LCD_CS1 = 0;
        LCD_CS2 = 1;//LCD_CS2 = 1;
    }                         
    else{                      // chip trai
        LCD_CS1 = 1;//LCD_CS1 = 1;
        LCD_CS2 = 0;//LCD_CS2 = 0;
    }
    glcd_delay();
}
/*----------------------------------------------------------------------------*/
/*------------------------ instruction GLCD ----------------------------------*/
void glcd_wait(void)
{
	/*
	  uint16_t temp;
    glcd_in_set();
    LCD_RW = 1;
    LCD_RS = 0;
    
    LCD_EN = 1;
    delay_us(1);
    LCD_EN = 0; 
    //temp = GPIO_ReadInputData(GPIOB);
	  temp = PINA;
    while((temp & 0x0080)!=0x0080){
			  //temp = GPIO_ReadInputData(GPIOB);
			temp = PINA;
        LCD_EN = 1;
        delay_us(1);
        LCD_EN = 0;
    }
	*/
	delay_en(1);
}

/*-----------------------     ON/OFF GLCD    ---------------------------------*/
void glcd_set_display( unsigned char i)
{
	  uint16_t temp;
    glcd_wait();
    glcd_out_set();
    LCD_RS = 0;
    LCD_RW = 0;  
    temp=DATA_OUT&0xff00;
    DATA_OUT= ((GLCD_DISPLAY + i)|temp);         // 1: on, 0: off
    LCD_EN = 1;
    glcd_delay();
    LCD_EN = 0;    
}

/*----------------------- chon dia chi cot (collumn) ---------------------------*/
void glcd_set_yaddress( unsigned char col)
{
	  uint16_t temp;
    glcd_wait();
    glcd_out_set();
    LCD_RS = 0;
    LCD_RW = 0;
    temp=DATA_OUT&0xff00;
    DATA_OUT = ((GLCD_YADDRESS + col)|temp);  	
    LCD_EN = 1;
    glcd_delay();
    LCD_EN = 0;  
}
/*----------------------- chon dia chi dong (line) ------------------------------*/
void glcd_set_xaddress( unsigned char line)
{
	  uint16_t temp;
    glcd_wait();
    glcd_out_set();
    LCD_RS = 0;
    LCD_RW = 0;    
    temp=DATA_OUT&0xff00;
    DATA_OUT = ((GLCD_XADDRESS + line)|temp);  
    LCD_EN = 1;
    glcd_delay();
    LCD_EN = 0; 
}
/*----------------------- chon dong start line    ----------------------------*/
void glcd_set_startline(unsigned char offset)
{
	  uint16_t temp;
    glcd_wait();
    glcd_out_set();
    LCD_RS = 0;
    LCD_RW = 0;
    
    glcd_set_side(0);
	  temp=DATA_OUT&0xff00;
    DATA_OUT = ((GLCD_STARTLINE + offset)|temp); 
    LCD_EN = 1;
    glcd_delay();
    LCD_EN = 0;
    
    glcd_set_side(1);
	  temp=DATA_OUT&0xff00;
    DATA_OUT = ((GLCD_STARTLINE + offset)|temp);

    LCD_EN = 1;
    glcd_delay();
    LCD_EN = 0; 
}
/*---------------------      ghi du lieu   -----------------------------------*/
void glcd_write( unsigned char data)
{
	  uint16_t temp;
    glcd_wait();
    glcd_out_set();
    LCD_RS = 1;
    LCD_RW = 0;
       
	  temp=DATA_OUT&0xff00;
    DATA_OUT = (data|temp);
    LCD_EN = 1;
    glcd_delay();
    LCD_EN = 0;
    glcd_delay();
}
/*----------------------   doc du lieu tai cot (collumn)      ----------------*/
//column=0:7
unsigned char glcd_read(unsigned char column)
{
    unsigned char read_data=0;              // khai bao bien chua du lieu
    //DATA_DDR = 0x00;                        // set input 
    mcu_set_pin_in();
    LCD_RS = 1;
    LCD_RW = 1;                
    #ifdef CS_ACTIVE_LOW
        //LCD_CS1 = (column>63);
	      if(column>63)
				{
					LCD_CS1 = 1;
				    LCD_CS2 = 0;
				}
				else
				{
					LCD_CS1 = 0;
					LCD_CS2 = 1;
				}
    #else         
        //LCD_CS1 = (column<64);         //Enable/Disable CS1'
				if(column<63)
				{
					LCD_CS1 = 1;
				    LCD_CS2 = 0;
				}
				else
				{
					LCD_CS1 = 0;
					LCD_CS2 = 1;
				}
    #endif
    //LCD_CS2 = !LCD_CS1;           //Disable/Enable CS2
    delay_us(1);        
    LCD_EN= 1;                       
    delay_us(1);        

    LCD_EN = 0;                      
    delay_us(20);          
    
    LCD_EN = 1;                       
    delay_us(1);               
                                  
    read_data = DATA_IN&0x00ff;   
	
    LCD_EN = 0;                        // xoa du lieu tu bus 
    delay_us(1);            
    
    //DATA_DDR = 0xFF;                        // set input tro lai
    return read_data;      
}
/*----------------------------------------------------------------------------*/
/*------------------------    cac ham ung dung       -------------------------*/
/*----------------------------------------------------------------------------*/

/*-----------------------     ham khoi dong glcd   ---------------------------*/
void glcd_init(void)
{
    //setbit(CTRL_OUT,RST);         // set chan RESET o muc cao    
    mcu_set_pin_out();
    glcd_set_side(0);             // set chip trai
    glcd_set_display(1);
    glcd_set_xaddress(0);
    glcd_set_yaddress(0);
    glcd_set_startline(0);
    
    glcd_set_side(1);             // set chip phai
    glcd_set_display(1);
    glcd_set_xaddress(0);
    glcd_set_yaddress(0);
    glcd_set_startline(0);    
}
/*---------------------     ham tro den dia chi line, col --------------------*/
//   line = 0:7, col = 0:127 
void glcd_gotolinecol( unsigned char line, unsigned char col)
{
    unsigned char side;
    side=col/64;
    glcd_set_side(side);
    col-=64*side;
    glcd_set_xaddress(line);
    glcd_set_yaddress(col);
}
/*-------------------      full toan bo man hinh   ---------------------------*/
void glcd_full(void)
{
    unsigned char line, col;
    for(line=0;line<8;line++){
        glcd_gotolinecol(line, 0);
        for(col=0;col<64;col++){
            glcd_write(0xff);
        }
    }
    for(line=0;line<8;line++){
        glcd_gotolinecol(line, 64);
        for(col=0;col<64;col++){
            glcd_write(0xff);
        }
    }
}
/*-------------------      xoa man hinh glcd    -----------------------------*/
void glcd_clear(void)
{
    unsigned char line, col;
    for(line=0;line<8;line++){
        glcd_gotolinecol(line, 0);
        for(col=0;col<64;col++){
            glcd_write(0);
        }
    }
    for(line=0;line<8;line++){
        glcd_gotolinecol(line, 64);
        for(col=0;col<64;col++){
            glcd_write(0);
        }
    }
}
/*--------------------   xoa 1 cot     ---------------------------------------*/
//col = 0:127
void glcd_clearcol(unsigned char col)
{
    unsigned char line;
    for(line=0;line<8;line++){
        glcd_gotolinecol(line, col);
        glcd_write(0);
    }
}
/*------------------  xoa 1 hang     ----------------------------------------*/
// line = 0:7
void glcd_clearline(unsigned char line)
{
    unsigned char col;
    for(col=0;col<128;col++){
        glcd_gotolinecol(line, col);
        glcd_write(0);                                                      
    }
}
/*-------------------   in mot ky tu trong font len glcd  ------------------*/
void glcd_putchar(unsigned char line, unsigned char col, unsigned char c)
{   
    unsigned char i;
    if( (col>57) && (col<64)){
        glcd_gotolinecol(line, col);
        for(i=0;i<64-col;i++){
            glcd_write(font[((c - 32) * 7)+i]);
        }                                      
        glcd_gotolinecol(line, 64);
        for(i=64-col;i<7;i++){
            glcd_write(font[((c - 32) * 7)+i]);
        }
    }                                         
    else{
        glcd_gotolinecol(line, col);
        for(i=0;i<7;i++){
            glcd_write(font[((c - 32) * 7)+i]);
        }
    }
}
/*----------------- in mot chuoi ky tu len glcd   --------------------------*/
void glcd_puts(unsigned char line, unsigned char col, char* str)
{
    unsigned char i, x;
    x = col;
    for (i=0; str[i]!=0; i++){
        if(x>=128){
            col = 0;
            x = col;
            line++;            
        }          
        glcd_putchar(line, x, str[i]);
        x += 8;
    }
}
/*----------------   in mot chuoi ki tu duoc luu trong flash   -------------*/
void glcd_putsf(unsigned char line, unsigned char col,  char* str)
{
    unsigned char i, x;
    x = col;
    for(i=0; str[i]!=0;i++){
        if(x>=128){
            col = 0;
            x = col;
            line++;
        }
    glcd_putchar(line, x, str[i]);
    x += 8;
    }
}
/*--------------  in mot chuoi ky tu duoc luu trong eeprom   ---------------*/
void glcd_putse(unsigned char line, unsigned char col,  char* str)
{   
    unsigned char i, x;
    x = col;
    for(i=0; str[i]!=0;i++){
        if(x>=128){
            col = 0;
            x = col;
            line++;
        }
    glcd_putchar(line, x, str[i]);
    x += 8;
    } 
}
/*--------------  in mot ky tu tu font5x7 len glcd ----------------------*/
void glcd_putchar5x7(unsigned char line, unsigned char col, unsigned char c)
{   
    unsigned char i;
    if( (col>59) && (col<64)){
        glcd_gotolinecol(line, col);
        for(i=0;i<64-col;i++){
            glcd_write(font5x7[((c - 32) * 5)+i]);
        }                                      
        glcd_gotolinecol(line, 64);
        for(i=64-col;i<5;i++){
            glcd_write(font5x7[((c - 32) * 5)+i]);
        }
    }                                         
    else{
        glcd_gotolinecol(line, col);
        for(i=0;i<5;i++){
            glcd_write(font5x7[((c - 32) * 5)+i]);
        }
    }
}

/*--------------  in mot chuoi ky tu trong font5x7 len glcd   ---------------*/
void glcd_puts5x7(unsigned char line, unsigned char col, char *str)
{  
    unsigned char i, x;
    x = col;
    for (i=0; str[i]!=0; i++){
        if(x>=128){
            col = 0;
            x = col;
            line++;            
        }          
        glcd_putchar5x7(line, x, str[i]);
        x += 6;
    }
}
/*--------------  in mot ky tu tu font3x5 len glcd ------------------------*/
void glcd_putchar3x5(unsigned char line, unsigned char col, unsigned char c)
{
   unsigned char i;
    if( (col>61) && (col<64)){
        glcd_gotolinecol(line, col);
        for(i=0;i<64-col;i++){
            glcd_write(font3x5[((c - 32) * 3)+i]);
        }                                      
        glcd_gotolinecol(line, 64);
        for(i=64-col;i<3;i++){
            glcd_write(font3x5[((c - 32) * 3)+i]);
        }
    }                                         
    else{
        glcd_gotolinecol(line, col);
        for(i=0;i<3;i++){
            glcd_write(font3x5[((c - 32) * 3)+i]);
        }
  
  } 
}
/*--------------  in mot chuoi ky tu trong font3x5 len glcd   ---------------*/
void glcd_puts3x5(unsigned char line, unsigned char col, char *str)
{
    unsigned char i, x;
    x = col;
    for (i=0; str[i]!=0; i++){
        if(x>=128){
            col = 0;
            x = col;
            line++;            
        }          
        glcd_putchar3x5(line, x, str[i]);
        x += 4;
    }
}
/*--------------  in 1 hinh anh dang bitmap len glcd    --------------------*/
void glcd_putbmp( unsigned char *bmp)
{
    unsigned int i;
    unsigned char line, col;
    for(line=0;line<8;line++){  
        glcd_gotolinecol(line, 0);
        for(col=0;col<64;col++){         // chon chip trai
            i = 128*line+col;      
            glcd_write(bmp[i]);
        }
        glcd_gotolinecol(line, 64);
        for(col=64;col<128;col++){      //  chon chip phai
            i = 128*line+col;      
            glcd_write(bmp[i]);
        }
    }
}
/*--------------  in 1 hinh anh dang bitmap da convert len glcd --------------*/
void glcd_putbmp_convert( unsigned char *bmp)
{
    unsigned int i;
    unsigned char line, col;
    for(line=0;line<8;line++){  
        glcd_gotolinecol(line, 0);
        for(col=0;col<64;col++){         // chon chip trai
            i = 128*line+col;      
            glcd_write(~bmp[i]);
        }
        glcd_gotolinecol(line, 64);
        for(col=64;col<128;col++){      //  chon chip phai
            i = 128*line+col;      
            glcd_write(~bmp[i]);
        }
    }    
}
/*----------------------------------------------------------------------------*/
/*------------------------    cac ham ve co ban   ----------------------------*/
/*----------------------------------------------------------------------------*/
// mo ta truc toa do   x(->)  - truc x tu trai sang phai( 0 ... 127)
//                     y(|)   - truc y tu duoi len tren ( 0 ... 63 )
//                        

/*------------------------    ve 1 diem anh    --------------------------------*/
// mo ta :       ve mot diem anh len glcd (den hoac trang)
// Inputs:        x  - toa do x cua diem anh ( 0 ... 127)
//                y  - toa do y cua diem anh ( 0 ... 63 )
void glcd_pixel(unsigned char x, unsigned char y, unsigned char color)
{
   unsigned char old_data, line;
   y = 63-y;                            // chuyen truc y xuoi duoi 
   line = y/8;
   y = y%8;
   
   glcd_gotolinecol(line, x);           // go to line, col to write/read
   old_data = glcd_read(x);             // doc du liieu cot x  
   switch(color){
        case 0:                         // light spot
            clrbit(old_data, y);
        break;                          
        case 1:                         // dark spot
            setbit(old_data, y);
        break;                             
   }                                                
   glcd_gotolinecol(line, x);
   glcd_write(old_data);                // ghi du lieu
}
/*------------------------    kiem tra  mau sac 1 diem   ----------------------*/
// mo ta :       liem tra mot diem anh len glcd (den hoac trang)
// Inputs:        x  - toa do x cua diem anh ( 0 ... 127)
//                y  - toa do y cua diem anh ( 0 ... 63 )
//output          1: neu color=1
//                0: neu color=0;
unsigned char glcd_getpixel(unsigned char x, unsigned char y)
{
    unsigned char data, line;
    y = 63-y;                            // chuyen truc y xuoi duoi 
    line = y/8;
    y = y%8;
    
    glcd_gotolinecol(line, x);           // go to line, col to write/read
    data = glcd_read(x);                // doc du liieu cot x
    
    if(data & (1<<y))
        return 1;
    else
        return 0;
    
}
/*------------------------    ve mot duong thang   ---------------------------*/
// mo ta :         ve mot duong thang len glcd
// Inputs:        (x1, y1) - toa do diem dau
//                (x2, y2) - toa do diem cuoi
//                color - ON hoac OFF
void glcd_line(unsigned char x1, unsigned char y1, 
                unsigned char x2, unsigned char y2, unsigned char color)
{
    signed int x, y, addx, addy, dx, dy;
    signed int P;
    int i;
    dx = abs((signed int)(x2 - x1));
    dy = abs((signed int)(y2 - y1));
    x = x1;
    y = y1;

    if(x1 > x2)
        addx = -1;
    else
        addx = 1;
    if(y1 > y2)
        addy = -1;
    else
        addy = 1;

    if(dx >= dy){
        P = 2*dy - dx;

        for(i=0; i<=dx; ++i){
            glcd_pixel(x, y, color);

            if(P < 0){
                P += 2*dy;
                x += addx;
            } 
            
            else{
                P += 2*dy - 2*dx;
                x += addx;
                y += addy;
            }
        }
    }
    
    else{
        P = 2*dx - dy;

        for(i=0; i<=dy; ++i){
            glcd_pixel(x, y, color);

            if(P < 0){
                P += 2*dx;
                y += addy;
            }
            
            else{
                P += 2*dx - 2*dy;
                x += addx;
                y += addy;
            }
        }
    }
}
/*------------------------    ve mot hinh chu nhat          -----------------*/
// mo ta :         ve mot hinh chu nhat len glcd
// Inputs:        (x1, y1) - toa do diem duoi ben trai
//                (x2, y2) - toa do diem tren ben phai
//                fill  - 1: to mau, 0: khong to mau
//                color - ON hoac OFF
void glcd_rectangle(unsigned char x1, unsigned char y1, 
                    unsigned char x2, unsigned char y2, 
                    unsigned char fill, unsigned char color)
{
   if(fill){
      int y, ymax;                          // Find the y min and max
      if(y1 < y2){
         y = y1;
         ymax = y2;
      }
      else{
         y = y2;
         ymax = y1;
      }

      for(; y<=ymax; ++y)                   // Draw lines to fill the rectangle
         glcd_line(x1, y, x2, y, color);
   }
   else{
      glcd_line(x1, y1, x2, y1, color);      // Draw the 4 sides
      glcd_line(x1, y2, x2, y2, color);
      glcd_line(x1, y1, x1, y2, color);
      glcd_line(x2, y1, x2, y2, color);
   }
}
/*------------------------    ve mot da giac        -----------------*/
// mo ta :         ve da giac
// Inputs:        n - so dinh +1 cua da giac
//                p[] - toa do cac diem cua da giac, 
//                     toa do dau trung toa do cuoi
//                fill  - 1: to mau, 0: khong to mau
//                color - ON hoac OFF
void glcd_polygon(unsigned char n,unsigned char p[], unsigned char fill, unsigned char color){
    int i,j,k,dy,dx;
    int y,temp;
    int a[20][2], xi[20];
    float slope[20];
    if(n>=2){
        if(fill){
            for(i=0;i<n;i++){
                a[i][0]=p[2*i];
                a[i][1]=p[2*i+1];
            }
            
            a[n][0]=a[0][0];
            a[n][1]=a[0][1];
            
            for(i=0;i<n;i++){
                dy=a[i+1][1]-a[i][1];
                dx=a[i+1][0]-a[i][0];

                if(dy==0) slope[i]=1.0;
                if(dx==0) slope[i]=0.0;

                if((dy!=0)&&(dx!=0)){
                    slope[i]=(float) dx/dy;
                }
            }

            for(y=0;y<64;y++){
                k=0;
                for(i=0;i<n;i++){
                    if( ((a[i][1]<=y)&&(a[i+1][1]>y))||((a[i][1]>y)&&(a[i+1][1]<=y))){
                        xi[k]=(int)(a[i][0]+slope[i]*(y-a[i][1]));
                        k++;
                    }
                }

                for(j=0;j<k-1;j++)
                for(i=0;i<k-1;i++){
                    if(xi[i]>xi[i+1]){
                        temp=xi[i];
                        xi[i]=xi[i+1];
                        xi[i+1]=temp;
                    }
                }

                for(i=0;i<k;i+=2){
                    glcd_line(xi[i],y,xi[i+1]+1,y,color);
                }
            }
        }
        
        else{
            glcd_line(p[0],p[1],p[2],p[3],color);
            for(i=1;i<(n-1);i++)
            glcd_line(p[(i*2)],p[((i*2)+1)],p[((i+1)*2)],p[(((i+1)*2)+1)],color);
        }
    }
} 
/*------------------------    ve mot duong tron         ----------------------*/
// mo ta :       ve mot duong tron len glcd
// Inputs:       (x0, y0) toa do tam duong tron
//               r        ban kinh duong tron  
//               fill  - to mau hoac khong
//               color - ON hoac OFF
void glcd_circle(unsigned char x, unsigned char y, 
                unsigned char r, unsigned char fill, unsigned char color)
{
   signed int a, b, P;
   a = 0;
   b = r;
   P = 3 - 2*r;

   do
   {
      if(fill)
      {
         glcd_line(x-a, y+b, x+a, y+b, color);
         glcd_line(x-a, y-b, x+a, y-b, color);
         glcd_line(x-b, y+a, x+b, y+a, color);
         glcd_line(x-b, y-a, x+b, y-a, color);
      }
      else
      {
         glcd_pixel(a+x, b+y, color);
         glcd_pixel(b+x, a+y, color);
         glcd_pixel(x-a, b+y, color);
         glcd_pixel(x-b, a+y, color);
         glcd_pixel(b+x, y-a, color);
         glcd_pixel(a+x, y-b, color);
         glcd_pixel(x-a, y-b, color);
         glcd_pixel(x-b, y-a, color);
      }

      if(P < 0)
         P+= 6 + 4*a++;
      else
         P+= 10 + 4*(a++ - b--);
    } while(a <= b);
}
/*------------------------    ve mot cung tron         ----------------------*/
// mo ta :       ve mot cung tron len glcd
// Inputs:       (xc, yc)             toa do tam cung tron
//               (startang, endang)   goc bat dau va ket thuc cung tron (do)
//               r                    ban kinh cung tron  
//               fill                 - to mau hoac khong
//               color                - ON hoac OFF
void glcd_arc( int xc, int yc, 
                int startang, int endang, 
                int r,unsigned char fill, unsigned char color)
{
    int x, y;
    float ang = (float)startang;
    int xx , yy;
    glcd_pixel(xc, yc, color); 
    if(fill){
        while(ang<=endang){
            ang+=0.5;
            x = xc + (int)(r * cos(ang * PI/180) + 0.5);
            y = yc + (int)(r * sin(ang * PI/180) + 0.5);
            xx = x;
            yy = y;
            glcd_line(xc, yc, xx, yy,color);
        }   
        
    }
    else{
        while(ang<=endang){
            ang+=0.5;
            x = xc + (int)(r * cos(ang * PI/180) + 0.5);
            y = yc + (int)(r * sin(ang * PI/180) + 0.5);
            xx = x;
            yy = y;
            glcd_line(x, y, xx, yy,color);
 
        }
    }
}
/*------------------------    ve mot hinh elipse        ----------------------*/
// mota  :       ve mot hinh elipse len GLCD
// Inputs:       (xc, yc) tam cua hinh elipse
//               a, b do dai 2 truc cua elipse
//               fill - YES or NO
//               color - ON or OFF
void glcd_ellipse(unsigned char xc,unsigned char yc,unsigned char a,unsigned char b,unsigned char fill, unsigned char color)
{
    int x,y;
    float a1,b1,p;
    a1=a*a;
    b1=b*b;
    x=0;
    y=b;
    p=2*(b1/a1)-2*b+1;
    while((b1/a1)*(x/y)<1){
        if(fill){
            glcd_line(xc+x,yc+y,xc-x,yc+y,color);
            glcd_line(xc+x,yc-y,xc-x,yc-y,color);
        }
        else{
            glcd_pixel(xc+x,yc+y,color);
            glcd_pixel(xc-x,yc+y,color);
            glcd_pixel(xc-x,yc-y,color);
            glcd_pixel(xc+x,yc-y,color);
        }
        if(p<0)
            p=p+2*(b1/a1)*(2*x+3);
        else{
            p=p-4*y+2*(b1/a1)*(2*x+3);
            y--;
        }
        x++;
    }
    y=0;
    x=a;
    p=2*(a1/b1)-2*a+1;
    while((a1/b1)*(y/x)<=1){
        if(fill){
            glcd_line(xc+x,yc+y,xc-x,yc+y,color);
            glcd_line(xc+x,yc-y,xc-x,yc-y,color);
        }
        else{
            glcd_pixel(xc+x,yc+y,color);
            glcd_pixel(xc-x,yc+y,color);
            glcd_pixel(xc-x,yc-y,color);
            glcd_pixel(xc+x,yc-y,color);
        }
        if(p<0)
            p=p+2*(a1/b1)*(2*y+3);
        else{
            p=p-4*x+2*(a1/b1)*(2*y+3);
            x--;
        }
    y++;
    }
}
/*------------------------    ve mot hinh hop chu nhat     ------------------*/
// mo ta :         ve mot hinh hop chu nhat len glcd
// Inputs:        (x1, y1) - toa do diem dau
//                (x2, y2) - toa do diem cuoi
//                width  - do rong cua hinh hop
//                color - ON hoac OFF
void glcd_bar(unsigned char x1, unsigned char y1, 
              unsigned char x2, unsigned char y2, 
              unsigned char width, unsigned char color)
{
   signed int  x, y, addx, addy, j;
   signed int P, dx, dy, c1, c2;
   int i;
   dx = abs((signed int)(x2 - x1));
   dy = abs((signed int)(y2 - y1));
   x = x1;
   y = y1;
   c1 = -dx*x1 - dy*y1;
   c2 = -dx*x2 - dy*y2;

   if(x1 > x2)
   {
      addx = -1;
      c1 = -dx*x2 - dy*y2;
      c2 = -dx*x1 - dy*y1;
   }
   else
      addx = 1;
   if(y1 > y2)
   {
      addy = -1;
      c1 = -dx*x2 - dy*y2;
      c2 = -dx*x1 - dy*y1;
   }
   else
      addy = 1;

   if(dx >= dy)
   {
      P = 2*dy - dx;

      for(i=0; i<=dx; ++i)
      {
         for(j=-(width/2); j<width/2+width%2; ++j)
         {
            if(dx*x+dy*(y+j)+c1 >= 0 && dx*x+dy*(y+j)+c2 <=0)
               glcd_pixel(x, y+j, color);
         }
         if(P < 0)
         {
            P += 2*dy;
            x += addx;
         }
         else
         {
            P += 2*dy - 2*dx;
            x += addx;
            y += addy;
         }
      }
   }
   else
   {
      P = 2*dx - dy;

      for(i=0; i<=dy; ++i)
      {
         if(P < 0)
         {
            P += 2*dx;
            y += addy;
         }
         else
         {
            P += 2*dx - 2*dy;
            x += addx;
            y += addy;
         }
         for(j=-(width/2); j<width/2+width%2; ++j)
         {
            if(dx*x+dy*(y+j)+c1 >= 0 && dx*x+dy*(y+j)+c2 <=0)
               glcd_pixel(x+j, y, color);
         }
      }
   }
}


/*------------------------    ve mot hinh khoi hop      ----------------------*/
// mota :       ve mot hinh khoi len glcd
// Inputs:        4 dinh cua hinh khoi
//                fill  - YES or NO
//                color - ON or OFF
void glcd_cuboid(unsigned char x11,unsigned char y11,
            unsigned char x12,unsigned char y12,
            unsigned char x21,unsigned char y21,
            unsigned char x22,unsigned char y22,
            unsigned char color)
{
    glcd_rectangle(x11,y11,x12,y12,0,color);
    glcd_rectangle(x21,y21,x22,y22,0,color);
    glcd_line(x11,y11,x21,y21,color);
    glcd_line(x12,y11,x22,y21,color);
    glcd_line(x11,y12,x21,y22,color);
    glcd_line(x12,y12,x22,y22,color);
}
