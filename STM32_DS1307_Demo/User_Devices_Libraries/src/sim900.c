#include "sim900.h"
#include "lcd16x2.h"
#include "my_uart.h"
#include "string.h"
#include "delay.h"
#include "stdio.h"
#include "def_stm32f10x.h"
extern char USART1_Buffer[255];
extern uint8_t Receiver_Ok,New_MSG;

void Clear_Buffer(void)
{
	uint8_t i;
	for(i=0;i<254;i++)
				USART1_Buffer[i]='\0';
}

void test(void)
{
	char str_return[9]={0x0D,0x0A,0x0D,0X0A,'O','K',0x0D,0X0A,'\0'};
	char *LENH="AT";
	char cmd[50];
	sprintf(cmd,"%s%s",LENH,str_return);
	LCD_GotoXY(0,0);
	LCD_PutStr(cmd);
	LCD_GotoXY(0,1);
	LCD_PutStr("TEST LCD");
	delay_ms(5000);
}
void Send_ATCommand(char *Command)
{
	USART_PutString(USART1,Command);
	USART_PutChar(USART1,0x0D);
	USART_PutChar(USART1,0x0A);
}

uint8_t Send_ATCommand_Ok(char *Command)
{
	uint8_t n;
	uint32_t TimeOut=5000;
	Clear_Buffer();
	Send_ATCommand(Command);
	while(Receiver_Ok==0 && TimeOut>0)
	{
		delay_ms(1);
		TimeOut--;
	}
	if(Receiver_Ok)
	{
		Receiver_Ok=0;
		n=strlen(USART1_Buffer);
		if(USART1_Buffer[n-4]=='O' && USART1_Buffer[n-3]=='K')
		{
			return OK;
		}		
		else
			return ERROR;
	}
	return ERROR;
}

void Send_SMS(char *phone_num, char *text)
{
	char buff[50];
	Clear_Buffer();
	sprintf(buff,"AT+CMGS=\"%s\"",phone_num);
	Send_ATCommand(buff);
	delay_ms(1500);
	USART_PutString(USART1,text);
	USART_PutChar(USART1,0x1A);
	Receiver_Ok=0;
	Clear_Buffer();
}

void Send_DATA(char *DATA)
{
	char buff[50];
	Clear_Buffer();
	sprintf(buff,"AT+CIPSEND");
	Send_ATCommand(buff);
	delay_ms(3000);
	USART_PutString(USART1,DATA);
	USART_PutChar(USART1,0x1A);
	Receiver_Ok=0;
	Clear_Buffer();
}
void Call(char *phone_num)
{
	char buff[50];
	Clear_Buffer();
	sprintf(buff,"ATD%s;",phone_num);
	Send_ATCommand(buff);
}

uint8_t SIM900_Init(void)
{
	uint8_t n;
	LCD_GotoXY(0,0);
	LCD_PutStr("SIM900 Initting...");
	if(Send_ATCommand_Ok("AT"))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT OK           ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT ERROR        ");
		return 0;
	}

	if(Send_ATCommand_Ok("AT+CMGF=1"))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CMGF=1 OK    ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CMGF=1 ERROR  ");
		return 0;
	}	
	
	if(Send_ATCommand_Ok("AT+CIPSHUT"))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CIPSHUT OK    ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CIPSHUT ERROR ");
		return 0;
	}	
	
	if(Send_ATCommand_Ok("AT+CIPMUX=0"))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CIPMUX OK     ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CIPMUX ERROR  ");
		return 0;
	}	
	
	if(Send_ATCommand_Ok("AT+CGATT=1"))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CGATT OK      ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CGATT ERROR   ");
		return 0;
	}	
	
	if(Send_ATCommand_Ok("AT+CSTT=\"WWW\",\"\",\"\""))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CSTT OK       ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CSTT ERROR"    );
		return 0;
	}	
	/*
	if(Send_ATCommand_Ok("AT+CIICR"))
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CIICR OK     ");
		delay_ms(1000);
	}
	else
	{
		LCD_GotoXY(0,1);
		LCD_PutStr("AT+CIICR ERROR");
		return 0;
	}	
	*/
	
	Send_ATCommand("AT+CIICR");
	delay_ms(5000);
	if(Receiver_Ok)
	{
		Receiver_Ok=0;
		n=strlen(USART1_Buffer);
		if(USART1_Buffer[n-4]=='O' && USART1_Buffer[n-3]=='K')
		{
			LCD_GotoXY(0,1);
			LCD_PutStr("AT+CIICR OK        ");
			delay_ms(1000);
			return OK;
		}
		else
		{
			LCD_GotoXY(0,1);
			LCD_PutStr("AT+CIICR ERROR     ");
			return ERROR;
		}
	}
	return 1;
}

void SIM900_GetIP(char IP_Buffer[20])
{
	uint8_t n,i=0;
	uint32_t TimeOut=2000;
	Clear_Buffer();
	Receiver_Ok=0;
	Send_ATCommand("AT+CIFSR");
	while(Receiver_Ok==0 && TimeOut>0)
	{
		delay_ms(1);
		TimeOut--;
	}
	
	if(Receiver_Ok)
	{
		Receiver_Ok=0;
		for(n=12;n<30;n++)
		{
			if(USART1_Buffer[n]==0x0d)
				break;
			else
				IP_Buffer[i++]=USART1_Buffer[n];
		}
	}
}
uint8_t Connect_Server(char *IP_Addr,char *PORT)
{
	
	char cmd[100];
	char result[50];
	//uint16_t n;
	uint8_t status_connect=0;
	uint8_t i,j=0;
	RE_CONNECT:
	LCD_GotoXY(0,0);
	LCD_PutStr("CONNECT TING...  ");
	LCD_GotoXY(0,1);
	LCD_PutStr("                ");
	Send_ATCommand_Ok("AT+CIPSHUT");
	delay_ms(2000);
	Clear_Buffer();
	Receiver_Ok=0;
	sprintf(cmd,"AT+CIPSTART=\"TCP\",\"%s\",\"%s\"",IP_Addr,PORT);
	Send_ATCommand(cmd);
	if(Send_ATCommand_Ok(cmd))
	{
		Clear_Buffer();
		while(!status_connect)
		{
			if(Receiver_Ok)
			{
				for(i=0;i<strlen(USART1_Buffer);i++)
					{
						if(USART1_Buffer[i]>=32 && USART1_Buffer[i]<=126)
							result[j++]=USART1_Buffer[i];
						/*
						if(USART1_Buffer[i]==0x0d)
							break;
						else
							result[i-2]=USART1_Buffer[i];
						*/
					}
				if(!strcmp(result,"CONNECT OK"))
					{
						Clear_Buffer();
						LCD_GotoXY(0,1);
						LCD_PutStr(result);
						delay_ms(2000);
						return OK;
					}
				if(!strcmp(result,"STATE: TCP CLOSEDCONNECT FAIL"))
					{
						Clear_Buffer();
						LCD_GotoXY(0,1);
						LCD_PutStr(result);
						delay_ms(2000);
						return ERROR;
					}
				Clear_Buffer();
				Receiver_Ok=0;
				delay_ms(1000);
				status_connect=1;
			}
		}
	}
	else
		goto RE_CONNECT;
	
	return OK;
}

void DATA_Process(void)
{
	char result[50];
	uint8_t i,j=0;
	//LCD_Clear();
	LCD_GotoXY(0,0);
	LCD_PutStr("RECEIVER DATA...  ");
	LCD_GotoXY(0,1);
	LCD_PutStr("                ");
	if(Receiver_Ok)
	{
		for(i=0;i<strlen(USART1_Buffer);i++)
		{
			if(USART1_Buffer[i]>=32 && USART1_Buffer[i]<=126)
				result[j++]=USART1_Buffer[i];
		}
		if(!strcmp(result,"ON LED1"))
			PORTB_0=0;
		if(!strcmp(result,"OFF LED1"))
			PORTB_0=1;
		if(!strcmp(result,"ON LED2"))
			PORTB_1=0;
		if(!strcmp(result,"OFF LED2"))
			PORTB_1=1;
		LCD_GotoXY(0,1);
		LCD_PutStr(result);
		for(i=0;i<50;i++)
			result[i]=0;
		Clear_Buffer();
		Receiver_Ok=0;
		delay_ms(1000);
	}
}
