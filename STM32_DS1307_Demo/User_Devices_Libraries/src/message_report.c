//#include "message.h"

//#include "output.h"
#include "main.h"


extern char ID_MODULE[65];
//Cac bien duoc su dung trong hoat dong module
extern uint8_t sys_mode;

//Cac bien su dung cho ouput mode
//Mode bom doi luu
extern Mode_Pump_TypeDef 															Mode_Convection_Pump;
//Mode bom cap nuoc lanh
extern Mode_Pump_TypeDef 															Mode_Cold_Water_Supply_Pump;
//Mode bom hoi duong ong
extern Mode_Pump_TypeDef 															Mode_Return_Pump;
//Mode bom tang ap
extern Mode_Pump_TypeDef 															Mode_Incresed_pressure_Pump;
//Mode bom nhiet bon gia nhiet
extern Mode_Pump_TypeDef															Mode_Heat_Pump;
//Mode dien tro nhiet
extern Mode_Heater_Resistor_TypeDef										Mode_Heater_Resistor;
//Mode van dien tu 3 nga
extern Mode_Valve_TypeDef															Mode_Three_Way_Valve;
//mode van dien tu 1 chieu
extern Mode_Valve_TypeDef															Mode_Blakflow_Valve;


//Cac bien cho Parametter config
//Nguong dieu khien bom doi luu
extern Para_Convection_Pump_TypeDef 										Para_Convection_Pump;
//Nguong dieu khien bom cap nuoc lanh
extern Para_Cold_Water_Supply_Pump_TypeDef 							Para_Cold_Water_Supply_Pump;
//Nguong dieu khien bom hoi duong ong
extern Para_Return_Pump_TypeDef 												Para_Return_Pump;
//Nguong dieu khien bom tang ap
extern Para_Incresed_pressure_Pump_TypeDef							Para_Incresed_pressure_Pump;
//Nguong dieu khien bom bon gia nhiet
extern Para_Heat_Pump_TypeDef														Para_Heat_Pump;
//Nguong dieu khien dien tro nhiet
extern Para_Heater_Resistor_TypeDef Para_Heater_Resistor;
//Nguong dieu khien van dien tu 3 nga
extern Para_Three_Way_Valve_TypeDef											Para_Three_Way_Valve;
//Nguong dieu khien van 1 chieu
extern Para_Blakflow_Valve_TypeDef 											Para_Blakflow_Valve;

//Bien dung cho Timer Counter config
extern Timer_Counter_TypeDef 														Timer_Counter_Config;

//Cac bien chua gia tri sensor
//cam bien nhiet do
extern Temp_Sensor_TypeDef															Temp_Sensor;
//cam bien ap suat
extern Pressure_Sensor_TypeDef													Pressure_Sensor;
//cam bien muc nuoc
extern Water_Level_TypeDef															Water_Level;
//cam bien cuong do nang luong mat troi
extern Lingh_Intensity_TypeDef													Lingh_Intensity;


//Bien chua trang thai hoat dong cua tai
extern Devices_Status_TypeDef													Devices_Status;

//Cac ham trong file message_report.c
/***************************************************************************/
//Reprot system mode module
uint8_t Sys_Mode_Report(void)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};

	Debug_Display("Nhan duoc ban tin check system mode");
	strcat(Buffer_TX,SYS_MODE_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung cho ban tin va gui di");
	convertbin2str(sys_mode,buffer_str);
	strcat(Buffer_TX,IE_SYS_MODE);
	strcat(Buffer_TX,buffer_str);
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);;
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));
	
	return Val_Return;
}
/***************************************************************************/

/***************************************************************************/
//Report Output mode module
uint8_t Output_Mode_Report(void)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};

	Debug_Display("Nhan duoc ban tin check output mode");
	strcat(Buffer_TX,OUT_MODE_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung cho ban tin va gui di");
	//Add header IE output mode
	strcat(Buffer_TX,IE_OUT_MODE);
	
	//Add mode bom doi luu v�o buffer truyen
	convertbin2str(Mode_Convection_Pump.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add time bom doi luu v�o buffer truyen
	convertbin2str(Mode_Convection_Pump.time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong bom doi luu vao buffer truyen
	convertbin2str(Mode_Convection_Pump.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode bom cap nuoc lanh v�o buffer truyen
	convertbin2str(Mode_Cold_Water_Supply_Pump.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add time bom cap nuoc lanh v�o buffer truyen
	convertbin2str(Mode_Cold_Water_Supply_Pump.time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong bom cap nuoc lanh vao buffer truyen
	convertbin2str(Mode_Cold_Water_Supply_Pump.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode bom hoi duong ong v�o buffer truyen
	convertbin2str(Mode_Return_Pump.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add time bom hoi duong ong v�o buffer truyen
	convertbin2str(Mode_Return_Pump.time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong bom hoi duong ong vao buffer truyen
	convertbin2str(Mode_Return_Pump.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode bom tang ap v�o buffer truyen
	convertbin2str(Mode_Incresed_pressure_Pump.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add time bom tang ap v�o buffer truyen
	convertbin2str(Mode_Incresed_pressure_Pump.time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong bom tang ap vao buffer truyen
	convertbin2str(Mode_Incresed_pressure_Pump.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode bom nhiet bon gia nhiet v�o buffer truyen
	convertbin2str(Mode_Heat_Pump.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong bom nhiet bon gia nhiet vao buffer truyen
	convertbin2str(Mode_Heat_Pump.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode Dien tro nhiet bon gia nhiet v�o buffer truyen
	convertbin2str(Mode_Heater_Resistor.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add nguong nhiet do delat t2 cua dien tro nhiet v�o buffer truyen
	convertbin2str(Mode_Heater_Resistor.delta_t2,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add nguong thoi gian delta t kich hoat R2 v�o buffer truyen
	convertbin2str(Mode_Heater_Resistor.delta_time_r2,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong vao khung truyen
	convertbin2str(Mode_Heater_Resistor.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode van 3 nga v�o buffer truyen
	convertbin2str(Mode_Three_Way_Valve.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong van 3 nga vao buffer truyen
	convertbin2str(Mode_Three_Way_Valve.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add mode van 1 chieu v�o buffer truyen
	convertbin2str(Mode_Blakflow_Valve.mode,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add byte du phong van 1 chieu vao buffer truyen
	convertbin2str(Mode_Blakflow_Valve.reserved,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);;
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));

	return Val_Return;
}
/***************************************************************************/

/***************************************************************************/
//Report Parameter module
uint8_t Parameter_Report(void)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};

	Debug_Display("Nhan duoc ban tin check parameter");
	strcat(Buffer_TX,PARAMETER_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung cho ban tin va gui di");
	//Add header IE Parametter mode
	strcat(Buffer_TX,IE_PARAM);
	
	//Add nguong dieu khien bom doi luu v�o buffer truyen
	convertbin2str(Para_Convection_Pump,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien bom cap nuoc lanh v�o buffer truyen
	//add muc nuoc M1
	convertbin2str(Para_Cold_Water_Supply_Pump.M1,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add muc nuoc M2
	convertbin2str(Para_Cold_Water_Supply_Pump.M2,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien bom hoi duong ong v�o buffer truyen
	//add dau khung thoi gian t1 hh
	convertbin2str(Para_Return_Pump.Time_T1_Hour,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add dau khung thoi gian t1 mm
	convertbin2str(Para_Return_Pump.Time_T1_Min,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add dau khung thoi gian t2 hh
	convertbin2str(Para_Return_Pump.Time_T2_Hour,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add dau khung thoi gian t2 mm
	convertbin2str(Para_Return_Pump.Time_T2_Min,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add nguong nhiet do delta t
	convertbin2str(Para_Return_Pump.Delta_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien bom tang ap v�o buffer truyen
	convertbin2str(Para_Incresed_pressure_Pump,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien bon gia nhiet v�o buffer truyen
	convertbin2str(Para_Heat_Pump,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien dien tro nhiet v�o buffer truyen
	//Add nguong thoi gian delta t
	convertbin2str(Para_Heater_Resistor.Delta_Time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//Add nguong nhiet do delta t
	convertbin2str(Para_Heater_Resistor.Delta_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien van dien tu 3 nga v�o buffer truyen
	//add dau khung thoi gian t1 hh
	convertbin2str(Para_Three_Way_Valve.Time_T1_Hour,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add dau khung thoi gian t1 mm
	convertbin2str(Para_Three_Way_Valve.Time_T1_Min,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add dau khung thoi gian t2 hh
	convertbin2str(Para_Three_Way_Valve.Time_T2_Hour,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add dau khung thoi gian t2 mm
	convertbin2str(Para_Three_Way_Valve.Time_T2_Min,buffer_str);
	strcat(Buffer_TX,buffer_str);
	//add nguong nhiet do delta t
	convertbin2str(Para_Three_Way_Valve.Delta_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add nguong dieu khien van 1 chieu v�o buffer truyen
	convertbin2str(Para_Blakflow_Valve,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);;
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));
	
	return Val_Return;
}
/***************************************************************************/

/***************************************************************************/
//Report Timer Counter module
uint8_t Timer_Counter_Report(void)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};

	Debug_Display("Nhan duoc ban tin check timer counter mode");
	strcat(Buffer_TX,TC_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung cho ban tin va gui di");
	//Add header IE Timer counter
	strcat(Buffer_TX,IE_TC);
	
	//Add Confirm time v�o buffer truyen
	convertbin2str(Timer_Counter_Config.Confirm_Time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add Resend time v�o buffer truyen
	convertbin2str(Timer_Counter_Config.Resend_Time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add Report time v�o buffer truyen
	convertbin2str(Timer_Counter_Config.Report_Time,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add Counter limit v�o buffer truyen
	convertbin2str(Timer_Counter_Config.MSG_Counter_Limit,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);;
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));

	return Val_Return;
}
/***************************************************************************/

/***************************************************************************/
//Report sensor value and on/off status report
uint8_t Sys_Status_Report(void)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};


	Debug_Display("Nhan duoc ban tin check status report");
	strcat(Buffer_TX,SYS_STT_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung sensor value cho ban tin");
	//Add header IE Sensor value 
	strcat(Buffer_TX,IE_SENSOR_VALUE);

	//Add cam bien nhiet do dan thu v�o buffer truyen
	convertbin2str(Temp_Sensor.Solar_Panel_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);

	//Add cam bien nhiet do bon Solar v�o buffer truyen
	convertbin2str(Temp_Sensor.Solar_Tank_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien muc nuoc bon Solar v�o buffer truyen
	convertbin2str(Water_Level.Water_Solar_Tank_level,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien nhiet do bon gia nhiet v�o buffer truyen
	convertbin2str(Temp_Sensor.Heater_Tank_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien ap suat bon gia nhiet v�o buffer truyen
	convertbin2str(Pressure_Sensor.Heater_Tank_Pressure,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien buc xa dan thu v�o buffer truyen
	convertbin2str(Lingh_Intensity,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien nhiet dinh bon Soler v�o buffer truyen
	convertbin2str(Temp_Sensor.Top_Solar_Tank_Temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien tran v�o buffer truyen
	convertbin2str(Water_Level.Water_Overflow,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien ap suat duong ong v�o buffer truyen
	convertbin2str(Pressure_Sensor.Pipeline_Pressure,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien nhiet do duong ong 1 v�o buffer truyen
	convertbin2str(Temp_Sensor.Pipeline_Temp_1,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add cam bien nhiet do duong ong 2 v�o buffer truyen
	convertbin2str(Temp_Sensor.Pipeline_Temp_2,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add byte du phong cho sensor report
	strcat(Buffer_TX,RESERVED_SENSOR);
	
	//Add thanh phan On/OFF status
	//Add header IE On/OFF status 
	strcat(Buffer_TX,IE_ON_OFF_STT);
	
	//Add trang thai cua tai vao buffer truyen
	convertbin2str((Devices_Status.Convection_Pump_1|Devices_Status.Convection_Pump_2|Devices_Status.Cold_Water_Supply_Pump_1|Devices_Status.Cold_Water_Supply_Pump_2),buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add trang thai cua tai vao buffer truyen
	convertbin2str((Devices_Status.Return_Pump_1|Devices_Status.Return_Pump_2|Devices_Status.Incresed_pressure_Pump_1|Devices_Status.Incresed_pressure_Pump_2),buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add trang thai cua tai vao buffer truyen
	convertbin2str((Devices_Status.Heat_Pump|Devices_Status.Heater_Resistor_1|Devices_Status.Heater_Resistor_2|Devices_Status.Three_Way_Valve),buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	//Add trang thai cua tai vao buffer truyen
	convertbin2str(Devices_Status.Blakflow_Valve,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));
	Debug_Display("Solar Panel Temp = ");
	Debug_Display_num(Temp_Sensor.Solar_Panel_Temp);
	Debug_Display("Pipeline_Temp_2 = ");
	Debug_Display_num(Temp_Sensor.Pipeline_Temp_2);
	Debug_Display("Heater_Tank_Temp = ");
	Debug_Display_num(Temp_Sensor.Heater_Tank_Temp);
	Debug_Display("Pipeline_Temp_1 = ");
	Debug_Display_num(Temp_Sensor.Pipeline_Temp_1);
	Debug_Display("Top_Solar_Tank_Temp = ");
	Debug_Display_num(Temp_Sensor.Top_Solar_Tank_Temp);
	Debug_Display("Solar_Tank_Temp = ");
	Debug_Display_num(Temp_Sensor.Solar_Tank_Temp);
	//Debug_Display("Pipeline_Pressure = ");
	//Debug_Display_num(Pressure_Sensor.Pipeline_Pressure)
	return Val_Return;
}
/***************************************************************************/


uint8_t Alarm_Report(uint8_t Alarm)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};
	uint8_t temp = Alarm;

	Debug_Display("Gui Alarm report");
	strcat(Buffer_TX,ALARM_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung sensor value cho ban tin");
	//Add header IE Sensor value 
	strcat(Buffer_TX,IE_ALARM);

	//Add trang thai alarm vao buffer truyen
	convertbin2str(temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));
	Val_Return = Watting_Alarm_Confirm_Message(20000,Alarm);
	
	
	return Val_Return;
}

//Ban tin gui huy canh bao len cho server
uint8_t Cancel_Alarm_Report(uint8_t Alarm)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char buffer_str[9]={'\0'};
	uint8_t temp = Alarm;

	Debug_Display("Gui Cancel Alarm report");
	strcat(Buffer_TX,ALARM_CANCEL);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	Debug_Display("Add noi dung sensor value cho ban tin");
	//Add header IE Sensor value 
	strcat(Buffer_TX,IE_ALARM);

	//Add trang thai alarm vao buffer truyen
	convertbin2str(temp,buffer_str);
	strcat(Buffer_TX,buffer_str);
	Debug_Display(Buffer_TX);
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);
	Debug_Display(Buffer_TX);
	Debug_Display_num(strlen(Buffer_TX));
	Val_Return = Watting_Alarm_Confirm_Message(20000,Alarm);
	
	
	return Val_Return;
}

//Ban tin gui bao cao cho server
uint8_t Send_MSG_Report(uint8_t msg_name)
{
	uint8_t Val_Return = 0;
	uint8_t msg_info = msg_name;
	

	//Neu la ban tin check system mode.
	if(msg_info == CheckSysMode)
	{
		Val_Return = Sys_Mode_Report();
	}
	//Neu la ban tin Output mode report
	if(msg_info == CheckOutMode)
	{
		Val_Return = Output_Mode_Report();
	}
	
	//Neu la ban tin Parametter report
	if(msg_info == CheckParam)
	{
		Val_Return = Parameter_Report();
	}
	
	//Neu la ban tin Timer Counter report
	if(msg_info == CheckTC)
	{
		Val_Return = Timer_Counter_Report();
	}
	
	//Neu la ban tin Check System Status
	if(msg_info == CheckSysStatus)
	{
		Val_Return = Sys_Status_Report();
	}
	
	return Val_Return;
}


//Function nay viet ngat 15/11/2017
//Thoi diem hoan thien tinh nang nap tien
//chuc nang: Gui ban tin bao noi dung thong tin tai khoan len server
//Tham so truyen vao: Chuoi thong tin nap tien/kiem tra tien
uint8_t Account_Report(char *Cmd)
{
	uint8_t Val_Return = 0;
	char Buffer_TX[512] = {'\0'}; 
	char respond[300]={0};

	//Goi ham check tai khoan trong module
	Sim90X_CheckMoney(Cmd,respond);
	Debug_Display(respond);
	strcat(Buffer_TX,ACC_REPORT);

	//Update ID v�o Buffer truyen du lieu
	strcat(Buffer_TX,IE_ID);
	strcat(Buffer_TX,ID_MODULE);

	//Add header IE Sensor value 
	strcat(Buffer_TX,IE_MONEY_NOTIFY);
	strcat(Buffer_TX,"&&");
	strcat(Buffer_TX,respond);
	strcat(Buffer_TX,"##");
	Debug_Display(Buffer_TX);
	Val_Return = Sim908_GPRS_SendData(Buffer_TX);
	return Val_Return;
}

