#ifndef __SIM900_H
#define __SIM900_H

#include "stm32f10x.h"

#define OK	1
#define ERROR	0


void Clear_Buffer(void);
void Send_ATCommand(char *Command);
uint8_t Send_ATCommand_Ok(char *Command);
void Send_SMS(char *phone_num, char *text);
void Call(char *phone_num);
uint8_t SIM900_Init(void);
void SIM900_GetIP(char *IP_Buffer);
uint8_t Connect_Server(char *IP_Addr,char *PORT);
void DATA_Process(void);
void Send_DATA(char *DATA);
void test(void);
#endif

