#ifndef __DELAY_H
#define __DELAY_H

#include "main.h"

void delay_us(__IO uint32_t nus);
void delay_ms(__IO uint32_t nms);


#endif
