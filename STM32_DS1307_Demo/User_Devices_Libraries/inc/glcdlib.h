/* Do an: Viet thu vien giao tiep voi graphic lcd 128x64 su dung chip atmega128     */
/* chuong trinh: glcdlib.h                                                          */
/* Nguoi thuc hien : Truong Van Bac                                                 */
/* Ngay/ Thang/ Nam: 29/01/2013                                                     */

// chip su dung atmega128, dong GLCD samsung ks0108, thach anh 16 MHz
#ifndef __GLCD12864_H
#define __GLCD12864_H

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f10x.h"
#include "def_stm32f10x.h"
#include "delay.h"


#define PI 3.1415926536
#define setbit(Port, Bit) (Port |= 1<<(Bit))
#define clrbit(Port, Bit) (Port &= ~(1<<(Bit)))


// dinh nghia port dieu khien
#ifdef KITSTM32

#define DATA_OUT    PORTA
#define DATA_IN     PINA

#define LCD_RS		PORTB_12
#define LCD_RW          PORTB_13
#define LCD_EN		PORTB_14

#define LCD_D0		PORTA_0
#define LCD_D1		PORTA_1
#define LCD_D2		PORTA_2
#define LCD_D3		PORTA_3
#define LCD_D4		PORTA_4
#define LCD_D5		PORTA_5
#define LCD_D6		PORTA_6
#define LCD_D7		PORTA_7

#define LCD_CS1   PORTB_11
#define LCD_CS2		PORTB_10

#else
// dinh nghia port du lieu
#define DATA_OUT    PORTC
#define DATA_IN     PINC

#define LCD_RS		PORTC_15
#define LCD_RW    PORTC_14
#define LCD_EN		PORTC_13

#define LCD_D0		PORTC_0
#define LCD_D1		PORTC_1
#define LCD_D2		PORTC_2
#define LCD_D3		PORTC_3
#define LCD_D4		PORTC_4
#define LCD_D5		PORTC_5
#define LCD_D6		PORTC_6
#define LCD_D7		PORTC_7

#define LCD_CS1   PORTC_8
#define LCD_CS2		PORTC_9

//#define LCD_CS1   PORTA_13
//#define LCD_CS2		PORTA_14
#endif
// dinh nghia cac chan dieu khien




// code cac lenh
#define GLCD_DISPLAY    0x003E      // ON/OFF GLCD 
#define GLCD_YADDRESS   0x0040      // set Y(collum) address
#define GLCD_XADDRESS   0x00B8      // set X(line) address
#define GLCD_STARTLINE  0x00C0      // set display start line



/*----------------------cac ham su dung trong thu vien----------------------------------------*/
/*-------------------------------------------------------------------------------------------------------
// cac ham dieu khien 
     void glcd_delay(void);                                         // ham de lay 16 chu ky may
     void glcd_out_set(void);                                       // ham set giao tiep tu AVR => GLCD 
     void glcd_in_set(void);                                        // ham set giao tiep tu glcd => GLCD
     void glcd_set_side( unsigned char side);                       // ham chon chip ks0108: 
                                                                           // side = 0 chip trai
                                                                           // side = 1 chip phai                                                                 
     void glcd_wait(void);                                          // ham cho glcd ranh
     void glcd_set_display( unsigned char i);                       // ham bat, tat hien thi len GLCD
                                                                           // i = 1 ON
                                                                           // i = 0 OFF
     void glcd_set_yaddress( unsigned char col);                    // ham chon dia chi cot 
                                                                           // col = 0:127
     void glcd_set_xaddress( unsigned char line);                   // ham chon dia chi hang
                                                                           // line = 0:7
     void glcd_set_startline(unsigned char offset);                 // ham chon dong startline
     void glcd_write( unsigned char data);                          // ham ghi du lieu/ lenh len GLCD
     unsigned char glcd_read(unsigned char column);                 // ham doc du lieu hien tai cua GLCD
     void glcd_init(void);                                          // ham khoi dong GLCD
     void glcd_gotolinecol( unsigned char line, unsigned char col); // ham di chuyen den dia chi hang line cot col
                                                                           // line = 0:7
                                                                           // col = 0:127
     void glcd_clear(void);                                         // ham xoa man hinh GLCD
     void glcd_clearcol(unsigned char col);                         // ham xoa cot col
                                                                           // col = 0:127
     void glcd_clearcol(unsigned char col);                         // ham xoa hang line
                                                                           // line = 0:7
// cac ham lam viec voi text                                                                            
     void glcd_putchar(5x7)(unsigned char line,                    // ham in moi ky tu trong font hoac font5x7 
                        unsigned char col,                         // tai vi tri line col cua GLCD
                        unsigned char c);
     void glcd_puts(5x7)(unsigned char line,                        // ham in mot chuoi len GLCD bat dau 
                        unsigned char col,                          // tu vi tri line col
                        char* str);
     void glcd_putsf(unsigned char line,                           // ham in mot chuoi tu bo nho flash
                        unsigned char col, 
                        flash char* str);
     void glcd_putse(unsigned char line,                           // ham in mot chuoi tu bo nho eeprom
                        unsigned char col, 
                        eeprom char* str);
     void glcd_putbmp(flash unsigned char *bmp);                    // ham in mot bitmap len GLCD
// cac ham ve co ban
// support : tao do GLCD duoc dung tren truc xOy
//          O(0, 0)
//          x = 0:127    y = 0:63
//          color = 0: cham khong mau 
//                = 1: cham den
//          fill  = 0: khong to mau
//                = 1: co to mau cho hinh
                     
     void glcd_pixel(unsigned char x,                              // ham ve mot diem anh tai vi tri toa do (x, y)
                unsigned char y,                           
                unsigned char color);
     unsigned char glcd_getpixel(unsigned char x,                  // ham tra ve gia tri mau tai diem toa do (x, y)
                        unsigned char y)                        
     void glcd_line(unsigned char x1, unsigned char y1,            // ham ve 1 duong thang tu diem (x1, y1) den diem (x2, y2)
                unsigned char x2, unsigned char y2, 
                unsigned char color);
     void glcd_rectangle(unsigned char x1, unsigned char y1,       // ham ve 1 hinh chu nhat 
                unsigned char x2, unsigned char y2,                       // (x1, y1) tao do dinh duoi ben trai
                unsigned char fill, unsigned char color);                  // (x2, y2) tao do dinh tren ben phai 
     void glcd_polygon(unsigned char n,unsigned char p[],          // ham ve mot da giac n-1 dinh
                unsigned char color)                               // p[]: tap hop toa do cac dinh cua da giac
     void glcd_circle(unsigned char x, unsigned char y,            // ham ve mot hinh tron
                unsigned char r, unsigned char fill,                    // (x, y) tao do tam hinh tro 
                unsigned char color);                              // r ban kinh cua hinh tron
     void glcd_ellipse(unsigned char xc,unsigned char yc,          // (x0, y0):toa do tam elip
                unsigned char a,unsigned char b,                   // a, b: khoang cach truc
                unsigned char fill, unsigned char color))
     void glcd_bar(unsigned char x1, unsigned char y1,             // ham ve mot hinh hop chu nhat voi do day width
                unsigned char x2, unsigned char y2,                     // (x1, y1) tao do diem dau
                unsigned char width, unsigned char color);               // (x2, y2) tao do diem cuoi
     void glcd_cuboid(unsigned char x11,unsigned char y11,         // ham ve mot hinh khoi
                unsigned char x12,unsigned char y12,                    // (x11, y11), (x12, y12), (x21, y21), (x22, y22)
                unsigned char x21,unsigned char y21,                    // tao do 4 dinh cua hinh khoi
                unsigned char x22,unsigned char y22,
                unsigned char color);
                                                                             
-------------------------------------------------------------------------------------------------------------------------------------*/
void glcd_delay(void);
void delay_en(uint32_t time);
void mcu_set_pin_out(void);
void mcu_set_pin_in(void);
void glcd_out_set(void);
void glcd_in_set(void);
void glcd_set_side( unsigned char side);
void glcd_wait(void);
void glcd_set_display( unsigned char i);
void glcd_set_yaddress( unsigned char col);
void glcd_set_xaddress( unsigned char line);
void glcd_set_startline(unsigned char offset);
void glcd_write( unsigned char data);
unsigned char glcd_read(unsigned char column);
void glcd_init(void);
void glcd_gotolinecol( unsigned char line, unsigned char col);
void glcd_full(void);
void glcd_clear(void);
void glcd_clearcol(unsigned char col);
void glcd_clearline(unsigned char line);
void glcd_putchar(unsigned char line, unsigned char col, unsigned char c);
void glcd_puts(unsigned char line, unsigned char col, char* str);
void glcd_putsf(unsigned char line, unsigned char col,  char* str);
void glcd_putse(unsigned char line, unsigned char col,  char* str);
void glcd_putchar5x7(unsigned char line, unsigned char col, unsigned char c);
void glcd_puts5x7(unsigned char line, unsigned char col, char *str);
void glcd_putchar3x5(unsigned char line, unsigned char col, unsigned char c);
void glcd_puts3x5(unsigned char line, unsigned char col, char *str);
void glcd_putbmp( unsigned char *bmp);
void glcd_putbmp_convert( unsigned char *bmp);
void glcd_pixel(unsigned char x, unsigned char y, unsigned char color);
unsigned char glcd_getpixel(unsigned char x, unsigned char y);
void glcd_line(unsigned char x1, unsigned char y1, 
                unsigned char x2, unsigned char y2, unsigned char color);
void glcd_rectangle(unsigned char x1, unsigned char y1, 
                    unsigned char x2, unsigned char y2, 
                    unsigned char fill, unsigned char color);
void glcd_polygon(unsigned char n,unsigned char p[], unsigned char fill, unsigned char color);
void glcd_circle(unsigned char x, unsigned char y, 
                unsigned char r, unsigned char fill, unsigned char color);
void glcd_arc( int xc, int yc, 
                int startang, int endang, 
                int r,unsigned char fill, unsigned char color);
void glcd_ellipse(unsigned char xc,unsigned char yc,unsigned char a,unsigned char b,unsigned char fill, unsigned char color);
void glcd_bar(unsigned char x1, unsigned char y1, 
              unsigned char x2, unsigned char y2, 
              unsigned char width, unsigned char color);
void glcd_cuboid(unsigned char x11,unsigned char y11,
            unsigned char x12,unsigned char y12,
            unsigned char x21,unsigned char y21,
            unsigned char x22,unsigned char y22,
            unsigned char color);							


#endif /*__GLCD12864_H*/


