#ifndef __MESSAGE_H
#define __MESSAGE_H

#include <ctype.h>
#include "stm32f10x.h"
#include "sim908.h"
#include "delay.h"
#include "glcdlib.h"
#include "mahoa.h"
#include "debug.h"
#include "parametter_location.h"

#define ID_NOT_READY    0
#define ID_READY        1


/**************************************************************************/
//Dinh nghia trang thai on/off cua cac tai
//#define DEVICES_ON                                    0x00

//#define CONVECTION_PUMP_1_OFF                0xC0
//#define CONVECTION_PUMP_2_OFF                0x30
//#define COLD_WATER_PUMP_1_OFF                0x0C
//#define COLD_WATER_PUMP_2_OFF                0x03
//#define RETURN_PUMP_1_OFF                        0xC0
//#define RETURN_PUMP_2_OFF                        0x30
//#define INC_PRESSURE_PUMP_1_OFF            0x0C
//#define INC_PRESSURE_PUMP_2_OFF            0x03
//#define HEAT_PUMP_OFF                                0xC0
//#define HEAT_RESISTOR_1_OFF                    0x30
//#define HEAT_RESISTOR_2_OFF                    0x0C
//#define THREE_WAY_VALVE_OFF                    0x03
//#define BLAKFLOW_VALVE                            0xC0

/**************************************************************************/



/**************************************************************************/
//Name of message downlink (Header message)
//Configuration message
#define DL_SYS_MODE_CONF                 "10000000"
#define DL_OUT_MODE_CONF                 "10000001"
#define DL_PARAM_CONF                    "10000010"
#define DL_TC_CONF                       "10000011"
#define DL_CHECK_SYS_MODE                "10000100"
#define DL_CHECK_OUT_MODE                "10000101"
#define DL_CHECK_PARAM                   "10000110"
#define DL_CHECK_TC                      "10000111"
#define DL_CHECK_SYS_STATUS              "10001000"
//Warning Message                      
#define DL_ALARM_CONFIRM                 "10010000"
#define DL_CANCEL_ALARM_CONFIRM          "10010001"
               
//Account message              
#define DL_ID_ASSIGN                     "10100000"
#define DL_CHECK_MODULE_ID               "10100001"
#define DL_CHECK_ACCOUNT                 "10100010"
#define DL_RECHARGE_ACCOUNT              "10100011"
#define DL_PASS_RESET                    "10100100"
#define DL_NEW_MD_CONFIRM                "10100101"
               
//Ermegency stop message               
#define DL_EMER_STOP_CONFIRM             "10110000"
#define DL_CANCEL_EMER_STOP_CONFIRM      "10110001"
#define DL_REQ_EMER_STOP                 "10110010"
#define DL_CANCEL_REQ_STOP_CONFIRM       "10110011"
/**************************************************************************/

/**************************************************************************/
//Name of message up link(Header message)
//Configuration message
#define SYS_MODE_CONF_COMPLETE          "00000000"
#define OUT_MODE_CONF_COMPLETE          "00000001"
#define PARAMETER_CONF_COMPLETE         "00000010"
#define TC_CONF_COMPLETE                "00000011"
#define SYS_MODE_REPORT                 "00000100"
#define OUT_MODE_REPORT                 "00000101"
#define PARAMETER_REPORT                "00000110"
#define TC_REPORT                       "00000111"
#define SYS_STT_REPORT                  "00001000"
#define ON_OFF_STT_REPORT               "00001001"

//Alarm message
#define ALARM_REPORT                    "00010000"
#define ALARM_CANCEL                    "00010001"

//Account message
#define ID_ASSIGN_COMPLETE              "00100000"
#define ID_REPORT                       "00100001"
#define ACC_REPORT                      "00100010"
#define RECHARGE_ACC_COMPLETE           "00100011"
#define PASS_RESET_COMPLETE             "00100100"
#define NEW_MD_NOTIFY                   "00100101"

//Emergency message
#define HARD_EMER_STOP                  "00110000"
#define HARD_EMER_CANCEL                "00110001"
#define SOFT_EMER_STOP                  "00110010"
#define SOFT_EMER_CANCEL                "00110011"
/**************************************************************************/

/**************************************************************************/
//IE NAME + LENGTH 
#define IE_SYS_MODE                     "0000000000000001"
#define IE_OUT_MODE                     "0000000100010110"
#define IE_PARAM                        "0000001000010010"
#define IE_TC                           "0000001100000100"
#define IE_ON_OFF_STT                   "0000010000000100"
#define IE_RUNTIME                      "0000010100010000"
#define IE_SENSOR_VALUE                 "0000011000010000"
#define IE_ALARM                        "0000011100000001"
#define IE_ID                           "0111000000001000"
#define IE_CHECK_MONEY                  "0111001000001110"
#define IE_MONEY_NOTIFY                 "0111001100001110"
#define IE_CARDCODE                     "01110100"
#define IE_IMSI                         "0111010100001010"




/**************************************************************************/
//He so BCD cho cac so 0-9
#define NUM_0                            "0000"
#define NUM_1                            "0001"
#define NUM_2                            "0010"
#define NUM_3             "0011"
#define NUM_4             "0100"
#define NUM_5             "0101"
#define NUM_6             "0110"
#define NUM_7             "0111"
#define NUM_8             "1000"
#define NUM_9             "1001"

//So byte du phong
#define RESERVED_IMSI            "00000000000000000000"
#define RESERVED_ID                "0000"
#define RESERVED_SENSOR        "0000000000000000000000000000000000000000"


//#define IMSI_BIN1                    "000000010010001101000101011001111000100110011001100110011001"




enum Name_Of_Message_DL
{
    SysModeCfg =1,
    OutModeCfg,
    ParamCfg,
    TC_Cfg,
    CheckSysMode,
    CheckOutMode,
    CheckParam,
    CheckTC,
    CheckSysStatus,
    Al_Confirm,
    Al_Cancel_Confirm,
    ID_Assign,
    Check_ID,
    Check_Money,
    Pay_Card,
    Reset_Pass,
    New_Module,
    Stop_Confirm,
    Cancel_Stop_Confirm,
    Req_Stop,
    Cancel_Req_Stop
};

/* Timer Counter TypeDef*/
typedef struct Timer_Counter_Type
{
    uint8_t Confirm_Time;
    uint8_t Resend_Time;
    uint8_t Report_Time;
    uint8_t MSG_Counter_Limit;
} Timer_Counter_TypeDef;

//Parametter TypeDef
//nguong dieu khien bom doi luu
typedef uint8_t Para_Convection_Pump_TypeDef;

//Bom cap nuoc lanh
typedef struct Para_Cold_Water_Supply_Pump_Type
{
    uint8_t M1;
    uint8_t M2;
}Para_Cold_Water_Supply_Pump_TypeDef;
//Bom hoi duong ong
typedef struct Para_Return_Pump_Type
{
    uint8_t Time_T1_Hour;
    uint8_t Time_T1_Min;
    uint8_t Time_T2_Hour;
    uint8_t Time_T2_Min;
    uint8_t    Delta_Temp;
}Para_Return_Pump_TypeDef;

//Nguong dieu khien dien tro nhiet
typedef struct Para_Heater_Resistor_Type
{
    uint8_t Delta_Time;                                                    //Nguong dieu khien thoi gian delat t
    uint8_t Delta_Temp;                         //Nguong nhiet do dieu khien
}Para_Heater_Resistor_TypeDef;

//Nguong dieu khien van dien tu 3 nga
typedef struct Para_Three_Way_Valve_Type
{
    uint8_t Time_T1_Hour;
    uint8_t Time_T1_Min;
    uint8_t Time_T2_Hour;
    uint8_t Time_T2_Min;
    uint8_t    Delta_Temp;
}Para_Three_Way_Valve_TypeDef;

//Nguong dieu khien van 1 chieu
typedef uint8_t Para_Blakflow_Valve_TypeDef;
//Nguong dieu khien bom bon gia nhiet
typedef uint8_t Para_Heat_Pump_TypeDef;
//Nguong dieu khien bom tang ap
typedef uint8_t Para_Incresed_pressure_Pump_TypeDef;



//Trang thai hoat dong cua cac Bom
enum Mode_Of_OUTPUT
{
    Manual_B1=0,
    Manual_B2,
    Manual_B1_B2,
    Auto_B1,
    Auto_B2
};

typedef struct Mode_Heater_Resistor_Type
{
    uint8_t mode;
    uint8_t delta_t2;
    uint8_t delta_time_r2;
    uint8_t reserved;
}Mode_Heater_Resistor_TypeDef;

//Mode Cua cac bom
typedef struct Mode_Pump_Type
{
    uint8_t mode;
    uint8_t time;
    uint8_t reserved;
}Mode_Pump_TypeDef;

//Mode Cua cac van dien tu
typedef struct Mode_Valve_Type
{
    uint8_t mode;
    uint8_t reserved;
}Mode_Valve_TypeDef;

//cam bien nhiet do
typedef struct Temp_Sensor_Type
{
    uint8_t Solar_Panel_Temp;                //cam bien nhiet do dan thu
    uint8_t Solar_Tank_Temp;                //cam bien nhiet do bon solar
    uint8_t Heater_Tank_Temp;                //cam bien nhiet do bon gia nhiet
    uint8_t Top_Solar_Tank_Temp;        //cam bien nhiet dinh bon solar
    uint8_t Pipeline_Temp_1;                //cam bien nhiet do duong ong 1
    uint8_t Pipeline_Temp_2;                //cam bien nhiet do duong ong 2
    uint8_t Environment_Temp;       //Cam bien nhiet do moi truong
}Temp_Sensor_TypeDef;

//cam bien ap suat
typedef struct Pressure_Sensor_Type
{
    uint8_t Heater_Tank_Pressure;                //cam bien ap suat bon gia nhiet
    uint8_t Pipeline_Pressure;                  //cam bien ap suat duong ong
}Pressure_Sensor_TypeDef;

//cam bien muc nuoc
typedef struct Water_Level_Type
{
    uint8_t Water_Solar_Tank_level;            //cam bien muc nuoc bon solar
    uint8_t Water_Overflow;                      //cam bien tran
}Water_Level_TypeDef;

typedef uint8_t         Lingh_Intensity_TypeDef;    //cam bien buc xa dan thu        

typedef struct Devices_Status_Type
{
    uint8_t Convection_Pump_1;                                    //Bom doi luu 1
    uint8_t Convection_Pump_2;                                    //Bom doi luu 2
    uint8_t Cold_Water_Supply_Pump_1;                        //Bom cap nuoc lanh 1
    uint8_t Cold_Water_Supply_Pump_2;                        //Bom cap nuoc lanh 1
    uint8_t Return_Pump_1;                                            //Bom hoi duong ong 1
    uint8_t Return_Pump_2;                                            //Bom hoi duong ong 2
    uint8_t Incresed_pressure_Pump_1;                        //Bom tang ap 1
    uint8_t Incresed_pressure_Pump_2;                        //Bom tang ap 2
    uint8_t Heat_Pump;                                                    //Bom nhiet bon gia nhiet
    uint8_t Heater_Resistor_1;                                    //Dien tro nhiet bon gia nhiet 1
    uint8_t Heater_Resistor_2;                                    //Dien tro nhiet bon gia nhiet 2
    uint8_t Three_Way_Valve;                                        //van dien tu 3 nga
    uint8_t Blakflow_Valve;                                            //van dien tu 1 chieu
}Devices_Status_TypeDef;


//Vong lap mai mai khi co loi voi ban tin
void Message_Halt(void);
//Chuyen doi so IMSI tu he thap phan sang chuoi nhi phan
void Convert_IMSI_2bin(char *imsi_bin, char * imsi_dec);
//Gui ban tin yeu cau cap ID
uint8_t send_Request_ID(void);
//Report gia tri sensor
//uint8_t Sensor_Report(uint8_t mode);

//Kiem tra ban tin gui xuong
uint8_t Message_Valid(void);
//Kiem tra thanh phan IE dau tien
uint8_t FirstIE_Valid(uint8_t msg_name);
//Kiem tra noi dung IE
uint8_t SecondIE_Valid(uint8_t msg_name);

void MSG_process(void);
//Doc cac thong so cau hinh cho module duoc luu trong bo nho
void Read_Para_Config(void);




//Cac ham trong file message_report.c
//Reprot system mode module
uint8_t Sys_Mode_Report(void);
//Report Output mode module
uint8_t Output_Mode_Report(void);
//Report Parameter module
uint8_t Parameter_Report(void);
//Report Timer Counter module
uint8_t Timer_Counter_Report(void);
//Report sensor value and on/off status report
uint8_t Sys_Status_Report(void);
//Ban tin gui bao cao cho server
uint8_t Send_MSG_Report(uint8_t msg_name);

//Ban tin gui canh bao len cho server
uint8_t Alarm_Report(uint8_t Alarm);

//Ban tin gui huy canh bao len cho server
uint8_t Cancel_Alarm_Report(uint8_t Alarm);


//Cac ham trong file message_confirm.c
//Kiem tra ban tin confirm tu server gui ve
uint8_t Message_Confirm_Valid(void);
//Cho nhan ban tin confirm sau khi gui ban tin di
uint8_t Watting_Confirm_Message(uint16_t time_out);
//Gui ban tin confirm da nhan duoc MSG tu server
uint8_t Send_Message_Confirm(uint8_t msg_name);


//Kiem tra xem co ban tin confirm cho ban tin canh bao da gui di hay ko
uint8_t Alarm_Confirm_Message_Valid(uint8_t Alarm);
//Cho nhan ban tin confirm da nhan duoc ban tin canh bao sau khi gui ban tin canh bao di
uint8_t Watting_Alarm_Confirm_Message(uint16_t time_out, uint8_t Alarm);

//Function nay viet ngat 15/11/2017
//Thoi diem hoan thien tinh nang nap tien
//chuc nang: Gui ban tin bao noi dung thong tin tai khoan len server
//Tham so truyen vao: Chuoi thong tin nap tien/kiem tra tien
uint8_t Account_Report(char *Cmd);


#endif /*__MESSAGE_H*/
