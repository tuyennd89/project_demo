#ifndef __ST7920_H
#define __ST7920_H

#include "main.h"



#define GLCD_ST7920_CS_PIN								GPIO_Pin_0
#define GLCD_ST7920_SID_PIN								GPIO_Pin_1
#define GLCD_ST7920_CLK_PIN								GPIO_Pin_2
#define GLCD_ST7920_PORT									GPIOC
#define GLCD_ST7920_CLK_PERIPERAL					RCC_APB2Periph_GPIOC


#define GLCD_ST7920_CS										PORTC_0
#define GLCD_ST7920_SID										PORTC_1
#define GLCD_ST7920_CLK										PORTC_2


#define LCD_write_com(uint8_t)	GLCD_ST7920_Write_Cmd(uint8_t)
#define LCD_write_dat(uint8_t)  GLCD_ST7920_Write_Data(uint8_t)



void GLCD_ST7920_PinConfiguration(void);
void GLCD_ST7920_Init(void);
void GLCD_ST7920_WriteByte(char Byte);
void GLCD_ST7920_Write_Cmd(char cmd);
void GLCD_ST7920_Write_Data(char Data);
void GLCD_ST7920_PutStr(char *Str);
void GLCD_ST7920_GotoXY(char x, char y);
void LCD_draw_point(char x, char y);
void LCD_draw_word(char *x, char y) ;
void print_sinx(void);
void LCD_draw_clr(void);
void LCD_PutChar5x7(char row,char col, char chr);
void LCD_PutStr5x7(char row,char col, char *str);









#endif	/*__ST7920_H*/
