#include "stm32f10x.h"
#include "def_stm32f10x.h"
#include "EEpromEmulator.h"
#include "parametter_location.h"

#define RELAY(num)									1<<(num)

#define RELAY_ALL										0xFFFFFFFF


#define __74HC595_CLK								PORTB_4
#define __74HC595_LATCH							PORTB_8
#define __74HC595_DATA							PORTB_9

#define __74HC595_CLK_Pin						GPIO_Pin_4
#define __74HC595_LATCH_Pin					GPIO_Pin_8
#define __74HC595_DATA_Pin					GPIO_Pin_9

#define __74HC595_PORT							GPIOB
#define __74HC595_RCC_Periph				RCC_APB2Periph_GPIOB


void __74HC595_Pin_Configuration(void);
void __74HC595_Shift_Byte(uint8_t data);
void __74HC595_Latch(void);
void __74HC595_OutWord(uint32_t Word);

void ON_RELAY(uint32_t Relay_Num);
void OFF_RELAY(uint32_t Relay_Num);

void Relay_Status_Save(uint32_t Relay_Stt);



