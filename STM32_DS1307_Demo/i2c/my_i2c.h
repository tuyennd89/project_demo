#ifndef __MY_I2C_H
#define __MY_I2C_H
#include "main.h"


#define I2C_SW


#ifndef I2C_SW
#define I2C_HW
#endif



#define I2C_SCL_PIN            GPIO_Pin_6
#define I2C_SCL_PORT           GPIOB
#define I2C_SCL_GPIO_CLK       RCC_APB2Periph_GPIOB
#define I2C_SDA_PIN            GPIO_Pin_7
#define I2C_SDA_PORT           GPIOB
#define I2C_SDA_GPIO_CLK       RCC_APB2Periph_GPIOB

#define   SDA_OUT          PORTB_7
#define   SDA_IN           PINB_7
#define   SCL              PORTB_6

#define I2C_SCL_H()            I2C_SCL_PORT->BSRR=I2C_SCL_PIN              
#define I2C_SCL_L()            I2C_SCL_PORT->BRR=I2C_SCL_PIN

#define I2C_SDA_H()            I2C_SDA_PORT->BSRR=I2C_SDA_PIN              
#define I2C_SDA_L()            I2C_SDA_PORT->BRR=I2C_SDA_PIN

void GPIO_SetState(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin,GPIOMode_TypeDef GPIO_Mode);


#ifdef I2C_SW
/*******************************************************************************
Noi Dung    :   Khoi tao giao thuc I2C
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/
void My_I2C_Init(void);
/*******************************************************************************
Noi Dung    :   Gui lenh Start I2C.
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/
void I2C_Start(void);
/*******************************************************************************
Noi Dung    :   Gui lenh Stop I2C.
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/
void I2C_Stop(void);
/*******************************************************************************
Noi Dung    :   Viet du lieu len kenh I2C.
Tham Bien   :   Data   :   Gia tri du lieu.
Tra Ve      :   Khong.
********************************************************************************/
uint8_t I2C_Write(uint8_t Data);
/*******************************************************************************
Noi Dung    :   Lay du lieu tren kenh I2C.
Tham Bien   :   Khong. 
Tra Ve      :   Gia tri du lieu.
********************************************************************************/
uint8_t I2C_Read(uint8_t Ack);

#else
/*User I2C HW*/

#endif /*I2C_SW*/

#endif  /*__MY_I2C_H*/
