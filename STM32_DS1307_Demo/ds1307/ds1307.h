#ifndef __DS1307_H
#define __DS1307_H

#include "main.h"

#define DS1307_ADDR_WRITE     0xd0
#define DS1307_ADDR_READ      0xd1

#define DS1307_ADDR_SECOND    0x00
#define DS1307_ADDR_MINUTE    0x01
#define DS1307_ADDR_HOUR      0x02
#define DS1307_ADDR_DAY       0x03
#define DS1307_ADDR_DATE      0x04
#define DS1307_ADDR_MONTH     0x05
#define DS1307_ADDR_YEAR      0x06

#define DS1307_ADDR_CONTROL   0x07
#define DS1307_SQW            0x10
#define DS1307_MODE_1Khz      0x00
#define DS1307_MODE_4096Hz    0x01
#define DS1307_MODE_8192Hz    0x10
#define DS1307_MODE_32768Hz   0x11
/*******************************************************************************
Noi Dung    :   Khoi Tao DS1307.
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_Init(void);

/*******************************************************************************
Noi Dung    :   
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_SetControl(uint8_t State,uint8_t Mode);

/*******************************************************************************
Noi Dung    :   Viet Du Lieu Vao DS1307.
Tham Bien   :   Address   :  Dia chi du lieu.
                Data      :  Gia tri du lieu.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_Write(uint8_t Address, uint8_t Data);

/*******************************************************************************
Noi Dung    :   Doc du lieu tu DS1307.
Tham Bien   :   Address   :  Dia choi du lieu.
Tra Ve      :   Gia tri cua du lieu.
********************************************************************************/

uint8_t DS1307_Read(uint8_t Address);

/*******************************************************************************
Noi Dung    :   Doc gia tri ngay, thang, nam, thu tu DS1307.
Tham Bien   :   *Day      :  Bien luu tru gia tri Thu.
                *Date     :  Bien luu tru gia tri Ngay.
                *Month    :  Bien luu tru gia tri Thang.
                *Year     :  Bien luu tru gia tri Nam.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_GetDate(uint8_t *Day, uint8_t* Date, uint8_t* Month, uint8_t* Year);

/*******************************************************************************
Noi Dung    :   Doc gia tri gio,phut,giay tu DS1307.
Tham Bien   :   *Second   :  Bien luu tru gia tri Giay.
                *Minute   :  Bien luu tru gia tri Phut.
                *Hour     :  Bien luu tru gia tri Gio.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_GetTime(uint8_t *Hour, uint8_t* Minute, uint8_t* Second);

/*******************************************************************************
Noi Dung    :   Viet gia tri Ngay, Thang, Nam, Thu vao DS1307.
Tham Bien   :   Day       :  Gia tri Thu can ghi.
                Date      :  Gia tri Ngay can ghi.
                Month     :  Gia tri Thang can ghi.
                Year      :  Gia tri Nam can ghi.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_SetDate(uint8_t Day, uint8_t Date, uint8_t Month, uint8_t Year);

/*******************************************************************************
Noi Dung    :   Viet gia tri Gio, Phut, Giay vao DS1307.
Tham Bien   :   Second   :  Gia tri Giay can ghi.
                Minute   :  Gia tri Phut can ghi.
                Hour     :  Gia tri Gio can ghi.
********************************************************************************/
void DS1307_SetTime(uint8_t Hour, uint8_t Minute, uint8_t Second);
#endif
/******************************KET THUC FILE******************************/

