#include "ds1307.h"
/*******************************************************************************
Noi Dung    :   Khoi Tao DS1307.
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/
void DS1307_Init()
{
   uint8_t second = DS1307_Read(DS1307_ADDR_SECOND);
   
   second&=(~(1<<7));   //Clear bit CH
   DS1307_Write(DS1307_ADDR_SECOND,second);
}
/*******************************************************************************
Noi Dung    :   Viet Du Lieu Vao DS1307.
Tham Bien   :   Address   :  Dia chi du lieu.
                Data      :  Gia tri du lieu.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_Write(uint8_t Address, uint8_t Data)
{
   uint8_t Dataw;
   Dataw =  ((Data/10)<<4) + (Data % 10);
   I2C_Start();
   I2C_Write(DS1307_ADDR_WRITE);
   I2C_Write(Address);
   I2C_Write(Dataw);
   I2C_Stop();
}
/*******************************************************************************
Noi Dung    :   Doc du lieu tu DS1307.
Tham Bien   :   Address   :  Dia choi du lieu.
Tra Ve      :   Gia tri cua du lieu.
********************************************************************************/

uint8_t DS1307_Read(uint8_t Address)
{
   uint8_t Data;
   I2C_Start();
   I2C_Write(DS1307_ADDR_WRITE);
   I2C_Write(Address);
   I2C_Start();
   I2C_Write(DS1307_ADDR_READ);
   Data=I2C_Read(0);
   I2C_Stop();
   Data = ((Data>>4) * 10 + Data % 16);
   return(Data);
}
/*******************************************************************************
Noi Dung    :   
Tham Bien   :   Khong.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_SetControl(uint8_t State,uint8_t Mode)
{
   if(!State)
   {
      DS1307_Write(DS1307_ADDR_CONTROL,0x00);
   }
   else
   {
      DS1307_Write(DS1307_ADDR_CONTROL,DS1307_SQW|Mode);
   }
}
/*******************************************************************************
Noi Dung    :   Doc gia tri ngay, thang, nam, thu tu DS1307.
Tham Bien   :   *Day      :  Bien luu tru gia tri Thu.
                *Date     :  Bien luu tru gia tri Ngay.
                *Month    :  Bien luu tru gia tri Thang.
                *Year     :  Bien luu tru gia tri Nam.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_GetDate(uint8_t *Day, uint8_t* Date, uint8_t* Month, uint8_t* Year)
{
      *Day    =   DS1307_Read(DS1307_ADDR_DAY);
      *Date   =   DS1307_Read(DS1307_ADDR_DATE);
      *Month  =   DS1307_Read(DS1307_ADDR_MONTH);
      *Year   =   DS1307_Read(DS1307_ADDR_YEAR);
      
}
/*******************************************************************************
Noi Dung    :   Doc gia tri gio,phut,giay tu DS1307.
Tham Bien   :   *Second   :  Bien luu tru gia tri Giay.
                *Minute   :  Bien luu tru gia tri Phut.
                *Hour     :  Bien luu tru gia tri Gio.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_GetTime(uint8_t *Hour, uint8_t* Minute, uint8_t* Second)
{
      *Second  =  DS1307_Read(DS1307_ADDR_SECOND);
      *Minute  =  DS1307_Read(DS1307_ADDR_MINUTE);
      *Hour    =  DS1307_Read(DS1307_ADDR_HOUR);
}

/*******************************************************************************
Noi Dung    :   Viet gia tri Ngay, Thang, Nam, Thu vao DS1307.
Tham Bien   :   Day       :  Gia tri Thu can ghi.
                Date      :  Gia tri Ngay can ghi.
                Month     :  Gia tri Thang can ghi.
                Year      :  Gia tri Nam can ghi.
Tra Ve      :   Khong.
********************************************************************************/

void DS1307_SetDate(uint8_t Day, uint8_t Date, uint8_t Month, uint8_t Year)
{
   DS1307_Write(DS1307_ADDR_DAY,Day);
   DS1307_Write(DS1307_ADDR_DATE,Date);
   DS1307_Write(DS1307_ADDR_MONTH,Month);
   DS1307_Write(DS1307_ADDR_YEAR,Year);
}
/*******************************************************************************
Noi Dung    :   Viet gia tri Gio, Phut, Giay vao DS1307.
Tham Bien   :   Second   :  Gia tri Giay can ghi.
                Minute   :  Gia tri Phut can ghi.
                Hour     :  Gia tri Gio can ghi.
********************************************************************************/
void DS1307_SetTime(uint8_t Hour, uint8_t Minute, uint8_t Second)
{
   DS1307_Write(DS1307_ADDR_SECOND,Second);
   DS1307_Write(DS1307_ADDR_MINUTE,Minute);
   DS1307_Write(DS1307_ADDR_HOUR,Hour);
}
/******************************KET THUC FILE******************************/
