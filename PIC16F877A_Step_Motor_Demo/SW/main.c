#include <main.h>
#include "lcd16x2/lcd16x2.c"
#include "keypad/key4x4.c"

char lcd_buff[16];
uint8_t Key;
uint8_t Dir=0;
int32 num_set=0;
int32 count_round=0;
int32 count_angle=0;
uint8_t mode=0;

int speed=10;



void main()
{
   DDRD=0x00;
   DDRC=0x0F;
   DDRB=0x00;
   LCD_Init();
   sprintf(lcd_buff,"Test LCD");
   LCD_Gotoxy(0,0);
   LCD_Puts(lcd_buff);
   //StepMotor_RunNStep(2048);
   StepMotor_OFF();
   while(TRUE)
   {
      while(mode==0)
      {
         Key=KEY4X4_GetKey();
          if(Key)
         {
            if(Key=='A')
            {
               mode=1;
            }
            
            if(Key=='B')
            {
               mode=2;
            }
         }
         sprintf(lcd_buff,"A: Setting Round ");
         LCD_Gotoxy(0,0);
         LCD_Puts(lcd_buff);
         sprintf(lcd_buff,"B: Setting Angle ");
         LCD_Gotoxy(0,1);
         LCD_Puts(lcd_buff);
      }
      
      LCD_Clear();
      while(mode==1)
      {
         sprintf(lcd_buff,"ROUND= %lu ",num_set);
         LCD_Gotoxy(0,0);
         LCD_Puts(lcd_buff);
         sprintf(lcd_buff,"B:START");
         LCD_Gotoxy(0,1);
         LCD_Puts(lcd_buff); 
         
         if(Dir==0)
         {
            LCD_Gotoxy(9,1);
            LCD_PutChar('<');
            LCD_Gotoxy(10,1);
            LCD_PutChar('-');
         }
         else
         {
            LCD_Gotoxy(9,1);
            LCD_PutChar('-');
            LCD_Gotoxy(10,1);
            LCD_PutChar('>');
         }
         
         sprintf(lcd_buff,"%u ",speed-4);
         LCD_Gotoxy(13,1);
         LCD_Puts(lcd_buff);
         Key=KEY4X4_GetKey();
         if(Key)
         {
            if(Key=='C')
            {
               num_set=0;
            }
            
            if((Key>='0') && (Key<='9'))
            {
               num_set*=10;
               num_set+=(key-48);
               delay_ms(300);
            }
            
            if(Key=='D')
            {
               Dir=(~Dir);
               delay_ms(500);
            }
            
            if(Key=='*')
            {
               delay_ms(200);
               speed--;
               if(speed<=5)
                  speed=5;
            }
            
            if(Key=='#')
            {
               delay_ms(200);
               speed++;
               if(speed>=14)
                  speed=14;
            }
            
            if(Key=='B')
            {
               //sprintf(lcd_buff,"B: START: %lu  ",count_round);
               //LCD_Gotoxy(0,1);
               //LCD_Puts(lcd_buff);
               while(count_round<num_set)
               {
                  StepMotor_RunNStep(2048);
                  count_round++;
                  //sprintf(lcd_buff,"B: START: %lu ",count_round);
                 // LCD_Gotoxy(0,1);
                  //LCD_Puts(lcd_buff);
               }
            }
         }
      }
      
      
      LCD_Clear();
      while(mode==2)
      {
         sprintf(lcd_buff,"Angle= %lu ",num_set);
         LCD_Gotoxy(0,0);
         LCD_Puts(lcd_buff);
         sprintf(lcd_buff,"A:START");
         LCD_Gotoxy(0,1);
         LCD_Puts(lcd_buff); 
         if(Dir==0)
         {
            LCD_Gotoxy(9,1);
            LCD_PutChar('<');
            LCD_Gotoxy(10,1);
            LCD_PutChar('-');
         }
         else
         {
            LCD_Gotoxy(9,1);
            LCD_PutChar('-');
            LCD_Gotoxy(10,1);
            LCD_PutChar('>');
         }
         
         sprintf(lcd_buff,"%u ",speed-4);
         LCD_Gotoxy(13,1);
         LCD_Puts(lcd_buff); 
         Key=KEY4X4_GetKey();
         if(Key)
         {
            if(Key=='C')
            {
               num_set=0;
            }
            
            if(Key=='D')
            {
               Dir=(~Dir);
               delay_ms(500);
            }
            
            if((Key>='0') && (Key<='9'))
            {
               num_set*=10;
               num_set+=(key-48);
               delay_ms(300);
            }
            
            if(Key=='A')
            {
               count_angle = (int32)num_set*5.7;
               StepMotor_RunNStep(count_angle);
               count_round++;
               //sprintf(lcd_buff,"B: START: %lu ",count_round);
               //LCD_Gotoxy(0,1);
               //LCD_Puts(lcd_buff);
            }
            
            if(Key=='*')
            {
               delay_ms(200);
               speed--;
               if(speed<=5)
                  speed=5;
            }
            
            if(Key=='#')
            {
               delay_ms(200);
               speed++;
               if(speed>=14)
                  speed=14;
            }
         }
      }
      
      
   }
}



void StepMotor_RunNStep(int32 nStep)
{
   static int16 i=0;
   
   while(nStep>0)
   {
      if(Dir==0)
      {
         i++;
         if(i>=2048)
            i=0;
      }
      else
      {
         if(i<=0)
            i=2048;
         i--;
      }
      StepMotor_RunStep(i%4);
      nStep--;
   }
}


void StepMotor_RunStep(int step)
{
   switch(step)
   {
   #ifdef SIMULATION
      case 0:
         PORTB=0x06;
         break;
      case 1:
         PORTB=0x0C;
         break;
      case 2:
         PORTB=0x09;
         break;
      case 3:
         PORTB=0x03;
         break;
   #else
      case 0:
         PORTD=0x60;
         break;
      case 1:
         PORTD=0xC0;
         break;
      case 2:
         PORTD=0x90;
         break;
      case 3:
         PORTD=0x30;
         break;
   #endif /*SIMULATION*/
   }
   delay_ms(speed);
}


void StepMotor_OFF(void)
{
   PORTB=0x00;
}
