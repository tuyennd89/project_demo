#ifndef __MAIN_H
#define __MAIN_H

#include <16F877A.h>
#device*=16 adc=10

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES HS                       //High speed Osc (> 4mhz for PCM/PCH) (>10mhz for PCD)
#FUSES NOPUT                    //No Power Up Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES NOWRT                    //Program memory not write protected
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOPROTECT                //Code not protected from reading

#use delay(clock=20000000)


/*   Kieu So Nguyen Co Dau   */
typedef   int1                    bit;
typedef   signed int              int8_t;
typedef   signed long             int16_t;
typedef   signed long long        int32_t;

/*   Kieu So Nguyen Khong Dau */
typedef   unsigned         int  uint8_t;
typedef   unsigned         long uint16_t;
typedef   unsigned long    long uint32_t;
/*   Kieu So Thuc */
typedef   float            float32_t;

#include "DEF_16F877A.h"

#include "lcd16x2/lcd16x2.h"
#include "keypad/key4x4.h"
#include <stdio.h>
#include <string.h>

#define SIMULATION

#define STEP_A1               PORTD_4
#define STEP_A2               PORTD_5
#define STEP_B1               PORTD_6
#define STEP_B2               PORTD_7

void StepMotor_RunStep(int step);
void StepMotor_OFF(void);
void StepMotor_RunNStep(int32 nStep);

#endif    /*__MAIN_H*/

