#ifndef __KEY_4X4_H
#define __KEY_4X4_H
#include "main.h"
 
#define         KEY4X4_ROW1     PINC_0
#define         KEY4X4_ROW2     PINC_1
#define         KEY4X4_ROW3     PINC_2
#define         KEY4X4_ROW4     PINC_3

#define         KEY4X4_COL1     PORTC_4 
#define         KEY4X4_COL2     PORTC_5 
#define         KEY4X4_COL3     PORTC_6 
#define         KEY4X4_COL4     PORTC_7


 /*******************************************************************************
Noi Dung    :   Kiem tra co nut duoc an hay khong..
Tham Bien   :   Khong.
Tra Ve      :   1:   Neu co nut duoc an.
                0:   Neu khong co nut duoc an.
********************************************************************************/
uint8_t  KEY4X4_IsPush(void);
 /*******************************************************************************
Noi Dung    :   Keo hang thu i xuong muc logic 0, de kiem tra co nut duoc an tai 
                hang thu i hay khong.
Tham Bien   :   i: vi tri hang can kiem tra.
Tra Ve      :   Khong.
********************************************************************************/
void KEY4X4_CheckRow(uint8_t  i);
 /*******************************************************************************
Noi Dung    :   Lay gia tri nut nhan duoc an.
Tham Bien   :   Khong.
Tra Ve      :   0:     Neu khong co nut duoc an.
            khac 0: Gia tri cua nut an.
********************************************************************************/
uint8_t  KEY4X4_GetKey(void);
#endif  /*__KEY_4X4_H*/
