#include "step_motor_28byj_48.h"

int stepsPerRevolution=0; 
int step_delay=1;
uint8_t step_direction=1;
int step_number=0;          // which step the motor is on


void Stepper_Init(int number_of_steps)
{
    STEP_DDR_PIN1 = 0;
    STEP_DDR_PIN2 = 0;
    STEP_DDR_PIN3 = 0;
    STEP_DDR_PIN4 = 0;
    
    stepsPerRevolution = number_of_steps;
}



void Stepper_SetSpeed(int speed)
{
    step_delay = 60*1000*1000/stepsPerRevolution/speed;
}


void Stepper_StepMotor(uint8_t thisStep)
{
    switch (thisStep) {
      case 0:  // 1010
        STEP_PIN1 = 1;
        STEP_PIN2 = 0;
        STEP_PIN3 = 1;
        STEP_PIN4 = 0;
      break;
      case 1:  // 0110
        STEP_PIN1 = 0;
        STEP_PIN2 = 1;
        STEP_PIN3 = 1;
        STEP_PIN4 = 0;
      break;
      case 2:  //0101
        STEP_PIN1 = 0;
        STEP_PIN2 = 1;
        STEP_PIN3 = 0;
        STEP_PIN4 = 1;
      break;
      case 3:  //1001
        STEP_PIN1 = 1;
        STEP_PIN2 = 0;
        STEP_PIN3 = 0;
        STEP_PIN4 = 1;
      break;
    }
}


void Stepper_step(int steps_to_move)
{
    int steps_left = abs(steps_to_move);  // how many steps to take
    
    if (steps_to_move > 0) { step_direction = 1; }
    if (steps_to_move < 0) { step_direction = 0; }
    
    while (steps_left > 0)
    {
        if(step_direction==1)
        {
            step_number++;
            if(step_number>=stepsPerRevolution)
            {
                step_number=0;
            }
        }
        else
        {
            if(step_number<=0)
            {
                step_number = stepsPerRevolution;
            }
            step_number--;
        }
        steps_left --;
        Stepper_StepMotor(step_number%4);
        delay_ms(1000);
    }
}


