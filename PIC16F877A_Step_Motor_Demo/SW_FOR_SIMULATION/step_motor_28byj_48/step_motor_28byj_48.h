#ifndef __STEP_MOTOR_28BYJ_48_H
#define __STEP_MOTOR_28BYJ_48_H

#include "main.h"
 
#define         STEP_PIN1       PORTB_0 
#define         STEP_PIN2       PORTB_1 
#define         STEP_PIN3       PORTB_2 
#define         STEP_PIN4       PORTB_3

#define         STEP_DDR_PIN1   DDRB_0
#define         STEP_DDR_PIN2   DDRB_1
#define         STEP_DDR_PIN3   DDRB_2
#define         STEP_DDR_PIN4   DDRB_3


void Stepper_Init(int number_of_steps);
void Stepper_SetSpeed(int speed);
void Stepper_StepMotor(uint8_t thisStep);
void Stepper_step(int steps_to_move);



#endif  /*__STEP_MOTOR_28BYJ_48_H*/
