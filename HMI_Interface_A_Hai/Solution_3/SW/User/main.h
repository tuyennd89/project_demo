#ifndef __MAIN_H
#define __MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f10x.h"
#include "def_stm32f10x.h"
#include "my_uart.h"
#include "my_nvic.h"
#include "modbusSlave.h"
#include "var.h"
#include "modbus1.h"
#include "modbus2.h"
#include "modbus3.h"

//#define TEST_MSG

typedef enum
{
    false=0,
    true
}
boolean;

typedef enum
{
	Upper_Casing = 0,
	Other
}
ModelTypeDef;

typedef enum
{
	A1 = 0,
	A2,
	A2_1,
	A3,
	A3_1,
	A4,
	A4_1,
	A5,
	A5_1,
	A6,
	A6_1,
	A7,
	A7_1,
	A8,
	A8_1
}
Mold_Number_TypeDef;


typedef enum
{
	Su = 1,							//Ro
	Yakitsuki,					//Chay
	Yujiwa,							//nhawn
	Kajiri,             //Xuoc
	Yogore,							//Ban
	Atsukon,            //Lom
	Dakon,              //Va dap
	Size_NG,            //Kich thuoc NG
	Cut_UnComplete,			//Cat khong het
	Atsumore,						//Ho khi
	Sonota							//Ngoai ra
}
ErrNameTypeDef;


typedef enum
{
	Aimen = 1,
	Sotogawa,
	Sokumen1,
	Uegawa,
	Uchigawa,
	Sokumen2
}
ErrSurfaceTypeDef;

typedef struct
{
	uint8_t column;
	uint8_t row;
}
ErrPositionTypeDef;

typedef struct
{
	ErrNameTypeDef					ErrName;
	ErrSurfaceTypeDef				ErrSurface;
	ErrPositionTypeDef			ErrPosition;	
	modbusAnaReg_TypeDef		ErrHmiAddr;
}
ErrTypeDef;


#define BUFFER_SIZE	50

#define DEBUG

#define MODBUS1_TX							PORTB_14
#define MODBUS3_TX							PORTB_13
#define MODBUS2_TX							PORTB_12

#define RS485_4_TX							PORTC_15

#define LED1 										PORTB_5
#define LED2 										PORTA_15


#define HMI_ERR_SU_ADDR														40001
#define HMI_ERR_YAKITSUKI_ADDR										40002
#define HMI_ERR_YUJIWA_ADDR												40003
#define HMI_ERR_KAJIRI_ADDR												40004
#define HMI_ERR_YOGORE_ADDR												40005
#define HMI_ERR_ATSUKON_ADDR											40006
#define HMI_ERR_DAKON_ADDR												40007
#define HMI_ERR_SIZE_NG_ADDR											40008
#define HMI_ERR_CUT_UNCOMPLETE_ADDR								40009
#define HMI_ERR_ATSUMORE_ADDR											40010
#define HMI_ERR_SONOTA_ADDR												40011

//Ky tu dai dien cho ten loi
//HMI 202D
#define A1																				2								//Loi chay tu HMI 202D
#define B1																				3								//Loi nhan tu HMI202D
#define C1																				4								//Loi xuoc 
#define D1																				7								//Loi va dap
#define E1																				15							//Loi ngoai ra
#define F1																				8								//Loi kich thuoc NG, loi nay chua co
#define G1																				9								//Loi cat khong het
#define H1																				10							//Loi ho khi
#define I1																				1								//Loi ro
#define J1																				6								//Loi lom
#define K1																				5								//Loi ban

//HMI 202M
#define A2																				17								//Loi chay
#define B2																				18								//Loi nhan
#define C2																				19								//Loi xuoc 
#define D2																				22								//Loi va dap
#define E2																				30							//Loi ngoai ra
#define F2																				23								//Loi kich thuoc NG
#define G2																				24								//Loi cat khong het
#define H2																				25							//Loi ho khi
#define I2																				16								//Loi ro
#define J2																				21								//Loi lom
#define K2																				20								//Loi ban

//HMI V22
#define A3																				32								//Loi chay
#define B3																				33								//Loi nhan
#define C3																				34								//Loi xuoc 
#define D3																				37								//Loi va dap
#define E3																				45							//Loi ngoai ra
#define F3																				38								//Loi kich thuoc NG
#define G3																				39								//Loi cat khong het
#define H3																				40							//Loi ho khi
#define I3																				31								//Loi ro
#define J3																				36								//Loi lom
#define K3																				35								//Loi ban

void delay_ms(uint32_t nms);
void Timer_Configuration(void);
void RS485_Puts(char *s);
void RS485_PutChar(char c);
void RS485_PutNum(uint8_t num);

void RS485_4_Puts(char *s);
void RS485_4_PutChar(char c);


void InitErrName(void);
void AssignedAddrForErr(void);

void System_SendMessage(ErrTypeDef NameOfErr);















#endif /*__MAIN_H*/
