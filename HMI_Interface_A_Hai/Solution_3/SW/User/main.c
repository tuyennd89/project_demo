/**
  ******************************************************************************
  * @file    GPIO/IOToggle/main.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "def_stm32f10x.h"

/** @addtogroup STM32F10x_StdPeriph_Examples
  * @{
  */

/** @addtogroup GPIO_IOToggle
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
GPIO_InitTypeDef GPIO_InitStructure;
volatile uint32_t time_count=0,ttt=0;
volatile uint32_t time_count1=500;

ErrTypeDef	ErrSu, ErrYakitsuki, ErrYujiwa, ErrKajiri, ErrYogore, ErrAtsukon, ErrDakon, ErrSizeNG;
ErrTypeDef	 ErrCutUnComplete, ErrAtsumore, ErrSonota;

modbusAnaReg_TypeDef Err_Valid;

char buff[20];
/* Private function prototypes -----------------------------------------------*/
void RS485_Puts(char *s)
{
	PORTA_8=1;
	USART5_PutString((unsigned char*)s);
	USART5_PutChar('\n');
	delay_ms(10);
	PORTA_8=0;
}

void RS485_PutChar(char c)
{
	PORTA_8=1;
	USART5_PutChar(c);
	delay_ms(10);
	PORTA_8=0;
}

void RS485_PutNum(uint8_t num)
{
	char buffer[10];
	
	sprintf(buffer,"so=%d\n",num);
	RS485_Puts(buffer);
}
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */     
	SystemInit();
       
  /* GPIOB Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

  /* Configure PD0 and PD2 in output pushpull mode */
  GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_5 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14);
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	Timer_Configuration();
	NVIC_Configuration();
	UART5_Init(9600,DISABLE);
	UART4_Init(9600,DISABLE);
	
	modbusDevice_setId(1);
	Modbus1_Init(USART1,9600);
	Modbus2_Init(USART2,9600);
	Modbus3_Init(USART3,9600);
	
	InitErrName();
	AssignedAddrForErr();
  /* To achieve GPIO toggling maximum frequency, the following  sequence is mandatory. 
     You can monitor PD0 or PD2 on the scope to measure the output signal. 
     If you need to fine tune this frequency, you can add more GPIO set/reset 
     cycles to minimize more the infinite loop timing.
     This code needs to be compiled with high speed optimization option.  */
	//RS485_4_Puts("Project HMI WEINTEK\n");
	USART5_PutString("Project HMI WEINTEK\n");
	RS485_4_TX = 0; 
  while (1)
  {
		if(Err_Valid.value != 0)
		{
			switch(Err_Valid.value)
			{
				case A1:
					//System_SendMessage(ErrSu);
					RS485_Puts("A1");
					RS485_4_Puts("A1");
					break;
				case B1:
					//System_SendMessage(ErrYakitsuki);
					RS485_Puts("B1");
					RS485_4_Puts("B1");
					break;
				case C1:
					//System_SendMessage(ErrYujiwa);
					RS485_Puts("C1");
					RS485_4_Puts("C1");
					break;
				case D1:
					//System_SendMessage(ErrKajiri);
					RS485_Puts("D1");
					RS485_4_Puts("D1");
					break;
				case E1:
					//System_SendMessage(ErrYogore);
					RS485_Puts("E1");
					RS485_4_Puts("E1");
					break;
				case F1:
					//System_SendMessage(ErrAtsukon);
					RS485_Puts("F1");
					RS485_4_Puts("F1");
					break;
				case G1:
					//System_SendMessage(ErrDakon);
					RS485_Puts("G1");
					RS485_4_Puts("G1");
					break;
				case H1:
					//System_SendMessage(ErrSizeNG);
					RS485_Puts("H1");
					RS485_4_Puts("H1");
					break;
				case I1:
					//System_SendMessage(ErrCutUnComplete);
					RS485_Puts("I1");
					RS485_4_Puts("I1");
					break;
				case J1:
					//System_SendMessage(ErrAtsumore);
					RS485_Puts("J1");
					RS485_4_Puts("J1");
					break;
				case K1:
					//System_SendMessage(ErrSonota);
					RS485_Puts("K1");
					RS485_4_Puts("K1");
					break;
				case A2:
					System_SendMessage(ErrSu);
					RS485_Puts("A2");
					RS485_4_Puts("A2");
					break;
				case B2:
					//System_SendMessage(ErrYakitsuki);
					RS485_Puts("B2");
					RS485_4_Puts("B2");
					break;
				case C2:
					//System_SendMessage(ErrYujiwa);
					RS485_Puts("C2");
					RS485_4_Puts("C2");
					break;
				case D2:
					//System_SendMessage(ErrKajiri);
					RS485_Puts("D2");
					RS485_4_Puts("D2");
					break;
				case E2:
					//System_SendMessage(ErrYogore);
					RS485_Puts("E2");
					RS485_4_Puts("E2");
					break;
				case F2:
					//System_SendMessage(ErrAtsukon);
					RS485_Puts("F2");
					RS485_4_Puts("F2");
					break;
				case G2:
					//System_SendMessage(ErrDakon);
					RS485_Puts("G2");
					RS485_4_Puts("G2");
					break;
				case H2:
					//System_SendMessage(ErrSizeNG);
					RS485_Puts("H2");
					RS485_4_Puts("H2");
					break;
				case I2:
					//System_SendMessage(ErrCutUnComplete);
					RS485_Puts("I2");
					RS485_4_Puts("I2");
					break;
				case J2:
					//System_SendMessage(ErrAtsumore);
					RS485_Puts("J2");
					RS485_4_Puts("J2");
					break;
				case K2:
					//System_SendMessage(ErrSonota);
					RS485_Puts("K2");
					RS485_4_Puts("K2");
					break;
				case A3:
					//System_SendMessage(ErrSu);
					RS485_Puts("A3");
					RS485_4_Puts("A3");
					break;
				case B3:
					//System_SendMessage(ErrYakitsuki);
					RS485_Puts("B3");
					RS485_4_Puts("B3");
					break;
				case C3:
					//System_SendMessage(ErrYujiwa);
					RS485_Puts("C3");
					RS485_4_Puts("C3");
					break;
				case D3:
					//System_SendMessage(ErrKajiri);
					RS485_Puts("D3");
					RS485_4_Puts("D3");
					break;
				case E3:
					//System_SendMessage(ErrYogore);
					RS485_Puts("E3");
					RS485_4_Puts("E3");
					break;
				case F3:
					//System_SendMessage(ErrAtsukon);
					RS485_Puts("F3");
					RS485_4_Puts("F3");
					break;
				case G3:
					//System_SendMessage(ErrDakon);
					RS485_Puts("G3");
					RS485_4_Puts("G3");
					break;
				case H3:
					//System_SendMessage(ErrSizeNG);
					RS485_Puts("H3");
					RS485_4_Puts("H3");
					break;
				case I3:
					//System_SendMessage(ErrCutUnComplete);
					RS485_Puts("I3");
					RS485_4_Puts("I3");
					break;
				case J3:
					//System_SendMessage(ErrAtsumore);
					RS485_Puts("J3");
					RS485_4_Puts("J3");
					break;
				case K3:
					//System_SendMessage(ErrSonota);
					RS485_Puts("K3");
					RS485_4_Puts("K3");
					break;
				default:
					USART5_PutString("Default\n");
					break;
			}
			Err_Valid.value = 0;
		}
		
		Modbus1_Run();
		Modbus2_Run();
		Modbus3_Run();
  }
}


void Timer_Configuration(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
   
 /* Time base configuration */
 TIM_TimeBaseStructure.TIM_Prescaler = 36000-1;    
 TIM_TimeBaseStructure.TIM_Period = 20 - 1;
 TIM_TimeBaseStructure.TIM_ClockDivision = 0;
 TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
 TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
 TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);
 TIM_Cmd(TIM4, ENABLE);
 
 NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
 NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
 NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
 NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 NVIC_Init(&NVIC_InitStructure);   
}

void TIM4_IRQHandler(void)
{
 if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
 {
	time_count++;
	if(time_count>100)
	{
		time_count=0;
		//ttt++;
		LED1 = ~LED1;
	}
		
	TIM_ClearITPendingBit(TIM4, TIM_IT_Update); 
 }
}


void delay_ms(uint32_t nms)
{
	uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		for(j=0;j<7200;j++);
	}
}


void InitErrName(void)
{
	ErrSu.ErrName = Su;
	ErrYakitsuki.ErrName = Yakitsuki;
	ErrYujiwa.ErrName = Yujiwa;
	ErrKajiri.ErrName = Kajiri;
	ErrYogore.ErrName =  Yogore;
	ErrAtsukon.ErrName = Atsukon;
	ErrDakon.ErrName = Dakon;
	ErrSizeNG.ErrName = Size_NG;
	ErrCutUnComplete.ErrName = Cut_UnComplete;
	ErrAtsumore.ErrName = Atsumore;
	ErrSonota.ErrName = Sonota;
}


void AssignedAddrForErr(void)
{
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_SU_ADDR,&ErrSu.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_YAKITSUKI_ADDR,&ErrYakitsuki.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_YUJIWA_ADDR,&ErrYujiwa.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_KAJIRI_ADDR,&ErrKajiri.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_YOGORE_ADDR,&ErrYogore.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_ATSUKON_ADDR,&ErrAtsukon.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_DAKON_ADDR,&ErrDakon.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_SIZE_NG_ADDR,&ErrSizeNG.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_CUT_UNCOMPLETE_ADDR,&ErrCutUnComplete.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_ATSUMORE_ADDR,&ErrAtsumore.ErrHmiAddr);
	modbusRegBank_assigned_AnaRegAddr(HMI_ERR_SONOTA_ADDR,&ErrSonota.ErrHmiAddr);
	
	modbusRegBank_assigned_AnaRegAddr(40020,&Err_Valid);
}


void RS485_4_PutChar(char c)
{
	RS485_4_TX = 1;
	USART4_PutChar(c);
	delay_ms(20);
	RS485_4_TX = 0;
}


void RS485_4_Puts(char *s)
{
	RS485_4_TX = 1;
	USART4_PutString((unsigned char*)s);
	delay_ms(20);
	RS485_4_TX = 0;
}




void System_SendMessage(ErrTypeDef NameOfErr)
{
#ifdef SOLUTION1
	char buff[20]={'\0'};
	sprintf(buff,"%d@%d#",NameOfErr.ErrName,NameOfErr.ErrHmiAddr.value);
	RS485_4_Puts(buff);
#endif
#ifdef TEST_MSG
	RS485_Puts(buff);
#endif
}



#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
