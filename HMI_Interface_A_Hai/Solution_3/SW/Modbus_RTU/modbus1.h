#include <stdint.h>
#include "main.h"

#ifndef MODBUSSLAVE1
#define MODBUSSLAVE1

#include "modbus.h"
#include "modbusDevice.h"


#define bitSet(x,n)				x|=(1<<n)
#define bitClear(x,n)			x&=(~(1<<n))

       
void Modbus1_Init(USART_TypeDef* USARTx, uint32_t  BaudRate);  
uint32_t Modbus1_GetBaudrate(void);

void Modbus1_CheckSerial(void);   
void Modbus1_SerialRx(void);
void Modbus1_Run(void);
void Clear_Modbus1_RX_Buff(void);
void Clear_Modbus1_TX_Buff(void);


#endif /*MODBUSSLAVE1*/
