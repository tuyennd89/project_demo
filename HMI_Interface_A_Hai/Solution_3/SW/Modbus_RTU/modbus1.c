#include "modbusSlave.h"
#include "modbus1.h"
#include "modbusDevice.h"


unsigned char *_msg1,_len1;

unsigned int _baud1, _crc1, _frameDelay1;
uint16_t *_data1;


unsigned char Modbus1_RX_Buff[BUFFER_SIZE]={'\0'};
unsigned char Modbus1_TX_Buff[50]={'\0'};
uint8_t Modbus1_Index=0;
boolean Modbus1_Received;

/*
Set the Serial Baud rate.
Reconfigure the UART for 8 data bits, no parity, and 1 stop bit.
and flush the serial port.
*/
void Modbus1_Init(USART_TypeDef* USARTx, uint32_t  BaudRate)
{
    USART1_Init(BaudRate,ENABLE);
		modbusRegBank();
    MODBUS1_TX = 0;
}


/*
  Checks the UART for query data
*/
void Modbus1_CheckSerial(void)
{
	if(Modbus1_Received == true)
	{
		Modbus1_Received=false;
		_len1=Modbus1_Index;
		//RS485_PutNum(_len1);
		Modbus1_Index=0;
	}
}

/*
Copies the contents of the UART to a buffer
*/
void Modbus1_SerialRx(void)
{
	unsigned char i;

	//allocate memory for the incoming query message
	_msg1 = (unsigned char*) malloc(_len1);
		//copy the query byte for byte to the new buffer
		for (i=0 ; i < _len1 ; i++)
			_msg1[i] = Modbus1_RX_Buff[i];
}



void Modbus1_Run(void)
{

//	unsigned char deviceId;
	unsigned char funcType;
	uint16_t field1;
	uint16_t field2;
	uint16_t field3;
    
    uint8_t j=0;
	//int i;
	
	//initialize mesasge length
	_len1 = 0;

	//check for data in the recieve buffer
	Modbus1_CheckSerial();

	//if there is nothing in the recieve buffer, bail.
	if(_len1 == 0)
	{
		return;
	}
	else
	{
		//RS485_PutNum(_len1);
	}

	//retrieve the query message from the serial uart
	Modbus1_SerialRx();
	
	//if the message id is not 255, and
	// and device id does not match bail
	if( (_msg1[0] != 0xFF) && 
		(_msg1[0] != modbusDevice_getId()) )
	{
		return;
	}
	//calculate the checksum of the query message minus the checksum it came with.
	_crc1 = Modbus_CalcCrc(_msg1,_len1);

	//if the checksum does not match, ignore the message
	if ( _crc1 != ((_msg1[_len1 - 2] << 8) + _msg1[_len1 - 1]))
		return;
	
	//copy the function type from the incoming query
	funcType = _msg1[1];

	//copy field 1 from the incoming query
	field1	= (_msg1[2] << 8) | _msg1[3];

	//copy field 2 from the incoming query
	field2  = (_msg1[4] << 8) | _msg1[5];
	//RS485_PutNum(field2);
	
  _data1 = (uint16_t*)malloc(field2);
	
  for(j=0;j<field2;j++)
  {
      _data1[j]=(_msg1[(j*2)+7] << 8) | _msg1[(j*2)+8];
  }
    
    //Data in case WRITE_MULTIPLE_REGISTERS
    field3  = (_msg1[7] << 8) | _msg1[8];
    
	//free the allocated memory for the query message
	free(_msg1);
	//reset the message length;
	_len1 = 0;

	//generate query response based on function type
	switch(funcType)
	{
	case READ_DI:
		_len1 = Modbus_GetDigitalStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("READ_DI:\n");
		break;
	case READ_DO:
		_len1 = Modbus_GetDigitalStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("READ_DO:\n");
		break;
	case READ_AI:
		_len1 = Modbus_GetAnalogStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("READ_AI:\n");
		break;
	case READ_AO:
		_len1 = Modbus_GetAnalogStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("READ_AO:\n");
		break;
	case WRITE_DO:
		_len1 = Modbus_SetStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("WRITE_DO:\n");
		break;
	case WRITE_AO:
		_len1 = Modbus_SetStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("WRITE_AO:\n");
		break;
  case WRITE_MULTIPLE_COILS:
    _len1 = Modbus_SetStatus(funcType, field1, field2,Modbus1_TX_Buff);
		//RS485_Puts("WRITE_MULTIPLE_COILS:\n");
    break;
  case WRITE_MULTIPLE_REGISTERS:
    _len1 = Modbus_SetNStatus(funcType, field1, _data1,field2,Modbus1_TX_Buff);
		//RS485_Puts("WRITE_MULTIPLE_REGISTERS:\n");
    break;
	default:
		//return;
		break;
	}
	free(_data1);

	//if a reply was generated
	if(_len1)
	{
		int i;
    MODBUS1_TX = 1;
		//send the reply to the serial UART
		//Senguino doesn't support a bulk serial write command....
		for(i = 0 ; i < _len1 ; i++)
			USART1_PutChar(Modbus1_TX_Buff[i]);
		//free the allocated memory for the reply message
		delay_ms(10);
		Clear_Modbus1_RX_Buff();
		Clear_Modbus1_TX_Buff();
		//reset the message length
		_len1 = 0;
		Modbus1_Index=0;
    MODBUS1_TX = 0;
	}
}






void USART1_IRQHandler(void)
{
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        Modbus1_RX_Buff[Modbus1_Index++]=USART_ReceiveData(USART1);
    }
    if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
    {
        USART1->SR;
        USART1->DR;
        Modbus1_Received=true;
    }
}
    	

void Clear_Modbus1_RX_Buff(void)
{
    uint16_t i=0;
	  for(i=0;i<BUFFER_SIZE;i++)
	     Modbus1_RX_Buff[i]='\0';
}	

void Clear_Modbus1_TX_Buff(void)
{
    uint16_t i=0;
	  for(i=0;i<50;i++)
	     Modbus1_TX_Buff[i]='\0';
}


