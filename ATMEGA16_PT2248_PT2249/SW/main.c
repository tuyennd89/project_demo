/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.0 Professional
Automatic Program Generator
� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 11/4/2017
Author  : NeVaDa
Company : 
Comments: 


Chip type               : ATmega16
Program type            : Application
AVR Core Clock frequency: 1.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 256
*****************************************************/

#include <mega16.h>
#include <delay.h>

// Declare your global variables here

#define IN1         PINC.1
#define IN2         PINC.2
#define IN3         PINC.3
#define IN4         PINC.4
#define IN5         PINC.5
#define IN6         PINC.6
#define IN7         PINC.7

#define LED1        PORTD.0
#define LED2        PORTD.1
#define LED3        PORTD.2
#define LED4        PORTD.3
#define LED5        PORTD.4
#define LED6        PORTD.5
#define LED7        PORTD.6




void main(void)
{
// Declare your local variables here

// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTA=0x00;
DDRA=0x00;

// Port B initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTB=0x00;
DDRB=0x00;

// Port C initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P 
PORTC=0x00;
DDRC=0x00;

// Port D initialization
// Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=Out Func1=Out Func0=Out 
// State7=1 State6=1 State5=1 State4=1 State3=1 State2=1 State1=1 State0=1 
PORTD=0xFF;
DDRD=0xFF;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: Timer1 Stopped
// Mode: Normal top=0xFFFF
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x00;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer2 Stopped
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
MCUCR=0x00;
MCUCSR=0x00;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x00;

// USART initialization
// USART disabled
UCSRB=0x00;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

// ADC initialization
// ADC disabled
ADCSRA=0x00;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;
while (1)
      {
      // Place your code here 
        if(IN1==1)
        {           
            delay_ms(2);
            if(IN1==1)
            {
                LED1=~LED1;
                while(IN1==1);
            }
        }    
        
        if(IN2==1)
        {           
            delay_ms(2);
            if(IN2==1)
            {
                LED2=~LED2;
                while(IN2==1);
            }
        }     
        
        if(IN3==1)
        {           
            delay_ms(2);
            if(IN3==1)
            {
                LED3=~LED3;
                while(IN3==1);
            }
        }
        
        if(IN4==1)
        {           
            delay_ms(2);
            if(IN4==1)
            {
                LED4=~LED4;
                while(IN4==1);
            }
        } 
        
        if(IN5==1)
        {           
            delay_ms(2);
            if(IN5==1)
            {
                LED5=~LED5;
                while(IN5==1);
            }
        }
        
        if(IN6==1)
        {           
            delay_ms(2);
            if(IN6==1)
            {
                LED6=~LED6;
                while(IN6==1);
            }
        }
        
        if(IN7==1)
        {           
            delay_ms(2);
            if(IN7==1)
            {
                LED7=~LED7;
                while(IN7==1);
            }
        }

      }
}
