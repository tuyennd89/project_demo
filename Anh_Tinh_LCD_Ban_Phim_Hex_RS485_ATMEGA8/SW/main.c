/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.0 Professional
Automatic Program Generator
� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 10/21/2017
Author  : NeVaDa
Company : 
Comments: 


Chip type               : ATmega8
Program type            : Application
AVR Core Clock frequency: 8.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 256
*****************************************************/


#include "main.h"

#ifndef RXB8
#define RXB8 1
#endif

#ifndef TXB8
#define TXB8 0
#endif

#ifndef UPE
#define UPE 2
#endif

#ifndef DOR
#define DOR 3
#endif

#ifndef FE
#define FE 4
#endif

#ifndef UDRE
#define UDRE 5
#endif

#ifndef RXC
#define RXC 7
#endif

#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<DOR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)


// USART Receiver buffer
#define RX_BUFFER_SIZE 150


char Data_Buffer[RX_BUFFER_SIZE];
unsigned char index=0;

char rx_buffer[RX_BUFFER_SIZE];

#if RX_BUFFER_SIZE <= 256
unsigned char rx_wr_index,rx_rd_index,rx_counter;
#else
unsigned int rx_wr_index,rx_rd_index,rx_counter;
#endif

// This flag is set on USART Receiver buffer overflow
bit rx_buffer_overflow;

// USART Receiver interrupt service routine
interrupt [USART_RXC] void usart_rx_isr(void)
{
char status,data;
status=UCSRA;
data=UDR;
if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
   {
   rx_buffer[rx_wr_index++]=data;
#if RX_BUFFER_SIZE == 256
   // special case for receiver buffer size=256
   if (++rx_counter == 0)
   {
#else
   if (rx_wr_index == RX_BUFFER_SIZE) rx_wr_index=0;
   if (++rx_counter == RX_BUFFER_SIZE)
      {
      rx_counter=0;
#endif
      rx_buffer_overflow=1;
      }
   }
}

#ifndef _DEBUG_TERMINAL_IO_
// Get a character from the USART Receiver buffer
#define _ALTERNATE_GETCHAR_
#pragma used+
char getchar(void)
{
char data;
while (rx_counter==0);
data=rx_buffer[rx_rd_index++];
#if RX_BUFFER_SIZE != 256
if (rx_rd_index == RX_BUFFER_SIZE) rx_rd_index=0;
#endif
#asm("cli")
--rx_counter;
#asm("sei")
return data;
}
#pragma used-
#endif



// Declare your global variables here
char *Device_ID = "001";

uint8_t key=0;
//char lcd_buff[16];

Mode_TypeDef sys_mode;
char timer_count=0;



char PASS_Buffer[10]={'\0'};
char index_password=0;
char pass_stt=0;
char set_ok=0;
char enter_en=0;

void main(void)
{
// Declare your local variables here

// Input/Output Ports initialization
// Port B initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTB=0x00;
DDRB=0x00;

// Port C initialization
// Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTC=0xFF;
DDRC=0x00;

// Port D initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
PORTD=0x02;
DDRD=0xFE;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
TCCR0=0x00;
TCNT0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 7.813 kHz
// Mode: Normal top=0xFFFF
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: On
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x05;
TCNT1H=0x63;
TCNT1L=0xC0;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer2 Stopped
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
MCUCR=0x00;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x04;

// USART initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART Receiver: On
// USART Transmitter: On
// USART Mode: Asynchronous
// USART Baud Rate: 9600
UCSRA=0x00;
UCSRB=0x98;
UCSRC=0x86;
UBRRH=0x00;
UBRRL=0x33;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

// ADC initialization
// ADC disabled
ADCSRA=0x00;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;

// Alphanumeric LCD initialization
// Connections specified in the
// Project|Configure|C Compiler|Libraries|Alphanumeric LCD menu:
// RS - PORTB Bit 0
// RD - PORTB Bit 1
// EN - PORTB Bit 2
// D4 - PORTB Bit 4
// D5 - PORTB Bit 5
// D6 - PORTB Bit 6
// D7 - PORTB Bit 7
// Characters/line: 16
lcd_init(16);

// Global enable interrupts
#asm("sei")
LED_LCD=0;

RS485_Puts("ABCDEFGH");

lcd_gotoxy(0,0);
lcd_puts("Welcome...      ");

while (1)
    {
        // Place your code here
        lcd_clear();
        Clear_PassBuffer();
        set_ok=0;
        enter_en=0;
        while(sys_mode==DEFAULT)
        {
            Default_Mode();
        }
        lcd_clear();
        Clear_PassBuffer();
        set_ok=0;
        enter_en=0;
        while(sys_mode==PASS_OFF)
        {
            Pass_Off_Mode();
        }
        lcd_clear();
        Clear_PassBuffer();
        set_ok=0;
        enter_en=0;
        while(sys_mode==PASS_ON)
        {
            Pass_On_Mode();
        }
        lcd_clear();
        Clear_PassBuffer();
        set_ok=0;
        while(sys_mode==CHANGE_PASS)
        {
            Change_Pass_Mode();
        }
        lcd_clear();
        Clear_PassBuffer();
        set_ok=0;
        enter_en=0;
        while(sys_mode==INFO)
        {
            Information_Mode();
        }

    }
}



char Read_Buffer(void)
{
  unsigned char Val_return=0;
  if(rx_counter==0)
  {
    Val_return = 0;
  }
  else
  {
    while(rx_counter>0)
    {
       Data_Buffer[index++]=getchar();
       delay_ms(2);
    }
    Val_return = 1;
  }
  return Val_return;  
}


void Clear_Data_Buffer(void)
{
    unsigned char i=0;
    for(i=0;i<255;i++)
        Data_Buffer[i]='\0';
    index=0;    
}

//Check password cu dung hay sai
char Check_OldPassword(void)
{
    char compare_buffer[16]={'\0'};
    char i=0;
    char *start;
    char old_pw_stt[16]={'\0'};
    char Val_Return=0;
    
    sprintf(compare_buffer,"@%sOLPW_",Device_ID);
    if(Read_Buffer()==1)
    {
        if(strncmp(Data_Buffer,compare_buffer,9)==0)
        {
            start=strchr(Data_Buffer,'_');
            i=0;
            start++;
            while((*start!='#') && (i<16))
            {
                old_pw_stt[i++]=*start;
                start++;
            }
            if(strcmp(old_pw_stt,"TRUE")==0)
                Val_Return = 1;
            else
                Val_Return = 0;
        }
        Clear_Data_Buffer();
    }
    return Val_Return;
}
//Hien thi thong tin len LCD
void Display_Info(void)
{
    char line1_buffer[16]={'\0'};
    char line2_buffer[16]={'\0'};
    char compare_buffer[16]={'\0'};
    char *start_line1,*start_line2;
    char i=0;
    
    sprintf(compare_buffer,"@%sDP",Device_ID);
    
    if(Read_Buffer()==1)
    {
        if(strncmp(Data_Buffer,compare_buffer,6)==0)
        {
            start_line1=strchr(Data_Buffer,'_');
            start_line2=strchr(Data_Buffer,'&');
            i=0;
            start_line1++;
            while((*start_line1!='&') && (i<16))
            {
                line1_buffer[i++]=*start_line1;
                start_line1++;
            }
            i=0;
            start_line2++;
            while((*start_line2!='#') && (i<16))
            {  
                line2_buffer[i++]=*start_line2;
                start_line2++;
            }
            lcd_clear();
            lcd_gotoxy(0,0);
            lcd_puts(line1_buffer);  
            lcd_gotoxy(0,1);
            lcd_puts(line2_buffer);
            set_ok=1;
        }
        Clear_Data_Buffer();
    }
}

//Xoa bo dem password
void Clear_PassBuffer(void)
{
    PASS_Buffer[0]='\0';
    index_password = 0;
}

//Chuong trinh default mode
void Default_Mode(void)
{
   if(set_ok==0)
   {
       lcd_gotoxy(0,0);
       lcd_puts("Welcome...      ");
   }
   key=KEY4X4_GetKey(); 
   if(key)
   {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
        lcd_gotoxy(0,0);
        lcd_puts("Tat He Thong    "); 
        set_ok=1;
        if(key=='#' && enter_en==1)
        {
            sys_mode++;
        }    
        while(key)
        {
            key=KEY4X4_GetKey();
        }
        enter_en=1;
   } 
}

//Chuong trinh password off
void Pass_Off_Mode(void)
{
    char RS485_TX_Buffer[30]={'\0'};
    if(set_ok==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Password OFF    ");
    } 
    key=KEY4X4_GetKey(); 
    if(key)
    {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
        //switch mode
        if(key=='*')
        {
            sys_mode++;
        } else
        if(key=='#')
        {
            sprintf(RS485_TX_Buffer,"@%sPWOF%s#",Device_ID,PASS_Buffer);
            RS485_Puts(RS485_TX_Buffer);
        } else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                ");
        } 
        else
        {   
            lcd_gotoxy(index_password,1);
            PASS_Buffer[index_password++]=key;
            lcd_putchar('*');
        }
             
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    } 
    Display_Info();
}

//Chuong trinh password on
void Pass_On_Mode(void)
{
    char RS485_TX_Buffer[30]={'\0'};
    if(set_ok==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Password ON     ");
    } 
    key=KEY4X4_GetKey();
    if(key)
    {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
         
        if(key=='*')
        {
            sys_mode++;
        } else
        if(key=='#')
        {
            sprintf(RS485_TX_Buffer,"@%sPWON%s#",Device_ID,PASS_Buffer);
            RS485_Puts(RS485_TX_Buffer);
        } else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                ");
        } 
        else
        {   
            lcd_gotoxy(index_password,1);
            PASS_Buffer[index_password++]=key;
            lcd_putchar('*');
        }
         
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    }
    Display_Info();    
}


//Chuong trinh thay doi password
void Change_Pass_Mode(void)
{
    char RS485_TX_Buffer[30]={'\0'};
    key=KEY4X4_GetKey();
    if(set_ok==0)
    {
        if(pass_stt==0)
        {
            lcd_gotoxy(0,0);
            lcd_puts("Doi Password    ");
        }
    
        if(pass_stt==1)
        {
            lcd_gotoxy(0,0);
            lcd_puts("Old Password    ");
        }
  
        if(pass_stt==2)
        {
            lcd_gotoxy(0,0);
            lcd_puts("New Password    ");
        }
    }
    
    if(key)
    {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
         
        if(key=='*')
        {
            sys_mode++;
        } else
        if(key=='#')
        {
            if(pass_stt==0)
            {
                pass_stt++;
            } else
            if(pass_stt==1)
            {
                sprintf(RS485_TX_Buffer,"@%sOLPW%s#",Device_ID,PASS_Buffer);
                RS485_Puts(RS485_TX_Buffer);
                Clear_PassBuffer();
            } else
            if(pass_stt==2)
            {
                sprintf(RS485_TX_Buffer,"@%sNEPW%s#",Device_ID,PASS_Buffer);
                RS485_Puts(RS485_TX_Buffer);
                Clear_PassBuffer();
            }
              
        } else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                ");
        } 
        else
        {   
            if(pass_stt== 1 || pass_stt==2)
            {
                lcd_gotoxy(index_password,1);
                PASS_Buffer[index_password++]=key;
                lcd_putchar('*');
            }
        }
         
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    }
    //kiem tra password cu dung hay sai
    if(Check_OldPassword()==1)
    {
        pass_stt=2;
        lcd_clear();
    }
    Display_Info();
}

//Chuong trinh hien thi thong tin
void Information_Mode(void)
{
    char RS485_TX_Buffer[30]={'\0'};
    if(set_ok==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Thong Tin HT    ");
    } 
    key=KEY4X4_GetKey();
    if(key)
    {
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
        if(key=='*')
        {
            sys_mode=DEFAULT;
        } else
        if(key=='#')
        {
            sprintf(RS485_TX_Buffer,"@%s001DP_INFSYS#",Device_ID);
            RS485_Puts(RS485_TX_Buffer);
        }
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    }
    Display_Info();
}


// Timer1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
// Reinitialize Timer1 value
TCNT1H=0x63C0 >> 8;
TCNT1L=0x63C0 & 0xff;
// Place your code here
    timer_count++;
    if(timer_count>3)
    {   
        LED_LCD = 0;
        timer_count=0;
        sys_mode=DEFAULT;  
        set_ok=0;
    }
}




void RS485_Puts(char *s)
{
    RS485_TX = 1;
    while(*s)
    {           
        while((UCSRA&0x20)==0x00);
        UDR=*s;
        s++;
    }      
    delay_ms(500);
    //RS485_TX = 0;  
}
