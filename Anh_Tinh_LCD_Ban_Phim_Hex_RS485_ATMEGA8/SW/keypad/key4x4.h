#ifndef __KEY_4X4_H
#define __KEY_4X4_H
#include "main.h"
 
#define         KEY4X4_ROW1     PINC.0
#define         KEY4X4_ROW2     PINC.1
#define         KEY4X4_ROW3     PINC.2
#define         KEY4X4_ROW4     PINC.3

#define         KEY4X4_COL1     PORTD.4 
#define         KEY4X4_COL2     PORTD.5 
#define         KEY4X4_COL3     PORTD.6 
#define         KEY4X4_COL4     PORTD.7


 /*******************************************************************************
Noi Dung    :   Kiem tra co nut duoc an hay khong..
Tham Bien   :   Khong.
Tra Ve      :   1:   Neu co nut duoc an.
                0:   Neu khong co nut duoc an.
********************************************************************************/
uint8_t  KEY4X4_IsPush(void);
 /*******************************************************************************
Noi Dung    :   Keo hang thu i xuong muc logic 0, de kiem tra co nut duoc an tai 
                hang thu i hay khong.
Tham Bien   :   i: vi tri hang can kiem tra.
Tra Ve      :   Khong.
********************************************************************************/
void KEY4X4_CheckRow(uint8_t  i);
 /*******************************************************************************
Noi Dung    :   Lay gia tri nut nhan duoc an.
Tham Bien   :   Khong.
Tra Ve      :   0:     Neu khong co nut duoc an.
            khac 0: Gia tri cua nut an.
********************************************************************************/
uint8_t  KEY4X4_GetKey(void);
#endif  /*__KEY_4X4_H*/
