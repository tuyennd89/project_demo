#include "key4x4.h"
static uint8_t  KEY_3x3[4][4]={'1','2','3','A',
                               '4','5','6','B',
                               '7','8','9','C',
                               '*','0','#','D'
                               };
                                               
 /*******************************************************************************
Noi Dung    :   Kiem tra co nut duoc an hay khong..
Tham Bien   :   Khong.
Tra Ve      :   1:   Neu co nut duoc an.
                0:   Neu khong co nut duoc an.
********************************************************************************/
uint8_t  KEY4X4_IsPush(void)
{
   if((KEY4X4_ROW1==0)|(KEY4X4_ROW2==0)|(KEY4X4_ROW3==0) |(KEY4X4_ROW4==0))
   {
       return 1;
   }
   else 
   {
       return 0;
   }
}
 /*******************************************************************************
Noi Dung    :   Keo hang thu i xuong muc logic 0, de kiem tra co nut duoc an tai 
                hang thu i hay khong.
Tham Bien   :   i: vi tri hang can kiem tra.
Tra Ve      :   Khong.
********************************************************************************/
void KEY4X4_CheckRow(uint8_t  i)
{
   KEY4X4_COL1=KEY4X4_COL2=KEY4X4_COL3=KEY4X4_COL4=1;
   if(i==0)
   {
      KEY4X4_COL1=0;
   }
   else if(i==1)
   {
      KEY4X4_COL2=0;
   }
   else if(i==2)
   {
      KEY4X4_COL3=0;
   }
   else if(i==3)
   {
      KEY4X4_COL4=0;
   }
}
 /*******************************************************************************
Noi Dung    :   Lay gia tri nut nhan duoc an.
Tham Bien   :   Khong.
Tra Ve      :   0:     Neu khong co nut duoc an.
            khac 0: Gia tri cua nut an.
********************************************************************************/
uint8_t  KEY4X4_GetKey(void)
{
   uint8_t  i;
   KEY4X4_COL1=KEY4X4_COL2=KEY4X4_COL3=KEY4X4_COL4=0;
   if(KEY4X4_IsPush())
   {
      delay_ms(5);
      if(KEY4X4_IsPush())
      {         
         for(i=0;i<4;i++)
         {            
            KEY4X4_CheckRow(i);
            if(!KEY4X4_ROW1) return KEY_3x3[0][i];
            if(!KEY4X4_ROW2) return KEY_3x3[1][i];
            if(!KEY4X4_ROW3) return KEY_3x3[2][i];
            if(!KEY4X4_ROW4) return KEY_3x3[3][i];
         }      
      }
   }
   return 0;
}



