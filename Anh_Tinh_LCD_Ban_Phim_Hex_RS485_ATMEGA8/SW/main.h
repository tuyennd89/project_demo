#ifndef __MAIN_H_
#define __MAIN_H_

typedef unsigned char uint8_t;

typedef enum
{
    DEFAULT = 0,
    PASS_OFF,
    PASS_ON,
    CHANGE_PASS,
    INFO
}Mode_TypeDef;


#include <delay.h>


#include <mega8.h>

// Alphanumeric LCD Module functions
#include <alcd.h>

// Standard Input/Output functions
#include <stdio.h>
#include <string.h>
#include "keypad/key4x4.h"

#define LED_LCD         PORTD.3
#define RS485_TX        PORTD.2




char Read_Buffer(void);
void Clear_Data_Buffer(void);
char Check_OldPassword(void);
void Display_Info(void);
void Clear_PassBuffer(void);
void Default_Mode(void);
void Pass_Off_Mode(void);
void Pass_On_Mode(void);
void Change_Pass_Mode(void);
void Information_Mode(void);


void RS485_Puts(char *s);


#endif /*__MAIN_H_*/