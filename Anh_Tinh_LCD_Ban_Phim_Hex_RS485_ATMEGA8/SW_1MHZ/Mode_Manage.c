#include "Mode_Manage.h"

extern char Data_Buffer[RX_BUFFER_SIZE];

extern Mode_TypeDef sys_mode;
extern char timer_count;
extern char PASS_Buffer[16];
extern char index_password;
extern char pass_stt;
extern char set_ok;
extern char enter_en;
extern char *Device_ID;

extern char step_setting;
extern char httt_stt;

//Chuong trinh default mode
/*************************************************************************
 ******************************Begin Default Mode*************************
**************************************************************************/
void Default_Mode(void)
{
   uint8_t key=0;
   
   if(httt_stt==0)
   {
        lcd_gotoxy(0,0);
        lcd_puts("Welcome...      ");    
   }
   
   key=KEY4X4_GetKey(); 
   if(key)
   {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
        if(key=='*')
        {
            sys_mode++;
        }else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                ");
            if(httt_stt==1)
            {
                lcd_clear();
                httt_stt=0;
            }
        }     
        while(key)
        {
            key=KEY4X4_GetKey();
        }
   }           
   Display_Info(); 
}
/*************************************************************************
 ********************************End Default Mode*************************
**************************************************************************/

//Chuong trinh password off
/*************************************************************************
 *************************Begin PassWord Off Mode*************************
**************************************************************************/
void Pass_Off_Mode(void)
{
    uint8_t key=0;
    char RS485_TX_Buffer[30]={'\0'}; 
    
    if(step_setting==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Tat He Thong    ");
    }                
    if(step_setting==1)
    {
        lcd_gotoxy(0,0);
        lcd_puts("PassWord OFF    ");
    } 
    key=KEY4X4_GetKey(); 
    if(key)
    {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
        //switch mode
        if(key=='*')
        {
            sys_mode++;
        } else
        if(key=='#')
        {   
            if(step_setting==0)
            {
                step_setting=1;    
            } else  
            if(step_setting==1)       
            {                  
                sprintf(RS485_TX_Buffer,"@%sPWOF%s#%c",Device_ID,PASS_Buffer,'\0');
                RS485_Puts(RS485_TX_Buffer);
                Clear_PassBuffer();
            }
        } else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                "); 
            if(httt_stt==1)
            {
                //lcd_clear();
                httt_stt=0;
            }
        } 
        else
        {              
            if(step_setting==1)
            {            
                lcd_gotoxy(index_password,1);
                PASS_Buffer[index_password++]=key;
                PASS_Buffer[index_password]=0;
                lcd_putchar('*');
            }
        }
             
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    } 
    Display_Info();
}
/*************************************************************************
 ***************************End PassWord Off Mode*************************
**************************************************************************/


/*************************************************************************
 **************************Begin PassWord On Mode*************************
**************************************************************************/
void Pass_On_Mode(void)
{
    uint8_t key=0;
    char RS485_TX_Buffer[30]={'\0'}; 
    
    if(step_setting==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Bat He Thong    ");
    }                
    if(step_setting==1)
    {
        lcd_gotoxy(0,0);
        lcd_puts("PassWord ON     ");
    } 
    key=KEY4X4_GetKey();
    if(key)
    {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
         
        if(key=='*')
        {
            sys_mode++;
        } else
        if(key=='#')
        {       
            if(step_setting==0)
            {
                step_setting=1;    
            } else       
            if(step_setting==1)       
            {                  
                sprintf(RS485_TX_Buffer,"@%sPWON%s#%c",Device_ID,PASS_Buffer,'\0');
                RS485_Puts(RS485_TX_Buffer);  
                Clear_PassBuffer(); 
            }
        } else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                ");  
            if(httt_stt==1)
            {
                //lcd_clear();
                httt_stt=0;
            }
        } 
        else
        {     
            if(step_setting==1)
            {
                lcd_gotoxy(index_password,1);
                PASS_Buffer[index_password++]=key; 
                PASS_Buffer[index_password]=0;
                lcd_putchar('*');
            }
        }
         
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    }
    Display_Info();    
}

/*************************************************************************
 ****************************End PassWord On Mode*************************
**************************************************************************/

/*************************************************************************
 **************************Begin Change PassWord Mode*********************
**************************************************************************/
void Change_Pass_Mode(void)
{
    uint8_t key=0;
    char RS485_TX_Buffer[30]={'\0'};
    
    
    if(step_setting==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Doi Password    ");
        
    } else  
    if(step_setting==1)
    {    
        if(pass_stt==0)
        {
            lcd_gotoxy(0,0);
            lcd_puts("Old Password    ");    
        }
  
        if(pass_stt==1)
        {
            lcd_gotoxy(0,0);
            lcd_puts("New Password    ");
        }
    }
    key=KEY4X4_GetKey();
    if(key)
    {    
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
         
        if(key=='*')
        {
            sys_mode++;
        } else
        if(key=='#')
        {              
            if(step_setting==0)
            {
                step_setting=1;    
            } else   
            if(step_setting==1) 
            {        
                if(pass_stt==0)
                {
                    sprintf(RS485_TX_Buffer,"@%sOLPW%s#%c",Device_ID,PASS_Buffer,'\0');
                    RS485_Puts(RS485_TX_Buffer);
                    Clear_PassBuffer();
                } else
                if(pass_stt==1)
                {
                    sprintf(RS485_TX_Buffer,"@%sNEPW%s#%c",Device_ID,PASS_Buffer,'\0');
                    RS485_Puts(RS485_TX_Buffer);
                    Clear_PassBuffer();   
                    pass_stt=2;
                }
                
            } 
        } else
        if(key=='C')
        {
            Clear_PassBuffer();
            lcd_gotoxy(0,1);
            lcd_puts("                "); 
            if(httt_stt==1)
            {
                //lcd_clear();
                httt_stt=0;
            }
        } 
        else
        {       
            if(step_setting==1)
            {
                 if(pass_stt== 0 || pass_stt==1)
                {
                    lcd_gotoxy(index_password,1);
                    PASS_Buffer[index_password++]=key;   
                    PASS_Buffer[index_password]=0;
                    lcd_putchar('*');
                }
            }
        }
         
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    }
    //kiem tra password cu dung hay sai  
    if(Display_Info()==1)
    {
        pass_stt=1; 
        lcd_clear();
    }
    
}
/*************************************************************************
 **************************End Change PassWord Mode************************
**************************************************************************/


/*************************************************************************
 **************************Begin Display Info Mode*********************
**************************************************************************/
void Information_Mode(void)
{
    uint8_t key=0;
    char RS485_TX_Buffer[30]={'\0'};   
    
    if(step_setting==0)
    {
        lcd_gotoxy(0,0);
        lcd_puts("Thong Tin HT    ");
    } 
    key=KEY4X4_GetKey();
    if(key)
    {
        LED_LCD=1;
        timer_count=0;
        TCNT1H=0x63;
        TCNT1L=0xC0;
        if(key=='*')
        {
            sys_mode=DEFAULT;
        } else
        if(key=='#')
        {    
            if(step_setting==0)
            {     
                step_setting=1;
            } else                  
            if(step_setting==1)
            {                    
                sprintf(RS485_TX_Buffer,"@%s001DP_INFSYS#%c",Device_ID,'\0');
                RS485_Puts(RS485_TX_Buffer);
            }
        }
        while(key)
        {
            key=KEY4X4_GetKey();
        }
    }
    Display_Info();
}



/*************************************************************************
 **************************End Display Info Mode************************
**************************************************************************/



