
;CodeVisionAVR C Compiler V2.05.0 Professional
;(C) Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega8
;Program type             : Application
;Clock frequency          : 2.000000 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 256 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : Yes
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;global 'const' stored in FLASH: No
;Enhanced core instructions    : On
;Smart register allocation     : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega8
	#pragma AVRPART MEMORY PROG_FLASH 8192
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1119
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0060
	.EQU __SRAM_END=0x045F
	.EQU __DSTACK_SIZE=0x0100
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	RCALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _index=R5
	.DEF _rx_wr_index=R4
	.DEF _rx_rd_index=R7
	.DEF _rx_counter=R6
	.DEF _Device_ID=R8
	.DEF _key=R11
	.DEF _sys_mode=R10
	.DEF _timer_count=R13
	.DEF _index_password=R12

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	RJMP __RESET
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP _timer1_ovf_isr
	RJMP 0x00
	RJMP 0x00
	RJMP _usart_rx_isr
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00

_tbl10_G101:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G101:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

;REGISTER BIT VARIABLES INITIALIZATION
__REG_BIT_VARS:
	.DW  0x0000

_0x2B:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
_0x36:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0
_0x54:
	.DB  0x0,0x0,0x0,LOW(_0xA),HIGH(_0xA),0x0,0x0,0x0
	.DB  0x0
_0x0:
	.DB  0x30,0x30,0x31,0x0,0x57,0x65,0x6C,0x63
	.DB  0x6F,0x6D,0x65,0x2E,0x2E,0x2E,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x0,0x40,0x25,0x73
	.DB  0x4F,0x4C,0x50,0x57,0x5F,0x0,0x54,0x52
	.DB  0x55,0x45,0x0,0x40,0x25,0x73,0x44,0x50
	.DB  0x0
_0x20003:
	.DB  0x31,0x32,0x33,0x41,0x34,0x35,0x36,0x42
	.DB  0x37,0x38,0x39,0x43,0x2A,0x30,0x23,0x44
_0x4000F:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0
_0x40024:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0
_0x40039:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0
_0x40058:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0,0x0,0x0
_0x40000:
	.DB  0x57,0x65,0x6C,0x63,0x6F,0x6D,0x65,0x2E
	.DB  0x2E,0x2E,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x0,0x54,0x61,0x74,0x20,0x48,0x65
	.DB  0x20,0x54,0x68,0x6F,0x6E,0x67,0x20,0x20
	.DB  0x20,0x20,0x0,0x50,0x61,0x73,0x73,0x57
	.DB  0x6F,0x72,0x64,0x20,0x4F,0x46,0x46,0x20
	.DB  0x20,0x20,0x20,0x0,0x40,0x25,0x73,0x50
	.DB  0x57,0x4F,0x46,0x25,0x73,0x23,0x25,0x63
	.DB  0x0,0x42,0x61,0x74,0x20,0x48,0x65,0x20
	.DB  0x54,0x68,0x6F,0x6E,0x67,0x20,0x20,0x20
	.DB  0x20,0x0,0x50,0x61,0x73,0x73,0x57,0x6F
	.DB  0x72,0x64,0x20,0x4F,0x4E,0x20,0x20,0x20
	.DB  0x20,0x20,0x0,0x40,0x25,0x73,0x50,0x57
	.DB  0x4F,0x4E,0x25,0x73,0x23,0x25,0x63,0x0
	.DB  0x44,0x6F,0x69,0x20,0x50,0x61,0x73,0x73
	.DB  0x77,0x6F,0x72,0x64,0x20,0x20,0x20,0x20
	.DB  0x0,0x4F,0x6C,0x64,0x20,0x50,0x61,0x73
	.DB  0x73,0x77,0x6F,0x72,0x64,0x20,0x20,0x20
	.DB  0x20,0x0,0x4E,0x65,0x77,0x20,0x50,0x61
	.DB  0x73,0x73,0x77,0x6F,0x72,0x64,0x20,0x20
	.DB  0x20,0x20,0x0,0x40,0x25,0x73,0x4F,0x4C
	.DB  0x50,0x57,0x25,0x73,0x23,0x25,0x63,0x0
	.DB  0x40,0x25,0x73,0x4E,0x45,0x50,0x57,0x25
	.DB  0x73,0x23,0x25,0x63,0x0,0x54,0x68,0x6F
	.DB  0x6E,0x67,0x20,0x54,0x69,0x6E,0x20,0x48
	.DB  0x54,0x20,0x20,0x20,0x20,0x0,0x40,0x25
	.DB  0x73,0x30,0x30,0x31,0x44,0x50,0x5F,0x49
	.DB  0x4E,0x46,0x53,0x59,0x53,0x23,0x25,0x63
	.DB  0x0
_0x2000003:
	.DB  0x80,0xC0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  0x02
	.DW  __REG_BIT_VARS*2

	.DW  0x04
	.DW  _0xA
	.DW  _0x0*2

	.DW  0x11
	.DW  _0xD
	.DW  _0x0*2+4

	.DW  0x05
	.DW  _0x34
	.DW  _0x0*2+30

	.DW  0x05
	.DW  _0x4B
	.DW  _0x0*2+30

	.DW  0x09
	.DW  0x05
	.DW  _0x54*2

	.DW  0x10
	.DW  _KEY_3x3_G001
	.DW  _0x20003*2

	.DW  0x11
	.DW  _0x40004
	.DW  _0x40000*2

	.DW  0x11
	.DW  _0x40004+17
	.DW  _0x40000*2+17

	.DW  0x11
	.DW  _0x40011
	.DW  _0x40000*2+34

	.DW  0x11
	.DW  _0x40011+17
	.DW  _0x40000*2+51

	.DW  0x11
	.DW  _0x40011+34
	.DW  _0x40000*2+17

	.DW  0x11
	.DW  _0x40026
	.DW  _0x40000*2+81

	.DW  0x11
	.DW  _0x40026+17
	.DW  _0x40000*2+98

	.DW  0x11
	.DW  _0x40026+34
	.DW  _0x40000*2+17

	.DW  0x11
	.DW  _0x4003B
	.DW  _0x40000*2+128

	.DW  0x11
	.DW  _0x4003B+17
	.DW  _0x40000*2+145

	.DW  0x11
	.DW  _0x4003B+34
	.DW  _0x40000*2+162

	.DW  0x11
	.DW  _0x4003B+51
	.DW  _0x40000*2+17

	.DW  0x11
	.DW  _0x4005A
	.DW  _0x40000*2+205

	.DW  0x02
	.DW  __base_y_G100
	.DW  _0x2000003*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,__SRAM_START
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x160

	.CSEG
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V2.05.0 Professional
;Automatic Program Generator
;� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 10/21/2017
;Author  : NeVaDa
;Company :
;Comments:
;
;
;Chip type               : ATmega8
;Program type            : Application
;AVR Core Clock frequency: 1.000000 MHz
;Memory model            : Small
;External RAM size       : 0
;Data Stack size         : 256
;*****************************************************/
;
;
;#include "main.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;#ifndef RXB8
;#define RXB8 1
;#endif
;
;#ifndef TXB8
;#define TXB8 0
;#endif
;
;#ifndef UPE
;#define UPE 2
;#endif
;
;#ifndef DOR
;#define DOR 3
;#endif
;
;#ifndef FE
;#define FE 4
;#endif
;
;#ifndef UDRE
;#define UDRE 5
;#endif
;
;#ifndef RXC
;#define RXC 7
;#endif
;
;#define FRAMING_ERROR (1<<FE)
;#define PARITY_ERROR (1<<UPE)
;#define DATA_OVERRUN (1<<DOR)
;#define DATA_REGISTER_EMPTY (1<<UDRE)
;#define RX_COMPLETE (1<<RXC)
;
;
;// USART Receiver buffer
;
;char Data_Buffer[RX_BUFFER_SIZE];
;unsigned char index=0;
;
;char rx_buffer[RX_BUFFER_SIZE];
;
;#if RX_BUFFER_SIZE <= 256
;unsigned char rx_wr_index,rx_rd_index,rx_counter;
;#else
;unsigned int rx_wr_index,rx_rd_index,rx_counter;
;#endif
;
;// This flag is set on USART Receiver buffer overflow
;bit rx_buffer_overflow;
;
;// USART Receiver interrupt service routine
;interrupt [USART_RXC] void usart_rx_isr(void)
; 0000 0050 {

	.CSEG
_usart_rx_isr:
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 0051 char status,data;
; 0000 0052 status=UCSRA;
	RCALL __SAVELOCR2
;	status -> R17
;	data -> R16
	IN   R17,11
; 0000 0053 data=UDR;
	IN   R16,12
; 0000 0054 if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	MOV  R30,R17
	ANDI R30,LOW(0x1C)
	BRNE _0x3
; 0000 0055    {
; 0000 0056    rx_buffer[rx_wr_index++]=data;
	MOV  R30,R4
	INC  R4
	RCALL SUBOPT_0x0
	ST   Z,R16
; 0000 0057 #if RX_BUFFER_SIZE == 256
; 0000 0058    // special case for receiver buffer size=256
; 0000 0059    if (++rx_counter == 0)
; 0000 005A    {
; 0000 005B #else
; 0000 005C    if (rx_wr_index == RX_BUFFER_SIZE) rx_wr_index=0;
	LDI  R30,LOW(150)
	CP   R30,R4
	BRNE _0x4
	CLR  R4
; 0000 005D    if (++rx_counter == RX_BUFFER_SIZE)
_0x4:
	INC  R6
	LDI  R30,LOW(150)
	CP   R30,R6
	BRNE _0x5
; 0000 005E       {
; 0000 005F       rx_counter=0;
	CLR  R6
; 0000 0060 #endif
; 0000 0061       rx_buffer_overflow=1;
	SET
	BLD  R2,0
; 0000 0062       }
; 0000 0063    }
_0x5:
; 0000 0064 }
_0x3:
	RCALL __LOADLOCR2P
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	RETI
;
;#ifndef _DEBUG_TERMINAL_IO_
;// Get a character from the USART Receiver buffer
;#define _ALTERNATE_GETCHAR_
;#pragma used+
;char getchar(void)
; 0000 006B {
_getchar:
; 0000 006C char data;
; 0000 006D while (rx_counter==0);
	ST   -Y,R17
;	data -> R17
_0x6:
	TST  R6
	BREQ _0x6
; 0000 006E data=rx_buffer[rx_rd_index++];
	MOV  R30,R7
	INC  R7
	RCALL SUBOPT_0x0
	LD   R17,Z
; 0000 006F #if RX_BUFFER_SIZE != 256
; 0000 0070 if (rx_rd_index == RX_BUFFER_SIZE) rx_rd_index=0;
	LDI  R30,LOW(150)
	CP   R30,R7
	BRNE _0x9
	CLR  R7
; 0000 0071 #endif
; 0000 0072 #asm("cli")
_0x9:
	cli
; 0000 0073 --rx_counter;
	DEC  R6
; 0000 0074 #asm("sei")
	sei
; 0000 0075 return data;
	MOV  R30,R17
	RJMP _0x2080007
; 0000 0076 }
;#pragma used-
;#endif
;
;
;
;// Declare your global variables here
;char *Device_ID = "001";

	.DSEG
_0xA:
	.BYTE 0x4
;
;uint8_t key=0;
;//char lcd_buff[16];
;
;Mode_TypeDef sys_mode;
;char timer_count=0;
;char PASS_Buffer[16]={'\0'};
;char index_password=0;
;char pass_stt=0;
;char set_ok=0;
;char enter_en=0;
;char step_setting=0;
;
;char httt_stt=0;
;void main(void)
; 0000 008D {

	.CSEG
_main:
; 0000 008E // Declare your local variables here
; 0000 008F 
; 0000 0090 // Input/Output Ports initialization
; 0000 0091 // Port B initialization
; 0000 0092 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 0093 // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 0094 PORTB=0x00;
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 0095 DDRB=0x00;
	OUT  0x17,R30
; 0000 0096 
; 0000 0097 // Port C initialization
; 0000 0098 // Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 0099 // State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 009A PORTC=0xFF;
	LDI  R30,LOW(255)
	OUT  0x15,R30
; 0000 009B DDRC=0x00;
	LDI  R30,LOW(0)
	OUT  0x14,R30
; 0000 009C 
; 0000 009D // Port D initialization
; 0000 009E // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 009F // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 00A0 PORTD=0xFF;
	LDI  R30,LOW(255)
	OUT  0x12,R30
; 0000 00A1 DDRD=0xFE;
	LDI  R30,LOW(254)
	OUT  0x11,R30
; 0000 00A2 
; 0000 00A3 // Timer/Counter 0 initialization
; 0000 00A4 // Clock source: System Clock
; 0000 00A5 // Clock value: Timer 0 Stopped
; 0000 00A6 TCCR0=0x00;
	LDI  R30,LOW(0)
	OUT  0x33,R30
; 0000 00A7 TCNT0=0x00;
	OUT  0x32,R30
; 0000 00A8 
; 0000 00A9 // Timer/Counter 1 initialization
; 0000 00AA // Clock source: System Clock
; 0000 00AB // Clock value: 7.813 kHz
; 0000 00AC // Mode: Normal top=0xFFFF
; 0000 00AD // OC1A output: Discon.
; 0000 00AE // OC1B output: Discon.
; 0000 00AF // Noise Canceler: Off
; 0000 00B0 // Input Capture on Falling Edge
; 0000 00B1 // Timer1 Overflow Interrupt: On
; 0000 00B2 // Input Capture Interrupt: Off
; 0000 00B3 // Compare A Match Interrupt: Off
; 0000 00B4 // Compare B Match Interrupt: Off
; 0000 00B5 TCCR1A=0x00;
	OUT  0x2F,R30
; 0000 00B6 TCCR1B=0x04;
	LDI  R30,LOW(4)
	OUT  0x2E,R30
; 0000 00B7 TCNT1H=0x63;
	RCALL SUBOPT_0x1
; 0000 00B8 TCNT1L=0xC0;
; 0000 00B9 ICR1H=0x00;
	LDI  R30,LOW(0)
	OUT  0x27,R30
; 0000 00BA ICR1L=0x00;
	OUT  0x26,R30
; 0000 00BB OCR1AH=0x00;
	OUT  0x2B,R30
; 0000 00BC OCR1AL=0x00;
	OUT  0x2A,R30
; 0000 00BD OCR1BH=0x00;
	OUT  0x29,R30
; 0000 00BE OCR1BL=0x00;
	OUT  0x28,R30
; 0000 00BF 
; 0000 00C0 // Timer/Counter 2 initialization
; 0000 00C1 // Clock source: System Clock
; 0000 00C2 // Clock value: Timer2 Stopped
; 0000 00C3 // Mode: Normal top=0xFF
; 0000 00C4 // OC2 output: Disconnected
; 0000 00C5 ASSR=0x00;
	OUT  0x22,R30
; 0000 00C6 TCCR2=0x00;
	OUT  0x25,R30
; 0000 00C7 TCNT2=0x00;
	OUT  0x24,R30
; 0000 00C8 OCR2=0x00;
	OUT  0x23,R30
; 0000 00C9 
; 0000 00CA // External Interrupt(s) initialization
; 0000 00CB // INT0: Off
; 0000 00CC // INT1: Off
; 0000 00CD MCUCR=0x00;
	OUT  0x35,R30
; 0000 00CE 
; 0000 00CF // Timer(s)/Counter(s) Interrupt(s) initialization
; 0000 00D0 TIMSK=0x04;
	LDI  R30,LOW(4)
	OUT  0x39,R30
; 0000 00D1 
; 0000 00D2 // USART initialization
; 0000 00D3 // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 00D4 // USART Receiver: On
; 0000 00D5 // USART Transmitter: On
; 0000 00D6 // USART Mode: Asynchronous
; 0000 00D7 // USART Baud Rate: 9600
; 0000 00D8 UCSRA=0x00;
	LDI  R30,LOW(0)
	OUT  0xB,R30
; 0000 00D9 UCSRB=0x98;
	LDI  R30,LOW(152)
	OUT  0xA,R30
; 0000 00DA UCSRC=0x86;
	LDI  R30,LOW(134)
	OUT  0x20,R30
; 0000 00DB UBRRH=0x00;
	LDI  R30,LOW(0)
	OUT  0x20,R30
; 0000 00DC UBRRL=0x0C;
	LDI  R30,LOW(12)
	OUT  0x9,R30
; 0000 00DD 
; 0000 00DE // Analog Comparator initialization
; 0000 00DF // Analog Comparator: Off
; 0000 00E0 // Analog Comparator Input Capture by Timer/Counter 1: Off
; 0000 00E1 ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 00E2 SFIOR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
; 0000 00E3 
; 0000 00E4 // ADC initialization
; 0000 00E5 // ADC disabled
; 0000 00E6 ADCSRA=0x00;
	OUT  0x6,R30
; 0000 00E7 
; 0000 00E8 // SPI initialization
; 0000 00E9 // SPI disabled
; 0000 00EA SPCR=0x00;
	OUT  0xD,R30
; 0000 00EB 
; 0000 00EC // TWI initialization
; 0000 00ED // TWI disabled
; 0000 00EE TWCR=0x00;
	OUT  0x36,R30
; 0000 00EF 
; 0000 00F0 // Alphanumeric LCD initialization
; 0000 00F1 // Connections specified in the
; 0000 00F2 // Project|Configure|C Compiler|Libraries|Alphanumeric LCD menu:
; 0000 00F3 // RS - PORTB Bit 0
; 0000 00F4 // RD - PORTB Bit 1
; 0000 00F5 // EN - PORTB Bit 2
; 0000 00F6 // D4 - PORTB Bit 4
; 0000 00F7 // D5 - PORTB Bit 5
; 0000 00F8 // D6 - PORTB Bit 6
; 0000 00F9 // D7 - PORTB Bit 7
; 0000 00FA // Characters/line: 16
; 0000 00FB lcd_init(16);
	LDI  R30,LOW(16)
	ST   -Y,R30
	RCALL _lcd_init
; 0000 00FC 
; 0000 00FD // Global enable interrupts
; 0000 00FE #asm("sei")
	sei
; 0000 00FF LED_LCD=0;
	CBI  0x12,3
; 0000 0100 
; 0000 0101 lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0000 0102 lcd_puts("Welcome...      ");
	__POINTW1MN _0xD,0
	RCALL SUBOPT_0x3
	RCALL _lcd_puts
; 0000 0103 RS485_TX = 0;
	CBI  0x12,2
; 0000 0104 while (1)
_0x10:
; 0000 0105     {
; 0000 0106         // Place your code here
; 0000 0107         lcd_clear();
	RCALL SUBOPT_0x4
; 0000 0108         Clear_PassBuffer();
; 0000 0109         set_ok=0;
; 0000 010A         enter_en=0;
	RCALL SUBOPT_0x5
; 0000 010B         step_setting=0;
	RCALL SUBOPT_0x6
; 0000 010C         httt_stt=0;
	RCALL SUBOPT_0x7
; 0000 010D         while(sys_mode==DEFAULT)
_0x13:
	TST  R10
	BRNE _0x15
; 0000 010E         {
; 0000 010F             Default_Mode();
	RCALL _Default_Mode
; 0000 0110         }
	RJMP _0x13
_0x15:
; 0000 0111         lcd_clear();
	RCALL SUBOPT_0x4
; 0000 0112         Clear_PassBuffer();
; 0000 0113         set_ok=0;
; 0000 0114         step_setting=0;
	RCALL SUBOPT_0x6
; 0000 0115         enter_en=0;
	RCALL SUBOPT_0x5
; 0000 0116         httt_stt=0;
	RCALL SUBOPT_0x7
; 0000 0117         while(sys_mode==PASS_OFF)
_0x16:
	LDI  R30,LOW(1)
	CP   R30,R10
	BRNE _0x18
; 0000 0118         {
; 0000 0119             Pass_Off_Mode();
	RCALL _Pass_Off_Mode
; 0000 011A         }
	RJMP _0x16
_0x18:
; 0000 011B         lcd_clear();
	RCALL SUBOPT_0x4
; 0000 011C         Clear_PassBuffer();
; 0000 011D         set_ok=0;
; 0000 011E         step_setting=0;
	RCALL SUBOPT_0x6
; 0000 011F         enter_en=0;
	RCALL SUBOPT_0x5
; 0000 0120         httt_stt=0;
	RCALL SUBOPT_0x7
; 0000 0121         while(sys_mode==PASS_ON)
_0x19:
	LDI  R30,LOW(2)
	CP   R30,R10
	BRNE _0x1B
; 0000 0122         {
; 0000 0123             Pass_On_Mode();
	RCALL _Pass_On_Mode
; 0000 0124         }
	RJMP _0x19
_0x1B:
; 0000 0125         lcd_clear();
	RCALL SUBOPT_0x4
; 0000 0126         Clear_PassBuffer();
; 0000 0127         set_ok=0;
; 0000 0128         step_setting=0;
	RCALL SUBOPT_0x6
; 0000 0129         pass_stt=0;
	RCALL SUBOPT_0x8
; 0000 012A         httt_stt=0;
	RCALL SUBOPT_0x9
; 0000 012B         while(sys_mode==CHANGE_PASS)
_0x1C:
	LDI  R30,LOW(3)
	CP   R30,R10
	BRNE _0x1E
; 0000 012C         {
; 0000 012D             Change_Pass_Mode();
	RCALL _Change_Pass_Mode
; 0000 012E         }
	RJMP _0x1C
_0x1E:
; 0000 012F         lcd_clear();
	RCALL SUBOPT_0x4
; 0000 0130         Clear_PassBuffer();
; 0000 0131         set_ok=0;
; 0000 0132         enter_en=0;
	RCALL SUBOPT_0x5
; 0000 0133         step_setting=0;
	RCALL SUBOPT_0x6
; 0000 0134         httt_stt=0;
	RCALL SUBOPT_0x7
; 0000 0135         while(sys_mode==INFO)
_0x1F:
	LDI  R30,LOW(4)
	CP   R30,R10
	BRNE _0x21
; 0000 0136         {
; 0000 0137             Information_Mode();
	RCALL _Information_Mode
; 0000 0138         }
	RJMP _0x1F
_0x21:
; 0000 0139 
; 0000 013A     }
	RJMP _0x10
; 0000 013B }
_0x22:
	RJMP _0x22

	.DSEG
_0xD:
	.BYTE 0x11
;
;
;
;char Read_Buffer(void)
; 0000 0140 {

	.CSEG
_Read_Buffer:
; 0000 0141   unsigned char Val_return=0;
; 0000 0142   if(rx_counter==0)
	RCALL SUBOPT_0xA
;	Val_return -> R17
	TST  R6
	BRNE _0x23
; 0000 0143   {
; 0000 0144     Val_return = 0;
	LDI  R17,LOW(0)
; 0000 0145   }
; 0000 0146   else
	RJMP _0x24
_0x23:
; 0000 0147   {
; 0000 0148     while(rx_counter>0)
_0x25:
	LDI  R30,LOW(0)
	CP   R30,R6
	BRSH _0x27
; 0000 0149     {
; 0000 014A        Data_Buffer[index++]=getchar();
	MOV  R30,R5
	INC  R5
	RCALL SUBOPT_0xB
	PUSH R31
	PUSH R30
	RCALL _getchar
	POP  R26
	POP  R27
	ST   X,R30
; 0000 014B        delay_ms(2);
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	RCALL SUBOPT_0xC
; 0000 014C     }
	RJMP _0x25
_0x27:
; 0000 014D     Val_return = 1;
	LDI  R17,LOW(1)
; 0000 014E   }
_0x24:
; 0000 014F   return Val_return;
	MOV  R30,R17
	RJMP _0x2080007
; 0000 0150 }
;
;
;void Clear_Data_Buffer(void)
; 0000 0154 {
_Clear_Data_Buffer:
; 0000 0155     unsigned char i=0;
; 0000 0156     for(i=0;i<RX_BUFFER_SIZE;i++)
	RCALL SUBOPT_0xA
;	i -> R17
	LDI  R17,LOW(0)
_0x29:
	CPI  R17,150
	BRSH _0x2A
; 0000 0157         Data_Buffer[i]='\0';
	MOV  R30,R17
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0xD
	SUBI R17,-1
	RJMP _0x29
_0x2A:
; 0000 0158 index=0;
	CLR  R5
; 0000 0159 }
	RJMP _0x2080007
;
;//Check password cu dung hay sai
;char Check_OldPassword(void)
; 0000 015D {
; 0000 015E     char compare_buffer[16]={'\0'};
; 0000 015F     char i=0;
; 0000 0160     char *start;
; 0000 0161     char old_pw_stt[16]={'\0'};
; 0000 0162     char Val_Return=0;
; 0000 0163 
; 0000 0164     sprintf(compare_buffer,"@%sOLPW_",Device_ID);
;	compare_buffer -> Y+20
;	i -> R17
;	*start -> R18,R19
;	old_pw_stt -> Y+4
;	Val_Return -> R16
; 0000 0165     if(Read_Buffer()==1)
; 0000 0166     {
; 0000 0167         if(strncmp(Data_Buffer,compare_buffer,9)==0)
; 0000 0168         {
; 0000 0169             start=strchr(Data_Buffer,'_');
; 0000 016A             i=0;
; 0000 016B             start++;
; 0000 016C             while((*start!='#') && (i<16))
; 0000 016D             {
; 0000 016E                 old_pw_stt[i++]=*start;
; 0000 016F                 start++;
; 0000 0170             }
; 0000 0171             if(strcmp(old_pw_stt,"TRUE")==0)
; 0000 0172                 Val_Return = 1;
; 0000 0173             else
; 0000 0174                 Val_Return = 0;
; 0000 0175         }
; 0000 0176         Clear_Data_Buffer();
; 0000 0177     }
; 0000 0178     return Val_Return;
; 0000 0179 }

	.DSEG
_0x34:
	.BYTE 0x5
;//Hien thi thong tin len LCD
;char Display_Info(void)
; 0000 017C {

	.CSEG
_Display_Info:
; 0000 017D     char line1_buffer[16]={'\0'};
; 0000 017E     char line2_buffer[16]={'\0'};
; 0000 017F     char compare_buffer[16]={'\0'};
; 0000 0180     char compare_password[16]={'\0'};
; 0000 0181     char *start_line1,*start_line2;
; 0000 0182     char i=0;
; 0000 0183     char Val_Return=0;
; 0000 0184     char *start;
; 0000 0185     char old_pw_stt[16]={'\0'};
; 0000 0186 
; 0000 0187     sprintf(compare_buffer,"@%sDP",Device_ID);
	SBIW R28,63
	SBIW R28,19
	LDI  R24,82
	RCALL SUBOPT_0xE
	LDI  R30,LOW(_0x36*2)
	LDI  R31,HIGH(_0x36*2)
	RCALL __INITLOCB
	RCALL __SAVELOCR6
;	line1_buffer -> Y+72
;	line2_buffer -> Y+56
;	compare_buffer -> Y+40
;	compare_password -> Y+24
;	*start_line1 -> R16,R17
;	*start_line2 -> R18,R19
;	i -> R21
;	Val_Return -> R20
;	*start -> Y+22
;	old_pw_stt -> Y+6
	LDI  R21,0
	LDI  R20,0
	MOVW R30,R28
	ADIW R30,40
	RCALL SUBOPT_0x3
	__POINTW1FN _0x0,35
	RCALL SUBOPT_0xF
; 0000 0188     sprintf(compare_password,"@%sOLPW_",Device_ID);
	MOVW R30,R28
	ADIW R30,24
	RCALL SUBOPT_0x3
	__POINTW1FN _0x0,21
	RCALL SUBOPT_0xF
; 0000 0189 
; 0000 018A     if(Read_Buffer()==1)
	RCALL _Read_Buffer
	CPI  R30,LOW(0x1)
	BREQ PC+2
	RJMP _0x37
; 0000 018B     {
; 0000 018C         if(strncmp(Data_Buffer,compare_buffer,6)==0)
	RCALL SUBOPT_0x10
	MOVW R30,R28
	ADIW R30,42
	RCALL SUBOPT_0x3
	LDI  R30,LOW(6)
	ST   -Y,R30
	RCALL _strncmp
	CPI  R30,0
	BREQ PC+2
	RJMP _0x38
; 0000 018D         {
; 0000 018E             start_line1=strchr(Data_Buffer,'_');
	RCALL SUBOPT_0x10
	LDI  R30,LOW(95)
	ST   -Y,R30
	RCALL _strchr
	MOVW R16,R30
; 0000 018F             start_line2=strchr(Data_Buffer,'&');
	RCALL SUBOPT_0x10
	LDI  R30,LOW(38)
	ST   -Y,R30
	RCALL _strchr
	MOVW R18,R30
; 0000 0190             i=0;
	LDI  R21,LOW(0)
; 0000 0191             start_line1++;
	__ADDWRN 16,17,1
; 0000 0192             while((*start_line1!='&') && (i<16))
_0x39:
	MOVW R26,R16
	LD   R26,X
	CPI  R26,LOW(0x26)
	BREQ _0x3C
	CPI  R21,16
	BRLO _0x3D
_0x3C:
	RJMP _0x3B
_0x3D:
; 0000 0193             {
; 0000 0194                 line1_buffer[i++]=*start_line1;
	RCALL SUBOPT_0x11
	MOVW R26,R28
	SUBI R26,LOW(-(72))
	SBCI R27,HIGH(-(72))
	RCALL SUBOPT_0x12
	MOVW R26,R16
	RCALL SUBOPT_0x13
; 0000 0195                 start_line1++;
	__ADDWRN 16,17,1
; 0000 0196             }
	RJMP _0x39
_0x3B:
; 0000 0197             i=0;
	LDI  R21,LOW(0)
; 0000 0198             start_line2++;
	__ADDWRN 18,19,1
; 0000 0199             while((*start_line2!='#') && (i<16))
_0x3E:
	MOVW R26,R18
	LD   R26,X
	CPI  R26,LOW(0x23)
	BREQ _0x41
	CPI  R21,16
	BRLO _0x42
_0x41:
	RJMP _0x40
_0x42:
; 0000 019A             {
; 0000 019B                 line2_buffer[i++]=*start_line2;
	RCALL SUBOPT_0x11
	MOVW R26,R28
	ADIW R26,56
	RCALL SUBOPT_0x12
	MOVW R26,R18
	RCALL SUBOPT_0x13
; 0000 019C                 start_line2++;
	__ADDWRN 18,19,1
; 0000 019D             }
	RJMP _0x3E
_0x40:
; 0000 019E             lcd_clear();
	RCALL _lcd_clear
; 0000 019F             lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0000 01A0             lcd_puts(line1_buffer);
	MOVW R30,R28
	SUBI R30,LOW(-(72))
	SBCI R31,HIGH(-(72))
	RCALL SUBOPT_0x14
; 0000 01A1             lcd_gotoxy(0,1);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x15
; 0000 01A2             lcd_puts(line2_buffer);
	MOVW R30,R28
	ADIW R30,56
	RCALL SUBOPT_0x14
; 0000 01A3             step_setting=2;
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x16
; 0000 01A4             httt_stt=1;
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x7
; 0000 01A5         } else
	RJMP _0x43
_0x38:
; 0000 01A6         if(strncmp(Data_Buffer,compare_password,9)==0)
	RCALL SUBOPT_0x10
	MOVW R30,R28
	ADIW R30,26
	RCALL SUBOPT_0x3
	LDI  R30,LOW(9)
	ST   -Y,R30
	RCALL _strncmp
	CPI  R30,0
	BRNE _0x44
; 0000 01A7         {
; 0000 01A8             start=strchr(Data_Buffer,'_');
	RCALL SUBOPT_0x10
	LDI  R30,LOW(95)
	ST   -Y,R30
	RCALL _strchr
	STD  Y+22,R30
	STD  Y+22+1,R31
; 0000 01A9             i=0;
	LDI  R21,LOW(0)
; 0000 01AA             start++;
	RCALL SUBOPT_0x17
; 0000 01AB             while((*start!='#') && (i<16))
_0x45:
	LDD  R26,Y+22
	LDD  R27,Y+22+1
	LD   R26,X
	CPI  R26,LOW(0x23)
	BREQ _0x48
	CPI  R21,16
	BRLO _0x49
_0x48:
	RJMP _0x47
_0x49:
; 0000 01AC             {
; 0000 01AD                 old_pw_stt[i++]=*start;
	RCALL SUBOPT_0x11
	MOVW R26,R28
	ADIW R26,6
	RCALL SUBOPT_0x12
	LDD  R26,Y+22
	LDD  R27,Y+22+1
	RCALL SUBOPT_0x13
; 0000 01AE                 start++;
	RCALL SUBOPT_0x17
; 0000 01AF             }
	RJMP _0x45
_0x47:
; 0000 01B0             if(strcmp(old_pw_stt,"TRUE")==0)
	MOVW R30,R28
	ADIW R30,6
	RCALL SUBOPT_0x3
	__POINTW1MN _0x4B,0
	RCALL SUBOPT_0x3
	RCALL _strcmp
	CPI  R30,0
	BRNE _0x4A
; 0000 01B1                 Val_Return = 1;
	LDI  R20,LOW(1)
; 0000 01B2             else
	RJMP _0x4C
_0x4A:
; 0000 01B3                 Val_Return = 0;
	LDI  R20,LOW(0)
; 0000 01B4         }
_0x4C:
; 0000 01B5         Clear_Data_Buffer();
_0x44:
_0x43:
	RCALL _Clear_Data_Buffer
; 0000 01B6     }
; 0000 01B7     return Val_Return;
_0x37:
	MOV  R30,R20
	RCALL __LOADLOCR6
	ADIW R28,63
	ADIW R28,25
	RET
; 0000 01B8 }

	.DSEG
_0x4B:
	.BYTE 0x5
;
;//Xoa bo dem password
;void Clear_PassBuffer(void)
; 0000 01BC {

	.CSEG
_Clear_PassBuffer:
; 0000 01BD     PASS_Buffer[0]='\0';
	LDI  R30,LOW(0)
	STS  _PASS_Buffer,R30
; 0000 01BE     index_password = 0;
	CLR  R12
; 0000 01BF }
	RET
;
;
;
;
;// Timer1 overflow interrupt service routine
;interrupt [TIM1_OVF] void timer1_ovf_isr(void)
; 0000 01C6 {
_timer1_ovf_isr:
	ST   -Y,R30
	IN   R30,SREG
	ST   -Y,R30
; 0000 01C7 // Reinitialize Timer1 value
; 0000 01C8 TCNT1H=0x63C0 >> 8;
	RCALL SUBOPT_0x1
; 0000 01C9 TCNT1L=0x63C0 & 0xff;
; 0000 01CA // Place your code here
; 0000 01CB     timer_count++;
	INC  R13
; 0000 01CC     if(timer_count>6)
	LDI  R30,LOW(6)
	CP   R30,R13
	BRSH _0x4D
; 0000 01CD     {
; 0000 01CE         LED_LCD = 0;
	CBI  0x12,3
; 0000 01CF         timer_count=0;
	CLR  R13
; 0000 01D0         sys_mode=DEFAULT;
	CLR  R10
; 0000 01D1         set_ok=0;
	LDI  R30,LOW(0)
	STS  _set_ok,R30
; 0000 01D2         pass_stt=0;
	RCALL SUBOPT_0x8
; 0000 01D3     }
; 0000 01D4 }
_0x4D:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R30,Y+
	RETI
;
;
;
;
;void RS485_Puts(char *s)
; 0000 01DA {
_RS485_Puts:
; 0000 01DB     RS485_TX = 1;
;	*s -> Y+0
	SBI  0x12,2
; 0000 01DC     puts(s);
	LD   R30,Y
	LDD  R31,Y+1
	RCALL SUBOPT_0x3
	RCALL _puts
; 0000 01DD     delay_ms(100);
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	RCALL SUBOPT_0xC
; 0000 01DE     RS485_TX = 0;
	CBI  0x12,2
; 0000 01DF }
	RJMP _0x2080004
;#include "key4x4.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;static uint8_t  KEY_3x3[4][4]={'1','2','3','A',
;                               '4','5','6','B',
;                               '7','8','9','C',
;                               '*','0','#','D'
;                               };

	.DSEG
;
; /*******************************************************************************
;Noi Dung    :   Kiem tra co nut duoc an hay khong..
;Tham Bien   :   Khong.
;Tra Ve      :   1:   Neu co nut duoc an.
;                0:   Neu khong co nut duoc an.
;********************************************************************************/
;uint8_t  KEY4X4_IsPush(void)
; 0001 000F {

	.CSEG
_KEY4X4_IsPush:
; 0001 0010    if((KEY4X4_ROW1==0)|(KEY4X4_ROW2==0)|(KEY4X4_ROW3==0) |(KEY4X4_ROW4==0))
	LDI  R26,0
	SBIC 0x13,0
	LDI  R26,1
	RCALL SUBOPT_0x18
	MOV  R0,R30
	LDI  R26,0
	SBIC 0x13,1
	LDI  R26,1
	RCALL SUBOPT_0x18
	OR   R0,R30
	LDI  R26,0
	SBIC 0x13,2
	LDI  R26,1
	RCALL SUBOPT_0x18
	OR   R0,R30
	LDI  R26,0
	SBIC 0x13,3
	LDI  R26,1
	RCALL SUBOPT_0x18
	OR   R30,R0
	BREQ _0x20004
; 0001 0011    {
; 0001 0012        return 1;
	LDI  R30,LOW(1)
	RET
; 0001 0013    }
; 0001 0014    else
_0x20004:
; 0001 0015    {
; 0001 0016        return 0;
	LDI  R30,LOW(0)
	RET
; 0001 0017    }
; 0001 0018 }
	RET
; /*******************************************************************************
;Noi Dung    :   Keo hang thu i xuong muc logic 0, de kiem tra co nut duoc an tai
;                hang thu i hay khong.
;Tham Bien   :   i: vi tri hang can kiem tra.
;Tra Ve      :   Khong.
;********************************************************************************/
;void KEY4X4_CheckRow(uint8_t  i)
; 0001 0020 {
_KEY4X4_CheckRow:
; 0001 0021    KEY4X4_COL1=KEY4X4_COL2=KEY4X4_COL3=KEY4X4_COL4=1;
;	i -> Y+0
	SBI  0x12,7
	SBI  0x12,6
	SBI  0x12,5
	SBI  0x12,4
; 0001 0022    if(i==0)
	LD   R30,Y
	CPI  R30,0
	BRNE _0x2000E
; 0001 0023    {
; 0001 0024       KEY4X4_COL1=0;
	CBI  0x12,4
; 0001 0025    }
; 0001 0026    else if(i==1)
	RJMP _0x20011
_0x2000E:
	LD   R26,Y
	CPI  R26,LOW(0x1)
	BRNE _0x20012
; 0001 0027    {
; 0001 0028       KEY4X4_COL2=0;
	CBI  0x12,5
; 0001 0029    }
; 0001 002A    else if(i==2)
	RJMP _0x20015
_0x20012:
	LD   R26,Y
	CPI  R26,LOW(0x2)
	BRNE _0x20016
; 0001 002B    {
; 0001 002C       KEY4X4_COL3=0;
	CBI  0x12,6
; 0001 002D    }
; 0001 002E    else if(i==3)
	RJMP _0x20019
_0x20016:
	LD   R26,Y
	CPI  R26,LOW(0x3)
	BRNE _0x2001A
; 0001 002F    {
; 0001 0030       KEY4X4_COL4=0;
	CBI  0x12,7
; 0001 0031    }
; 0001 0032 }
_0x2001A:
_0x20019:
_0x20015:
_0x20011:
	RJMP _0x2080003
; /*******************************************************************************
;Noi Dung    :   Lay gia tri nut nhan duoc an.
;Tham Bien   :   Khong.
;Tra Ve      :   0:     Neu khong co nut duoc an.
;            khac 0: Gia tri cua nut an.
;********************************************************************************/
;uint8_t  KEY4X4_GetKey(void)
; 0001 003A {
_KEY4X4_GetKey:
; 0001 003B    uint8_t  i;
; 0001 003C    KEY4X4_COL1=KEY4X4_COL2=KEY4X4_COL3=KEY4X4_COL4=0;
	ST   -Y,R17
;	i -> R17
	CBI  0x12,7
	CBI  0x12,6
	CBI  0x12,5
	CBI  0x12,4
; 0001 003D    if(KEY4X4_IsPush())
	RCALL _KEY4X4_IsPush
	CPI  R30,0
	BREQ _0x20025
; 0001 003E    {
; 0001 003F       delay_ms(5);
	LDI  R30,LOW(5)
	LDI  R31,HIGH(5)
	RCALL SUBOPT_0xC
; 0001 0040       if(KEY4X4_IsPush())
	RCALL _KEY4X4_IsPush
	CPI  R30,0
	BREQ _0x20026
; 0001 0041       {
; 0001 0042          for(i=0;i<4;i++)
	LDI  R17,LOW(0)
_0x20028:
	CPI  R17,4
	BRSH _0x20029
; 0001 0043          {
; 0001 0044             KEY4X4_CheckRow(i);
	ST   -Y,R17
	RCALL _KEY4X4_CheckRow
; 0001 0045             if(!KEY4X4_ROW1) return KEY_3x3[0][i];
	SBIC 0x13,0
	RJMP _0x2002A
	MOV  R30,R17
	RCALL SUBOPT_0x19
	SUBI R30,LOW(-_KEY_3x3_G001)
	SBCI R31,HIGH(-_KEY_3x3_G001)
	LD   R30,Z
	RJMP _0x2080007
; 0001 0046             if(!KEY4X4_ROW2) return KEY_3x3[1][i];
_0x2002A:
	SBIC 0x13,1
	RJMP _0x2002B
	__POINTW2MN _KEY_3x3_G001,4
	RCALL SUBOPT_0x1A
	RJMP _0x2080007
; 0001 0047             if(!KEY4X4_ROW3) return KEY_3x3[2][i];
_0x2002B:
	SBIC 0x13,2
	RJMP _0x2002C
	__POINTW2MN _KEY_3x3_G001,8
	RCALL SUBOPT_0x1A
	RJMP _0x2080007
; 0001 0048             if(!KEY4X4_ROW4) return KEY_3x3[3][i];
_0x2002C:
	SBIC 0x13,3
	RJMP _0x2002D
	__POINTW2MN _KEY_3x3_G001,12
	RCALL SUBOPT_0x1A
	RJMP _0x2080007
; 0001 0049          }
_0x2002D:
	SUBI R17,-1
	RJMP _0x20028
_0x20029:
; 0001 004A       }
; 0001 004B    }
_0x20026:
; 0001 004C    return 0;
_0x20025:
	LDI  R30,LOW(0)
	RJMP _0x2080007
; 0001 004D }
;
;
;
;#include "Mode_Manage.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;extern char Data_Buffer[RX_BUFFER_SIZE];
;
;extern Mode_TypeDef sys_mode;
;extern char timer_count;
;extern char PASS_Buffer[16];
;extern char index_password;
;extern char pass_stt;
;extern char set_ok;
;extern char enter_en;
;extern char *Device_ID;
;
;extern char step_setting;
;extern char httt_stt;
;
;//Chuong trinh default mode
;/*************************************************************************
; ******************************Begin Default Mode*************************
;**************************************************************************/
;void Default_Mode(void)
; 0002 0016 {

	.CSEG
_Default_Mode:
; 0002 0017    uint8_t key=0;
; 0002 0018 
; 0002 0019    if(httt_stt==0)
	RCALL SUBOPT_0xA
;	key -> R17
	LDS  R30,_httt_stt
	CPI  R30,0
	BRNE _0x40003
; 0002 001A    {
; 0002 001B         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 001C         lcd_puts("Welcome...      ");
	__POINTW1MN _0x40004,0
	RCALL SUBOPT_0x14
; 0002 001D    }
; 0002 001E 
; 0002 001F    key=KEY4X4_GetKey();
_0x40003:
	RCALL SUBOPT_0x1B
; 0002 0020    if(key)
	CPI  R17,0
	BREQ _0x40005
; 0002 0021    {
; 0002 0022         LED_LCD=1;
	RCALL SUBOPT_0x1C
; 0002 0023         timer_count=0;
; 0002 0024         TCNT1H=0x63;
; 0002 0025         TCNT1L=0xC0;
; 0002 0026         if(key=='*')
	CPI  R17,42
	BRNE _0x40008
; 0002 0027         {
; 0002 0028             sys_mode++;
	INC  R10
; 0002 0029         }else
	RJMP _0x40009
_0x40008:
; 0002 002A         if(key=='C')
	CPI  R17,67
	BRNE _0x4000A
; 0002 002B         {
; 0002 002C             Clear_PassBuffer();
	RCALL SUBOPT_0x1D
; 0002 002D             lcd_gotoxy(0,1);
	RCALL SUBOPT_0x15
; 0002 002E             lcd_puts("                ");
	__POINTW1MN _0x40004,17
	RCALL SUBOPT_0x14
; 0002 002F             if(httt_stt==1)
	RCALL SUBOPT_0x1E
	BRNE _0x4000B
; 0002 0030             {
; 0002 0031                 lcd_clear();
	RCALL _lcd_clear
; 0002 0032                 httt_stt=0;
	RCALL SUBOPT_0x9
; 0002 0033             }
; 0002 0034         }
_0x4000B:
; 0002 0035         while(key)
_0x4000A:
_0x40009:
_0x4000C:
	CPI  R17,0
	BREQ _0x4000E
; 0002 0036         {
; 0002 0037             key=KEY4X4_GetKey();
	RCALL SUBOPT_0x1B
; 0002 0038         }
	RJMP _0x4000C
_0x4000E:
; 0002 0039    }
; 0002 003A    Display_Info();
_0x40005:
	RCALL _Display_Info
; 0002 003B }
_0x2080007:
	LD   R17,Y+
	RET

	.DSEG
_0x40004:
	.BYTE 0x22
;/*************************************************************************
; ********************************End Default Mode*************************
;**************************************************************************/
;
;//Chuong trinh password off
;/*************************************************************************
; *************************Begin PassWord Off Mode*************************
;**************************************************************************/
;void Pass_Off_Mode(void)
; 0002 0045 {

	.CSEG
_Pass_Off_Mode:
; 0002 0046     uint8_t key=0;
; 0002 0047     char RS485_TX_Buffer[30]={'\0'};
; 0002 0048 
; 0002 0049     if(step_setting==0)
	RCALL SUBOPT_0x1F
	LDI  R30,LOW(_0x4000F*2)
	LDI  R31,HIGH(_0x4000F*2)
	RCALL __INITLOCB
	RCALL SUBOPT_0xA
;	key -> R17
;	RS485_TX_Buffer -> Y+1
	RCALL SUBOPT_0x20
	BRNE _0x40010
; 0002 004A     {
; 0002 004B         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 004C         lcd_puts("Tat He Thong    ");
	__POINTW1MN _0x40011,0
	RCALL SUBOPT_0x14
; 0002 004D     }
; 0002 004E     if(step_setting==1)
_0x40010:
	RCALL SUBOPT_0x21
	BRNE _0x40012
; 0002 004F     {
; 0002 0050         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 0051         lcd_puts("PassWord OFF    ");
	__POINTW1MN _0x40011,17
	RCALL SUBOPT_0x14
; 0002 0052     }
; 0002 0053     key=KEY4X4_GetKey();
_0x40012:
	RCALL SUBOPT_0x1B
; 0002 0054     if(key)
	CPI  R17,0
	BREQ _0x40013
; 0002 0055     {
; 0002 0056         LED_LCD=1;
	RCALL SUBOPT_0x1C
; 0002 0057         timer_count=0;
; 0002 0058         TCNT1H=0x63;
; 0002 0059         TCNT1L=0xC0;
; 0002 005A         //switch mode
; 0002 005B         if(key=='*')
	CPI  R17,42
	BRNE _0x40016
; 0002 005C         {
; 0002 005D             sys_mode++;
	INC  R10
; 0002 005E         } else
	RJMP _0x40017
_0x40016:
; 0002 005F         if(key=='#')
	CPI  R17,35
	BRNE _0x40018
; 0002 0060         {
; 0002 0061             if(step_setting==0)
	RCALL SUBOPT_0x20
	BRNE _0x40019
; 0002 0062             {
; 0002 0063                 step_setting=1;
	RCALL SUBOPT_0x22
; 0002 0064             } else
	RJMP _0x4001A
_0x40019:
; 0002 0065             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x4001B
; 0002 0066             {
; 0002 0067                 sprintf(RS485_TX_Buffer,"@%sPWOF%s#%c",Device_ID,PASS_Buffer,'\0');
	RCALL SUBOPT_0x23
	__POINTW1FN _0x40000,68
	RCALL SUBOPT_0x24
; 0002 0068                 RS485_Puts(RS485_TX_Buffer);
	RCALL SUBOPT_0x25
; 0002 0069                 Clear_PassBuffer();
; 0002 006A             }
; 0002 006B         } else
_0x4001B:
_0x4001A:
	RJMP _0x4001C
_0x40018:
; 0002 006C         if(key=='C')
	CPI  R17,67
	BRNE _0x4001D
; 0002 006D         {
; 0002 006E             Clear_PassBuffer();
	RCALL SUBOPT_0x1D
; 0002 006F             lcd_gotoxy(0,1);
	RCALL SUBOPT_0x15
; 0002 0070             lcd_puts("                ");
	__POINTW1MN _0x40011,34
	RCALL SUBOPT_0x14
; 0002 0071             if(httt_stt==1)
	RCALL SUBOPT_0x1E
	BRNE _0x4001E
; 0002 0072             {
; 0002 0073                 //lcd_clear();
; 0002 0074                 httt_stt=0;
	RCALL SUBOPT_0x9
; 0002 0075             }
; 0002 0076         }
_0x4001E:
; 0002 0077         else
	RJMP _0x4001F
_0x4001D:
; 0002 0078         {
; 0002 0079             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x40020
; 0002 007A             {
; 0002 007B                 lcd_gotoxy(index_password,1);
	ST   -Y,R12
	RCALL SUBOPT_0x15
; 0002 007C                 PASS_Buffer[index_password++]=key;
	RCALL SUBOPT_0x26
; 0002 007D                 PASS_Buffer[index_password]=0;
; 0002 007E                 lcd_putchar('*');
	RCALL SUBOPT_0x27
; 0002 007F             }
; 0002 0080         }
_0x40020:
_0x4001F:
_0x4001C:
_0x40017:
; 0002 0081 
; 0002 0082         while(key)
_0x40021:
	CPI  R17,0
	BREQ _0x40023
; 0002 0083         {
; 0002 0084             key=KEY4X4_GetKey();
	RCALL SUBOPT_0x1B
; 0002 0085         }
	RJMP _0x40021
_0x40023:
; 0002 0086     }
; 0002 0087     Display_Info();
_0x40013:
	RJMP _0x2080006
; 0002 0088 }

	.DSEG
_0x40011:
	.BYTE 0x33
;/*************************************************************************
; ***************************End PassWord Off Mode*************************
;**************************************************************************/
;
;
;/*************************************************************************
; **************************Begin PassWord On Mode*************************
;**************************************************************************/
;void Pass_On_Mode(void)
; 0002 0092 {

	.CSEG
_Pass_On_Mode:
; 0002 0093     uint8_t key=0;
; 0002 0094     char RS485_TX_Buffer[30]={'\0'};
; 0002 0095 
; 0002 0096     if(step_setting==0)
	RCALL SUBOPT_0x1F
	LDI  R30,LOW(_0x40024*2)
	LDI  R31,HIGH(_0x40024*2)
	RCALL __INITLOCB
	RCALL SUBOPT_0xA
;	key -> R17
;	RS485_TX_Buffer -> Y+1
	RCALL SUBOPT_0x20
	BRNE _0x40025
; 0002 0097     {
; 0002 0098         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 0099         lcd_puts("Bat He Thong    ");
	__POINTW1MN _0x40026,0
	RCALL SUBOPT_0x14
; 0002 009A     }
; 0002 009B     if(step_setting==1)
_0x40025:
	RCALL SUBOPT_0x21
	BRNE _0x40027
; 0002 009C     {
; 0002 009D         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 009E         lcd_puts("PassWord ON     ");
	__POINTW1MN _0x40026,17
	RCALL SUBOPT_0x14
; 0002 009F     }
; 0002 00A0     key=KEY4X4_GetKey();
_0x40027:
	RCALL SUBOPT_0x1B
; 0002 00A1     if(key)
	CPI  R17,0
	BREQ _0x40028
; 0002 00A2     {
; 0002 00A3         LED_LCD=1;
	RCALL SUBOPT_0x1C
; 0002 00A4         timer_count=0;
; 0002 00A5         TCNT1H=0x63;
; 0002 00A6         TCNT1L=0xC0;
; 0002 00A7 
; 0002 00A8         if(key=='*')
	CPI  R17,42
	BRNE _0x4002B
; 0002 00A9         {
; 0002 00AA             sys_mode++;
	INC  R10
; 0002 00AB         } else
	RJMP _0x4002C
_0x4002B:
; 0002 00AC         if(key=='#')
	CPI  R17,35
	BRNE _0x4002D
; 0002 00AD         {
; 0002 00AE             if(step_setting==0)
	RCALL SUBOPT_0x20
	BRNE _0x4002E
; 0002 00AF             {
; 0002 00B0                 step_setting=1;
	RCALL SUBOPT_0x22
; 0002 00B1             } else
	RJMP _0x4002F
_0x4002E:
; 0002 00B2             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x40030
; 0002 00B3             {
; 0002 00B4                 sprintf(RS485_TX_Buffer,"@%sPWON%s#%c",Device_ID,PASS_Buffer,'\0');
	RCALL SUBOPT_0x23
	__POINTW1FN _0x40000,115
	RCALL SUBOPT_0x24
; 0002 00B5                 RS485_Puts(RS485_TX_Buffer);
	RCALL SUBOPT_0x25
; 0002 00B6                 Clear_PassBuffer();
; 0002 00B7             }
; 0002 00B8         } else
_0x40030:
_0x4002F:
	RJMP _0x40031
_0x4002D:
; 0002 00B9         if(key=='C')
	CPI  R17,67
	BRNE _0x40032
; 0002 00BA         {
; 0002 00BB             Clear_PassBuffer();
	RCALL SUBOPT_0x1D
; 0002 00BC             lcd_gotoxy(0,1);
	RCALL SUBOPT_0x15
; 0002 00BD             lcd_puts("                ");
	__POINTW1MN _0x40026,34
	RCALL SUBOPT_0x14
; 0002 00BE             if(httt_stt==1)
	RCALL SUBOPT_0x1E
	BRNE _0x40033
; 0002 00BF             {
; 0002 00C0                 //lcd_clear();
; 0002 00C1                 httt_stt=0;
	RCALL SUBOPT_0x9
; 0002 00C2             }
; 0002 00C3         }
_0x40033:
; 0002 00C4         else
	RJMP _0x40034
_0x40032:
; 0002 00C5         {
; 0002 00C6             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x40035
; 0002 00C7             {
; 0002 00C8                 lcd_gotoxy(index_password,1);
	ST   -Y,R12
	RCALL SUBOPT_0x15
; 0002 00C9                 PASS_Buffer[index_password++]=key;
	RCALL SUBOPT_0x26
; 0002 00CA                 PASS_Buffer[index_password]=0;
; 0002 00CB                 lcd_putchar('*');
	RCALL SUBOPT_0x27
; 0002 00CC             }
; 0002 00CD         }
_0x40035:
_0x40034:
_0x40031:
_0x4002C:
; 0002 00CE 
; 0002 00CF         while(key)
_0x40036:
	CPI  R17,0
	BREQ _0x40038
; 0002 00D0         {
; 0002 00D1             key=KEY4X4_GetKey();
	RCALL SUBOPT_0x1B
; 0002 00D2         }
	RJMP _0x40036
_0x40038:
; 0002 00D3     }
; 0002 00D4     Display_Info();
_0x40028:
	RJMP _0x2080006
; 0002 00D5 }

	.DSEG
_0x40026:
	.BYTE 0x33
;
;/*************************************************************************
; ****************************End PassWord On Mode*************************
;**************************************************************************/
;
;/*************************************************************************
; **************************Begin Change PassWord Mode*********************
;**************************************************************************/
;void Change_Pass_Mode(void)
; 0002 00DF {

	.CSEG
_Change_Pass_Mode:
; 0002 00E0     uint8_t key=0;
; 0002 00E1     char RS485_TX_Buffer[30]={'\0'};
; 0002 00E2 
; 0002 00E3 
; 0002 00E4     if(step_setting==0)
	RCALL SUBOPT_0x1F
	LDI  R30,LOW(_0x40039*2)
	LDI  R31,HIGH(_0x40039*2)
	RCALL __INITLOCB
	RCALL SUBOPT_0xA
;	key -> R17
;	RS485_TX_Buffer -> Y+1
	RCALL SUBOPT_0x20
	BRNE _0x4003A
; 0002 00E5     {
; 0002 00E6         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 00E7         lcd_puts("Doi Password    ");
	__POINTW1MN _0x4003B,0
	RJMP _0x40067
; 0002 00E8 
; 0002 00E9     } else
_0x4003A:
; 0002 00EA     if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x4003D
; 0002 00EB     {
; 0002 00EC         if(pass_stt==0)
	LDS  R30,_pass_stt
	CPI  R30,0
	BRNE _0x4003E
; 0002 00ED         {
; 0002 00EE             lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 00EF             lcd_puts("Old Password    ");
	__POINTW1MN _0x4003B,17
	RCALL SUBOPT_0x14
; 0002 00F0         }
; 0002 00F1 
; 0002 00F2         if(pass_stt==1)
_0x4003E:
	RCALL SUBOPT_0x28
	BRNE _0x4003F
; 0002 00F3         {
; 0002 00F4             lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 00F5             lcd_puts("New Password    ");
	__POINTW1MN _0x4003B,34
_0x40067:
	ST   -Y,R31
	ST   -Y,R30
	RCALL _lcd_puts
; 0002 00F6         }
; 0002 00F7     }
_0x4003F:
; 0002 00F8     key=KEY4X4_GetKey();
_0x4003D:
	RCALL SUBOPT_0x1B
; 0002 00F9     if(key)
	CPI  R17,0
	BREQ _0x40040
; 0002 00FA     {
; 0002 00FB         LED_LCD=1;
	RCALL SUBOPT_0x1C
; 0002 00FC         timer_count=0;
; 0002 00FD         TCNT1H=0x63;
; 0002 00FE         TCNT1L=0xC0;
; 0002 00FF 
; 0002 0100         if(key=='*')
	CPI  R17,42
	BRNE _0x40043
; 0002 0101         {
; 0002 0102             sys_mode++;
	INC  R10
; 0002 0103         } else
	RJMP _0x40044
_0x40043:
; 0002 0104         if(key=='#')
	CPI  R17,35
	BRNE _0x40045
; 0002 0105         {
; 0002 0106             if(step_setting==0)
	RCALL SUBOPT_0x20
	BRNE _0x40046
; 0002 0107             {
; 0002 0108                 step_setting=1;
	RCALL SUBOPT_0x22
; 0002 0109             } else
	RJMP _0x40047
_0x40046:
; 0002 010A             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x40048
; 0002 010B             {
; 0002 010C                 if(pass_stt==0)
	LDS  R30,_pass_stt
	CPI  R30,0
	BRNE _0x40049
; 0002 010D                 {
; 0002 010E                     sprintf(RS485_TX_Buffer,"@%sOLPW%s#%c",Device_ID,PASS_Buffer,'\0');
	RCALL SUBOPT_0x23
	__POINTW1FN _0x40000,179
	RCALL SUBOPT_0x24
; 0002 010F                     RS485_Puts(RS485_TX_Buffer);
	RCALL SUBOPT_0x25
; 0002 0110                     Clear_PassBuffer();
; 0002 0111                 } else
	RJMP _0x4004A
_0x40049:
; 0002 0112                 if(pass_stt==1)
	RCALL SUBOPT_0x28
	BRNE _0x4004B
; 0002 0113                 {
; 0002 0114                     sprintf(RS485_TX_Buffer,"@%sNEPW%s#%c",Device_ID,PASS_Buffer,'\0');
	RCALL SUBOPT_0x23
	__POINTW1FN _0x40000,192
	RCALL SUBOPT_0x24
; 0002 0115                     RS485_Puts(RS485_TX_Buffer);
	RCALL SUBOPT_0x25
; 0002 0116                     Clear_PassBuffer();
; 0002 0117                     pass_stt=2;
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x8
; 0002 0118                 }
; 0002 0119 
; 0002 011A             }
_0x4004B:
_0x4004A:
; 0002 011B         } else
_0x40048:
_0x40047:
	RJMP _0x4004C
_0x40045:
; 0002 011C         if(key=='C')
	CPI  R17,67
	BRNE _0x4004D
; 0002 011D         {
; 0002 011E             Clear_PassBuffer();
	RCALL SUBOPT_0x1D
; 0002 011F             lcd_gotoxy(0,1);
	RCALL SUBOPT_0x15
; 0002 0120             lcd_puts("                ");
	__POINTW1MN _0x4003B,51
	RCALL SUBOPT_0x14
; 0002 0121             if(httt_stt==1)
	RCALL SUBOPT_0x1E
	BRNE _0x4004E
; 0002 0122             {
; 0002 0123                 //lcd_clear();
; 0002 0124                 httt_stt=0;
	RCALL SUBOPT_0x9
; 0002 0125             }
; 0002 0126         }
_0x4004E:
; 0002 0127         else
	RJMP _0x4004F
_0x4004D:
; 0002 0128         {
; 0002 0129             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x40050
; 0002 012A             {
; 0002 012B                  if(pass_stt== 0 || pass_stt==1)
	LDS  R26,_pass_stt
	CPI  R26,LOW(0x0)
	BREQ _0x40052
	RCALL SUBOPT_0x28
	BRNE _0x40051
_0x40052:
; 0002 012C                 {
; 0002 012D                     lcd_gotoxy(index_password,1);
	ST   -Y,R12
	RCALL SUBOPT_0x15
; 0002 012E                     PASS_Buffer[index_password++]=key;
	RCALL SUBOPT_0x26
; 0002 012F                     PASS_Buffer[index_password]=0;
; 0002 0130                     lcd_putchar('*');
	RCALL SUBOPT_0x27
; 0002 0131                 }
; 0002 0132             }
_0x40051:
; 0002 0133         }
_0x40050:
_0x4004F:
_0x4004C:
_0x40044:
; 0002 0134 
; 0002 0135         while(key)
_0x40054:
	CPI  R17,0
	BREQ _0x40056
; 0002 0136         {
; 0002 0137             key=KEY4X4_GetKey();
	RCALL SUBOPT_0x1B
; 0002 0138         }
	RJMP _0x40054
_0x40056:
; 0002 0139     }
; 0002 013A     //kiem tra password cu dung hay sai
; 0002 013B     if(Display_Info()==1)
_0x40040:
	RCALL _Display_Info
	CPI  R30,LOW(0x1)
	BRNE _0x40057
; 0002 013C     {
; 0002 013D         pass_stt=1;
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x8
; 0002 013E         lcd_clear();
	RCALL _lcd_clear
; 0002 013F     }
; 0002 0140 
; 0002 0141 }
_0x40057:
	RJMP _0x2080005

	.DSEG
_0x4003B:
	.BYTE 0x44
;/*************************************************************************
; **************************End Change PassWord Mode************************
;**************************************************************************/
;
;
;/*************************************************************************
; **************************Begin Display Info Mode*********************
;**************************************************************************/
;void Information_Mode(void)
; 0002 014B {

	.CSEG
_Information_Mode:
; 0002 014C     uint8_t key=0;
; 0002 014D     char RS485_TX_Buffer[30]={'\0'};
; 0002 014E 
; 0002 014F     if(step_setting==0)
	RCALL SUBOPT_0x1F
	LDI  R30,LOW(_0x40058*2)
	LDI  R31,HIGH(_0x40058*2)
	RCALL __INITLOCB
	RCALL SUBOPT_0xA
;	key -> R17
;	RS485_TX_Buffer -> Y+1
	RCALL SUBOPT_0x20
	BRNE _0x40059
; 0002 0150     {
; 0002 0151         lcd_gotoxy(0,0);
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x2
	RCALL _lcd_gotoxy
; 0002 0152         lcd_puts("Thong Tin HT    ");
	__POINTW1MN _0x4005A,0
	RCALL SUBOPT_0x14
; 0002 0153     }
; 0002 0154     key=KEY4X4_GetKey();
_0x40059:
	RCALL SUBOPT_0x1B
; 0002 0155     if(key)
	CPI  R17,0
	BREQ _0x4005B
; 0002 0156     {
; 0002 0157         LED_LCD=1;
	RCALL SUBOPT_0x1C
; 0002 0158         timer_count=0;
; 0002 0159         TCNT1H=0x63;
; 0002 015A         TCNT1L=0xC0;
; 0002 015B         if(key=='*')
	CPI  R17,42
	BRNE _0x4005E
; 0002 015C         {
; 0002 015D             sys_mode=DEFAULT;
	CLR  R10
; 0002 015E         } else
	RJMP _0x4005F
_0x4005E:
; 0002 015F         if(key=='#')
	CPI  R17,35
	BRNE _0x40060
; 0002 0160         {
; 0002 0161             if(step_setting==0)
	RCALL SUBOPT_0x20
	BRNE _0x40061
; 0002 0162             {
; 0002 0163                 step_setting=1;
	RCALL SUBOPT_0x22
; 0002 0164             } else
	RJMP _0x40062
_0x40061:
; 0002 0165             if(step_setting==1)
	RCALL SUBOPT_0x21
	BRNE _0x40063
; 0002 0166             {
; 0002 0167                 sprintf(RS485_TX_Buffer,"@%s001DP_INFSYS#%c",Device_ID,'\0');
	RCALL SUBOPT_0x23
	__POINTW1FN _0x40000,222
	RCALL SUBOPT_0x3
	MOVW R30,R8
	CLR  R22
	CLR  R23
	RCALL __PUTPARD1
	__GETD1N 0x0
	RCALL __PUTPARD1
	LDI  R24,8
	RCALL _sprintf
	ADIW R28,12
; 0002 0168                 RS485_Puts(RS485_TX_Buffer);
	RCALL SUBOPT_0x23
	RCALL _RS485_Puts
; 0002 0169             }
; 0002 016A         }
_0x40063:
_0x40062:
; 0002 016B         while(key)
_0x40060:
_0x4005F:
_0x40064:
	CPI  R17,0
	BREQ _0x40066
; 0002 016C         {
; 0002 016D             key=KEY4X4_GetKey();
	RCALL SUBOPT_0x1B
; 0002 016E         }
	RJMP _0x40064
_0x40066:
; 0002 016F     }
; 0002 0170     Display_Info();
_0x4005B:
_0x2080006:
	RCALL _Display_Info
; 0002 0171 }
_0x2080005:
	LDD  R17,Y+0
	ADIW R28,31
	RET

	.DSEG
_0x4005A:
	.BYTE 0x11
;
;
;
;/*************************************************************************
; **************************End Display Info Mode************************
;**************************************************************************/
;
;
;
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.DSEG

	.CSEG
__lcd_write_nibble_G100:
	LD   R30,Y
	ANDI R30,LOW(0x10)
	BREQ _0x2000004
	SBI  0x18,4
	RJMP _0x2000005
_0x2000004:
	CBI  0x18,4
_0x2000005:
	LD   R30,Y
	ANDI R30,LOW(0x20)
	BREQ _0x2000006
	SBI  0x18,5
	RJMP _0x2000007
_0x2000006:
	CBI  0x18,5
_0x2000007:
	LD   R30,Y
	ANDI R30,LOW(0x40)
	BREQ _0x2000008
	SBI  0x18,6
	RJMP _0x2000009
_0x2000008:
	CBI  0x18,6
_0x2000009:
	LD   R30,Y
	ANDI R30,LOW(0x80)
	BREQ _0x200000A
	SBI  0x18,7
	RJMP _0x200000B
_0x200000A:
	CBI  0x18,7
_0x200000B:
	__DELAY_USB 1
	SBI  0x18,2
	__DELAY_USB 3
	CBI  0x18,2
	__DELAY_USB 3
	RJMP _0x2080003
__lcd_write_data:
	LD   R30,Y
	RCALL SUBOPT_0x29
    ld    r30,y
    swap  r30
    st    y,r30
	LD   R30,Y
	RCALL SUBOPT_0x29
	__DELAY_USB 33
	RJMP _0x2080003
_lcd_gotoxy:
	LD   R30,Y
	RCALL SUBOPT_0x19
	SUBI R30,LOW(-__base_y_G100)
	SBCI R31,HIGH(-__base_y_G100)
	LD   R30,Z
	LDD  R26,Y+1
	ADD  R30,R26
	RCALL SUBOPT_0x2A
	LDD  R30,Y+1
	STS  __lcd_x,R30
	LD   R30,Y
	STS  __lcd_y,R30
_0x2080004:
	ADIW R28,2
	RET
_lcd_clear:
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x2A
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	RCALL SUBOPT_0xC
	LDI  R30,LOW(12)
	RCALL SUBOPT_0x2A
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x2A
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	RCALL SUBOPT_0xC
	LDI  R30,LOW(0)
	STS  __lcd_y,R30
	STS  __lcd_x,R30
	RET
_lcd_putchar:
	LD   R26,Y
	CPI  R26,LOW(0xA)
	BREQ _0x2000011
	LDS  R30,__lcd_maxx
	LDS  R26,__lcd_x
	CP   R26,R30
	BRLO _0x2000010
_0x2000011:
	RCALL SUBOPT_0x2
	LDS  R30,__lcd_y
	SUBI R30,-LOW(1)
	STS  __lcd_y,R30
	ST   -Y,R30
	RCALL _lcd_gotoxy
	LD   R26,Y
	CPI  R26,LOW(0xA)
	BRNE _0x2000013
	RJMP _0x2080003
_0x2000013:
_0x2000010:
	LDS  R30,__lcd_x
	SUBI R30,-LOW(1)
	STS  __lcd_x,R30
	SBI  0x18,0
	LD   R30,Y
	RCALL SUBOPT_0x2A
	CBI  0x18,0
	RJMP _0x2080003
_lcd_puts:
	ST   -Y,R17
_0x2000014:
	RCALL SUBOPT_0x2B
	BREQ _0x2000016
	ST   -Y,R17
	RCALL _lcd_putchar
	RJMP _0x2000014
_0x2000016:
	RJMP _0x2080002
_lcd_init:
	SBI  0x17,4
	SBI  0x17,5
	SBI  0x17,6
	SBI  0x17,7
	SBI  0x17,2
	SBI  0x17,0
	SBI  0x17,1
	CBI  0x18,2
	CBI  0x18,0
	CBI  0x18,1
	LD   R30,Y
	STS  __lcd_maxx,R30
	SUBI R30,-LOW(128)
	__PUTB1MN __base_y_G100,2
	LD   R30,Y
	SUBI R30,-LOW(192)
	__PUTB1MN __base_y_G100,3
	LDI  R30,LOW(20)
	LDI  R31,HIGH(20)
	RCALL SUBOPT_0xC
	LDI  R30,LOW(48)
	RCALL SUBOPT_0x29
	RCALL SUBOPT_0x2C
	RCALL SUBOPT_0x2C
	__DELAY_USB 67
	LDI  R30,LOW(32)
	RCALL SUBOPT_0x29
	__DELAY_USB 67
	LDI  R30,LOW(40)
	RCALL SUBOPT_0x2A
	LDI  R30,LOW(4)
	RCALL SUBOPT_0x2A
	LDI  R30,LOW(133)
	RCALL SUBOPT_0x2A
	LDI  R30,LOW(6)
	RCALL SUBOPT_0x2A
	RCALL _lcd_clear
	RJMP _0x2080003
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG
_putchar:
putchar0:
     sbis usr,udre
     rjmp putchar0
     ld   r30,y
     out  udr,r30
_0x2080003:
	ADIW R28,1
	RET
_puts:
	ST   -Y,R17
_0x2020003:
	RCALL SUBOPT_0x2B
	BREQ _0x2020005
	ST   -Y,R17
	RCALL _putchar
	RJMP _0x2020003
_0x2020005:
	LDI  R30,LOW(10)
	ST   -Y,R30
	RCALL _putchar
_0x2080002:
	LDD  R17,Y+0
	ADIW R28,3
	RET
_put_buff_G101:
	RCALL __SAVELOCR2
	RCALL SUBOPT_0x2D
	ADIW R26,2
	RCALL __GETW1P
	SBIW R30,0
	BREQ _0x2020010
	RCALL SUBOPT_0x2D
	RCALL SUBOPT_0x2E
	MOVW R16,R30
	SBIW R30,0
	BREQ _0x2020012
	__CPWRN 16,17,2
	BRLO _0x2020013
	MOVW R30,R16
	SBIW R30,1
	MOVW R16,R30
	__PUTW1SNS 2,4
_0x2020012:
	RCALL SUBOPT_0x2D
	ADIW R26,2
	RCALL SUBOPT_0x2F
	SBIW R30,1
	LDD  R26,Y+4
	STD  Z+0,R26
	RCALL SUBOPT_0x2D
	RCALL __GETW1P
	TST  R31
	BRMI _0x2020014
	RCALL SUBOPT_0x2D
	RCALL SUBOPT_0x2F
_0x2020014:
_0x2020013:
	RJMP _0x2020015
_0x2020010:
	RCALL SUBOPT_0x2D
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	ST   X+,R30
	ST   X,R31
_0x2020015:
	RCALL __LOADLOCR2
	ADIW R28,5
	RET
__print_G101:
	SBIW R28,6
	RCALL __SAVELOCR6
	LDI  R17,0
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	ST   X+,R30
	ST   X,R31
_0x2020016:
	LDD  R30,Y+18
	LDD  R31,Y+18+1
	ADIW R30,1
	STD  Y+18,R30
	STD  Y+18+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R18,R30
	CPI  R30,0
	BRNE PC+2
	RJMP _0x2020018
	MOV  R30,R17
	CPI  R30,0
	BRNE _0x202001C
	CPI  R18,37
	BRNE _0x202001D
	LDI  R17,LOW(1)
	RJMP _0x202001E
_0x202001D:
	RCALL SUBOPT_0x30
_0x202001E:
	RJMP _0x202001B
_0x202001C:
	CPI  R30,LOW(0x1)
	BRNE _0x202001F
	CPI  R18,37
	BRNE _0x2020020
	RCALL SUBOPT_0x30
	RJMP _0x20200C9
_0x2020020:
	LDI  R17,LOW(2)
	LDI  R20,LOW(0)
	LDI  R16,LOW(0)
	CPI  R18,45
	BRNE _0x2020021
	LDI  R16,LOW(1)
	RJMP _0x202001B
_0x2020021:
	CPI  R18,43
	BRNE _0x2020022
	LDI  R20,LOW(43)
	RJMP _0x202001B
_0x2020022:
	CPI  R18,32
	BRNE _0x2020023
	LDI  R20,LOW(32)
	RJMP _0x202001B
_0x2020023:
	RJMP _0x2020024
_0x202001F:
	CPI  R30,LOW(0x2)
	BRNE _0x2020025
_0x2020024:
	LDI  R21,LOW(0)
	LDI  R17,LOW(3)
	CPI  R18,48
	BRNE _0x2020026
	ORI  R16,LOW(128)
	RJMP _0x202001B
_0x2020026:
	RJMP _0x2020027
_0x2020025:
	CPI  R30,LOW(0x3)
	BREQ PC+2
	RJMP _0x202001B
_0x2020027:
	CPI  R18,48
	BRLO _0x202002A
	CPI  R18,58
	BRLO _0x202002B
_0x202002A:
	RJMP _0x2020029
_0x202002B:
	LDI  R26,LOW(10)
	MUL  R21,R26
	MOV  R21,R0
	MOV  R30,R18
	SUBI R30,LOW(48)
	ADD  R21,R30
	RJMP _0x202001B
_0x2020029:
	MOV  R30,R18
	CPI  R30,LOW(0x63)
	BRNE _0x202002F
	RCALL SUBOPT_0x31
	RCALL SUBOPT_0x32
	RCALL SUBOPT_0x31
	LDD  R26,Z+4
	ST   -Y,R26
	RCALL SUBOPT_0x33
	RJMP _0x2020030
_0x202002F:
	CPI  R30,LOW(0x73)
	BRNE _0x2020032
	RCALL SUBOPT_0x34
	RCALL SUBOPT_0x35
	RCALL SUBOPT_0x36
	RCALL _strlen
	MOV  R17,R30
	RJMP _0x2020033
_0x2020032:
	CPI  R30,LOW(0x70)
	BRNE _0x2020035
	RCALL SUBOPT_0x34
	RCALL SUBOPT_0x35
	RCALL SUBOPT_0x36
	RCALL _strlenf
	MOV  R17,R30
	ORI  R16,LOW(8)
_0x2020033:
	ORI  R16,LOW(2)
	ANDI R16,LOW(127)
	LDI  R19,LOW(0)
	RJMP _0x2020036
_0x2020035:
	CPI  R30,LOW(0x64)
	BREQ _0x2020039
	CPI  R30,LOW(0x69)
	BRNE _0x202003A
_0x2020039:
	ORI  R16,LOW(4)
	RJMP _0x202003B
_0x202003A:
	CPI  R30,LOW(0x75)
	BRNE _0x202003C
_0x202003B:
	LDI  R30,LOW(_tbl10_G101*2)
	LDI  R31,HIGH(_tbl10_G101*2)
	RCALL SUBOPT_0x37
	LDI  R17,LOW(5)
	RJMP _0x202003D
_0x202003C:
	CPI  R30,LOW(0x58)
	BRNE _0x202003F
	ORI  R16,LOW(8)
	RJMP _0x2020040
_0x202003F:
	CPI  R30,LOW(0x78)
	BREQ PC+2
	RJMP _0x2020071
_0x2020040:
	LDI  R30,LOW(_tbl16_G101*2)
	LDI  R31,HIGH(_tbl16_G101*2)
	RCALL SUBOPT_0x37
	LDI  R17,LOW(4)
_0x202003D:
	SBRS R16,2
	RJMP _0x2020042
	RCALL SUBOPT_0x34
	RCALL SUBOPT_0x35
	RCALL SUBOPT_0x38
	LDD  R26,Y+11
	TST  R26
	BRPL _0x2020043
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	RCALL __ANEGW1
	RCALL SUBOPT_0x38
	LDI  R20,LOW(45)
_0x2020043:
	CPI  R20,0
	BREQ _0x2020044
	SUBI R17,-LOW(1)
	RJMP _0x2020045
_0x2020044:
	ANDI R16,LOW(251)
_0x2020045:
	RJMP _0x2020046
_0x2020042:
	RCALL SUBOPT_0x34
	RCALL SUBOPT_0x35
	RCALL SUBOPT_0x38
_0x2020046:
_0x2020036:
	SBRC R16,0
	RJMP _0x2020047
_0x2020048:
	CP   R17,R21
	BRSH _0x202004A
	SBRS R16,7
	RJMP _0x202004B
	SBRS R16,2
	RJMP _0x202004C
	ANDI R16,LOW(251)
	MOV  R18,R20
	SUBI R17,LOW(1)
	RJMP _0x202004D
_0x202004C:
	LDI  R18,LOW(48)
_0x202004D:
	RJMP _0x202004E
_0x202004B:
	LDI  R18,LOW(32)
_0x202004E:
	RCALL SUBOPT_0x30
	SUBI R21,LOW(1)
	RJMP _0x2020048
_0x202004A:
_0x2020047:
	MOV  R19,R17
	SBRS R16,1
	RJMP _0x202004F
_0x2020050:
	CPI  R19,0
	BREQ _0x2020052
	SBRS R16,3
	RJMP _0x2020053
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	LPM  R18,Z+
	RCALL SUBOPT_0x37
	RJMP _0x2020054
_0x2020053:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LD   R18,X+
	STD  Y+6,R26
	STD  Y+6+1,R27
_0x2020054:
	RCALL SUBOPT_0x30
	CPI  R21,0
	BREQ _0x2020055
	SUBI R21,LOW(1)
_0x2020055:
	SUBI R19,LOW(1)
	RJMP _0x2020050
_0x2020052:
	RJMP _0x2020056
_0x202004F:
_0x2020058:
	LDI  R18,LOW(48)
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	RCALL __GETW1PF
	STD  Y+8,R30
	STD  Y+8+1,R31
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,2
	RCALL SUBOPT_0x37
_0x202005A:
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CP   R26,R30
	CPC  R27,R31
	BRLO _0x202005C
	SUBI R18,-LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	SUB  R30,R26
	SBC  R31,R27
	RCALL SUBOPT_0x38
	RJMP _0x202005A
_0x202005C:
	CPI  R18,58
	BRLO _0x202005D
	SBRS R16,3
	RJMP _0x202005E
	SUBI R18,-LOW(7)
	RJMP _0x202005F
_0x202005E:
	SUBI R18,-LOW(39)
_0x202005F:
_0x202005D:
	SBRC R16,4
	RJMP _0x2020061
	CPI  R18,49
	BRSH _0x2020063
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,1
	BRNE _0x2020062
_0x2020063:
	RJMP _0x20200CA
_0x2020062:
	CP   R21,R19
	BRLO _0x2020067
	SBRS R16,0
	RJMP _0x2020068
_0x2020067:
	RJMP _0x2020066
_0x2020068:
	LDI  R18,LOW(32)
	SBRS R16,7
	RJMP _0x2020069
	LDI  R18,LOW(48)
_0x20200CA:
	ORI  R16,LOW(16)
	SBRS R16,2
	RJMP _0x202006A
	ANDI R16,LOW(251)
	ST   -Y,R20
	RCALL SUBOPT_0x33
	CPI  R21,0
	BREQ _0x202006B
	SUBI R21,LOW(1)
_0x202006B:
_0x202006A:
_0x2020069:
_0x2020061:
	RCALL SUBOPT_0x30
	CPI  R21,0
	BREQ _0x202006C
	SUBI R21,LOW(1)
_0x202006C:
_0x2020066:
	SUBI R19,LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,2
	BRLO _0x2020059
	RJMP _0x2020058
_0x2020059:
_0x2020056:
	SBRS R16,0
	RJMP _0x202006D
_0x202006E:
	CPI  R21,0
	BREQ _0x2020070
	SUBI R21,LOW(1)
	LDI  R30,LOW(32)
	ST   -Y,R30
	RCALL SUBOPT_0x33
	RJMP _0x202006E
_0x2020070:
_0x202006D:
_0x2020071:
_0x2020030:
_0x20200C9:
	LDI  R17,LOW(0)
_0x202001B:
	RJMP _0x2020016
_0x2020018:
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	RCALL __GETW1P
	RCALL __LOADLOCR6
	ADIW R28,20
	RET
_sprintf:
	PUSH R15
	MOV  R15,R24
	SBIW R28,6
	RCALL __SAVELOCR4
	RCALL SUBOPT_0x39
	SBIW R30,0
	BRNE _0x2020072
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	RJMP _0x2080001
_0x2020072:
	MOVW R26,R28
	ADIW R26,6
	RCALL __ADDW2R15
	MOVW R16,R26
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x37
	LDI  R30,LOW(0)
	STD  Y+8,R30
	STD  Y+8+1,R30
	MOVW R26,R28
	ADIW R26,10
	RCALL __ADDW2R15
	RCALL __GETW1P
	RCALL SUBOPT_0x3
	ST   -Y,R17
	ST   -Y,R16
	LDI  R30,LOW(_put_buff_G101)
	LDI  R31,HIGH(_put_buff_G101)
	RCALL SUBOPT_0x3
	MOVW R30,R28
	ADIW R30,10
	RCALL SUBOPT_0x3
	RCALL __print_G101
	MOVW R18,R30
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LDI  R30,LOW(0)
	ST   X,R30
	MOVW R30,R18
_0x2080001:
	RCALL __LOADLOCR4
	ADIW R28,10
	POP  R15
	RET

	.CSEG
_strchr:
    ld   r26,y+
    ld   r30,y+
    ld   r31,y+
strchr0:
    ld   r27,z
    cp   r26,r27
    breq strchr1
    adiw r30,1
    tst  r27
    brne strchr0
    clr  r30
    clr  r31
strchr1:
    ret
_strcmp:
    ld   r30,y+
    ld   r31,y+
    ld   r26,y+
    ld   r27,y+
strcmp0:
    ld   r22,x+
    ld   r23,z+
    cp   r22,r23
    brne strcmp1
    tst  r22
    brne strcmp0
strcmp3:
    clr  r30
    ret
strcmp1:
    sub  r22,r23
    breq strcmp3
    ldi  r30,1
    brcc strcmp2
    subi r30,2
strcmp2:
    ret
_strlen:
    ld   r26,y+
    ld   r27,y+
    clr  r30
    clr  r31
strlen0:
    ld   r22,x+
    tst  r22
    breq strlen1
    adiw r30,1
    rjmp strlen0
strlen1:
    ret
_strlenf:
    clr  r26
    clr  r27
    ld   r30,y+
    ld   r31,y+
strlenf0:
	lpm  r0,z+
    tst  r0
    breq strlenf1
    adiw r26,1
    rjmp strlenf0
strlenf1:
    movw r30,r26
    ret
_strncmp:
    clr  r22
    clr  r23
    ld   r24,y+
    ld   r30,y+
    ld   r31,y+
    ld   r26,y+
    ld   r27,y+
strncmp0:
    tst  r24
    breq strncmp1
    dec  r24
    ld   r22,x+
    ld   r23,z+
    cp   r22,r23
    brne strncmp1
    tst  r22
    brne strncmp0
strncmp3:
    clr  r30
    ret
strncmp1:
    sub  r22,r23
    breq strncmp3
    ldi  r30,1
    brcc strncmp2
    subi r30,2
strncmp2:
    ret

	.CSEG

	.DSEG
_Data_Buffer:
	.BYTE 0x96
_rx_buffer:
	.BYTE 0x96
_PASS_Buffer:
	.BYTE 0x10
_pass_stt:
	.BYTE 0x1
_set_ok:
	.BYTE 0x1
_enter_en:
	.BYTE 0x1
_step_setting:
	.BYTE 0x1
_httt_stt:
	.BYTE 0x1
_KEY_3x3_G001:
	.BYTE 0x10
__base_y_G100:
	.BYTE 0x4
__lcd_x:
	.BYTE 0x1
__lcd_y:
	.BYTE 0x1
__lcd_maxx:
	.BYTE 0x1

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x0:
	LDI  R31,0
	SUBI R30,LOW(-_rx_buffer)
	SBCI R31,HIGH(-_rx_buffer)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(99)
	OUT  0x2D,R30
	LDI  R30,LOW(192)
	OUT  0x2C,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 28 TIMES, CODE SIZE REDUCTION:25 WORDS
SUBOPT_0x2:
	LDI  R30,LOW(0)
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 62 TIMES, CODE SIZE REDUCTION:59 WORDS
SUBOPT_0x3:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0x4:
	RCALL _lcd_clear
	RCALL _Clear_PassBuffer
	LDI  R30,LOW(0)
	STS  _set_ok,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x5:
	STS  _enter_en,R30
	LDI  R30,LOW(0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x6:
	STS  _step_setting,R30
	LDI  R30,LOW(0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x7:
	STS  _httt_stt,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x8:
	STS  _pass_stt,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x9:
	LDI  R30,LOW(0)
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0xA:
	ST   -Y,R17
	LDI  R17,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xB:
	LDI  R31,0
	SUBI R30,LOW(-_Data_Buffer)
	SBCI R31,HIGH(-_Data_Buffer)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0xC:
	RCALL SUBOPT_0x3
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xD:
	LDI  R26,LOW(0)
	STD  Z+0,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xE:
	LDI  R26,LOW(0)
	LDI  R27,HIGH(0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0xF:
	RCALL SUBOPT_0x3
	MOVW R30,R8
	CLR  R22
	CLR  R23
	RCALL __PUTPARD1
	LDI  R24,4
	RCALL _sprintf
	ADIW R28,8
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x10:
	LDI  R30,LOW(_Data_Buffer)
	LDI  R31,HIGH(_Data_Buffer)
	RJMP SUBOPT_0x3

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x11:
	MOV  R30,R21
	SUBI R21,-1
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x12:
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x13:
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x14:
	RCALL SUBOPT_0x3
	RJMP _lcd_puts

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x15:
	LDI  R30,LOW(1)
	ST   -Y,R30
	RJMP _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x16:
	STS  _step_setting,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x17:
	LDD  R30,Y+22
	LDD  R31,Y+22+1
	ADIW R30,1
	STD  Y+22,R30
	STD  Y+22+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x18:
	LDI  R30,LOW(0)
	RCALL __EQB12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x19:
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x1A:
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R30,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x1B:
	RCALL _KEY4X4_GetKey
	MOV  R17,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x1C:
	SBI  0x12,3
	CLR  R13
	RJMP SUBOPT_0x1

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1D:
	RCALL _Clear_PassBuffer
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x1E:
	LDS  R26,_httt_stt
	CPI  R26,LOW(0x1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x1F:
	SBIW R28,30
	LDI  R24,30
	RJMP SUBOPT_0xE

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x20:
	LDS  R30,_step_setting
	CPI  R30,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x21:
	LDS  R26,_step_setting
	CPI  R26,LOW(0x1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x22:
	LDI  R30,LOW(1)
	RJMP SUBOPT_0x16

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x23:
	MOVW R30,R28
	ADIW R30,1
	RJMP SUBOPT_0x3

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:52 WORDS
SUBOPT_0x24:
	RCALL SUBOPT_0x3
	MOVW R30,R8
	CLR  R22
	CLR  R23
	RCALL __PUTPARD1
	LDI  R30,LOW(_PASS_Buffer)
	LDI  R31,HIGH(_PASS_Buffer)
	CLR  R22
	CLR  R23
	RCALL __PUTPARD1
	__GETD1N 0x0
	RCALL __PUTPARD1
	LDI  R24,12
	RCALL _sprintf
	ADIW R28,16
	RJMP SUBOPT_0x23

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x25:
	RCALL _RS485_Puts
	RJMP _Clear_PassBuffer

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0x26:
	MOV  R30,R12
	INC  R12
	RCALL SUBOPT_0x19
	SUBI R30,LOW(-_PASS_Buffer)
	SBCI R31,HIGH(-_PASS_Buffer)
	ST   Z,R17
	MOV  R30,R12
	RCALL SUBOPT_0x19
	SUBI R30,LOW(-_PASS_Buffer)
	SBCI R31,HIGH(-_PASS_Buffer)
	RJMP SUBOPT_0xD

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x27:
	LDI  R30,LOW(42)
	ST   -Y,R30
	RJMP _lcd_putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x28:
	LDS  R26,_pass_stt
	CPI  R26,LOW(0x1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x29:
	ST   -Y,R30
	RJMP __lcd_write_nibble_G100

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x2A:
	ST   -Y,R30
	RJMP __lcd_write_data

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x2B:
	LDD  R26,Y+1
	LDD  R27,Y+1+1
	LD   R30,X+
	STD  Y+1,R26
	STD  Y+1+1,R27
	MOV  R17,R30
	CPI  R30,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2C:
	__DELAY_USB 67
	LDI  R30,LOW(48)
	RJMP SUBOPT_0x29

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x2D:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2E:
	ADIW R26,4
	RCALL __GETW1P
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2F:
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:22 WORDS
SUBOPT_0x30:
	ST   -Y,R18
	LDD  R30,Y+13
	LDD  R31,Y+13+1
	RCALL SUBOPT_0x3
	LDD  R30,Y+17
	LDD  R31,Y+17+1
	ICALL
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x31:
	LDD  R30,Y+16
	LDD  R31,Y+16+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x32:
	SBIW R30,4
	STD  Y+16,R30
	STD  Y+16+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x33:
	LDD  R30,Y+13
	LDD  R31,Y+13+1
	RCALL SUBOPT_0x3
	LDD  R30,Y+17
	LDD  R31,Y+17+1
	ICALL
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x34:
	RCALL SUBOPT_0x31
	RJMP SUBOPT_0x32

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x35:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	RJMP SUBOPT_0x2E

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x36:
	STD  Y+6,R30
	STD  Y+6+1,R31
	RJMP SUBOPT_0x3

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x37:
	STD  Y+6,R30
	STD  Y+6+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x38:
	STD  Y+10,R30
	STD  Y+10+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x39:
	MOVW R26,R28
	ADIW R26,12
	RCALL __ADDW2R15
	RCALL __GETW1P
	RET


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x1F4
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__ADDW2R15:
	CLR  R0
	ADD  R26,R15
	ADC  R27,R0
	RET

__ANEGW1:
	NEG  R31
	NEG  R30
	SBCI R31,0
	RET

__EQB12:
	CP   R30,R26
	LDI  R30,1
	BREQ __EQB12T
	CLR  R30
__EQB12T:
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	SBIW R26,1
	RET

__GETW1PF:
	LPM  R0,Z+
	LPM  R31,Z
	MOV  R30,R0
	RET

__PUTPARD1:
	ST   -Y,R23
	ST   -Y,R22
	ST   -Y,R31
	ST   -Y,R30
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

__LOADLOCR2P:
	LD   R16,Y+
	LD   R17,Y+
	RET

__INITLOCB:
__INITLOCW:
	ADD  R26,R28
	ADC  R27,R29
__INITLOC0:
	LPM  R0,Z+
	ST   X+,R0
	DEC  R24
	BRNE __INITLOC0
	RET

;END OF CODE MARKER
__END_OF_CODE:
